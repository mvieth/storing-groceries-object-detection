#include "util.hpp"
//#include <ros/ros.h>
//#include <tf2_geometry_msgs/tf2_geometry_msgs.h>

//#include <eigen_conversions/eigen_msg.h> // for tf::transformMsgToEigen
#include <tf2_eigen/tf2_eigen.h> // for transformToEigen
#include <tf2_ros/buffer.h>
#include <ostream> // for std::ostream
#include <geometry_msgs/TransformStamped.h>
#include <ros/console.h> // for ROS_WARN, ROS_INFO, ...
#include <string>                           // for string

std::ostream &operator<<(std::ostream &os, const Eigen::Quaternionf &q) {
  os << "x=" << q.x() << " y=" << q.y() << " z=" << q.z() << " w=" << q.w() << std::endl;
  return os;
}

void hsv2rgb(double h, double s, double v, float &r, float &g, float &b) {
  double hh, p, q, t, ff;
  long i;

  if (s <= 0.0) { // < is bogus, just shuts up warnings
    r = v;
    g = v;
    b = v;
    return;
  }
  hh = h;
  if (hh >= 360.0)
    hh = 0.0;
  hh /= 60.0;
  i = (long)hh;
  ff = hh - i;
  p = v * (1.0 - s);
  q = v * (1.0 - (s * ff));
  t = v * (1.0 - (s * (1.0 - ff)));

  switch (i) {
  case 0:
    r = v;
    g = t;
    b = p;
    break;
  case 1:
    r = q;
    g = v;
    b = p;
    break;
  case 2:
    r = p;
    g = v;
    b = t;
    break;

  case 3:
    r = p;
    g = q;
    b = v;
    break;
  case 4:
    r = t;
    g = p;
    b = v;
    break;
  case 5:
  default:
    r = v;
    g = p;
    b = q;
    break;
  }
  ROS_DEBUG("called hsv2rgb(h=%g, s=%g, v=%g). result: r=%g, g=%g, b=%g", h, s, v, r, g, b);
}

void rgb2hsv(double r, double g, double b, float & h, float & s, float & v) {
    double      min, max, delta;

    min = r < g ? r : g;
    min = min  < b ? min  : b;

    max = r > g ? r : g;
    max = max  > b ? max  : b;

    v = max;                                // v
    delta = max - min;
    if (delta < 0.00001)
    {
        s = 0;
        h = 0; // undefined, maybe nan?
        return;
    }
    if( max > 0.0 ) { // NOTE: if Max is == 0, this divide would cause a crash
        s = (delta / max);                  // s
    } else {
        // if max is 0, then r = g = b = 0              
        // s = 0, h is undefined
        s = 0.0;
        h = NAN;                            // its now undefined
        return;
    }
    if( r >= max )                           // > is bogus, just keeps compilor happy
        h = ( g - b ) / delta;        // between yellow & magenta
    else
    if( g >= max )
        h = 2.0 + ( b - r ) / delta;  // between cyan & yellow
    else
        h = 4.0 + ( r - g ) / delta;  // between magenta & cyan

    h *= 60.0;                              // degrees

    if( h < 0.0 )
        h += 360.0;
}

bool isSimulation() {
  std::vector<std::string> nodes;
  if(!ros::master::getNodes(nodes)) {
    ROS_ERROR("isSimulation(): getNodes returned false");
    return false; // TODO this is not really good
  }
  for(const std::string& name : nodes) {
    if(name.find("gazebo") != std::string::npos) {
      ROS_INFO("Found node with name %s, so probably simulation", name.c_str());
      return true;
    }
  }
  ROS_INFO("Found no node name that contains gazebo, so probably not simulation");
  return false;
}

/*Eigen::Affine3d getAffineTransform(const tf2_ros::Buffer &tfBuffer,
                                   const std::string &target_frame,
                                   const std::string &source_frame) {
  try {
    geometry_msgs::TransformStamped transformStamped = tfBuffer.lookupTransform(
        target_frame, source_frame, ros::Time(0)); // Latest transform
    Eigen::Affine3d e;
    tf::transformMsgToEigen(transformStamped.transform, e);//TODO replace with tf2?
    return e;
  } catch (tf2::TransformException &ex) {
    ROS_WARN("%s", ex.what());
    return Eigen::Affine3d::Identity();
  }
}*/

Eigen::Isometry3d getIsometryTransform(const tf2_ros::Buffer &tfBuffer,
                                   const std::string &target_frame,
                                   const std::string &source_frame,
                                   const ros::Time& time) {
  try {
    geometry_msgs::TransformStamped transformStamped = tfBuffer.lookupTransform(
        target_frame, source_frame, time);
    Eigen::Isometry3d e=tf2::transformToEigen(transformStamped);
    return e;
  } catch (tf2::TransformException &ex) {
    ROS_WARN("%s", ex.what());
    return Eigen::Isometry3d::Identity();
  }
}

Eigen::Isometry3d affine2isometry(const Eigen::Affine3d& t) {
    Eigen::Isometry3d ret;
    ret.translation() = t.translation();
    ret.linear() = t.rotation();
    return ret;
}

Eigen::Affine3d isometry2affine(const Eigen::Isometry3d& t) {
    Eigen::Affine3d ret;
    ret.translation() = t.translation();
    ret.linear() = t.rotation();
    return ret;
}

sensor_msgs::Image
getSubImage(const sensor_msgs::Image &input, uint32_t xmin, uint32_t ymin,
            uint32_t xmax,
            uint32_t ymax) { // TODO better name? Maybe better with cv?
  ROS_INFO("getSubImage: xmin=%u, ymin=%u, xmax=%u, ymax=%u", xmin, ymin, xmax, ymax);
  if (xmin > xmax) {
    uint32_t temp = xmin;
    xmin = xmax;
    xmax = temp;
    ROS_WARN("getSubImage: xmin bigger than xmax, swapped values");
  }
  if (ymin > ymax) {
    uint32_t temp = ymin;
    ymin = ymax;
    ymax = temp;
    ROS_WARN("getSubImage: ymin bigger than ymax, swapped values");
  }
  if (xmax > input.width) {
    xmax = input.width;
    ROS_WARN("getSubImage: xmax too large, set to image width");
  }
  if (ymax > input.height) {
    ymax = input.height;
    ROS_WARN("getSubImage: ymax too large, set to image height");
  }
  sensor_msgs::Image result;
  result.header = input.header;
  result.height = ymax - ymin;
  result.width = xmax - xmin;
  result.encoding = input.encoding;
  result.is_bigendian = input.is_bigendian;
  uint32_t pixelsize = input.step / input.width;
  result.step = pixelsize * result.width;
  result.data.resize(pixelsize * result.height * result.width);
  for (size_t i = 0; i < result.height; i++) { // Copy row by row
    memcpy(&result.data[0] + i * result.step,
           &input.data[0] + (i + ymin) * input.step + xmin * pixelsize,
           result.step);
  }
  /*cv_bridge::CvImagePtr cvptr=toCvCopy(input);
  cv::Rect myROI(xmin, ymin, xmax, ymax);
  cv::Mat croppedImage = cvptr->image(myROI);
  cv_bridge::CvImage(input.header, input.encoding, croppedImage).toImageMsg();*/
  return result;
}

bool intersectionPointOfTwoLines2D(double a1, double b1, double c1, double a2,
                                   double b2, double c2, double &xs,
                                   double &ys) {
  ROS_INFO("intersectionPointOfTwoLines2D(a1=%g,b1=%g,c1=%g,a2=%g,b2=%g,c2=%g)", a1, b1, c1, a2, b2, c2);
  const double temp = a1 * b2 - a2 * b1;
  if (fabs(temp) < 1e-10) {
    ROS_WARN("intersectionPointOfTwoLines2D: temp is too small, lines might be "
             "parallel, returning");
    return false;
  }
  xs = (c1 * b2 - c2 * b1) / temp;
  ys = (a1 * c2 - a2 * c1) / temp;
  return true;
}

bool nearestIntersectionBetweenLines(const Eigen::Vector3d& a1, const Eigen::Vector3d& n1, const Eigen::Vector3d& a2, const Eigen::Vector3d& n2, Eigen::Vector3d * p1, Eigen::Vector3d * p2) {
    const double alpha1 =std::pow(n1.x(), 2)+std::pow(n1.y(), 2)+std::pow(n1.z(), 2); // TODO use more of Eigen's functions like .dot()?
    const double beta1  =-(n1.x()*n2.x()+n1.y()*n2.y()+n1.z()*n2.z());
    const double gamma1 =n1.x()*(a1.x()-a2.x())+n1.y()*(a1.y()-a2.y())+n1.z()*(a1.z()-a2.z());
    const double alpha2 =beta1;
    const double beta2  =std::pow(n2.x(), 2)+std::pow(n2.y(), 2)+std::pow(n2.z(), 2);
    const double gamma2 =n2.x()*(a2.x()-a1.x())+n2.y()*(a2.y()-a1.y())+n2.z()*(a2.z()-a1.z());
    ROS_DEBUG("alpha1=%g, beta1=%g, gamma1=%g, alpha2=%g, beta2=%g, gamma2=%g", alpha1, beta1, gamma1, alpha2, beta2, gamma2);
    const double denom=(alpha1*beta2-alpha2*beta1);
    if(std::abs(denom) < 1e-6) {
        ROS_ERROR("nearestIntersectionBetweenLines: denominator is too small (%g), the lines might be parallel", denom);
        return false;
    }
    //ROS_DEBUG("t1=%g, t2=%g", t1, t2);
    if(p1!=nullptr) {
        const double t1=(beta1*gamma2-beta2*gamma1)/denom;
        *p1=a1+t1*n1;
    }
    if(p2!=nullptr) {
        const double t2=(alpha2*gamma1-alpha1*gamma2)/denom;
        *p2=a2+t2*n2;
    }
    //ROS_DEBUG("p1=(%g,%g,%g), p2=(%g,%g,%g)", p1.x(), p1.y(), p1.z(), p2.x(), p2.y(), p2.z());
    return true;
}

bool intersectionPointOfLineAndPlane(double a, double b, double c, double d, double lx, double ly, double lz, double& ix, double& iy, double& iz) {
  ROS_INFO("intersectionPointOfLineAndPlane called with plane (a=%g, b=%g, c=%g, d=%g), line (lx=%g, ly=%g, lz=%g)", a, b, c, d, lx, ly, lz);
  const double denominator = (a*lx+b*ly+c*lz);
  if (std::abs(denominator) < 1e-6) {
    ROS_WARN("intersectionPointOfLineAndPlane: denominator is too small, line (%g,%g,%g) and plane (%g,%g,%g,%g) are parallel", lx, ly, lz, a, b, c, d);
    return false;
  }
  const double r=-d/denominator;
  ix=r*lx;
  iy=r*ly;
  iz=r*lz;
  return true;
}

double signedDistancePointToPlane(double a, double b, double c, double d,
                                  double x1, double y1, double z1) {
  const double temp = sqrt(a * a + b * b + c * c);
  if (fabs(temp) < 1e-6) {
    ROS_INFO("Called signedDistancePointToPlane(a=%f, b=%f, c=%f, d=%f, x1=%f, y1=%f, z1=%f)", a, b, c, d, x1, y1, z1);
    ROS_ERROR("signedDistancePointToPlane(): temp (length of plane normal vector) is too small, cannot compute");
    return 0.0;
  }
  const double result = (a * x1 + b * y1 + c * z1 - d) / temp;
  ROS_INFO("Called signedDistancePointToPlane(a=%f, b=%f, c=%f, d=%f, x1=%f, y1=%f, z1=%f), result=%g", a, b, c, d, x1, y1, z1, result);
  return result;
}

double distPointToLine2D(double x0, double y0, double x1, double y1, double x2, double y2) {
    //printf("distPointToLine2D called\n");
    const double denominator=sqrt((y2-y1)*(y2-y1)+(x2-x1)*(x2-x1));
    if(denominator<1e-6) {
        ROS_WARN("denominator is too small in distPointToLine2D()");
        return INFINITY;
    }
    return fabs((y2-y1)*x0 - (x2-x1)*y0 + x2*y1 - y2*x1)/denominator;
}

double distPointToLine3D(const Eigen::Vector3f& x0, const Eigen::Vector3f& x1, const Eigen::Vector3f& x2) {
    const double denominator=(x2-x1).norm();
    if(denominator<1e-6) {
        ROS_WARN("denominator is too small in distPointToLine3D()");
        return INFINITY;
    }
    return ((x0-x1).cross(x0-x2)).norm()/denominator;
}

// Angle between planes TODO

double angleBetweenVectors(double x1, double y1, double z1, double x2, double y2, double z2) {
    const double tmp1=sqrt(x1 * x1 + y1 * y1 + z1 * z1);
    if(tmp1 < 1e-10) {
        ROS_ERROR("angleBetweenVectors: length of vector 1 is too small (%g)", tmp1);
        return 0.0;
    }
    const double tmp2=sqrt(x2 * x2 + y2 * y2 + z2 * z2);
    if(tmp2 < 1e-10) {
        ROS_ERROR("angleBetweenVectors: length of vector 2 is too small (%g)", tmp2);
        return 0.0;
    }
    return acos((x1 * x2 + y1 * y2 + z1 * z2)/tmp1/tmp2);
}

double yaw_from_quat(const geometry_msgs::Quaternion & quat) {
    ROS_INFO("yaw_from_quat called (quat=(x=%g, y=%g, z=%g, w=%g))", quat.x, quat.y, quat.z, quat.w);
    return 2.0 * acos((quat.z < 0.0 ? -1.0 : 1.0) * quat.w / std::sqrt(std::pow(quat.w, 2) + std::pow(quat.z, 2))); // TODO use 2.0*atan2(z, w) instead? // TODO what if w and z are zero? Is that possible?
}

double angleModulo(double angle) {
  const double orig=angle;
  while (angle > M_PI)
    angle -= 2.0 * M_PI;
  while (angle <= -M_PI)
    angle += 2.0 * M_PI;
  ROS_INFO("angleModulo called with angle=%g, is now %g", orig, angle);
  return angle;
}

void randomQuaternion(double& x, double& y, double& z, double& w) {
  const double u1=drand48(), u2=drand48(), u3=drand48();
  const double tmp1=std::sqrt(1.0-u1);
  const double tmp2=std::sqrt(u1);
  x=tmp1*std::sin(2.0*M_PI*u2);
  y=tmp1*std::cos(2.0*M_PI*u2);
  z=tmp2*std::sin(2.0*M_PI*u3);
  w=tmp2*std::cos(2.0*M_PI*u3);
}

shape_msgs::Plane transformPlane(const double a, const double b, const double c, const double d,
                                 const Eigen::Affine3d& e) {
  Eigen::Vector3d n1(a, b, c);
  Eigen::Vector3d o1(-d * a, -d * b, -d * c);
  Eigen::Vector3d n2 = (e.linear().inverse().transpose() * n1).normalized();
  Eigen::Vector3d o2 = e * o1;
  shape_msgs::Plane result; // ax + by + cz + d = 0
  result.coef[0] = n2.x();  // a
  result.coef[1] = n2.y();  // b
  result.coef[2] = n2.z();  // c
  result.coef[3] = -(n2.x() * o2.x() + n2.y() * o2.y() + n2.z() * o2.z()); // d
  return result;
}

shape_msgs::Plane transformPlane(const double a, const double b, const double c, const double d,
                                 const Eigen::Isometry3d& e) {
  Eigen::Vector3d n1(a, b, c);
  Eigen::Vector3d o1(-d * a, -d * b, -d * c);
  Eigen::Vector3d n2 = (e.linear().inverse().transpose() * n1).normalized();
  Eigen::Vector3d o2 = e * o1;
  shape_msgs::Plane result; // ax + by + cz + d = 0
  result.coef[0] = n2.x();  // a
  result.coef[1] = n2.y();  // b
  result.coef[2] = n2.z();  // c
  result.coef[3] = -(n2.x() * o2.x() + n2.y() * o2.y() + n2.z() * o2.z()); // d
  return result;
}

// From
// https://en.wikipedia.org/wiki/Conversion_between_quaternions_and_Euler_angles
/*static void toEulerAngle(const Eigen::Quaterniond& q, double& roll, double&
pitch, double& yaw)
{
    // roll (x-axis rotation)
    double sinr_cosp = +2.0 * (q.w() * q.x() + q.y() * q.z());
    double cosr_cosp = +1.0 - 2.0 * (q.x() * q.x() + q.y() * q.y());
    roll = atan2(sinr_cosp, cosr_cosp);

    // pitch (y-axis rotation)
    double sinp = +2.0 * (q.w() * q.y() - q.z() * q.x());
    if (fabs(sinp) >= 1)
        pitch = copysign(M_PI / 2, sinp); // use 90 degrees if out of range
    else
        pitch = asin(sinp);

    // yaw (z-axis rotation)
    double siny_cosp = +2.0 * (q.w() * q.z() + q.x() * q.y());
    double cosy_cosp = +1.0 - 2.0 * (q.y() * q.y() + q.z() * q.z());
    yaw = atan2(siny_cosp, cosy_cosp);
}*/
