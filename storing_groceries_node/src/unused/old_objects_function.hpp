

// Originally based on clf_grasping/.../test.py
std::vector<vision_msgs::Detection3D>
getObjectsAndAddToPlanningScene2(const sensor_msgs::Image& cam_image,
                                const image_geometry::PinholeCameraModel& cam_model,
                                pcl::PointCloud<point_t>::Ptr cloud,
                                const std::string src_frame,
                                const Eigen::Isometry3d * /*transform1*/,
                                const float table_height,
                                const bool addToPlanningScene,
                                const bool make_all_standard_cylinder,
                                std::vector<std::string> * objectIds) {
  const float shape_prob_threshold=0.2;
  const float classification_prob_threshold=0.6;
  ROS_INFO("getObjectsAndAddToPlanningScene called");
  Eigen::Isometry3d transform=getIsometryTransform(*tfBuffer1, src_frame, BASE_FRAME);
  image_pub.publish(cam_image);//TODO remove?
  ROS_INFO("Making call to Detection3D service");
  /*clf_object_recognition_msgs::Detect3D srv1;
  if (object_merger_client.call(srv1))
  {
    ROS_INFO("Call service success (object merger)");
  }
  else
  {
    ROS_ERROR("Error with service call (object merger)");
    return NULL;
  }*/
  if (cloud->size() == 0) {
    ROS_WARN("getObjectsAndAddToPlanningScene(): input cloud is empty");
    return *(new std::vector<vision_msgs::Detection3D>());
  }
  std::vector<vision_msgs::Detection3D> detections = segmentation(cloud, src_frame /*, 2.0*/);

  ROS_INFO("Got %lu detections\n", detections.size());
  clf_object_recognition_msgs::Classify2D srv3;
  int mapDetToRec[detections.size()]; // TODO check this
  if (cam_image.height!=0 && cam_image.width!=0) {
    for (size_t i = 0; i < detections.size(); i++) {
      if (detections[i].source_cloud.header.frame_id.length() ==
          0) {
        ROS_INFO("Empty frame id, skipping object"); // Probably no point cloud
                                                     // or bbox, only 2D
                                                     // detections
        mapDetToRec[i] = -1;
        continue;
      }
      ROS_INFO_STREAM("Detection bbox: " << detections[i].bbox);
      if (table_height > 0.1 &&
          detections[i].bbox.center.position.z <
              (table_height - 0.05)) {
        ROS_INFO("Point cluster below table height");
        // mapDetToRec[i]=-1;
        // continue;
      }
      sensor_msgs::Image image;
      // Adapted from
      // https://github.com/CentralLabFacilities/clf_object_recognition/blob/kinetic-devel/clf_object_recognition_merger/src/object_merger.cpp
      float xmin = cam_image.width, ymin = cam_image.height, xmax = 0.0,
            ymax = 0.0;
      // Iterate through all 8 edges of the bounding box, project them on the
      // camera image, and find a 2D box that includes them all.
      for (int k = -1; k <= 1; k += 2) {
        for (int l = -1; l <= 1; l += 2) {
          for (int m = -1; m <= 1; m += 2) {
            float imx, imy;
            tf2::Vector3 vec(k * detections[i].bbox.size.x / 2.0,
                             l * detections[i].bbox.size.y / 2.0,
                             m * detections[i].bbox.size.z / 2.0),
                vec2(detections[i].bbox.center.position.x,
                     detections[i].bbox.center.position.y,
                     detections[i].bbox.center.position.z);
            tf2::Quaternion rot;
            tf2::fromMsg(detections[i].bbox.center.orientation,
                         rot);
            tf2::Transform trans(rot);
            vec = trans * vec;
            vec += vec2;
            // ROS_INFO_STREAM(vec.x() << " " << vec.y() << " " << vec.z());
            // imx=520.0*vec.x()/vec.z()+cam_image.width/2.0;
            // imy=520.0*vec.y()/vec.z()+cam_image.height/2.0;
            // imx=535.0*vec.x()/vec.z()+319.0;//From camera matrix
            // imy=535.0*vec.y()/vec.z()+253.0;
            cv::Point3d pt_cv(vec.x(), vec.y(), vec.z());
            cv::Point2d uv = cam_model.project3dToPixel(pt_cv);
            imx = uv.x;
            imy = uv.y;
            // printf("imx=%f, imy=%f\n", imx, imy);
            if (imx < xmin)
              xmin = imx;
            if (imx > xmax)
              xmax = imx;
            if (imy < ymin)
              ymin = imy;
            if (imy > ymax)
              ymax = imy;
          }
        }
      }
      if (xmin < 0.0)
        xmin = 0.0;
      if (ymin < 0.0)
        ymin = 0.0;
      if (xmax > cam_image.width)
        xmax = cam_image.width;
      if (ymax > cam_image.height)
        ymax = cam_image.height;
      // printf("cam_image.width=%u, cam_image.height=%u\n", cam_image.width,
      // cam_image.height);
      // image=getSubImage(cam_image,
      // cam_image.width*(estimatedBbox2D.center.x-estimatedBbox2D.size_x/2),
      // cam_image.height*(estimatedBbox2D.center.y-estimatedBbox2D.size_y/2),
      // cam_image.width*(estimatedBbox2D.center.x+estimatedBbox2D.size_x/2),
      // cam_image.height*(estimatedBbox2D.center.y+estimatedBbox2D.size_y/2));
      if (fabs(xmax - xmin) > 5 &&
          fabs(ymax - ymin) > 5) { // If image is not at least 5 by 5 pixel,
                                   // this does not make sense
        image = getSubImage(cam_image, xmin, ymin, xmax, ymax);
        ROS_INFO("Publish image from cloud");
        // image_pub.publish(image);
        srv3.request.images.push_back(image);
        ROS_INFO_STREAM(
            "Classify images count: " << srv3.request.images.size());
        mapDetToRec[i] = srv3.request.images.size() - 1;
      } else {
        ROS_WARN("Sub image of object is too small, continue");
        mapDetToRec[i] = -1;
      }
    }
    if (srv3.request.images.size() != 0) {
      ROS_INFO("Calling recognition service");
      if (classify_client.call(srv3)) {
        ROS_INFO("Call service success (classify)");
      } else {
        ROS_ERROR("Error with service call (classify)");
      }
    } else {
      ROS_INFO("No images, so not calling recognition service");
    }
  } else {
    ROS_INFO("No camera image, so skipping recognition");
  }
  clf_object_recognition_msgs::BoundingBox3DArray bboxes;
  if (detections.size() != 0) {
    bboxes.header.frame_id =
        detections[0].source_cloud.header.frame_id;
  }
  // std::string supportSurfaceId="";
  // float supportSurfaceHeight=INFINITY;
  std::vector<moveit_msgs::CollisionObject> collision_objects;
  size_t mapDetToRecCounter = 0;
  for (size_t i = 0; i < detections.size();) {
    ROS_INFO("Next object:");
    // std::cout << detections[i] << std::endl;//This also prints
    // the source_cloud, which is way too large
    for (size_t j = 0; j < detections[i].results.size() && j < 3;
         j++) {
      ROS_INFO_STREAM("ObjectHypothesis: id: "
                << getObjectLabelById(*nh1,
                                      detections[i].results[j].id)
                << ", score: " << detections[i].results[j].score);
    }
    if (detections[i].results.size() == 0) {
      ROS_INFO("No ObjectsHypothesis");
    }
    ROS_INFO_STREAM(
        "Index in classifications: " << mapDetToRec[mapDetToRecCounter]);
    vision_msgs::Classification2D classification =
        (mapDetToRec[mapDetToRecCounter] != -1 &&
                 srv3.response.classifications.size() > 0
             ? srv3.response.classifications[mapDetToRec[mapDetToRecCounter]]
             : *(new vision_msgs::Classification2D())); // TODO check for NULL?
    if (mapDetToRec[mapDetToRecCounter] != -1 &&
        srv3.response.classifications.size() > 0) {
      detections[i].results.resize(
          srv3.response.classifications.size());
      for (size_t j = 0; j < srv3.response.classifications.size(); j++) {
        detections[i].results[j].id =
            classification.results[j].id;
        detections[i].results[j].score =
            classification.results[j].score;
      }
    }
    mapDetToRecCounter++;
    if (detections[i].source_cloud.header.frame_id.length() ==
        0) {
      ROS_INFO("Empty frame id, skipping object"); // Probably no point cloud or
                                                   // bbox, only 2D detections
      detections.erase(detections.begin() + i);
      continue;
    }
    if (table_height > 0.1 && detections[i].bbox.center.position.z < (table_height - 0.05)) { // TODO is this correct? right frame?
      ROS_INFO("Point cluster below table height");
      // continue;
    }
    bboxes.boxes.push_back(detections[i].bbox);
    // std::cout << "source_cloud header:\n" <<
    // detections[i].source_cloud.header << std::endl;
    // std::cout << "Source cloud info: " <<
    // detections[i].source_cloud.height << " width:" <<
    // detections[i].source_cloud.width << " " <<
    // detections[i].source_cloud.is_bigendian << " point_step:"
    // << detections[i].source_cloud.point_step << " row_step:" <<
    // detections[i].source_cloud.row_step << " is dense:" <<
    // (detections[i].source_cloud.is_dense?"true":"false") <<
    // std::endl;
    ROS_INFO("Classifications:");
    for (size_t j = 0; j < classification.results.size() && j < 3; j++) {
      ROS_INFO_STREAM("ObjectHypothesis from new classification: id: "
                      << getObjectLabelById(*nh1, classification.results[j].id)
                      << ", score: " << classification.results[j].score);
    }
    std::multimap<float, std::string> size_predictions =
        classifyBySize(detections[i].bbox.size.x,
                       detections[i].bbox.size.y,
                       detections[i].bbox.size.z);
    // std::cout << "Size predictions: most likely: " <<
    // size_predictions.crbegin()->second << " with score " <<
    // size_predictions.crbegin()->first << std::endl;
    size_t j = 0;
    for (std::multimap<float, std::string>::const_reverse_iterator iter =
             size_predictions.crbegin();
         iter != size_predictions.crend() && j < 3; iter++) {
      ROS_INFO_STREAM("Size predictions: id: " << iter->second
                << ", score: " << iter->first);
      j++;
    }
    int obj_or_surf =
        (classifyObject(detections[i].bbox.size.x,
                        detections[i].bbox.size.y,
                        detections[i].bbox.size.z) ==
                 0 /*&& (classification.results.size()==0 ||
                      classification.results[0].score>0.5)*/ ? 0 : 1); // TODO maybe treat surfaces separately?
    if (obj_or_surf == 1) {
      ROS_INFO(
          "Does not seem to be an object, so not adding it to planning scene");
      detections.erase(detections.begin() + i);
      continue; // Only add objects to planning scene
    }

    // Prepare to call object fitter
    clf_grasping_msgs::CloudToCollision srv2;
    srv2.request.source_cloud = detections[i].source_cloud;
    float probabilities[5] = {0.0, 0.0, 0.0, 0.0, 0.0};
    for (size_t j = 0; j < classification.results.size();
         j++) { // TODO use detections from srv1 if available?
      probabilities[getPrimitiveOfObject(
          getObjectLabelById(*nh1, classification.results[j].id))] +=
          classification.results[j].score;
    }
    probabilities[0] =
        1.0 - probabilities[1] - probabilities[2] - probabilities[3] -
        probabilities[4]; // Make sure all probabilities sum up to 1.0
    ROS_INFO_STREAM("Probable shapes: other: "
                    << probabilities[OTHER] << ", box: " << probabilities[BOX]
                    << ", sphere: " << probabilities[SPHERE]
                    << ", cylinder: " << probabilities[CYLINDER]
                    << ", cone: " << probabilities[CONE]);
    if (probabilities[BOX] > shape_prob_threshold) {
      srv2.request.shapes.push_back("box");
    }
    if (probabilities[CYLINDER] > shape_prob_threshold) {
      srv2.request.shapes.push_back("cylinder");
    }
    if (probabilities[SPHERE] > shape_prob_threshold) {
      srv2.request.shapes.push_back("sphere");
    }
    if ((probabilities[OTHER] + probabilities[CONE]) > shape_prob_threshold) {
      srv2.request.shapes.push_back("any");
    }
    std::cout << "Telling fitter to try shapes: ";
    for (size_t j = 0; j < srv2.request.shapes.size(); j++) {
      std::cout << srv2.request.shapes[j] << " ";
    }
    std::cout << std::endl;
    // TODO if object fitter is not started, there is a segfault somewhere in
    // the next lines
    // Add shapes/plane
    if (table_height != 0.0) {
      try {
        // TODO use getIsometryTransform from util.hpp?
        geometry_msgs::TransformStamped transformStamped =
            tfBuffer1->lookupTransform(
                srv2.request.source_cloud.header.frame_id, BASE_FRAME,
                ros::Time(0)); // Latest transform
        Eigen::Isometry3d transf=tf2::transformToEigen(transformStamped);
        //tf::transformMsgToEigen(transformStamped.transform, transf);
        srv2.request.table_plane =
            transformPlane(0.0, 0.0, 1.0, -table_height, transf);
        srv2.request.use_plane = true;
      } catch (tf2::TransformException &ex) {
        ROS_WARN("%s", ex.what());
        srv2.request.use_plane = false;
      }
    } else {
      srv2.request.use_plane = false;
    }
    ROS_INFO("Calling object fitter service"); // TODO segfault somewhere here
                                               // if object fitter node is not
                                               // running
    if (obj_or_surf == 0 && object_fitter_client.call(srv2)) {
      ROS_INFO("Call service success (fitter)");
      // TODO: check if fitted oject is in bbox
      /*if(srv2.response.collision_object.primitives[0].type =
      srv2.response.collision_object.primitives[0].CYLINDER) {
          //TODO idea: make sure that the size of the cylinder is correct
      }*/
    } else {
      ROS_WARN("Possibly error with service call (fitter)");
      srv2.response.collision_object.primitives.resize(1);
      srv2.response.collision_object.primitives[0].type =
          srv2.response.collision_object.primitives[0].BOX; //(probabilities[CYLINDER]>0.7?srv2.response.collision_object.primitives[0].CYLINDER:srv2.response.collision_object.primitives[0].BOX);//TODO
                    // use array probabilities?
      srv2.response.collision_object.primitives[0].dimensions.resize(3);
      srv2.response.collision_object.primitives[0].dimensions[0] =
          detections[i].bbox.size.x;
      srv2.response.collision_object.primitives[0].dimensions[1] =
          detections[i].bbox.size.y;
      srv2.response.collision_object.primitives[0].dimensions[2] =
          detections[i].bbox.size.z;
      
      srv2.response.collision_object.primitive_poses.resize(1);
      srv2.response.collision_object.primitive_poses[0] =
          detections[i].bbox.center;
    }
    // if(obj_or_surf==1) {
    //    srv2.response.collision_object.primitives[0].dimensions[0]+=0.07;//Make
    //    bigger to be sure
    //    srv2.response.collision_object.primitives[0].dimensions[1]+=0.07;
    //    srv2.response.collision_object.primitives[0].dimensions[2]+=2.0;//Make
    //    the surface width bigger, because the edges may be out of view //TODO
    //    find a better solution
    //}
    srv2.response.collision_object.operation =
        srv2.response.collision_object.ADD;
    if (classification.results.size() > 0 &&
        classification.results[0].score > classification_prob_threshold)
      srv2.response.collision_object.type.key = getObjectLabelById(
          *nh1, classification.results[0]
                    .id); // This seems to get cleared at some point?
    srv2.response.collision_object.header =
        detections[i]
            .source_cloud
            .header; // TODO use detections[i].header instead?
    std::ostringstream oss;
    oss << (obj_or_surf == 1
                ? "surface"
                : (classification.results.size() > 0 &&
                           classification.results[0].score > classification_prob_threshold
                       ? getObjectLabelById(*nh1, classification.results[0].id)
                       : "object"))
        << global_unique_id++;
    srv2.response.collision_object.id = oss.str();
    // TODO use knowledge which object it is for dimensions?
    if (classification.results.size() > 0 &&
        classification.results[0].score > classification_prob_threshold) {
      Object *obj =
          getObject(getObjectLabelById(*nh1, classification.results[0].id));
      if (obj != NULL && obj->primitive == CYLINDER &&
          srv2.response.collision_object.primitives[0].type == 3) {
        ROS_INFO_STREAM(
            "Adapting cylinder dimensions: "
            << srv2.response.collision_object.primitives[0].dimensions[0]
            << " to " << obj->dim0 << " and "
            << srv2.response.collision_object.primitives[0].dimensions[1]
            << " to " << obj->dim1 / 2.0);
        srv2.response.collision_object.primitives[0].dimensions[0] = obj->dim0;
        srv2.response.collision_object.primitives[0].dimensions[1] =
            obj->dim1 / 2.0;
      }
    }
    if(make_all_standard_cylinder) {
        ROS_WARN("Making this object to a cylinder (height=0.08, radius=0.02), no matter what it is!");
        srv2.response.collision_object.primitives[0].type = 3;
        srv2.response.collision_object.primitives[0].dimensions.resize(2);
        srv2.response.collision_object.primitives[0].dimensions[0] = 0.08;
        srv2.response.collision_object.primitives[0].dimensions[1] = 0.02;
        //Eigen::Quaterniond orientation(1.0, 0.0, 0.0, 0.0);
        //Eigen::Quaterniond new_orientation=orientation*(transform.rotation());
        Eigen::Quaterniond new_orientation(transform.rotation());
        //tf::Quaternion orientation(0.0, 0.0, 0.0, 1.0);
        //tf::Quaternion new_orientation=transform*orientation;
        //srv2.response.collision_object.primitive_poses[0].orientation = tf2::toMsg(new_orientation);
        srv2.response.collision_object.primitive_poses[0].orientation.x = new_orientation.x();
        srv2.response.collision_object.primitive_poses[0].orientation.y = new_orientation.y();
        srv2.response.collision_object.primitive_poses[0].orientation.z = new_orientation.z();
        srv2.response.collision_object.primitive_poses[0].orientation.w = new_orientation.w();
        //if(srv2.response.collision_object.primitive_poses[0].position.z < (table_height + 0.04 + 0.01)) { // This does not work because the coordinates are in the camera frame
        //    srv2.response.collision_object.primitive_poses[0].position.z = (table_height + 0.04 + 0.01);
        //}
    }
    // srv2.response.collision_object.primitive_poses[0].position.x+=0.02;
    // TODO rotate cylinders (and other objects?) towards robot if invariant?
    collision_objects.push_back(srv2.response.collision_object);
    if (objectIds != NULL) {
      objectIds->push_back(srv2.response.collision_object.id);
    }
    std::cout << srv2.response.collision_object << std::endl;
    // if(obj_or_surf==1 &&
    // detections[i].bbox.center.position.z<supportSurfaceHeight)
    // {
    //    supportSurfaceHeight=detections[i].bbox.center.position.z;
    //    supportSurfaceId=srv2.response.collision_object.id;
    //}
    // usleep(10000000);
    i++;
  }
  bbox_pub.publish(bboxes);
  ROS_INFO("Found %lu objects\n", collision_objects.size());

  if (addToPlanningScene) {
    printf("Adding %lu collision objects to planning scene\n",
           collision_objects.size());
    if (planning_scene_interface1 != NULL) {
      planning_scene_interface1->applyCollisionObjects(collision_objects);
    } else {
      ROS_WARN("planning_scene_interface is NULL");
    }
  }
  return detections;
  // return supportSurfaceId;
}
