/*
 * Software License Agreement (BSD License)
 *
 *  Copyright (c) 2009, Willow Garage, Inc.
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *   * Neither the name of Willow Garage, Inc. nor the names of its
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 *  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 *  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 *  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 *  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *  POSSIBILITY OF SUCH DAMAGE.
 */

#include "topic2.h"
#include <ros/callback_queue.h>

namespace ros
{
namespace topic
{

void waitForMessagesImpl(SubscribeOptions& ops0,
                         SubscribeOptions& ops1,
			const boost::function<bool(void)>& ready_pred0,
			const boost::function<bool(void)>& ready_pred1, 
			NodeHandle& nh, ros::Duration timeout)
{
  ros::CallbackQueue queue0;
  ops0.callback_queue = &queue0;
  ros::CallbackQueue queue1;
  ops1.callback_queue = &queue1;

  ros::Subscriber sub0 = nh.subscribe(ops0); // TODO unsubscribe later?
  ros::Subscriber sub1 = nh.subscribe(ops1);

  ros::Time end = ros::Time::now() + timeout;
  while (nh.ok() && (timeout.isZero() || ros::Time::now() < end))
  {
    if (!ready_pred0())
    {
      queue0.callOne(ros::WallDuration(0.05));
      if (!ready_pred1())
      {
        queue1.callOne(ros::WallDuration(0.05));
      }
    }
    else
    {
      if (!ready_pred1())
      {
        queue1.callOne(ros::WallDuration(0.05));
      }
      else
      {
        return;
      }
    }
    /*if (ready_pred0() && ready_pred1()) {
      return;
    }
    queue0.callAvailable(ros::WallDuration(0.05));
    queue1.callAvailable(ros::WallDuration(0.05));*/
  }
}

} // namespace topic
} // namespace ros
