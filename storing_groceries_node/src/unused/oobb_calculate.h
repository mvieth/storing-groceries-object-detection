/**
 * @file
 * @author  Markus Vieth
 * @section DESCRIPTION
 *
 * Calculate bounding boxes
 */
#pragma once

#include <pcl/PCLPointCloud2.h>
#include <pcl/common/common.h>
#include <pcl/common/pca.h>
#include <pcl/conversions.h>
#include <pcl/features/moment_of_inertia_estimation.h>
#include <pcl/point_types.h>
#include <pcl/visualization/pcl_visualizer.h>
#include <pcl/visualization/point_cloud_color_handlers.h>
#include <pcl_conversions/pcl_conversions.h>
#include <pcl_ros/transforms.h>

#include <sensor_msgs/PointCloud2.h>
#include <vision_msgs/BoundingBox3D.h>

#include "util.hpp"

namespace OOBB_Calculate {
vision_msgs::BoundingBox3D
calculate(pcl::PointCloud<point_t>::Ptr temp_cloud) {
  vision_msgs::BoundingBox3D ret;

  // http://codextechnicanum.blogspot.com/2015/04/find-minimum-oriented-bounding-box-of.html
  //pcl::PCLPointCloud2 pcl_pc2;
  /*pcl_conversions::toPCL(cloud, pcl_pc2);

  pcl::PointCloud<point_t>::Ptr temp_cloud(new
  pcl::PointCloud<point_t>);
  pcl::fromPCLPointCloud2(pcl_pc2, *temp_cloud);*/

  if (temp_cloud->size() < 3) {
    ROS_ERROR_STREAM("cloud too small, could not create BB");
    return ret;
  }

  Eigen::Vector4f pcaCentroid;
  pcl::compute3DCentroid(*temp_cloud, pcaCentroid);

  Eigen::Matrix3f covariance;
  computeCovarianceMatrixNormalized(*temp_cloud, pcaCentroid, covariance);
  Eigen::SelfAdjointEigenSolver<Eigen::Matrix3f> eigen_solver(
      covariance, Eigen::ComputeEigenvectors);
  Eigen::Matrix3f eigenVectorsPCA = eigen_solver.eigenvectors();
  eigenVectorsPCA.col(2) = eigenVectorsPCA.col(0).cross(eigenVectorsPCA.col(1));

  // Note that getting the eigenvectors can also be obtained via the PCL PCA
  // interface with something like:
  // pcl::PointCloud<point_t>::Ptr cloudPCAprojection (new
  // pcl::PointCloud<point_t>);
  // pcl::PCA<point_t> pca;
  // pca.setInputCloud(temp_cloud);
  // pca.project(*temp_cloud, *cloudPCAprojection);
  // std::cerr << std::endl << "PCLEigenVectors: " << pca.getEigenVectors() <<
  // std::endl;
  // std::cerr << std::endl << "EigenVectors: " << eigenVectorsPCA << std::endl;

  // Transform the original cloud to the origin where the principal components
  // correspond to the axes.
  Eigen::Matrix4f projectionTransform(Eigen::Matrix4f::Identity());
  projectionTransform.block<3, 3>(0, 0) = eigenVectorsPCA.transpose();
  projectionTransform.block<3, 1>(0, 3) =
      -1.f * (projectionTransform.block<3, 3>(0, 0) * pcaCentroid.head<3>());
  pcl::PointCloud<point_t>::Ptr cloudPointsProjected(
      new pcl::PointCloud<point_t>);
  pcl::transformPointCloud(*temp_cloud, *cloudPointsProjected,
                           projectionTransform);

  // Get the minimum and maximum points of the transformed cloud.
  point_t minPoint, maxPoint;
  pcl::getMinMax3D(*cloudPointsProjected, minPoint, maxPoint);
  const Eigen::Vector3f meanDiagonal =
      0.5f * (maxPoint.getVector3fMap() + minPoint.getVector3fMap());

  // Final transform
  const Eigen::Quaternionf bboxQuaternion(eigenVectorsPCA);
  const Eigen::Vector3f bboxTransform =
      eigenVectorsPCA * meanDiagonal + pcaCentroid.head<3>();

  ret.size.x = maxPoint.x - minPoint.x;
  ret.size.y = maxPoint.y - minPoint.y;
  ret.size.z = maxPoint.z - minPoint.z;
  ret.center.position.x = bboxTransform[0];
  ret.center.position.y = bboxTransform[1];
  ret.center.position.z = bboxTransform[2];
  ret.center.orientation.w = (double)bboxQuaternion.w();
  ret.center.orientation.x = bboxQuaternion.x();
  ret.center.orientation.y = bboxQuaternion.y();
  ret.center.orientation.z = bboxQuaternion.z();

  return ret;
}

vision_msgs::BoundingBox3D
calculate2(pcl::PointCloud<point_t>::Ptr temp_cloud) {
  vision_msgs::BoundingBox3D ret;
  pcl::MomentOfInertiaEstimation<point_t> feature_extractor;
  feature_extractor.setInputCloud(temp_cloud);
  feature_extractor.compute();

  point_t min_point_OBB;
  point_t max_point_OBB;
  point_t position_OBB;
  Eigen::Matrix3f rotational_matrix_OBB;
  feature_extractor.getOBB(min_point_OBB, max_point_OBB, position_OBB,
                           rotational_matrix_OBB);
  ret.size.x = max_point_OBB.x - min_point_OBB.x;
  ret.size.y = max_point_OBB.y - min_point_OBB.y;
  ret.size.z = max_point_OBB.z - min_point_OBB.z;
  ret.center.position.x =
      (max_point_OBB.x + min_point_OBB.x) / 2.0 + position_OBB.x;
  ret.center.position.y =
      (max_point_OBB.y + min_point_OBB.y) / 2.0 + position_OBB.y;
  ret.center.position.z =
      (max_point_OBB.z + min_point_OBB.z) / 2.0 + position_OBB.z;
  const Eigen::Quaternionf q(rotational_matrix_OBB);
  ret.center.orientation.w = (double)q.w();
  ret.center.orientation.x = q.x();
  ret.center.orientation.y = q.y();
  ret.center.orientation.z = q.z();
  return ret;
}

vision_msgs::BoundingBox3D
calculateAABB(pcl::PointCloud<point_t>::Ptr temp_cloud) {
  vision_msgs::BoundingBox3D ret;
  pcl::MomentOfInertiaEstimation<point_t> feature_extractor;
  feature_extractor.setInputCloud(temp_cloud);
  feature_extractor.compute();

  point_t min_point_AABB;
  point_t max_point_AABB;
  feature_extractor.getAABB(min_point_AABB, max_point_AABB);
  ret.size.x = max_point_AABB.x - min_point_AABB.x;
  ret.size.y = max_point_AABB.y - min_point_AABB.y;
  ret.size.z = max_point_AABB.z - min_point_AABB.z;
  ret.center.position.x = (max_point_AABB.x + min_point_AABB.x) / 2.0;
  ret.center.position.y = (max_point_AABB.y + min_point_AABB.y) / 2.0;
  ret.center.position.z = (max_point_AABB.z + min_point_AABB.z) / 2.0;
  //"standard" orientation
  ret.center.orientation.w = 1.0;
  return ret;
}

void orderSizeAndAdaptOrientation(Eigen::Vector3f& size, Eigen::Quaternionf& orientation, bool asc) {
    //TODO could all rotations be (0.5, +-0.5, +-0.5, +-0.5)? maybe that would be more elegant?
    if(size.x() < size.y()) {
        if(size.z() < size.y()) {
            if(size.z() < size.x()) {
                if(asc) {
                    size << size.z(), size.x(), size.y();
                    orientation=Eigen::Quaternionf(-0.5, 0.5, 0.5, 0.5)*orientation;
                } else {
                    size << size.y(), size.x(), size.z();
                    orientation=Eigen::Quaternionf(0.707106781, 0.0, 0.0, 0.707106781)*orientation;
                }
            } else {
                if(asc) {
                    size << size.x(), size.z(), size.y();
                    orientation=Eigen::Quaternionf(0.707106781, 0.707106781, 0.0, 0.0)*orientation;
                } else {
                    size << size.y(), size.z(), size.x();
                    orientation=Eigen::Quaternionf(0.5, 0.5, 0.5, 0.5)*orientation;
                }
            }
        } else {
            if(asc) {
                // order is already correct
                // size << size.x(), size.y(), size.z();
                // orientation=Eigen::Quaternionf(1.0, 0.0, 0.0, 0.0)*orientation;
            } else {
                size << size.z(), size.y(), size.x();
                orientation=Eigen::Quaternionf(0.707106781, 0.0, 0.707106781, 0.0)*orientation;
            }
        }
    } else {
        if(size.z() < size.x()) {
            if(size.z() < size.y()) {
                if(asc) {
                    size << size.z(), size.y(), size.x();
                    orientation=Eigen::Quaternionf(0.707106781, 0.0, 0.707106781, 0.0)*orientation;
                } else {
                    // order is already correct
                    // size << size.x(), size.y(), size.z();
                    // orientation=Eigen::Quaternionf(1.0, 0.0, 0.0, 0.0)*orientation;
                }
            } else {
                if(asc) {
                    size << size.y(), size.z(), size.x();
                    orientation=Eigen::Quaternionf(0.5, 0.5, 0.5, 0.5)*orientation;
                } else {
                    size << size.x(), size.z(), size.y();
                    orientation=Eigen::Quaternionf(0.707106781, 0.707106781, 0.0, 0.0)*orientation;
                }
            }
        } else {
            if(asc) {
                size << size.y(), size.x(), size.z();
                orientation=Eigen::Quaternionf(0.707106781, 0.0, 0.0, 0.707106781)*orientation;
            } else {
                size << size.z(), size.x(), size.y();
                orientation=Eigen::Quaternionf(-0.5, 0.5, 0.5, 0.5)*orientation;
            }
        }
    }
}

vision_msgs::BoundingBox3D chooseBest(
    pcl::PointCloud<point_t>::Ptr
        temp_cloud) { // TODO feature_extractor.compute() seems to take quite
                      // some time and gives more than we need. Maybe switch to
                      // calculate() and another method for the AABB instead?
  printf("OOBB_Calculate::chooseBest called\n");

  pcl::MomentOfInertiaEstimation<point_t> feature_extractor;
  feature_extractor.setInputCloud(temp_cloud);
  feature_extractor.compute();

  point_t min_point_AABB;
  point_t max_point_AABB;
  feature_extractor.getAABB(min_point_AABB, max_point_AABB);
  vision_msgs::BoundingBox3D a;
  a.center.position.x = (max_point_AABB.x + min_point_AABB.x) / 2.0;
  a.center.position.y = (max_point_AABB.y + min_point_AABB.y) / 2.0;
  a.center.position.z = (max_point_AABB.z + min_point_AABB.z) / 2.0;
  //float dim0 = max_point_AABB.x - min_point_AABB.x,
  //      dim1 = max_point_AABB.y - min_point_AABB.y,
  //      dim2 = max_point_AABB.z - min_point_AABB.z;
  // Make sure that a.size.z <= a.size.y <= a.size.x
  Eigen::Vector3f size(max_point_AABB.x - min_point_AABB.x, max_point_AABB.y - min_point_AABB.y, max_point_AABB.z - min_point_AABB.z);
  Eigen::Quaternionf quat(1.0, 0.0, 0.0, 0.0);
  orderSizeAndAdaptOrientation(size, quat, false);
  a.size = tf2::toMsg(size);
  a.center.orientation = tf2::toMsg(quat);
  /*if (dim0 <= dim1 && dim1 <= dim2) {
    a.size.x = dim2;
    a.size.y = dim1;
    a.size.z = dim0;
    a.center.orientation.w = 0.707106781;
    a.center.orientation.x = 0.0;
    a.center.orientation.y = 0.707106781;
    a.center.orientation.z = 0.0;
  } else if (dim0 <= dim2 && dim2 <= dim1) {
    a.size.x = dim1;
    a.size.y = dim2;
    a.size.z = dim0;
    a.center.orientation.w = 0.5;
    a.center.orientation.x = 0.5;
    a.center.orientation.y = 0.5;
    a.center.orientation.z = 0.5;
  } else if (dim1 <= dim0 && dim0 <= dim2) {
    a.size.x = dim2;
    a.size.y = dim0;
    a.size.z = dim1;
    a.center.orientation.w = -0.5;
    a.center.orientation.x = 0.5;
    a.center.orientation.y = 0.5;
    a.center.orientation.z = 0.5;
  } else if (dim1 <= dim2 && dim2 <= dim0) {
    a.size.x = dim0;
    a.size.y = dim2;
    a.size.z = dim1;
    a.center.orientation.w = 0.707106781;
    a.center.orientation.x = 0.707106781;
    a.center.orientation.y = 0.0;
    a.center.orientation.z = 0.0;
  } else if (dim2 <= dim0 && dim0 <= dim1) {
    a.size.x = dim1;
    a.size.y = dim0;
    a.size.z = dim2;
    a.center.orientation.w = 0.707106781;
    a.center.orientation.x = 0.0;
    a.center.orientation.y = 0.0;
    a.center.orientation.z = 0.707106781;
  } else if (dim2 <= dim1 && dim1 <= dim0) {
    a.size.x = dim0;
    a.size.y = dim1;
    a.size.z = dim2;
    a.center.orientation.w = 1.0;
    a.center.orientation.x = 0.0;
    a.center.orientation.y = 0.0;
    a.center.orientation.z = 0.0;
  }*/

  point_t min_point_OBB;
  point_t max_point_OBB;
  point_t position_OBB;
  Eigen::Matrix3f rotational_matrix_OBB;
  feature_extractor.getOBB(min_point_OBB, max_point_OBB, position_OBB,
                           rotational_matrix_OBB);
  vision_msgs::BoundingBox3D c;
  c.size.x = max_point_OBB.x - min_point_OBB.x;
  c.size.y = max_point_OBB.y - min_point_OBB.y;
  c.size.z = max_point_OBB.z - min_point_OBB.z;
  c.center.position.x =
      (max_point_OBB.x + min_point_OBB.x) / 2.0 + position_OBB.x;
  c.center.position.y =
      (max_point_OBB.y + min_point_OBB.y) / 2.0 + position_OBB.y;
  c.center.position.z =
      (max_point_OBB.z + min_point_OBB.z) / 2.0 + position_OBB.z;
  const Eigen::Quaternionf q(rotational_matrix_OBB);
  c.center.orientation.w = (double)q.w();
  c.center.orientation.x = q.x();
  c.center.orientation.y = q.y();
  c.center.orientation.z = q.z();

  double a_size = std::max(a.size.x, 0.01) * std::max(a.size.y, 0.01) *
                  std::max(a.size.z, 0.01);
  double c_size = std::max(c.size.x, 0.01) * std::max(c.size.y, 0.01) *
                  std::max(c.size.z, 0.01);
  //printf("a dims: %f %f %f\n", a.size.x, a.size.y, a.size.z);
  //printf("c dims: %f %f %f\n", c.size.x, c.size.y, c.size.z);
  //printf("OOBB_Calculate::chooseBest(): a_size=%f, c_size=%f\n", a_size, c_size);
  if (a_size <= c_size) {
    return a;
  } else {
    return c;
  }
}

vision_msgs::BoundingBox3D
chooseBest2(pcl::PointCloud<point_t>::Ptr temp_cloud) {
  printf("OOBB_Calculate::chooseBest called\n");
  vision_msgs::BoundingBox3D a = calculate(temp_cloud);
  vision_msgs::BoundingBox3D b = calculate2(temp_cloud);
  vision_msgs::BoundingBox3D c = calculateAABB(temp_cloud);
  double a_size = std::max(a.size.x, 0.01) * std::max(a.size.y, 0.01) *
                  std::max(a.size.z, 0.01);
  double b_size = std::max(b.size.x, 0.01) * std::max(b.size.y, 0.01) *
                  std::max(b.size.z, 0.01);
  double c_size = std::max(c.size.x, 0.01) * std::max(c.size.y, 0.01) *
                  std::max(c.size.z, 0.01);
  //printf("a dims: %f %f %f\n", a.size.x, a.size.y, a.size.z);
  //printf("c dims: %f %f %f\n", c.size.x, c.size.y, c.size.z);
  //printf("OOBB_Calculate::chooseBest(): a_size=%f, b_size=%f, c_size=%f\n", a_size, b_size, c_size);
  if (a_size <= b_size) {
    if (a_size <= c_size) {
      return a;
    } else {
      return c;
    }
  } else {
    if (b_size <= c_size) {
      return b;
    } else {
      return c;
    }
  }
}
}
