// Adapted from
// http://pointclouds.org/documentation/tutorials/planar_segmentation.php#planar-segmentation
// and http://wiki.ros.org/ROS/Tutorials/WritingPublisherSubscriber%28c%2B%2B%29
#include <iostream>
#include <thread>
#include <vector>

#include <pcl/ModelCoefficients.h>
#include <pcl/features/moment_of_inertia_estimation.h>
#include <pcl/filters/passthrough.h>
#include <pcl/filters/project_inliers.h>
#include <pcl/io/pcd_io.h>
#include <pcl/point_types.h>
#include <pcl/sample_consensus/method_types.h>
#include <pcl/sample_consensus/model_types.h>
#include <pcl/segmentation/sac_segmentation.h>
#include <pcl/surface/concave_hull.h>
#include <pcl/visualization/cloud_viewer.h>

#include <eigen_conversions/eigen_msg.h>
#include <pcl_conversions/pcl_conversions.h>
#include <pcl_ros/filters/extract_indices.h>
#include <pcl_ros/filters/filter.h>
#include <pcl_ros/transforms.h>
#include <ros/ros.h>
#include <tf2_ros/transform_listener.h>

#include <sensor_msgs/PointCloud2.h>
#include <shape_msgs/Plane.h>
#include <visualization_msgs/Marker.h>

#define DISTANCE_THRESHOLD 0.01
#define MIN_NUM_POINTS_SEG 20
#define SUBSCRIBE_TOPIC "/xtion/depth_registered/points" //"/voxel_grid/output"
#define PUBLISH_TOPIC "/plane"
#define BASE_FRAME "base_footprint"

ros::Publisher plane_pub;
ros::Publisher planeviz_pub;
tf2_ros::Buffer tfBuffer;
tf2_ros::TransformListener *tfListener;

void deleteAllMarkers() {
  visualization_msgs::Marker marker;
  // Set the frame ID and timestamp.  See the TF tutorials for information on
  // these.
  marker.header.frame_id = BASE_FRAME;
  marker.header.stamp = ros::Time::now();

  // Set the namespace and id for this marker.  This serves to create a unique
  // ID
  // Any marker sent with the same namespace and id will overwrite the old one
  marker.ns = "basic_shapes";
  marker.id = 0; // num;

  // Set the marker type.  Initially this is CUBE, and cycles between that and
  // SPHERE, ARROW, and CYLINDER
  marker.type = visualization_msgs::Marker::CUBE;

  // Set the marker action.  Options are ADD, DELETE, and new in ROS Indigo: 3
  // (DELETEALL)
  marker.action = 3;

  planeviz_pub.publish(marker);
}

void findPlane(const sensor_msgs::PointCloud2 &msg_in) {
  // ROS_INFO("I heard: [%s]", msg->data.c_str());
  sensor_msgs::PointCloud2 transCloud;
  // tfListener.lookupTransform("map", "", ros::Time(0), transform);
  // pcl_ros::transformPointCloud("map", msg_in, transCloud, tfListener);

  geometry_msgs::TransformStamped transformStamped;
  try {
    transformStamped = tfBuffer.lookupTransform(
        BASE_FRAME, msg_in.header.frame_id, msg_in.header.stamp);
  } catch (tf2::TransformException &ex) {
    ROS_WARN("%s", ex.what());
    return;
  }
  Eigen::Affine3d e;
  tf::transformMsgToEigen(transformStamped.transform, e);
  pcl_ros::transformPointCloud(e.matrix().cast<float>(), msg_in, transCloud);

  pcl::PointCloud<pcl::PointXYZ>::Ptr cloud(new pcl::PointCloud<pcl::PointXYZ>),
      cloud_projected(new pcl::PointCloud<pcl::PointXYZ>);
  pcl::fromROSMsg(transCloud, *cloud);

  // Do the segmentation
  pcl::ModelCoefficients::Ptr coefficients(new pcl::ModelCoefficients);
  pcl::PointIndices::Ptr inliers(new pcl::PointIndices);
  // Create the segmentation object
  pcl::SACSegmentation<pcl::PointXYZ> seg;
  // Optional
  seg.setOptimizeCoefficients(true);
  // Mandatory
  seg.setModelType(pcl::SACMODEL_PERPENDICULAR_PLANE);
  seg.setAxis(Eigen::Vector3f(0.0, 0.0, 1.0)); // This line and the next to only
                                               // search for horizontal planes
                                               // (e.g. table or shelf surfaces)
  seg.setEpsAngle(0.15); //
  seg.setMethodType(pcl::SAC_RANSAC);
  seg.setMaxIterations(100); // Default is 50?
  seg.setDistanceThreshold(DISTANCE_THRESHOLD);
  int num = 0;
  while (cloud->size() > MIN_NUM_POINTS_SEG /* && num++<20*/) {
    std::cout << "cloud size:" << cloud->size() << std::endl;
    num++;
    seg.setInputCloud(cloud);
    seg.segment(
        *inliers,
        *coefficients); // TODO idea: pre-set coefficients if shelf is known?
    std::cout << "Number of inliers:" << inliers->indices.size() << '\n';

    if (inliers->indices.size() < MIN_NUM_POINTS_SEG) {
      // PCL_ERROR ("Could not estimate a planar model for the given dataset.");
      return;
    }

    // Project the model inliers
    pcl::ProjectInliers<pcl::PointXYZ> proj;
    proj.setModelType(pcl::SACMODEL_PLANE);
    proj.setIndices(inliers);
    proj.setInputCloud(cloud);
    proj.setModelCoefficients(coefficients);
    proj.filter(*cloud_projected);
    // std::cerr << "PointCloud after projection has: "
    //		    << cloud_projected->points.size () << " data points." <<
    //std::endl;

    // Create a Concave Hull representation of the projected inliers
    pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_hull(
        new pcl::PointCloud<pcl::PointXYZ>);
    pcl::ConcaveHull<pcl::PointXYZ> chull;
    chull.setInputCloud(cloud_projected);
    chull.setAlpha(0.1);
    chull.reconstruct(*cloud_hull);

    double xmin = INFINITY, xmax = -INFINITY, ymin = INFINITY, ymax = -INFINITY;
    for (pcl::PointCloud<pcl::PointXYZ>::iterator it = cloud_hull->begin();
         it != cloud_hull->end(); ++it) {
      if (it->x < xmin) {
        xmin = it->x;
      }
      if (it->x > xmax) {
        xmax = it->x;
      }
      if (it->y < ymin) {
        ymin = it->y;
      }
      if (it->y > ymax) {
        ymax = it->y;
      }
    }
    std::cout << "xmin:" << xmin << " xmax:" << xmax << " ymin:" << ymin
              << " ymax:" << ymax << std::endl;

    pcl::ExtractIndices<pcl::PointXYZ> eifilter(true); // TODO apparently does
                                                       // not really extract,
                                                       // the cloud does not get
                                                       // smaller
    eifilter.setInputCloud(cloud);
    eifilter.setIndices(inliers);
    eifilter.setNegative(true);
    eifilter.filterDirectly(cloud);

    // if(coefficients->values[2]<0.99 && coefficients->values[2]>-0.99)
    // continue; //We only want plane parallel to the ground (horizontal)

    shape_msgs::Plane msg;
    msg.coef[0] = coefficients->values[0];
    msg.coef[1] = coefficients->values[1];
    msg.coef[2] = coefficients->values[2];
    msg.coef[3] = coefficients->values[3];
    plane_pub.publish(msg);

    // ROS_INFO("%s", msg.data.c_str());
    // Source: https://wiki.ros.org/rviz/Tutorials/Markers%3A%20Basic%20Shapes
    visualization_msgs::Marker marker;
    // Set the frame ID and timestamp.  See the TF tutorials for information on
    // these.
    marker.header.frame_id = BASE_FRAME;
    marker.header.stamp = msg_in.header.stamp; // ros::Time::now();

    // Set the namespace and id for this marker.  This serves to create a unique
    // ID
    // Any marker sent with the same namespace and id will overwrite the old one
    marker.ns = "basic_shapes";
    marker.id = num;

    // Set the marker type.  Initially this is CUBE, and cycles between that and
    // SPHERE, ARROW, and CYLINDER
    marker.type = visualization_msgs::Marker::CUBE;

    // Set the marker action.  Options are ADD, DELETE, and new in ROS Indigo: 3
    // (DELETEALL)
    marker.action = visualization_msgs::Marker::ADD;

    // Set the pose of the marker.  This is a full 6DOF pose relative to the
    // frame/time specified in the header
    double squared = coefficients->values[0] * coefficients->values[0] +
                     coefficients->values[1] * coefficients->values[1] +
                     coefficients->values[2] * coefficients->values[2];
    marker.pose.position.x =
        (xmin + xmax) /
        2.0; //-coefficients->values[0]*coefficients->values[3]/squared;
    marker.pose.position.y =
        (ymin + ymax) /
        2.0; //-coefficients->values[1]*coefficients->values[3]/squared;
    marker.pose.position.z =
        -coefficients->values[2] * coefficients->values[3] / squared;
    Eigen::Vector3f v1(0.0, 0.0, 1.0);
    Eigen::Vector3f v2(coefficients->values[0], coefficients->values[1],
                       coefficients->values[2]);
    // Vector3d a=v2.cross(v1);
    Eigen::Quaternionf quat;
    quat = quat.FromTwoVectors(v1, v2);

    // std::cout << "This quaternion consists of a scalar " << quat.w() << " and
    // a vector " << std::endl << quat.vec() << std::endl;

    marker.pose.orientation.x = quat.x(); // 0.0;
    marker.pose.orientation.y = quat.y(); // 0.0;
    marker.pose.orientation.z = quat.z(); // 0.0;
    marker.pose.orientation.w = quat.w(); // 1.0;

    // Set the scale of the marker -- 1x1x1 here means 1m on a side
    marker.scale.x = xmax - xmin; // 1.0;
    marker.scale.y = ymax - ymin; // 1.0;
    marker.scale.z = 0.02;

    // Set the color -- be sure to set alpha to something non-zero!
    marker.color.r = 0.0f;
    marker.color.g = 1.0f;
    marker.color.b = 0.0f;
    marker.color.a = 1.0;

    marker.lifetime = ros::Duration();
    if (num == 1)
      deleteAllMarkers();
    planeviz_pub.publish(marker);
  }
}

int moment_of_inertia(int argc, char **argv) {
  if (argc != 2)
    return (0);

  pcl::PointCloud<pcl::PointXYZ>::Ptr cloud(
      new pcl::PointCloud<pcl::PointXYZ>());
  if (pcl::io::loadPCDFile(argv[1], *cloud) == -1)
    return (-1);

  pcl::MomentOfInertiaEstimation<pcl::PointXYZ> feature_extractor;
  feature_extractor.setInputCloud(cloud);
  feature_extractor.compute();

  std::vector<float> moment_of_inertia;
  std::vector<float> eccentricity;
  pcl::PointXYZ min_point_AABB;
  pcl::PointXYZ max_point_AABB;
  pcl::PointXYZ min_point_OBB;
  pcl::PointXYZ max_point_OBB;
  pcl::PointXYZ position_OBB;
  Eigen::Matrix3f rotational_matrix_OBB;
  float major_value, middle_value, minor_value;
  Eigen::Vector3f major_vector, middle_vector, minor_vector;
  Eigen::Vector3f mass_center;

  feature_extractor.getMomentOfInertia(moment_of_inertia);
  feature_extractor.getEccentricity(eccentricity);
  feature_extractor.getAABB(min_point_AABB, max_point_AABB);
  feature_extractor.getOBB(min_point_OBB, max_point_OBB, position_OBB,
                           rotational_matrix_OBB);
  feature_extractor.getEigenValues(major_value, middle_value, minor_value);
  feature_extractor.getEigenVectors(major_vector, middle_vector, minor_vector);
  feature_extractor.getMassCenter(mass_center);

  pcl::visualization::PCLVisualizer::Ptr viewer(
      new pcl::visualization::PCLVisualizer("3D Viewer"));
  viewer->setBackgroundColor(0, 0, 0);
  viewer->addCoordinateSystem(1.0);
  viewer->initCameraParameters();
  viewer->addPointCloud<pcl::PointXYZ>(cloud, "sample cloud");
  viewer->addCube(min_point_AABB.x, max_point_AABB.x, min_point_AABB.y,
                  max_point_AABB.y, min_point_AABB.z, max_point_AABB.z, 1.0,
                  1.0, 0.0, "AABB");
  viewer->setShapeRenderingProperties(
      pcl::visualization::PCL_VISUALIZER_REPRESENTATION,
      pcl::visualization::PCL_VISUALIZER_REPRESENTATION_WIREFRAME, "AABB");

  Eigen::Vector3f position(position_OBB.x, position_OBB.y, position_OBB.z);
  Eigen::Quaternionf quat(rotational_matrix_OBB);
  viewer->addCube(position, quat, max_point_OBB.x - min_point_OBB.x,
                  max_point_OBB.y - min_point_OBB.y,
                  max_point_OBB.z - min_point_OBB.z, "OBB");
  viewer->setShapeRenderingProperties(
      pcl::visualization::PCL_VISUALIZER_REPRESENTATION,
      pcl::visualization::PCL_VISUALIZER_REPRESENTATION_WIREFRAME, "OBB");

  pcl::PointXYZ center(mass_center(0), mass_center(1), mass_center(2));
  pcl::PointXYZ x_axis(major_vector(0) + mass_center(0),
                       major_vector(1) + mass_center(1),
                       major_vector(2) + mass_center(2));
  pcl::PointXYZ y_axis(middle_vector(0) + mass_center(0),
                       middle_vector(1) + mass_center(1),
                       middle_vector(2) + mass_center(2));
  pcl::PointXYZ z_axis(minor_vector(0) + mass_center(0),
                       minor_vector(1) + mass_center(1),
                       minor_vector(2) + mass_center(2));
  viewer->addLine(center, x_axis, 1.0f, 0.0f, 0.0f, "major eigen vector");
  viewer->addLine(center, y_axis, 0.0f, 1.0f, 0.0f, "middle eigen vector");
  viewer->addLine(center, z_axis, 0.0f, 0.0f, 1.0f, "minor eigen vector");

  while (!viewer->wasStopped()) {
    viewer->spinOnce(100);
    // std::this_thread::sleep_for(std::chrono_literals::100ms);
    usleep(100000);
  }

  return (0);
}

int main(int argc, char **argv) {
  return moment_of_inertia(argc, argv);
  /*ros::init(argc, argv, "find_plane");
  ros::NodeHandle n;
  plane_pub = n.advertise<shape_msgs::Plane>(PUBLISH_TOPIC, 1000);
  planeviz_pub = n.advertise<visualization_msgs::Marker>("/planeviz", 1000);
  //plane_pub = n.advertise<visualization_msgs::Marker>(PUBLISH_TOPIC, 1000);
  ros::Subscriber sub = n.subscribe(SUBSCRIBE_TOPIC, 1, findPlane);
  tf2_ros::TransformListener newListener(tfBuffer);
  tfListener=&newListener;

  ros::spin();
  return (0);*/
}
