/*
 * Software License Agreement (BSD License)
 *
 *  Copyright (c) 2009, Willow Garage, Inc.
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *   * Neither the name of Willow Garage, Inc. nor the names of its
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 *  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 *  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 *  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 *  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *  POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef ROSCPP_TOPIC2_H
#define ROSCPP_TOPIC2_H

#include <ros/topic.h>
#include <ros/common.h>
#include <ros/node_handle.h>
#include <boost/shared_ptr.hpp>

namespace ros
{
namespace topic
{

/**
 * \brief Internal method, do not use
 */
ROSCPP_DECL void waitForMessagesImpl(SubscribeOptions& ops0, SubscribeOptions& ops1, const boost::function<bool(void)>& ready_pred0, const boost::function<bool(void)>& ready_pred1, NodeHandle& nh, ros::Duration timeout);

template<class M>
class SubscribeHelper2
{
public:
  typedef boost::shared_ptr<M const> MConstPtr;
  void callback(const MConstPtr& message)
  {
    ROS_INFO_STREAM("SubscribeHelper2::callback() called (" <<  ros::message_traits::datatype<M>() << ") with timestamp " << message->header.stamp);
    message_ = message;
  }

  bool hasMessage()
  {
    ROS_INFO("SubscribeHelper2::hasMessage() called (%s)", ros::message_traits::datatype<M>());
    return static_cast<bool>(message_);
  }

  MConstPtr getMessage()
  {
    ROS_INFO("SubscribeHelper2::getMessage() called (%s)", ros::message_traits::datatype<M>());
    return message_;
  }

private:
  MConstPtr message_;
};

/**
 * \brief Wait for a two messages to arrive on a topic, with timeout
 *
 * \param M <template> The message type
 * \param topic The topic to subscribe on
 * \param nh The NodeHandle to use to do the subscription
 * \param timeout The amount of time to wait before returning if no message is received
 * \param msg0, msg1 The messages.  Empty boost::shared_ptr if waitForMessages is interrupted by the node shutting down
 */
template<class M0, class M1>
void waitForMessages(const std::string& topic0, const std::string& topic1, NodeHandle& nh, ros::Duration timeout, boost::shared_ptr<M0 const> &msg0, boost::shared_ptr<M1 const> &msg1)
{
  //TODO: trade-off between duration of this method, time stamp difference between the messages, and network traffic
  struct timeval start;
  struct timeval end;
  gettimeofday(&start, NULL);
  SubscribeHelper2<M0> helper0;
  SubscribeHelper2<M1> helper1;
  SubscribeOptions ops0, ops1;
  ops0.template init<M0>(topic0, 1, boost::bind(&SubscribeHelper2<M0>::callback, &helper0, _1));
  ops1.template init<M1>(topic1, 1, boost::bind(&SubscribeHelper2<M1>::callback, &helper1, _1));

  waitForMessagesImpl(ops0, ops1, boost::bind(&SubscribeHelper2<M0>::hasMessage, &helper0), boost::bind(&SubscribeHelper2<M1>::hasMessage, &helper1), nh, timeout);

  msg0 = helper0.getMessage();
  msg1 = helper1.getMessage();
  gettimeofday(&end, NULL);
  ROS_INFO("waitForMessages finished in %f seconds", ((end.tv_sec-start.tv_sec)+(end.tv_usec-start.tv_usec)/1000000.0));
}

} // namespace topic
} // namespace ros

#endif // ROSCPP_TOPIC2_H
