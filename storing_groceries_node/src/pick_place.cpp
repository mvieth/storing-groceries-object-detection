//#include <actionlib/server/simple_action_server.h>
#include <moveit/move_group_interface/move_group_interface.h>
//#include <moveit/move_group_pick_place_capability/capability_names.h>
//#include <moveit/planning_scene_interface/planning_scene_interface.h>
#include <ros/ros.h>

//#include <eigen_conversions/eigen_msg.h>
#include <moveit_msgs/PickupAction.h>
#include <moveit_msgs/PlaceAction.h>
#include <tf2_geometry_msgs/tf2_geometry_msgs.h>
#include <trajectory_msgs/JointTrajectory.h>
#include <visualization_msgs/Marker.h>

#include "pick_place.hpp"
#include "util.hpp" // for hsv2rgb

#define VISUALIZE_GRASPS true
#ifdef SIMULATION
#define DIST_TO_OBJ 0.15
#else
#define DIST_TO_OBJ 0.22
#endif

ros::Publisher graspviz_pub; // TODO make to pointer/shared_ptr?

void init_grasp_publisher(ros::NodeHandle &nh) {
  graspviz_pub = nh.advertise<visualization_msgs::Marker>("/graspviz", 1000);
}

void deleteAllGraspMarkers() {
  visualization_msgs::Marker marker;
  marker.header.frame_id = BASE_FRAME;
  marker.header.stamp = ros::Time::now();
  marker.ns = "grasp_markers";
  marker.id = 0;
  marker.action = 3; // DELETE ALL
  graspviz_pub.publish(marker);
}

moveit_msgs::CollisionObject addRandomCollisionObject(uint8_t type) {
  srand48(time(nullptr));
  // uint8_t type=(((random()&1)==0)?1:3);//BOX=1, SPHERE=2, CYLINDER=3
  moveit_msgs::CollisionObject collision_object;
  collision_object.header.frame_id = BASE_FRAME;
  collision_object.id = "random";
  collision_object.primitives.resize(1);
  collision_object.primitives[0].type = type; // TODO other shapes
  collision_object.primitives[0].dimensions.resize(
      type == 2 ? 1 : (type == 1 ? 3 : 2));
  collision_object.primitives[0].dimensions[0] =
      0.03 * drand48() + 0.02; // 0.085;//
  if (type != 2) {
    collision_object.primitives[0].dimensions[1] =
        0.035 * drand48() + 0.01; // 0.050;//
  }
  if (type == 1) {
    collision_object.primitives[0].dimensions[2] =
        0.1 * drand48() + 0.01; // 0.015;//
  }
  collision_object.primitive_poses.resize(1);
  collision_object.primitive_poses[0].position.x =
      0.7; // position should not be random
  collision_object.primitive_poses[0].position.y = 0.0;
  collision_object.primitive_poses[0].position.z = 0.7;
  tf2::Quaternion q(2.0 * drand48() - 1.0, 2.0 * drand48() - 1.0,
                    2.0 * drand48() - 1.0, 2.0 * drand48() - 1.0);
  q.normalize();
  // tf2::Quaternion q;
  // q.setRPY(0, 0, M_PI/2.0);
  collision_object.primitive_poses[0].orientation = tf2::toMsg(q); // TODO
  collision_object.operation = collision_object.ADD;
  ROS_INFO_STREAM("adding Object: " << collision_object);
  //planning_scene_interface->applyCollisionObjects(collision_objects);
  return collision_object;
}

shape_msgs::Mesh generateDish(const double base_radius, const double upper_radius, const double height, const size_t points) {
    ROS_INFO("generateDish(base_radius=%g, upper_radius=%g, height=%g, points=%lu) called", base_radius, upper_radius, height, points);
    shape_msgs::Mesh dish;
    dish.vertices.reserve(2*points+1);
    dish.triangles.reserve(3*points);
    geometry_msgs::Point center;
    center.x=0.0;
    center.y=0.0;
    center.z=0.0;
    dish.vertices.push_back(center);
    for(size_t i=0; i<points; i++) {
        geometry_msgs::Point p1;
        p1.x=base_radius*cos(2.0*M_PI*i/points);
        p1.y=base_radius*sin(2.0*M_PI*i/points);
        p1.z=0.0;
        dish.vertices.push_back(p1);

        geometry_msgs::Point p2; // on top edge
        p2.x=upper_radius*cos(2.0*M_PI*(i+0.5)/points);
        p2.y=upper_radius*sin(2.0*M_PI*(i+0.5)/points);
        p2.z=height;
        dish.vertices.push_back(p2);

        shape_msgs::MeshTriangle tri1; // bottom
        tri1.vertex_indices[0]=0; // center
        tri1.vertex_indices[1]=1+2*i; // p1
        tri1.vertex_indices[2]=1+2*((i+1)%points);
        dish.triangles.push_back(tri1);

        shape_msgs::MeshTriangle tri2; // side, pointing up
        tri2.vertex_indices[0]=1+2*i; // p1
        tri2.vertex_indices[1]=2+2*i; // p2
        tri2.vertex_indices[2]=1+2*((i+1)%points);
        dish.triangles.push_back(tri2);

        shape_msgs::MeshTriangle tri3; // side, pointing down
        tri3.vertex_indices[0]=1+2*((i+1)%points);
        tri3.vertex_indices[1]=2+2*i; // p2
        tri3.vertex_indices[2]=2+2*((i+1)%points);
        dish.triangles.push_back(tri3);
    }
    ROS_INFO("generateDish generated a mesh with %lu triangles and %lu vertices", dish.triangles.size(), dish.vertices.size());
    return dish;
}

void openGripper(trajectory_msgs::JointTrajectory &posture, bool simulation) {
  if(simulation) {
      // Simulated robot
      posture.joint_names.resize(2);
      posture.joint_names[0] = "gripper_left_finger_joint";
      posture.joint_names[1] = "gripper_right_finger_joint";
      posture.points.resize(1);
      posture.points[0].positions.resize(2);
      posture.points[0].positions[0] = 0.044;
      posture.points[0].positions[1] = 0.044;
      posture.points[0].time_from_start = ros::Duration(0.5);
  } else {
      // Real life robot
      // Two trajectory points to make sure that the gripper is open/closed.
      // With only one point, the movement was sometimes done too late
      posture.joint_names.resize(1);
      posture.joint_names[0] = "gripper_finger_joint";
      posture.points.resize(2);
      posture.points[0].positions.resize(1);
      posture.points[0].positions[0] = 0.024;
      posture.points[0].time_from_start = ros::Duration(0.5);
      posture.points[1].positions.resize(1);
      posture.points[1].positions[0] = 0.025;
      posture.points[1].time_from_start = ros::Duration(2.0);
  }
}

void closedGripper(trajectory_msgs::JointTrajectory &posture,
                   double dist, bool simulation) {
  if(simulation) {
      // Simulated robot
      posture.joint_names.resize(2);
      posture.joint_names[0] = "gripper_left_finger_joint";
      posture.joint_names[1] = "gripper_right_finger_joint";
      posture.points.resize(1);
      posture.points[0].positions.resize(2);
      posture.points[0].positions[0] = dist / 2.0; // TODO
      posture.points[0].positions[1] = dist / 2.0;
      posture.points[0].effort.resize(2);
      posture.points[0].effort[0] = 0.1; // TODO is this even used?
      posture.points[0].effort[0] = 0.1;
      posture.points[0].time_from_start = ros::Duration(0.5);
  } else {
      // Real life robot
      // Two trajectory points to make sure that the gripper is open/closed.
      // With only one point, the movement was sometimes done too late
      posture.joint_names.resize(1);
      posture.joint_names[0] = "gripper_finger_joint";
      posture.points.resize(2);
      posture.points[0].positions.resize(1);
      posture.points[0].positions[0] = 0.002;
      posture.points[0].effort.resize(1);
      posture.points[0].effort[0] = 0.1;
      posture.points[0].time_from_start = ros::Duration(0.5);
      posture.points[1].positions.resize(1);
      posture.points[1].positions[0] = 0.0;
      posture.points[1].effort.resize(1);
      posture.points[1].effort[0] = 0.1;
      posture.points[1].time_from_start = ros::Duration(2.0);
  }
}

// generates possible Grasps for the first primitive of the CollisionObject.
// Works with cylinders, boxes, and maybe spheres (but further testing needed)
std::vector<moveit_msgs::Grasp>
generateGrasps(const moveit_msgs::CollisionObject& object) { // TODO Maybe use
  // SolidPrimitive and Pose
  // instead of CollisionObject?
  const uint8_t CYLINDER_CONST1 = 12; // How many steps around the cylinder
  std::vector<moveit_msgs::Grasp> grasps;
  if (object.primitives[0].type == object.primitives[0].CYLINDER) {
    // TODO only upright cylinders? Should work with cylinders in any
    // orientation now, but more testing is needed
    // if(object.primitives[0].dimensions[0]<=MAX_GRIPPER_OPEN)//TODO
    if (2.0 * object.primitives[0].dimensions[1] <=
        MAX_GRIPPER_OPEN) { // Is it small enough to grasp?
      for (int a = 0; a < CYLINDER_CONST1; a++) {
        for (int b = -1; b < 2;
             b++) { // TODO do this in fixed steps instead, e.g. every 2 cm
          for (int c = 0; c < 2; c++) { // gripper 180 deg rotated
            /*moveit_msgs::Grasp new_grasp;
            float x, y, z;
            new_grasp.grasp_pose.header = object.header;
                x=object.primitive_poses[0].position.x-cos(2.0*M_PI/8.0*a)*DIST_TO_OBJ;
                y=object.primitive_poses[0].position.y-sin(2.0*M_PI/8.0*a)*DIST_TO_OBJ;
                z=object.primitive_poses[0].position.z+object.primitives[0].dimensions[0]/3.0*b;

            tf2::Quaternion orientation;
                        orientation.setRPY(-M_PI/2.0, 0.0, 2.0*M_PI/8.0*a);
                        new_grasp.grasp_pose.pose.orientation =
            tf2::toMsg(orientation);
            new_grasp.grasp_pose.pose.position.x = x;
            new_grasp.grasp_pose.pose.position.y = y;
            new_grasp.grasp_pose.pose.position.z = z;

            new_grasp.pre_grasp_approach.direction.header = object.header;
            new_grasp.pre_grasp_approach.direction.vector.x =
            cos(2.0*M_PI/8.0*a);
            new_grasp.pre_grasp_approach.direction.vector.y =
            sin(2.0*M_PI/8.0*a);
            new_grasp.pre_grasp_approach.min_distance = 0.1;
            new_grasp.pre_grasp_approach.desired_distance = 0.12;

            new_grasp.post_grasp_retreat.direction.header = object.header;
            new_grasp.post_grasp_retreat.direction.vector.z = 1.0;
            new_grasp.post_grasp_retreat.min_distance = 0.1;
            new_grasp.post_grasp_retreat.desired_distance = 0.25;

            openGripper(new_grasp.pre_grasp_posture);
            closedGripper(new_grasp.grasp_posture,
            2.0*object.primitives[0].dimensions[1]);
            new_grasp.allowed_touch_objects.resize(1);
            new_grasp.allowed_touch_objects[0]=object.id;
            //TODO specify max_force?
            grasps.push_back(new_grasp);*/

            moveit_msgs::Grasp new_grasp;
            // new_grasp.id = ; //TODO
            float x, y, z;
            new_grasp.grasp_pose.header = object.header;
            tf2::Quaternion pose, grasp, total;
            tf2::fromMsg(object.primitive_poses[0].orientation, pose);
            grasp.setRPY((c == 0 ? -M_PI / 2.0 : M_PI / 2.0), 0.0,
                         2.0 * M_PI / CYLINDER_CONST1 * a);
            // ROS_INFO(grasp.getAxis().x() << grasp.getAxis().y() <<
            // grasp.getAxis().z() << grasp.getW());
            total = pose * grasp; // TODO switch order?
            // ROS_INFO(total.getAxis().x() << total.getAxis().y() <<
            // total.getAxis().z() << total.getW());
            total.normalize();
            tf2::Vector3 vec(
                -(DIST_TO_OBJ + object.primitives[0].dimensions[1]),
                object.primitives[0].dimensions[0] / 3.0 * b, 0.0),
                vec2(1.0, 0.0, 0.0);
            tf2::Transform trans(total);
            vec = trans * vec;
            vec2 = trans * vec2;
            x = object.primitive_poses[0].position.x + vec.x();
            y = object.primitive_poses[0].position.y + vec.y();
            z = object.primitive_poses[0].position.z + vec.z();
            // ROS_INFO("new grasp: x=%f, y=%f, z=%f, x=%f, y=%f, z=%f\n", x, y,
            // z, vec.x(), vec.y(), vec.z());
            new_grasp.grasp_pose.pose.orientation = tf2::toMsg(total);
            new_grasp.grasp_pose.pose.position.x = x;
            new_grasp.grasp_pose.pose.position.y = y;
            new_grasp.grasp_pose.pose.position.z = z;

            new_grasp.grasp_quality =
                1.0 / sqrt(x * x + y * y) + 0.1; // The grasps on the side of
                                                 // the cylinder that is facing
                                                 // the robot are far more
                                                 // likely to succeed TODO

            new_grasp.pre_grasp_approach.direction.header = object.header;
            new_grasp.pre_grasp_approach.direction.vector.x = vec2.x();
            new_grasp.pre_grasp_approach.direction.vector.y = vec2.y();
            new_grasp.pre_grasp_approach.direction.vector.z = vec2.z();
            new_grasp.pre_grasp_approach.min_distance = 0.12;
            new_grasp.pre_grasp_approach.desired_distance = 0.15;

            new_grasp.post_grasp_retreat.direction.header = object.header;
            new_grasp.post_grasp_retreat.direction.vector.z = 1.0;
            new_grasp.post_grasp_retreat.min_distance = 0.1;
            new_grasp.post_grasp_retreat.desired_distance = 0.25;

#ifdef SIMULATION
            openGripper(new_grasp.pre_grasp_posture, true);
            closedGripper(new_grasp.grasp_posture,
                          2.0 * object.primitives[0].dimensions[1] -
                              0.011, true); // Close the gipper enough, so the object
                                      // cannot escape, but not too much, or
                                      // else the controller fails
#else
            openGripper(new_grasp.pre_grasp_posture, false);
            closedGripper(new_grasp.grasp_posture,
                          2.0 * object.primitives[0].dimensions[1], false);
#endif
            // new_grasp.post_place_retreat = ; //TODO
            // new_grasp.max_contact_force = ; //TODO
            // new_grasp.allowed_touch_objects.resize(1);
            // new_grasp.allowed_touch_objects[0]=object.id;
            grasps.push_back(new_grasp);
          }
        }
      }
    } else {
      ROS_ERROR("generateGrasps: Cylinder too big");
    }
  } else if (object.primitives[0].type == object.primitives[0].BOX) {
    for (int a = 0; a < 3; a++) { // Three dimensions
      if (object.primitives[0].dimensions[a] <= MAX_GRIPPER_OPEN) {
        for (int c = 0; c < 2; c++) { // Each side can have two grasp poses (90
                                      // degrees rotated) //TODO or four?
          for (int b = -1; b <= 1;
               b += 2) { // Two sides in each dimension: front/back, left/right,
                         // top/bottom
            for (int d = -1; d < 2; d++) {
              moveit_msgs::Grasp new_grasp;
              new_grasp.grasp_pose.header = object.header;
              tf2::Quaternion pose, grasp, total;
              tf2::fromMsg(object.primitive_poses[0].orientation, pose);
              float alpha = 0.0, beta = 0.0;
              if (a == 0) {
                grasp.setEuler(M_PI / 2.0, 0.0,
                               (c == 0 && b == 1 ? M_PI : 0.0) +
                                   c * b * M_PI / 2.0);
                alpha = object.primitives[0].dimensions[c == 0 ? 2 : 1] / 2.0;
                beta = object.primitives[0].dimensions[c == 0 ? 1 : 2];
              } else if (a == 1) {
                grasp.setEuler((c == 0 && b == 1 ? M_PI : 0.0) +
                                   c * b * M_PI / 2.0,
                               M_PI / 2.0, 0.0);
                alpha = object.primitives[0].dimensions[c == 0 ? 0 : 2] / 2.0;
                beta = object.primitives[0].dimensions[c == 0 ? 2 : 0];
              } else if (a == 2) {
                grasp.setEuler(0.0, 0.0, (c == 0 && b == 1 ? M_PI : 0.0) +
                                             c * b * M_PI / 2.0);
                alpha = object.primitives[0].dimensions[c == 0 ? 0 : 1] / 2.0;
                beta = object.primitives[0].dimensions[c == 0 ? 1 : 0];
              }
              tf2::Vector3 vec(-(DIST_TO_OBJ + alpha), beta / 3.0 * d, 0.0),
                  vec2(1.0, 0.0, 0.0);
              // grasp.setEuler(0.0, ((a==1)?b*M_PI/2.0:0.0),
              // ((a==0)?b*M_PI/2.0:(a==2&&b==-1?M_PI:0.0)));
              // tf2::Vector3 rotvec(1.0, (a==1?1.0:0.0), (a==0||a==2?1.0:0.0));
              // grasp.setRotation(rotvec, a==2?(b==1?M_PI:0.0):b*M_PI*3.0/4.0);
              total = pose * grasp; // TODO switch order?
              total.normalize();
              tf2::Transform trans(total);
              vec = trans * vec;
              vec2 = trans * vec2;
              new_grasp.grasp_pose.pose.orientation = tf2::toMsg(total);
              new_grasp.grasp_pose.pose.position.x =
                  object.primitive_poses[0].position.x + vec.x();
              new_grasp.grasp_pose.pose.position.y =
                  object.primitive_poses[0].position.y + vec.y();
              new_grasp.grasp_pose.pose.position.z =
                  object.primitive_poses[0].position.z + vec.z();

              new_grasp.grasp_quality =
                  (d == 0 ? 1.0 : 0.5) /
                  object.primitives[0].dimensions[a]; // Favour the grasps in
                                                      // the middle, favour
                                                      // grasps on the smaller
                                                      // sides TODO

              new_grasp.pre_grasp_approach.direction.header = object.header;
              new_grasp.pre_grasp_approach.direction.vector.x = vec2.x();
              new_grasp.pre_grasp_approach.direction.vector.y = vec2.y();
              new_grasp.pre_grasp_approach.direction.vector.z = vec2.z();
              new_grasp.pre_grasp_approach.min_distance = 0.1;
              new_grasp.pre_grasp_approach.desired_distance = 0.12;

              new_grasp.post_grasp_retreat.direction.header = object.header;
              new_grasp.post_grasp_retreat.direction.vector.z = 1.0;
              new_grasp.post_grasp_retreat.min_distance = 0.1;
              new_grasp.post_grasp_retreat.desired_distance = 0.25;

#ifdef SIMULATION
              bool simulation=true;
#else
              bool simulation=false;
#endif
              openGripper(new_grasp.pre_grasp_posture, simulation);
              closedGripper(new_grasp.grasp_posture,
                            object.primitives[0].dimensions[a], simulation);
              // new_grasp.allowed_touch_objects.resize(1);
              // new_grasp.allowed_touch_objects[0]=object.id;
              // TODO id? max_force? post_place_retreat?
              grasps.push_back(new_grasp);
            }
          }
        }
      } else {
        ROS_WARN("generateGrasps: Side %i of box too big\n", a);
      }
    }
  } else if (object.primitives[0].type == object.primitives[0].SPHERE) { // TODO sphere
    if (2.0 * object.primitives[0].dimensions[0] <= MAX_GRIPPER_OPEN) {
      // regular dodecahedron (20 vertices)
      const float phi = 1.618; // golden ratio
      float vertices[20][3] = {
          {1, 1, 1},           {-1, 1, 1},         {1, -1, 1},
          {-1, -1, 1},         {1, 1, -1},         {-1, 1, -1},
          {1, -1, -1},         {-1, -1, -1},       {0, phi, 1 / phi},
          {0, -phi, 1 / phi},  {0, phi, -1 / phi}, {0, -phi, -1 / phi},
          {1 / phi, 0, phi},   {-1 / phi, 0, phi}, {1 / phi, 0, -phi},
          {-1 / phi, 0, -phi}, {phi, 1 / phi, 0},  {-phi, 1 / phi, 0},
          {phi, -1 / phi, 0},  {-phi, -1 / phi, 0}};
      for (int a = 0; a < 20; a++) {
        moveit_msgs::Grasp new_grasp;
        // new_grasp.id = ; //TODO
        float x, y, z;
        new_grasp.grasp_pose.header = object.header;
        Eigen::Quaternionf total;
        // ROS_INFO(total.getAxis().x() << total.getAxis().y() << total.getAxis().z() << total.getW());
        //total.normalize(); // TODO is this necessary?
        Eigen::Vector3f vec(vertices[a][0], vertices[a][1], vertices[a][2]);
        vec *= -(DIST_TO_OBJ + object.primitives[0].dimensions[0]) / sqrt(3.0);
        Eigen::Vector3f vec2 = -vec;
        total.setFromTwoVectors(Eigen::Vector3f(-1.0, 0.0, 0.0), vec);
        x = object.primitive_poses[0].position.x + vec.x();
        y = object.primitive_poses[0].position.y + vec.y();
        z = object.primitive_poses[0].position.z + vec.z();
        // ROS_INFO("new grasp: x=%f, y=%f, z=%f, x=%f, y=%f, z=%f\n", x, y, z,
        // vec.x(), vec.y(), vec.z());
        new_grasp.grasp_pose.pose.orientation.x = total.x();
        new_grasp.grasp_pose.pose.orientation.y = total.y();
        new_grasp.grasp_pose.pose.orientation.z = total.z();
        new_grasp.grasp_pose.pose.orientation.w = total.w();
        new_grasp.grasp_pose.pose.position.x = x;
        new_grasp.grasp_pose.pose.position.y = y;
        new_grasp.grasp_pose.pose.position.z = z;

        new_grasp.grasp_quality = 1.0 / sqrt(x * x + y * y) +
                                  0.1; // The grasps on the side of the sphere
                                       // that is facing the robot are far more
                                       // likely to succeed TODO

        new_grasp.pre_grasp_approach.direction.header = object.header;
        new_grasp.pre_grasp_approach.direction.vector.x = vec2.x();
        new_grasp.pre_grasp_approach.direction.vector.y = vec2.y();
        new_grasp.pre_grasp_approach.direction.vector.z = vec2.z();
        new_grasp.pre_grasp_approach.min_distance = 0.12;
        new_grasp.pre_grasp_approach.desired_distance = 0.15;

        new_grasp.post_grasp_retreat.direction.header = object.header;
        new_grasp.post_grasp_retreat.direction.vector.z = 1.0;
        new_grasp.post_grasp_retreat.min_distance = 0.1;
        new_grasp.post_grasp_retreat.desired_distance = 0.25;

#ifdef SIMULATION
        bool simulation=true;
#else
        bool simulation=false;
#endif
        openGripper(new_grasp.pre_grasp_posture, simulation);
        closedGripper(new_grasp.grasp_posture,
                      2.0 * object.primitives[0].dimensions[0], simulation);
        // new_grasp.post_place_retreat = ; //TODO
        // new_grasp.max_contact_force = ; //TODO
        // new_grasp.allowed_touch_objects.resize(1);
        // new_grasp.allowed_touch_objects[0]=object.id;

        grasps.push_back(new_grasp);
      }
    } else {
      ROS_ERROR("generateGrasps: Sphere too big");
    }
  } else {
    ROS_ERROR("generateGrasps cannot handle object type");
  }
  ROS_INFO("There are %lu generated grasps", grasps.size());
  if (VISUALIZE_GRASPS) {
    deleteAllGraspMarkers();
    for (size_t i = 0; i < grasps.size(); i++) {
      moveit_msgs::Grasp grasp = grasps[i];
      ROS_INFO(
          "new grasp: x=%f, y=%f, z=%f", grasp.grasp_pose.pose.position.x,
          grasp.grasp_pose.pose.position.y, grasp.grasp_pose.pose.position.z);
      visualization_msgs::Marker marker;
      marker.header = grasp.grasp_pose.header;
      marker.ns = "grasp_markers";
      marker.id = i + 65;
      marker.type = visualization_msgs::Marker::ARROW;
      marker.action = visualization_msgs::Marker::ADD;
      marker.pose.position.x = grasp.grasp_pose.pose.position.x;
      marker.pose.position.y = grasp.grasp_pose.pose.position.y;
      marker.pose.position.z = grasp.grasp_pose.pose.position.z;
      marker.pose.orientation.x = grasp.grasp_pose.pose.orientation.x;
      marker.pose.orientation.y = grasp.grasp_pose.pose.orientation.y;
      marker.pose.orientation.z = grasp.grasp_pose.pose.orientation.z;
      marker.pose.orientation.w = grasp.grasp_pose.pose.orientation.w;
      // marker.points.resize(6);
      // marker.points[1].x=0.1;
      // marker.points[3].y=0.1;
      // marker.points[5].z=0.1;
      marker.scale.x = 0.1;
      marker.scale.y = 0.01;
      marker.scale.z = 0.01;
      // Red-Green-Blue-Red
      hsv2rgb(i * 360.0 / grasps.size(), 1.0, 1.0, marker.color.r,
              marker.color.g, marker.color.b);
      // marker.color.r = 0.0f;//Green
      // marker.color.g = 1.0f;
      // marker.color.b = 0.0f;
      marker.color.a = 1.0f;
      marker.lifetime = ros::Duration();
      graspviz_pub.publish(marker);
    }
  }
  ROS_INFO("generateGrasps returns grasps");
  return grasps;
}

// Inverse of transformToEndEffectorGoal from
// https://github.com/ros-planning/moveit/blob/master/moveit_ros/manipulation/pick_place/src/place.cpp
/*bool transformFromEndEffectorGoal(const geometry_msgs::PoseStamped& goal_pose,
                                const robot_state::AttachedBody* attached_body,
geometry_msgs::PoseStamped& place_pose)
{
  const EigenSTL::vector_Affine3d& fixed_transforms =
attached_body->getFixedTransforms();
  if (fixed_transforms.empty())
    return false;

  Eigen::Affine3d end_effector_transform;
  tf2::fromMsg(goal_pose.pose, end_effector_transform);
  end_effector_transform = end_effector_transform * fixed_transforms[0];
  place_pose.header = goal_pose.header;
  place_pose.pose = tf2::toMsg(end_effector_transform);
  return true;
}*/

// generates possible PlaceLocations on a horizontal rectangle (sizex by sizey,
// yaw angle is angle) which has its center at (x,y,z)
std::vector<moveit_msgs::PlaceLocation>
generatePlaces(const moveit_msgs::CollisionObject& object, float x, float y, float z,
               float sizex, float sizey,
               float angle, bool simulation) { // TODO is object not needed?
  const double approach_angle = M_PI/4.0; // For the pre_place_approach: 0 means parallel to ground, M_PI/2.0 means straight down
  const double obj_sur_dist = 0.01; // Distance between object and surface in place position
  ROS_INFO("generatePlace called with parameters x=%g, y=%g, z=%g, sizex=%g, sizey=%g, angle=%g", x, y, z, sizex, sizey, angle);
  if(std::isnan(angle) || std::isnan(x) || std::isnan(y) || std::isnan(z)) {
    ROS_ERROR("A parameter is nan");
    return std::vector<moveit_msgs::PlaceLocation>(); // TODO other behaviour? set the to zero/other value?
  }
  if (object.primitives.empty()) {
    ROS_WARN("generatePlace(): parameter object has no primitives");
    return std::vector<moveit_msgs::PlaceLocation>(); // TODO some other behaviour?
  }
  std::multimap<double, moveit_msgs::PlaceLocation, std::greater<double>> places;
  // 5 by 5 in a rectangle
  for (int i = -2; i < 3; i++) { // i = -2, -1, 0, 1, 2
    for (int j = -2; j < 3; j++) { // j = -2, -1, 0, 1, 2
      //ROS_INFO("i=%i, j=%i", i, j);
      // TODO different yaw rotations
      moveit_msgs::PlaceLocation new_place;

      // TODO set id?
      // Set post_place_posture
      openGripper(new_place.post_place_posture, simulation);
      // Set place_pose
      // TODO right side up?
      new_place.place_pose.header.frame_id = BASE_FRAME;
      new_place.place_pose.pose.position.x =
          x + cos(angle) * i * sizex / 5.0 - sin(angle) * j * sizey / 5.0;// - cos(angle_to_robot) * DIST_TO_OBJ; // EEF pose: include the last term
      new_place.place_pose.pose.position.y =
          y + sin(angle) * i * sizex / 5.0 + cos(angle) * j * sizey / 5.0;// - sin(angle_to_robot) * DIST_TO_OBJ; // EEF pose: include the last term
      // height depending on object? and grasp on object?
      //new_place.place_pose.pose.position.z = z;
      // transformFromEndEffectorGoal(new_place.place_pose.pose,
      // planning_scene_interface->getCurrentState().getAttachedBody(object.id),
      // new_place.place_pose.pose);
      ROS_INFO("position=(%g,%g,%g)", new_place.place_pose.pose.position.x,
               new_place.place_pose.pose.position.y, new_place.place_pose.pose.position.z);
      // Set pre_place_approach
      const double angle_to_robot =
          atan2(new_place.place_pose.pose.position.y - 0.0, new_place.place_pose.pose.position.x - 0.0); // TODO relative to gripper origin // TODO actual position, not the center of the rectangle?
      new_place.pre_place_approach.direction.header.frame_id =
          BASE_FRAME; // TODO use gripper_grasping_frame instead?
      new_place.pre_place_approach.direction.vector.x =
          1.0 * cos(angle_to_robot) * cos(approach_angle);
      new_place.pre_place_approach.direction.vector.y =
          1.0 * sin(angle_to_robot) * cos(approach_angle);
      new_place.pre_place_approach.direction.vector.z =
          -1.0 * sin(approach_angle);
      new_place.pre_place_approach.desired_distance = 0.15;
      new_place.pre_place_approach.min_distance = 0.1;
      // Set post_place_retreat
      new_place.post_place_retreat.direction.header.frame_id = "arm_tool_link";// Didn't really work with gripper_grasping_frame
      new_place.post_place_retreat.direction.vector.x = -1.0;
      new_place.post_place_retreat.direction.vector.y = 0.0;
      new_place.post_place_retreat.direction.vector.z = 0.0;
      new_place.post_place_retreat.desired_distance = 0.25;
      new_place.post_place_retreat.min_distance = 0.1;
      // TODO set allowed_touch_objects?

      double quality = 1.0/(std::sqrt(std::pow(new_place.place_pose.pose.position.x-x, 2)+std::pow(new_place.place_pose.pose.position.y-y, 2))+0.1); // Closer to the middle is better
      quality *= 1.0/(std::sqrt(std::pow(new_place.place_pose.pose.position.x, 2)+std::pow(new_place.place_pose.pose.position.y, 2))+0.1); // Closer to the robot is more likely to succeed
      if(object.primitives[0].type == object.primitives[0].CYLINDER) {
        const uint8_t YAW_ROT = 4;
        new_place.place_pose.pose.position.z = z+obj_sur_dist+object.primitives[0].dimensions[0]/2.0; // cylinder height
        for(uint8_t l=0; l<2; l++) { // On top or on bottom
          for(uint8_t k=0; k<YAW_ROT; k++) { // yaw rotations
            tf2::Quaternion orientation;
            orientation.setRPY(l * M_PI, 0.0, (2.0 * M_PI * rand()) / RAND_MAX + k * 2.0 * M_PI / YAW_ROT);
            new_place.place_pose.pose.orientation = tf2::toMsg(orientation);
            quality *= (l==0?1.0:0.5); // prefer right side up (standing on bottom)
            places.insert({quality, new_place});
          }
        }
      } else if(object.primitives[0].type == object.primitives[0].SPHERE) {
        new_place.place_pose.pose.position.z = z+obj_sur_dist+object.primitives[0].dimensions[0]; // radius
        for(uint8_t l=0; l<5; l++) { // Just a few random rotations
          randomQuaternion(new_place.place_pose.pose.orientation.x, new_place.place_pose.pose.orientation.y,
                           new_place.place_pose.pose.orientation.z, new_place.place_pose.pose.orientation.w);
          places.insert({quality, new_place});
        }
      } else if(object.primitives[0].type == object.primitives[0].BOX) {
        for(uint8_t m=0; m<3; m++) { // yaw rotations
          for(uint8_t l=0; l<3; l++) { // Which dimension
            for(uint8_t k=0; k<2; k++) { // Two sides in each dimension: front/back, left/right, top/bottom
                new_place.place_pose.pose.position.z = z+obj_sur_dist+object.primitives[0].dimensions[l]/2.0;//TODO for box, all three sides must be considered
                tf2::Quaternion orientation;
                orientation.setRPY((l==1?M_PI/2.0+(k==1?M_PI:0.0):(l==2&&k==1?M_PI:0.0)), (l==0?M_PI/2.0+(k==1?M_PI:0.0):0.0), (2.0 * M_PI * rand()) / RAND_MAX + 2.0 * M_PI / 3.0 * m);
                new_place.place_pose.pose.orientation = tf2::toMsg(orientation);
                //quality *= 1.0/(object.primitives[0].dimensions[l]+0.1); // TODO prefer places with a large support side?
                places.insert({quality, new_place});
            }
          }
        }
      } else {
        ROS_ERROR("bad primitive: %u", object.primitives[0].type);
      }
    }
  }
  std::vector<moveit_msgs::PlaceLocation> places_vec;
  places_vec.reserve(places.size());
  for(auto item : places) {
    ROS_DEBUG("Quality=%g", item.first);
    places_vec.push_back(item.second);
  }
  if (VISUALIZE_GRASPS) {
    deleteAllGraspMarkers();
    for (size_t i = 0; i < places_vec.size(); i++) {
      moveit_msgs::PlaceLocation place = places_vec[i];
      ROS_DEBUG("new place: x=%g, y=%g, z=%g, (x=%g, y=%g, z=%g, w=%g)", place.place_pose.pose.position.x,
                place.place_pose.pose.position.y, place.place_pose.pose.position.z, place.place_pose.pose.orientation.x, place.place_pose.pose.orientation.y, place.place_pose.pose.orientation.z, place.place_pose.pose.orientation.w);
      visualization_msgs::Marker marker;
      marker.header = place.place_pose.header;
      marker.ns = "grasp_markers";
      marker.id = i + 65;
      marker.action = visualization_msgs::Marker::ADD;
      marker.pose.position.x = place.place_pose.pose.position.x;
      marker.pose.position.y = place.place_pose.pose.position.y;
      marker.pose.position.z = place.place_pose.pose.position.z;
      marker.pose.orientation.x = place.place_pose.pose.orientation.x;
      marker.pose.orientation.y = place.place_pose.pose.orientation.y;
      marker.pose.orientation.z = place.place_pose.pose.orientation.z;
      marker.pose.orientation.w = place.place_pose.pose.orientation.w;
      if (false) {
          marker.type = visualization_msgs::Marker::ARROW; // TODO instead of arrows, maybe compute the effective pose of the CollisionObject and publish that?
          marker.scale.x = 0.1;
          marker.scale.y = 0.01;
          marker.scale.z = 0.01;
      } else {
          switch(object.primitives[0].type) {
            case 1: // box
                marker.type = visualization_msgs::Marker::CUBE;
                marker.scale.x = object.primitives[0].dimensions[0];
                marker.scale.y = object.primitives[0].dimensions[1];
                marker.scale.z = object.primitives[0].dimensions[2];
                break;
            case 2: // sphere
                marker.type = visualization_msgs::Marker::SPHERE;
                marker.scale.x = 2.0*object.primitives[0].dimensions[0];
                marker.scale.y = 2.0*object.primitives[0].dimensions[0];
                marker.scale.z = 2.0*object.primitives[0].dimensions[0];
                break;
            case 3: // cylinder
                marker.type = visualization_msgs::Marker::CYLINDER;
                marker.scale.x = 2.0*object.primitives[0].dimensions[1];
                marker.scale.y = 2.0*object.primitives[0].dimensions[1];
                marker.scale.z = object.primitives[0].dimensions[0];
                break;
            case 4: // cone
                marker.type = visualization_msgs::Marker::ARROW;
                marker.scale.x = 2.0*object.primitives[0].dimensions[1];
                marker.scale.y = 2.0*object.primitives[0].dimensions[1];
                marker.scale.z = object.primitives[0].dimensions[0];
                break;
            default:
                ROS_WARN("Unknown shape type");
                marker.type = visualization_msgs::Marker::CUBE;
          }
      }
      // Hue is Red-Green-Blue-Red
      hsv2rgb(120.0 * (1.0 - 1.0 * i / places_vec.size()), 1.0, 1.0, marker.color.r,
              marker.color.g, marker.color.b); // TODO places with same qualities should have same colours
      // marker.color.r = 0.0f;//Green
      // marker.color.g = 1.0f;
      // marker.color.b = 0.0f;
      marker.color.a = 1.0f;
      marker.lifetime = ros::Duration();
      graspviz_pub.publish(marker);
    }
  }
  return places_vec;
}

// Old, pick object at (x, y, z)
/*void pick(const std::string name, const float x, const float y, const float z)
{
  std::vector<moveit_msgs::Grasp> grasps;
  grasps.resize(1);
  grasps[0].grasp_pose.header.frame_id = BASE_FRAME;
  tf2::Quaternion orientation;
  orientation.setRPY(-M_PI / 2, 0.0, 0.0);
  grasps[0].grasp_pose.pose.orientation = tf2::toMsg(orientation);
  grasps[0].grasp_pose.pose.position.x = x;//-0.3; //0.4;
  grasps[0].grasp_pose.pose.position.y = y; //0;
  grasps[0].grasp_pose.pose.position.z = z; //0.5;
  grasps[0].pre_grasp_approach.direction.header.frame_id = BASE_FRAME;
  grasps[0].pre_grasp_approach.direction.vector.x = 1.0;
  grasps[0].pre_grasp_approach.min_distance = 0.095;
  grasps[0].pre_grasp_approach.desired_distance = 0.115;
  grasps[0].post_grasp_retreat.direction.header.frame_id = BASE_FRAME;
  grasps[0].post_grasp_retreat.direction.vector.z = 1.0;
  grasps[0].post_grasp_retreat.min_distance = 0.1;
  grasps[0].post_grasp_retreat.desired_distance = 0.25;
  openGripper(grasps[0].pre_grasp_posture);
  closedGripper(grasps[0].grasp_posture, 0.02);
  ROS_INFO("pick now");
  group->pick(name, grasps);
}*/

// Old, place object with name "name" at static location
/*void place(std::string name) {
    std::vector<moveit_msgs::PlaceLocation> place_location;
    place_location.resize(1);
    place_location[0].place_pose.header.frame_id = BASE_FRAME;
    tf2::Quaternion orientation;
    orientation.setRPY(0, 0, M_PI / 2);
    place_location[0].place_pose.pose.orientation = tf2::toMsg(orientation);
    place_location[0].place_pose.pose.position.x = 0.6;
    place_location[0].place_pose.pose.position.y = 0.1;
    place_location[0].place_pose.pose.position.z = 0.5;
    place_location[0].pre_place_approach.direction.header.frame_id = BASE_FRAME;
    place_location[0].pre_place_approach.direction.vector.z = -1.0;
    place_location[0].pre_place_approach.min_distance = 0.095;
    place_location[0].pre_place_approach.desired_distance = 0.115;
    place_location[0].post_place_retreat.direction.header.frame_id = BASE_FRAME;
    place_location[0].post_place_retreat.direction.vector.y = -1.0;
    place_location[0].post_place_retreat.min_distance = 0.1;
    place_location[0].post_place_retreat.desired_distance = 0.25;
    openGripper(place_location[0].post_place_posture);
    //group->setSupportSurfaceName("table2");
    group->place(name, place_location);
}*/

template <typename T>
void waitForAction(ros::NodeHandle &nh, const T &action,
                   const std::string &name, const ros::WallTime &timeout,
                   double allotted_time) {
  ROS_DEBUG_NAMED("move_group_interface",
                  "Waiting for move_group action server (%s)...", name.c_str());

  // wait for the server (and spin as needed)
  if (timeout == ros::WallTime()) // wait forever
  {
    while (nh.ok() && !action->isServerConnected()) {
      ros::WallDuration(0.001).sleep();
      // explicit ros::spinOnce on the callback queue used by NodeHandle that
      // manages the action client
      ros::CallbackQueue *queue =
          dynamic_cast<ros::CallbackQueue *>(nh.getCallbackQueue());
      if (queue) {
        queue->callAvailable();
      } else // in case of nodelets and specific callback queue implementations
      {
        ROS_WARN_ONCE_NAMED(
            "move_group_interface",
            "Non-default CallbackQueue: Waiting for external queue "
            "handling.");
      }
    }
  } else // wait with timeout
  {
    while (nh.ok() && !action->isServerConnected() &&
           timeout > ros::WallTime::now()) {
      ros::WallDuration(0.001).sleep();
      // explicit ros::spinOnce on the callback queue used by NodeHandle that
      // manages the action client
      ros::CallbackQueue *queue =
          dynamic_cast<ros::CallbackQueue *>(nh.getCallbackQueue());
      if (queue) {
        queue->callAvailable();
      } else // in case of nodelets and specific callback queue implementations
      {
        ROS_WARN_ONCE_NAMED(
            "move_group_interface",
            "Non-default CallbackQueue: Waiting for external queue "
            "handling.");
      }
    }
  }

  if (!action->isServerConnected()) {
    std::stringstream error;
    error << "Unable to connect to move_group action server '" << name
          << "' within allotted time (" << allotted_time << "s)";
    //throw std::runtime_error(error.str());
    ROS_ERROR_STREAM(error.str());
  } else {
    ROS_DEBUG_NAMED("move_group_interface", "Connected to '%s'", name.c_str());
  }
}

std::unique_ptr<actionlib::SimpleActionClient<moveit_msgs::PickupAction>>
    pick_action_client_;
std::unique_ptr<actionlib::SimpleActionClient<moveit_msgs::PlaceAction>>
    place_action_client_;

void initActionClients(ros::NodeHandle &nh, const ros::WallDuration &wait_for_servers) {
  ROS_INFO("initActionClients called. Waiting for servers for %g seconds.", wait_for_servers.toSec());
  // Initialize PlaceAction-client, from moveit source code
  ros::WallTime timeout_for_servers = ros::WallTime::now() + wait_for_servers;
  if (wait_for_servers == ros::WallDuration())
    timeout_for_servers = ros::WallTime(); // wait for ever
  double allotted_time = wait_for_servers.toSec();
  pick_action_client_.reset(
      new actionlib::SimpleActionClient<moveit_msgs::PickupAction>(
          nh, "/pickup", false));
  waitForAction(nh, pick_action_client_, "/pickup", timeout_for_servers,
                allotted_time);
  if (!pick_action_client_->isServerConnected()) {
    ROS_ERROR_STREAM_NAMED("move_group_interface",
                           "Pick action server not connected");
  }
  place_action_client_.reset(
      new actionlib::SimpleActionClient<moveit_msgs::PlaceAction>(nh, "/place",
                                                                  false));
  waitForAction(nh, place_action_client_, "/place", timeout_for_servers,
                allotted_time);
  if (!place_action_client_->isServerConnected()) {
    ROS_ERROR_STREAM_NAMED("move_group_interface",
                           "Place action server not connected");
  }
}

void my_constructGoal(moveit::planning_interface::MoveGroupInterface *group,
                      moveit_msgs::PlaceGoal &goal_out,
                      const std::string &object,
                      const std::string &support_surface_,
                      const bool allow_gripper_support_collision) {
  moveit_msgs::PlaceGoal goal;
  goal.attached_object_name = object;
  goal.group_name = group->getName();
  goal.allowed_planning_time = 10.0;
  goal.support_surface_name = support_surface_;
  goal.planner_id = group->getPlannerId();
  if (!support_surface_.empty())
      goal.allow_gripper_support_collision = allow_gripper_support_collision;
  // if (path_constraints_)//TODO
  // goal.path_constraints = *path_constraints_;
  goal_out = goal;
}

void my_constructGoal(moveit::planning_interface::MoveGroupInterface *group,
                      moveit_msgs::PickupGoal &goal_out,
                      const std::string &object,
                      const std::string &support_surface_) {
  moveit_msgs::PickupGoal goal;
  goal.target_name = object;
  goal.group_name = group->getName();
  goal.end_effector = group->getEndEffector();
  goal.allowed_planning_time = 10.0;
  goal.support_surface_name = support_surface_;
  goal.planner_id = group->getPlannerId();
  // if (!support_surface_.empty())
  //    goal.allow_gripper_support_collision = true;
  // if (path_constraints_)
  //    goal.path_constraints = *path_constraints_;

  goal_out = goal;
}

moveit::planning_interface::MoveItErrorCode
my_pick(moveit::planning_interface::MoveGroupInterface *group,
        const std::string &object,
        const std::vector<moveit_msgs::Grasp> &grasps,
        const std::string& support_surface_, bool plan_only = false) {
  if (!pick_action_client_) {
    ROS_ERROR_STREAM_NAMED("move_group_interface",
                           "Pick action client not found");
    return moveit::planning_interface::MoveItErrorCode(
        moveit_msgs::MoveItErrorCodes::FAILURE);
  }
  if (!pick_action_client_->isServerConnected()) {
    ROS_ERROR_STREAM_NAMED("move_group_interface",
                           "Pick action server not connected");
    return moveit::planning_interface::MoveItErrorCode(
        moveit_msgs::MoveItErrorCodes::FAILURE);
  }
  moveit_msgs::PickupGoal goal;
  my_constructGoal(group, goal, object, support_surface_);
  goal.possible_grasps = grasps;
  goal.planning_options.plan_only = plan_only;
  // goal.planning_options.look_around = can_look_;//TODO
  // goal.planning_options.replan = can_replan_;
  // goal.planning_options.replan_delay = replan_delay_;
  goal.planning_options.planning_scene_diff.is_diff = true;
  goal.planning_options.planning_scene_diff.robot_state.is_diff = true;

  pick_action_client_->sendGoal(goal);
  ROS_INFO("my_pick: waiting for result ...");
  if (!pick_action_client_->waitForResult(ros::Duration(30.0))) { // Wait for at most 30s
    ROS_INFO_STREAM_NAMED("move_group_interface",
                          "Pickup action returned early");
  }
  if (pick_action_client_->getState() ==
      actionlib::SimpleClientGoalState::SUCCEEDED) {
    return moveit::planning_interface::MoveItErrorCode(
        pick_action_client_->getResult()->error_code);
  } else {
    ROS_WARN_STREAM_NAMED(
        "move_group_interface",
        "Fail: " << pick_action_client_->getState().toString() << ": "
                 << pick_action_client_->getState().getText());
    return moveit::planning_interface::MoveItErrorCode(
        pick_action_client_->getResult()->error_code);
  }
}

// Own implementation of place, only difference is that the bool "place_eef" in
// the PlaceGoal is set to true
moveit::planning_interface::MoveItErrorCode
my_place(moveit::planning_interface::MoveGroupInterface *group,
         const std::string &object,
         const std::vector<moveit_msgs::PlaceLocation> &locations,
         const std::string& support_surface_, const bool allow_gripper_support_collision, bool place_eef, bool plan_only) {
  if (!place_action_client_) {
    ROS_ERROR_STREAM_NAMED("move_group_interface",
                           "Place action client not found");
    return moveit::planning_interface::MoveItErrorCode(
        moveit_msgs::MoveItErrorCodes::FAILURE);
  }
  if (!place_action_client_->isServerConnected()) {
    ROS_ERROR_STREAM_NAMED("move_group_interface",
                           "Place action server not connected");
    return moveit::planning_interface::MoveItErrorCode(
        moveit_msgs::MoveItErrorCodes::FAILURE);
  }
  moveit_msgs::PlaceGoal goal;
  my_constructGoal(group, goal, object, support_surface_, allow_gripper_support_collision);
  goal.place_locations = locations;
  goal.place_eef = place_eef; // This is different from the usual
                         // place()-implementation true->eef, false->object
  goal.planning_options.plan_only = plan_only;
  // goal.planning_options.look_around = can_look_;//TODO
  // goal.planning_options.replan = can_replan_;
  // goal.planning_options.replan_delay = replan_delay_;
  goal.planning_options.planning_scene_diff.is_diff = true;
  goal.planning_options.planning_scene_diff.robot_state.is_diff = true;

  place_action_client_->sendGoal(goal);
  ROS_DEBUG_NAMED("move_group_interface", "Sent place goal with %d locations",
                  (int)goal.place_locations.size());
  if (!place_action_client_->waitForResult()) {
    ROS_INFO_STREAM_NAMED("move_group_interface",
                          "Place action returned early");
  }
  if (place_action_client_->getState() ==
      actionlib::SimpleClientGoalState::SUCCEEDED) {
    return moveit::planning_interface::MoveItErrorCode(
        place_action_client_->getResult()->error_code);
  } else {
    ROS_WARN_STREAM_NAMED(
        "move_group_interface",
        "Fail: " << place_action_client_->getState().toString() << ": "
                 << place_action_client_->getState().getText());
    return moveit::planning_interface::MoveItErrorCode(
        place_action_client_->getResult()->error_code);
  }
}
