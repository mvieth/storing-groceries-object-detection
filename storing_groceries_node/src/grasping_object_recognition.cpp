#include <pcl/io/pcd_io.h>
//#include <ros/ros.h>
#include <ros/console.h>                                 // for LogLocation
#include <ros/duration.h>                                // for Duration
#include <ros/init.h>                                    // for init, spin
#include <ros/node_handle.h>                             // for NodeHandle
#include <ros/publisher.h>                               // for Publisher
#include <ros/service_server.h>                          // for ServiceServer
#include <ros/time.h>                                    // for operator<<
#include <ros/topic.h>                                   // for waitForMessage
#include <tf2_ros/transform_listener.h>
#include <cv_bridge/cv_bridge.h>
#include <opencv2/imgcodecs.hpp>
#include <pcl_conversions/pcl_conversions.h> // for fromROSMsg
#include <pcl/common/transforms.h> // for transformPointCloud
#include <dynamic_reconfigure/server.h>
#include <Eigen/Geometry>
#include <boost/smart_ptr/shared_ptr.hpp>                // for shared_ptr
#include <ostream>                                       // for operator<<
#include <string>                                        // for string, char...
#include <image_geometry/pinhole_camera_model.h>         // for PinholeCamer...
#include <pcl/exceptions.h>                              // for IOException
#include <pcl/point_cloud.h>                             // for PointCloud
#include <pcl/surface/convex_hull.h>                     // for ConvexHull
//#include <pcl/features/normal_3d_omp.h>                  // for NormalEstimationOMP
//#include <pcl/filters/extract_indices.h>                 // for ExtractIndices
#include <tf2_ros/buffer.h>                              // for Buffer
#include <tf2_eigen/tf2_eigen.h>
#include <actionlib/server/simple_action_server.h>
#include <moveit/move_group_interface/move_group_interface.h>
// msgs
#include <clf_grasping_msgs/FindObjectsInROI.h>
#include <clf_grasping_msgs/FindTable.h>
#include <clf_object_recognition_msgs/Detect3D.h>
#include <vision_msgs/Detection3DArray.h>
#include <std_srvs/Empty.h>
//#include <moveit_msgs/CollisionObject.h>
#include <vision_msgs/BoundingBox3D.h>                   // for BoundingBox3D
#include <sensor_msgs/CameraInfo.h>                      // for CameraInfo
#include <sensor_msgs/Image.h>                           // for Image
#include <storing_groceries_msgs/PlaceOnRectangleAction.h>
#include <storing_groceries_msgs/GetPlaceRectangle.h>
#include <storing_groceries_msgs/FindShelfAndObjects.h>
#include <storing_groceries_msgs/LearnObjects.h>
#include <control_msgs/FollowJointTrajectoryAction.h>
// own
#include "high_level_functions.hpp"
#include "object_knowledge.hpp"
#include "point_clouds.hpp"
#include "bounding_boxes.hpp" // for minimum_bbox_of_plane
#include "util.hpp" // for getIsometryTransform
#include "pick_place.hpp"
//#include "topic2.h"
//#include "topic2.hpp"
#include <storing_groceries_node/dyn_reconfConfig.h>

#define BASE_FRAME "base_footprint"

ros::NodeHandle *nh;
tf2_ros::Buffer tfBuffer(ros::Duration(15.0)); // Default is 10
tf2_ros::TransformListener *tfListener;
ros::Publisher detection_pub;
actionlib::SimpleActionServer<storing_groceries_msgs::PlaceOnRectangleAction> *place_as;
moveit::planning_interface::MoveGroupInterface *group = nullptr;

double seg_weight[3];
bool makeTableToBox = true;
bool restCloudAsOctoMap = true;
float tableSafetyMargin = 0.07;
bool makeAllStandardCylinder = false;
std::string learnObjectsPath = "";
std::string pointcloud_topic;
std::string uncoloured_pointcloud_topic="/xtion/depth_registered/points"; // TODO should normally be uncoloured but gazebo does not provide them TODO make reconfigurable?
std::string rgb_image_topic;
std::string depth_image_topic;
const std::string rgb_camera_info_topic = "/xtion/rgb/camera_info"; // TODO make reconfigurable?
image_geometry::PinholeCameraModel * cam_model;

// For dynamic reconfigure
void reconfigure_callback(grasping_object_recognition::dyn_reconfConfig &config,
                          uint32_t level) {
  makeTableToBox = config.makeTableToBox;
  restCloudAsOctoMap = config.restCloudAsOctoMap;
  tableSafetyMargin = config.tableSafetyMargin;
  makeAllStandardCylinder = config.makeAllStandardCylinder;
  learnObjectsPath = config.learnObjectsPath;
  //TODO test if learnObjectsPath ends with a "/"?
  if(learnObjectsPath.empty()) {
    learnObjectsPath = "/tmp/";
  }
  pointcloud_topic = config.pointcloud_topic;
  rgb_image_topic = config.rgb_image_topic;
  depth_image_topic = config.depth_image_topic;
  set_high_level_options(config.useAlternativePrimitiveFitting);
  seg_weight[0] = config.seg_weight0;
  seg_weight[1] = config.seg_weight1;
  seg_weight[2] = config.seg_weight2;
  if((level&1)!=0)
    parseConfigYAML(config.objects_config_path);
  
  ROS_INFO("Got reconfigure call, learnObjectsPath is now >%s<, makeTableToBox is now %s, "
           "tableSafetyMargin is now %f, makeAllStandardCylinder is now %s, pointcloud_topic is now %s, rgb_image_topic is now %s, depth_image_topic is now %s, objects_config_path is now %s",
           learnObjectsPath.c_str(), (makeTableToBox ? "true" : "false"), tableSafetyMargin, (makeAllStandardCylinder ? "true" : "false"), pointcloud_topic.c_str(), rgb_image_topic.c_str(), depth_image_topic.c_str(), config.objects_config_path.c_str());
}

pcl::PointCloud<point_t>::Ptr moveHeadAround() {
  const double leftright=0.5;
  const double down=-0.9;
  const double up=-0.1;
  actionlib::SimpleActionClient<control_msgs::FollowJointTrajectoryAction> ac(
      "/head_controller/follow_joint_trajectory", true);
  ac.waitForServer();
  control_msgs::FollowJointTrajectoryGoal goal;
  goal.trajectory.joint_names.push_back("head_1_joint");
  goal.trajectory.joint_names.push_back("head_2_joint");
  goal.trajectory.points.resize(1);
  goal.trajectory.points[0].positions.resize(2);
  goal.trajectory.points[0].positions[0]=leftright;
  goal.trajectory.points[0].positions[1]=down;
  goal.trajectory.points[0].time_from_start = ros::Duration(3, 0);
  ac.sendGoal(goal);
  ac.waitForResult();
  pcl::PointCloud<point_t>::Ptr cloud1 = getPointCloud(*nh, tfBuffer, pointcloud_topic, true, nullptr);
  goal.trajectory.points[0].positions[0]=leftright;
  goal.trajectory.points[0].positions[1]=up;
  ac.sendGoal(goal);
  ac.waitForResult();
  pcl::PointCloud<point_t>::ConstPtr cloud2 = getPointCloud(*nh, tfBuffer, pointcloud_topic, true, nullptr);
  goal.trajectory.points[0].positions[0]=-leftright;
  goal.trajectory.points[0].positions[1]=up;
  ac.sendGoal(goal);
  ac.waitForResult();
  pcl::PointCloud<point_t>::ConstPtr cloud3 = getPointCloud(*nh, tfBuffer, pointcloud_topic, true, nullptr);
  goal.trajectory.points[0].positions[0]=-leftright;
  goal.trajectory.points[0].positions[1]=down;
  ac.sendGoal(goal);
  ac.waitForResult();
  pcl::PointCloud<point_t>::ConstPtr cloud4 = getPointCloud(*nh, tfBuffer, pointcloud_topic, true, nullptr);
  *cloud1 += *cloud2;//TODO maybe some ICP here?
  *cloud1 += *cloud3;
  *cloud1 += *cloud4;
  try {
    pcl::io::savePCDFile ("/tmp/stitched_cloud.pcd", *cloud1, true);
  } catch (pcl::IOException &e) {
    ROS_WARN("%s", e.what());
  }
  ROS_INFO("moveHeadAround() finished");
  return cloud1;
}

bool findTableService(clf_grasping_msgs::FindTable::Request &req,
                      clf_grasping_msgs::FindTable::Response &res) {
  ROS_INFO("findTableService() called");
  
  // Get camera info and point cloud messages
  if(cam_model == nullptr) {// only do this once, since it does not change
      ROS_INFO("Getting camera info ... (5s timeout)");
      boost::shared_ptr<sensor_msgs::CameraInfo const> sharedCameraInfo = ros::topic::waitForMessage<sensor_msgs::CameraInfo>(rgb_camera_info_topic, *nh, ros::Duration(5, 0));
      ROS_INFO("done");
      if (sharedCameraInfo != nullptr) {
          cam_model=new image_geometry::PinholeCameraModel();
          cam_model->fromCameraInfo(*sharedCameraInfo);
      } else {
          ROS_WARN("Failed to get camera info");
      }
  }
  
  //This is a possible alternative for getPointCloud() below. But not sure if it is faster, probably depends, e.g. on network conditions
  //ROS_INFO("Waiting for depth image message");
  //boost::shared_ptr<sensor_msgs::Image const> sharedDepthImage = ros::topic::waitForMessage<sensor_msgs::Image>(depth_image_topic, *nh, ros::Duration(10, 0));
  //if(sharedDepthImage==nullptr) {
  //    ROS_ERROR("Failed to get a depth image");
  //    return false;
  //}
  //std::string orig_frame=sharedDepthImage->header.frame_id;
  //pcl::PointCloud<point_t>::Ptr raw_point_cloud=depthImageToPointcloud(*sharedDepthImage, nullptr, *cam_model);
  //ROS_INFO_STREAM("Got depth image with stamp " << sharedDepthImage->header.stamp);
  //Eigen::Isometry3d transform=getIsometryTransform(tfBuffer, sharedDepthImage->header.frame_id, BASE_FRAME);
  //ROS_INFO("Transforming point cloud to %s", BASE_FRAME);
  //pcl::transformPointCloud(*raw_point_cloud, *raw_point_cloud, transform.inverse().matrix());//TODO this seems to take some time, maybe there is a solution where this is not necessary? Or maybe it's faster when source and target are not the same?
  //ROS_INFO("Transformation done");
  // Make unorganized
  //raw_point_cloud->width = (raw_point_cloud->width) * (raw_point_cloud->height);
  //raw_point_cloud->height = 1;


  std::string orig_frame;
  pcl::PointCloud<point_t>::Ptr raw_point_cloud = getPointCloud(*nh, tfBuffer, uncoloured_pointcloud_topic, true/*false*/, &orig_frame);//TODO depth image instead of point cloud? Or uncolored points?
  if(raw_point_cloud==nullptr || raw_point_cloud->points.empty()) {
      ROS_ERROR("Failed to get a point cloud");
      return false;
  }
  ROS_INFO("Got point cloud, now continuing with getCloudROI");
  
  /*
  // This is a possible replacement for the other getCloudROI function. The difference is that point_cloud does not have to be in BASE_FRAME, thus possibly reducing the cost for transforming the whole cloud
  Eigen::Isometry3d transform=getIsometryTransform(tfBuffer, orig_frame, BASE_FRAME);
  vision_msgs::BoundingBox3D crop_roi;
  crop_roi.size.x = 3.0;
  crop_roi.size.y = 4.0;
  crop_roi.size.z = 1.9;
  Eigen::Vector3d new_pos=transform*(*new Eigen::Vector3d(1.5, 0.0, 1.05));
  crop_roi.center.position = tf2::toMsg(new_pos);
  crop_roi.center.orientation = tf2::toMsg(*new Eigen::Quaterniond(transform.rotation()));
  pcl::PointCloud<point_t>::Ptr point_cloud = getCloudROI(raw_point_cloud, crop_roi); // Remove ground
  ROS_INFO("Calling transformPointCloud ...");
  pcl::transformPointCloud(*point_cloud, *point_cloud, isometry2affine(transform.inverse()));
  ROS_INFO("done");
  */
  pcl::PointCloud<point_t>::Ptr point_cloud = getCloudROI(raw_point_cloud, 0.0, 3.0, -2.0, 2.0, 0.1, 2.0); // Remove ground

  ROS_INFO("Saving cloud under /tmp/cloud_filtered_ROI.pcd");
  try {
    pcl::io::savePCDFile ("/tmp/cloud_filtered_ROI.pcd", *point_cloud, true);
  } catch (pcl::IOException &e) {
    ROS_WARN("%s", e.what());
  }
  ROS_INFO("Saving done");
  
  plane_t * table=searchAndAddTableToScene(point_cloud, orig_frame, cam_model, true, req.addToPlanningScene, makeTableToBox, tableSafetyMargin); // Here point_cloud must be in BASE_FRAME
  if(table != nullptr) {
    res.plane = table->plane;
    res.bbox = table->bbox;
    res.hull = table->hull;
    delete table;
  } else {
    ROS_WARN("findTableService() finished, but no table found!");
    return false;
  }
  ROS_INFO("findTableService() finished");
  return true;
}

bool findShelfService(clf_grasping_msgs::FindTable::Request &req,
                      clf_grasping_msgs::FindTable::Response &res) {
  ROS_INFO("findShelfService() called");

  // Get camera info and point cloud messages
  if(cam_model == nullptr) {// only do this once, since it does not change
      ROS_INFO("Getting camera info ... (5s timeout)");
      boost::shared_ptr<sensor_msgs::CameraInfo const> sharedCameraInfo = ros::topic::waitForMessage<sensor_msgs::CameraInfo>(rgb_camera_info_topic, *nh, ros::Duration(5, 0));
      ROS_INFO("done");
      if (sharedCameraInfo != nullptr) {
          cam_model=new image_geometry::PinholeCameraModel();
          cam_model->fromCameraInfo(*sharedCameraInfo);
      } else {
          ROS_WARN("Failed to get camera info");
      }
  }

  std::string orig_frame;
  pcl::PointCloud<point_t>::Ptr raw_point_cloud = getPointCloud(*nh, tfBuffer, pointcloud_topic, true/*false*/, &orig_frame);//TODO depth image instead of point cloud? Or uncolored points?
  if(raw_point_cloud==nullptr || raw_point_cloud->points.empty()) {
      ROS_ERROR("Failed to get a point cloud");
      return false;
  }
  ROS_INFO("Got point cloud, now continuing with getCloudROI");

  pcl::PointCloud<point_t>::Ptr point_cloud = getCloudROI(raw_point_cloud, 0.0, 3.0, -2.0, 2.0, 0.1, 2.0); // Remove ground
  try {
    pcl::io::savePCDFile ("/tmp/cloud_filtered_ROI.pcd", *point_cloud, true);
  } catch (pcl::IOException &e) {
    ROS_WARN("%s", e.what());
  }

  float shelfx, shelfy, shelfangle;
  if (searchForShelf(point_cloud, shelfx, shelfy, shelfangle)) {
    //if (shelfangle < -M_PI / 2.0 || shelfangle > M_PI / 2.0) {
    //  shelfangle = angleModulo(shelfangle + M_PI);
    //}
    ROS_INFO("Found shelf: x=%f, y=%f, angle=%f", shelfx, shelfy, shelfangle);
    if(req.addToPlanningScene) {
      addShelfToPlanningScene(shelfx, shelfy, shelfangle, point_cloud);
    }
    res.bbox.center.position.x = shelfx;
    res.bbox.center.position.y = shelfy;
    //res.bbox.center.position.z = ;TODO
    res.bbox.center.orientation.z = std::sin(shelfangle/2.0);
    res.bbox.center.orientation.w = std::cos(shelfangle/2.0);
    //res.bbox.size TODO

    try {
      pcl::io::savePCDFile ("/tmp/cloud_wo_shelf.pcd", *point_cloud, true);
    } catch (pcl::IOException &e) {
      ROS_WARN("%s", e.what());
    }
  } else {
    ROS_WARN("No shelf found, trying emergency solution");
    if (shelfx == 0.0)
      shelfx = 0.7;
    shelfy = 0.0;
    shelfangle = 0.0;
    addShelfToPlanningScene(shelfx, shelfy, shelfangle, point_cloud);
    // TODO just add the oobbs?
    ROS_WARN("findShelfService() finished, but no shelf found!");
    return false;
  }

  //pcl::PointCloud<point_t>::Ptr object_cloud(new pcl::PointCloud<point_t>());
  //pcl::transformPointCloud(*point_cloud, *object_cloud, transform.matrix());
  //std::vector<vision_msgs::Detection3D> detections = getObjectsAndAddToPlanningScene(rgb_image, *cam_model, object_cloud, orig_frame, &transform, 0.0, true, false, nullptr); // Here point_cloud must be in the original camera frame

  ROS_INFO("findShelfService() finished");
  return true;
}

bool findShelfAndObjectsService(
    storing_groceries_msgs::FindShelfAndObjects::Request &req,
    storing_groceries_msgs::FindShelfAndObjects::Response &res) {
  ROS_INFO_STREAM("findShelfAndObjectsService() called\nOptions: addToPlanningScene: "
                  << (req.addToPlanningScene ? "true" : "false"));
  if(cam_model == nullptr) {// only do this once, since it does not change
      ROS_INFO("Getting camera info ... (5s timeout)");
      boost::shared_ptr<sensor_msgs::CameraInfo const> sharedCameraInfo = ros::topic::waitForMessage<sensor_msgs::CameraInfo>(rgb_camera_info_topic, *nh, ros::Duration(5, 0));
      ROS_INFO("done");
      if (sharedCameraInfo != nullptr) {
          cam_model=new image_geometry::PinholeCameraModel();
          cam_model->fromCameraInfo(*sharedCameraInfo);
      } else {
          ROS_ERROR("Failed to get camera info");
          return false;
      }
  }
  
  std_msgs::Header orig_header;
  Eigen::Isometry3d transform;
  sensor_msgs::Image rgb_image;
  pcl::PointCloud<point_t>::Ptr cloud=getSceneData(*nh, tfBuffer, cam_model, depth_image_topic, rgb_image_topic, orig_header, transform, rgb_image);
  // Needed after this point: cloud, orig_frame, transform, rgb_image
  pcl::PointCloud<point_t>::Ptr point_cloud;
  point_cloud = getCloudROI(cloud/*getPointCloud(*nh, tfBuffer, pointcloud_topic, true, &orig_frame)*/, 0.0, 3.0, -2.0, 2.0, 0.1, 2.0);//Remove ground
  if(point_cloud==nullptr || point_cloud->points.empty()) {
      ROS_ERROR("Failed to get a point cloud");
      return false;
  }
  
  float shelfx, shelfy, shelfangle;
  if (searchForShelf(point_cloud, shelfx, shelfy, shelfangle)) {
    //if (shelfangle < -M_PI / 2.0 || shelfangle > M_PI / 2.0) {
    //  shelfangle = angleModulo(shelfangle + M_PI);
    //}
    ROS_INFO("Found shelf: x=%f, y=%f, angle=%f", shelfx, shelfy, shelfangle);
    if (req.addToPlanningScene) {
      addShelfToPlanningScene(shelfx, shelfy, shelfangle, point_cloud);
    }
    res.shelf_x = shelfx;
    res.shelf_y = shelfy;
    res.shelf_angle = shelfangle;
    
    try {
      pcl::io::savePCDFile ("/tmp/cloud_wo_shelf.pcd", *point_cloud, true);
    } catch (pcl::IOException &e) {
      ROS_WARN("%s", e.what());
    }
    std::vector<float> shelf_floor_heights;
    // Only consider the objects that are in the shelf
    vision_msgs::BoundingBox3D inside_shelf_bbox;
    getShelfSize(&inside_shelf_bbox.size.z, &inside_shelf_bbox.size.y, &inside_shelf_bbox.size.x, &shelf_floor_heights);
    inside_shelf_bbox.center.position.x=shelfx;
    inside_shelf_bbox.center.position.y=shelfy;
    inside_shelf_bbox.center.position.z=inside_shelf_bbox.size.z/2.0;
    inside_shelf_bbox.center.orientation.z=std::sin(shelfangle/2.0);
    inside_shelf_bbox.center.orientation.w=std::cos(shelfangle/2.0);
    point_cloud=getCloudROI(point_cloud, inside_shelf_bbox);
    res.floor_width = inside_shelf_bbox.size.y;
    res.floor_depth = inside_shelf_bbox.size.x;
    res.floor_heights.resize(shelf_floor_heights.size());
    for(size_t i=0; i<shelf_floor_heights.size(); ++i) {
      res.floor_heights[i]=shelf_floor_heights[i];
    }
  } else {
    ROS_WARN("No shelf found, trying emergency solution");
    if (shelfx == 0.0)
      shelfx = 0.7;
    shelfy = 0.0;
    shelfangle = 0.0;
    addShelfToPlanningScene(shelfx, shelfy, shelfangle, point_cloud);
    // TODO just add the oobbs?
    ROS_WARN("findShelfService() finished, but no shelf found!");
    return false;
  }
  
  pcl::PointCloud<point_t>::Ptr object_cloud(new pcl::PointCloud<point_t>());
  const Eigen::Matrix4f transform_matrix=transform.matrix().cast<float>(); // float precision should be enough, and faster
  pcl::transformPointCloud(*point_cloud, *object_cloud, transform_matrix);
  
  std::vector<vision_msgs::Detection3D> detections;
  detections = getObjectsAndAddToPlanningScene(rgb_image, *cam_model, object_cloud, orig_header.frame_id, &transform, 0.0, req.addToPlanningScene, makeAllStandardCylinder, &res.objectIds); // Here point_cloud must be in the original camera frame
  //if(restCloudAsOctoMap) addPointCloudAsOctomap();
  for(size_t i=0; i<detections.size(); ++i) { // Transform bbox poses from camera frame to base_footprint TODO inconsistent with header and source_cloud?
    Eigen::Isometry3d pose;
    tf2::fromMsg(detections[i].bbox.center, pose);
    detections[i].bbox.center = tf2::toMsg(transform.inverse() * pose);
  }

  res.detections = detections;
  vision_msgs::Detection3DArray det_msg;
  det_msg.header = orig_header;
  //det_msg.header.frame_id = orig_frame;
  //det_msg.header.stamp // TODO also set stamp?
  det_msg.detections = detections;
  detection_pub.publish(det_msg);
  /*for (size_t i = 0; i < res.detections.size(); i++) {
    res.detections[i].source_cloud.data.clear(); // This is too much data to print, maybe remove this later
  }*/
  ROS_INFO("findShelfAndObjectsService() finished");
  return true;
}

bool findObjectsInROIService(
    clf_grasping_msgs::FindObjectsInROI::Request &req,
    clf_grasping_msgs::FindObjectsInROI::Response &res) {
  ROS_INFO_STREAM("findObjectsInROIService() called\nOptions: roi:"
                  << req.roi << " addToPlanningScene: "
                  << (req.addToPlanningScene ? "true" : "false"));
  if(cam_model == nullptr) {// only do this once, since it does not change
      ROS_INFO("Getting camera info ... (5s timeout)");
      boost::shared_ptr<sensor_msgs::CameraInfo const> sharedCameraInfo = ros::topic::waitForMessage<sensor_msgs::CameraInfo>(rgb_camera_info_topic, *nh, ros::Duration(5, 0));
      ROS_INFO("done");
      if (sharedCameraInfo != nullptr) {
          cam_model=new image_geometry::PinholeCameraModel();
          cam_model->fromCameraInfo(*sharedCameraInfo);
      } else {
          ROS_ERROR("Failed to get camera info");
          return false;
      }
  }
  
  std_msgs::Header orig_header;
  Eigen::Isometry3d transform;
  sensor_msgs::Image rgb_image;
  pcl::PointCloud<point_t>::Ptr cloud=getSceneData(*nh, tfBuffer, cam_model, depth_image_topic, rgb_image_topic, orig_header, transform, rgb_image);
  // Needed after this point: cloud, orig_frame, transform, rgb_image
  pcl::PointCloud<point_t>::Ptr point_cloud;
  if (req.roi.size.x == 0.0 && req.roi.size.y == 0.0 && req.roi.size.z == 0.0) { // if req.roi has sizes 0, then use a standard roi
    ROS_INFO("ROI volume is zero, using standard roi");
    point_cloud = getCloudROI(cloud/*getPointCloud(*nh, tfBuffer, pointcloud_topic, true, &orig_frame)*/, 0.0, 3.0, -2.0, 2.0, 0.1, 2.0);//Remove ground
  } else {
    point_cloud = getCloudROI(cloud/*getPointCloud(*nh, tfBuffer, pointcloud_topic, true, &orig_frame)*/, req.roi);
  }
  if(point_cloud==nullptr || point_cloud->points.empty()) {
      ROS_ERROR("Failed to get a point cloud");
      return false;
  }
  
  plane_t * table = searchAndAddTableToScene(point_cloud, orig_header.frame_id, nullptr, true, false, makeTableToBox, tableSafetyMargin); // Here point_cloud must be in BASE_FRAME TODO why no cam model?
  
  float table_height;
  if(table!=nullptr) {
      table_height=table->bbox.center.position.z;
      
      // TODO maybe only if req.roi has size 0?
      vision_msgs::BoundingBox3D above_table_bbox(table->bbox);
      above_table_bbox.center.position.z=(table_height+2.0)/2.0;
      above_table_bbox.size.z=2.0-table_height;
      above_table_bbox.size.x+=0.05; // For objects that are close to the edge of the table
      above_table_bbox.size.y+=0.05;
      try {
        ROS_INFO("Saving cloud under /tmp/cloud_wo_table.pcd");
        pcl::io::savePCDFile ("/tmp/cloud_wo_table.pcd", *point_cloud, true);
      } catch (pcl::IOException &e) {
        ROS_WARN("%s", e.what());
      }
      point_cloud=getCloudROI(point_cloud, above_table_bbox); // Only consider the objects that are on the table // TODO error here?
      delete table;
  } else {
      table_height=0.0;
  }
  
  pcl::PointCloud<point_t>::Ptr object_cloud(new pcl::PointCloud<point_t>());
  const Eigen::Matrix4f transform_matrix=transform.matrix().cast<float>(); // float precision should be enough, and faster
  pcl::transformPointCloud(*point_cloud, *object_cloud, transform_matrix);
  
  std::vector<vision_msgs::Detection3D> detections;
  detections = getObjectsAndAddToPlanningScene(rgb_image, *cam_model, object_cloud, orig_header.frame_id, &transform, table_height, req.addToPlanningScene, makeAllStandardCylinder, &res.objectIds); // Here point_cloud must be in the original camera frame
  //if(restCloudAsOctoMap) addPointCloudAsOctomap();

  res.detections = detections;
  vision_msgs::Detection3DArray det_msg;
  det_msg.header = orig_header;
  //det_msg.header.frame_id = orig_frame;
  //det_msg.header.stamp // TODO also set stamp?
  det_msg.detections = detections;
  detection_pub.publish(det_msg);
  /*for (size_t i = 0; i < res.detections.size(); i++) {
    res.detections[i].source_cloud.data.clear(); // This is too much data to print, maybe remove this later
  }*/
  ROS_INFO("findObjectsInROIService() finished");
  return true;
}

bool learnObjectsService(
    storing_groceries_msgs::LearnObjects::Request& req2,
    storing_groceries_msgs::LearnObjects::Response&) {
    //std_srvs::Empty::Request&,
    //std_srvs::Empty::Response&) {
  ROS_INFO_STREAM("learnObjectsService() called");
  clf_grasping_msgs::FindObjectsInROI::Request req;//TODO fill req?
  req.roi = vision_msgs::BoundingBox3D();
  req.addToPlanningScene = false;
  clf_grasping_msgs::FindObjectsInROI::Response res;
  //findObjectsInROIService(req, res);
  ROS_INFO_STREAM("findObjectsInROIService() called\nOptions: roi:"
                  << req.roi << " addToPlanningScene: "
                  << (req.addToPlanningScene ? "true" : "false"));
  
  ROS_INFO("Waiting for camera image and point cloud ...");
  //boost::shared_ptr<sensor_msgs::Image const> sharedRGBImage;
  boost::shared_ptr<sensor_msgs::Image const> sharedRGBImage = ros::topic::waitForMessage<sensor_msgs::Image>(rgb_image_topic, *nh, ros::Duration(10, 0));
  //boost::shared_ptr<sensor_msgs::PointCloud2 const> sharedCloud;
  //ros::topic::waitForMessages<sensor_msgs::Image, sensor_msgs::PointCloud2>(rgb_image_topic, pointcloud_topic, *nh, ros::Duration(10, 0), sharedRGBImage, sharedCloud);
  sensor_msgs::Image depth_image=averageImages(*nh, "/xtion/depth_registered/image_raw", /*20*/1, 0.1);
  ROS_INFO("done");
  if(cam_model == nullptr) {// only do this once, since it does not change
      ROS_INFO("Getting camera info ... (5s timeout)");
      boost::shared_ptr<sensor_msgs::CameraInfo const> sharedCameraInfo = ros::topic::waitForMessage<sensor_msgs::CameraInfo>(rgb_camera_info_topic, *nh, ros::Duration(5, 0));
      ROS_INFO("done");
      if (sharedCameraInfo != nullptr) {
          cam_model=new image_geometry::PinholeCameraModel();
          cam_model->fromCameraInfo(*sharedCameraInfo);
      } else {
          ROS_ERROR("Failed to get camera info");
          return false;
      }
  }
  
  //if (sharedCloud == nullptr) {
  //  ROS_ERROR("Failed to get point cloud");
  //  return false;
  //}
  if (sharedRGBImage == nullptr) {
    ROS_ERROR("Failed to get camera image");
    return false;
  }
  pcl::PointCloud<point_t>::Ptr cloud = depthImageToPointcloud(depth_image, &(*sharedRGBImage), *cam_model); // Regarding the second parameter: is a boost shared pointer, should be a normal pointer
  Eigen::Isometry3d transform=getIsometryTransform(tfBuffer, depth_image.header.frame_id, BASE_FRAME, depth_image.header.stamp);
  const Eigen::Matrix4f transform_matrix=transform.inverse().matrix().cast<float>(); // float precision should be enough, and faster
  pcl::transformPointCloud(*cloud, *cloud, transform_matrix);
  // Make unorganized
  cloud->width = (cloud->width) * (cloud->height);
  cloud->height = 1;
  
  //ROS_INFO("Getting camera image");
  ROS_INFO_STREAM("Got camera image with stamp " << sharedRGBImage->header.stamp);
  sensor_msgs::Image rgb_image = *sharedRGBImage;
  // Save image (e.g. for logging/introspection purposes)
  uint32_t time = ros::WallTime::now().sec;
  std::ostringstream imgstreamtmp;
  imgstreamtmp << "/tmp/" << time << ".png";
  cv::imwrite(imgstreamtmp.str(), cv_bridge::toCvShare(boost::make_shared<sensor_msgs::Image>(rgb_image), sensor_msgs::image_encodings::BGR8)->image);

  std::string orig_frame = depth_image.header.frame_id;
  pcl::PointCloud<point_t>::Ptr point_cloud;
  if (req.roi.size.x == 0.0 && req.roi.size.y == 0.0 && req.roi.size.z == 0.0) { // if req.roi has sizes 0, then use a standard roi
    ROS_INFO("ROI volume is zero, using standard roi");
    point_cloud = getCloudROI(cloud/*getPointCloud(*nh, tfBuffer, pointcloud_topic, true, &orig_frame)*/, 0.0, 3.0, -2.0, 2.0, 0.1, 2.0);//Remove ground
  } else {
    point_cloud = getCloudROI(cloud/*getPointCloud(*nh, tfBuffer, pointcloud_topic, true, &orig_frame)*/, req.roi);
  }
  if(point_cloud==nullptr || point_cloud->points.empty()) {
      ROS_ERROR("Failed to get a point cloud");
      return false;
  }
  
  plane_t * table = searchAndAddTableToScene(point_cloud, orig_frame, nullptr, true, false, makeTableToBox, tableSafetyMargin); // Here point_cloud must be in BASE_FRAME
  
  float table_height;
  if(table!=nullptr) {
      table_height=table->bbox.center.position.z;
      
      // TODO maybe only if req.roi has size 0?
      vision_msgs::BoundingBox3D above_table_bbox(table->bbox);
      above_table_bbox.center.position.z=(table_height+2.0)/2.0;
      above_table_bbox.size.z=2.0-table_height;
      above_table_bbox.size.x+=0.05; // For objects that are close to the edge of the table
      above_table_bbox.size.y+=0.05;
      point_cloud=getCloudROI(point_cloud, above_table_bbox); // Only consider the objects that are on the table
      delete table;
  } else {
      table_height=0.0;
  }
  
  pcl::PointCloud<point_t>::Ptr object_cloud(new pcl::PointCloud<point_t>());
  pcl::transformPointCloud(*point_cloud, *object_cloud, transform.matrix());
  
  std::vector<vision_msgs::Detection3D> detections;
  detections = getObjectsAndAddToPlanningScene(rgb_image, *cam_model, object_cloud, orig_frame, &transform, table_height, req.addToPlanningScene, makeAllStandardCylinder, &res.objectIds); // Here point_cloud must be in the original camera frame

  res.detections = detections;
  vision_msgs::Detection3DArray det_msg;
  det_msg.header = depth_image.header;
  //det_msg.header.frame_id = orig_frame;
  //det_msg.header.stamp // TODO also set stamp?
  det_msg.detections = detections;
  detection_pub.publish(det_msg);
  //for (size_t i = 0; i < res.detections.size(); i++) {
  //  res.detections[i].source_cloud.data.clear(); // This is too much data to print, maybe remove this later
  //}
  ROS_INFO("findObjectsInROIService() finished");
  
  
  
  
  //uint32_t time = ros::WallTime::now().sec;
  //TODO test if learnObjectsPath ends with a "/"?
  //TODO create folders if not existant?
  std::ostringstream imgname;
  imgname << learnObjectsPath << "/images/" << time << ".png";
  bool saving_image_successful = cv::imwrite(imgname.str(), cv_bridge::toCvShare(boost::make_shared<sensor_msgs::Image>(rgb_image), sensor_msgs::image_encodings::BGR8)->image);
  if(!saving_image_successful) {
    std::ostringstream imgdirname;
    imgdirname << learnObjectsPath << "/images";
    ROS_INFO("Saving the image to %s was unsuccessful, trying to create directory %s", imgname.str().c_str(), imgdirname.str().c_str());
    mkdir(imgdirname.str().c_str(), 0777);
    saving_image_successful = cv::imwrite(imgname.str(), cv_bridge::toCvShare(boost::make_shared<sensor_msgs::Image>(rgb_image), sensor_msgs::image_encodings::BGR8)->image);
  }
  ROS_INFO("Saving the image to %s was: %s", imgname.str().c_str(), (saving_image_successful?"successful":"unsuccessful"));
  
  std::ofstream label_file;
  std::ostringstream txtstream;
  txtstream << learnObjectsPath << "/labels/" << time << ".txt";
  label_file.open (txtstream.str());
  if(!label_file.is_open()) {
    std::ostringstream labeldirname;
    labeldirname << learnObjectsPath << "/labels";
    mkdir(labeldirname.str().c_str(), 0777);
    label_file.open (txtstream.str());
  }
  ROS_INFO("labels file (%s) is open:%s", txtstream.str().c_str(), (label_file.is_open()?"true":"false"));

  if(!res.detections.empty()) {
    pcl::PointCloud<point_t>::Ptr detection_cloud(new pcl::PointCloud<point_t>);
    pcl::fromROSMsg(res.detections[0].source_cloud, *detection_cloud);

    if(label_file.is_open()) {
      float xmin, xmax, ymin, ymax;
      //subimageBoundaries(tf2::Vector3(res.detections[0].bbox.center.position.x, res.detections[0].bbox.center.position.y, res.detections[0].bbox.center.position.z), tf2::Quaternion(res.detections[0].bbox.center.orientation.x, res.detections[0].bbox.center.orientation.y, res.detections[0].bbox.center.orientation.z, res.detections[0].bbox.center.orientation.w), tf2::Vector3(res.detections[0].bbox.size.x, res.detections[0].bbox.size.y, res.detections[0].bbox.size.z), *cam_model, xmin, ymin, xmax, ymax);
      subimageBoundaries(detection_cloud, *cam_model, xmin, ymin, xmax, ymax);
      label_file << req2.data[0] << " " << (xmin+xmax)/2.0/rgb_image.width << " " << (ymin+ymax)/2.0/rgb_image.height << " " << (xmax-xmin)/rgb_image.width << " " << (ymax-ymin)/rgb_image.height << "\n"; // TODO selectable id
    } else ROS_WARN("Label file could not be opened");
    
    std::ostringstream cloudstream;
    cloudstream << learnObjectsPath << "/clouds";
    struct stat statbuf;
    if(stat(cloudstream.str().c_str(), &statbuf) == -1) {
      ROS_INFO("%s does not seem to exist, making dir", cloudstream.str().c_str());
      mkdir(cloudstream.str().c_str(), 0777);
    }
    cloudstream << "/" << req2.data[0];
    if(stat(cloudstream.str().c_str(), &statbuf) == -1) {
      ROS_INFO("%s does not seem to exist, making dir", cloudstream.str().c_str());
      mkdir(cloudstream.str().c_str(), 0777);
    }
    cloudstream << "/" << time << ".pcd";
    ROS_INFO("Saving object cloud to %s ...", cloudstream.str().c_str());
    // TODO move to origin?
    try {
      pcl::io::savePCDFile (cloudstream.str(), *detection_cloud, true);
    } catch (pcl::IOException &e) {
      ROS_WARN("%s", e.what());
    }
  } else ROS_ERROR("No detections");
  
  if(label_file.is_open()) {
    label_file.close();
  }
  return true;
}

bool computeTargetRectangleService(
    storing_groceries_msgs::GetPlaceRectangle::Request& req,
    storing_groceries_msgs::GetPlaceRectangle::Response& res) {
  double angle;
  moveit_msgs::CollisionObject objectInGripper;
  std::string support_surface = computeTargetPosition(req.name, res.center.pose.position.x, res.center.pose.position.y,
                                  res.center.pose.position.z, res.size_x, res.size_y,
                                  angle, req.shelf_x, req.shelf_y,
                                  req.shelf_angle, &objectInGripper);
  // TODO angle to res.center.pose.orientation
  
  ROS_INFO_STREAM("objectInGripper=" << objectInGripper);
  std::vector<moveit_msgs::PlaceLocation> places =
      generatePlaces(objectInGripper, res.center.pose.position.x, res.center.pose.position.y, res.center.pose.position.z, res.size_x, res.size_y, angle);
  return true;
}

bool attachTestObjectService(
    std_srvs::Empty::Request& /*req*/,
    std_srvs::Empty::Response& /*res*/) {
  moveit_msgs::AttachedCollisionObject aco;
  // TODO fill aco
  aco.link_name = "arm_tool_link";
  aco.object.header.frame_id = "arm_tool_link";
  aco.object.id = "pringles0";
  aco.object.primitives.resize(1);
  aco.object.primitive_poses.resize(1);
  
  //aco.object.primitives[0].type = aco.object.primitives[0].CYLINDER;
  //aco.object.primitives[0].dimensions.resize(2);
  //aco.object.primitives[0].dimensions[0] = 0.22;
  //aco.object.primitives[0].dimensions[1] = 0.04;
  
  aco.object.primitives[0].type = aco.object.primitives[0].BOX;
  aco.object.primitives[0].dimensions.resize(3);
  aco.object.primitives[0].dimensions[0] = 0.05;
  aco.object.primitives[0].dimensions[1] = 0.15;
  aco.object.primitives[0].dimensions[2] = 0.25;
  
  //aco.object.primitives[0].type = aco.object.primitives[0].SPHERE;
  //aco.object.primitives[0].dimensions.resize(1);
  //aco.object.primitives[0].dimensions[0] = 0.04;
  
  aco.object.primitive_poses[0].position.x = 0.19;
  aco.object.primitive_poses[0].position.y = 0.0;
  aco.object.primitive_poses[0].position.z = 0.0;
  aco.object.primitive_poses[0].orientation.x = -0.5;
  aco.object.primitive_poses[0].orientation.y = 0.5;
  aco.object.primitive_poses[0].orientation.z = 0.5;
  aco.object.primitive_poses[0].orientation.w = 0.5;
  //aco.touch_links = ;
  //aco.detach_posture = ;
  //aco.weight = ;
  ROS_INFO_STREAM("Attaching object:" << std::endl << aco);
  applyAttachedCollisionObject(aco);
  return true;
}

// TODO unsuccessful returns
// prerequisites/preconditions: Object in gripper, lift down, arm close to torso
// TODO
// postconditions: No object in gripper, lift down, arm close to torso TODO
void placeObjectOnRectangleCB(
    const storing_groceries_msgs::PlaceOnRectangleGoalConstPtr & goal) { // TODO what if goal->center is not specified in base_footprint frame?
  ROS_INFO_STREAM("placeObjectOnRectangleCB called, goal->support=" << goal->support);
  storing_groceries_msgs::PlaceOnRectangleResult result;
  storing_groceries_msgs::PlaceOnRectangleFeedback feedback;

  // Get object to place (attached to the robot)
  std::map<std::string, moveit_msgs::AttachedCollisionObject> attachedObjects = getAttachedObjects();
  ROS_INFO_STREAM("Number of attached objects: " << attachedObjects.size());
  moveit_msgs::CollisionObject objectInGripper;
  if (!attachedObjects.empty()) {
    objectInGripper = attachedObjects.cbegin()->second.object;
    ROS_INFO_STREAM("objectInGripper=" << attachedObjects.cbegin()->second);
    if (attachedObjects.size() > 1) {
      ROS_WARN("There are %lu objects attached to the robot, should only be one!", attachedObjects.size());
    }
  } else {
    ROS_WARN("There are no objects attached to the robot!");
    result.result_code = 99999; // FAILURE
    place_as->setAborted(result, "no object attached");
    return;
  }

  geometry_msgs::Point place_position;
  place_position.x = goal->center.pose.position.x;
  place_position.y = goal->center.pose.position.y;
  place_position.z = goal->center.pose.position.z;
  double goal_size_x = goal->size_x, goal_size_y = goal->size_y;
  double angle = 2.0 * acos((goal->center.pose.orientation.z < 0.0 ? -1.0 : 1.0) * goal->center.pose.orientation.w / sqrt(std::pow(goal->center.pose.orientation.w, 2) + std::pow(goal->center.pose.orientation.z, 2))); // TODO what if w and z are zero? Is that possible? TODO use yaw_from_quat instead?

  // If only a support surface is given, no center pose, place it anywhere on the surface
  if(place_position.x == 0.0 && place_position.y == 0.0 && place_position.z == 0.0 && goal_size_x == 0.0 && goal_size_y == 0.0) {
    ROS_INFO("Center and size of placing surface seems to not be set - trying to figure it out by planning scene geometry ...");
    std::map<std::string, moveit_msgs::CollisionObject> support_surface = getObjects({goal->support});
    if(support_surface.size()==1) {
      const moveit_msgs::CollisionObject& support=support_surface.cbegin()->second;
      if(support.primitives.size()==1 && support.primitive_poses.size()==1 && support.primitives[0].type==support.primitives[0].BOX) {
        // TODO determine whether the box orientation is good (one side parallel to ground) and which dimension corresponds to z-direction
        place_position.x = support.primitive_poses[0].position.x;
        place_position.y = support.primitive_poses[0].position.y;
        place_position.z = support.primitive_poses[0].position.z + support.primitives[0].dimensions[2]/2.0;
        goal_size_x = support.primitives[0].dimensions[0];
        goal_size_y = support.primitives[0].dimensions[1];
        angle = 2.0 * acos((support.primitive_poses[0].orientation.z < 0.0 ? -1.0 : 1.0) * support.primitive_poses[0].orientation.w / sqrt(std::pow(support.primitive_poses[0].orientation.w, 2) + std::pow(support.primitive_poses[0].orientation.z, 2))); // TODO what if w and z are zero? Is that possible? TODO use yaw_from_quat instead?
      } else {
        ROS_ERROR("Collision object is not a box");
      }
    } else {
      ROS_ERROR("No support surface in planning scene with name %s", goal->support.c_str());
    }
  }

#ifdef SIMULATION
  bool simulation=true;
#else
  bool simulation=false;
#endif
  const std::vector<moveit_msgs::PlaceLocation> places =
      generatePlaces(objectInGripper, place_position.x, place_position.y, place_position.z, goal_size_x, goal_size_y, angle, simulation);
  ROS_INFO("generatePlaces() gave %lu place locations", places.size());
  if (places.empty()) {
    result.result_code = 99999; // FAILURE
    place_as->setAborted(result, "no place found");
    return;
  }

  feedback.state = "generated places";
  place_as->publishFeedback(feedback);

  if (place_as->isPreemptRequested() || !ros::ok()) {
    ROS_INFO("Preempted");
    result.result_code = -7; // PREEMPTED
    place_as->setPreempted(result, "preempted"); // set the action state to preempted
    return;
  }

  ROS_INFO("Calling place ...");
  moveit::planning_interface::MoveItErrorCode moveit_result;
  const bool plan_only = false; // true=plan only, false=also execute
  const bool place_eef = false;
  bool allow_gripper_support_collision = false;
  ROS_INFO_STREAM("Setting support surface " << goal->support);
  if(group == nullptr) {
    ROS_ERROR("No MoveGroupInterface!");
  }
  group->setSupportSurfaceName(goal->support);
  //moveit_result = group->place(objectInGripper.id, places, plan_only);
  moveit_result = my_place(group, objectInGripper.id, places, goal->support, allow_gripper_support_collision,place_eef, plan_only);

  if (!moveit_result) {
    ROS_INFO("First try failed, trying again with support surface collision allowed ...");
    allow_gripper_support_collision = true;
    //moveit_result = group->place(objectInGripper.id, places, plan_only);
    moveit_result = my_place(group, objectInGripper.id, places, goal->support, allow_gripper_support_collision, place_eef, plan_only);
  }
  group->setSupportSurfaceName("");
  ROS_INFO("done");

  if (!moveit_result) {
    ROS_ERROR("place failed with code %i", moveit_result.val);
    result.result_code = moveit_result.val;
    place_as->setAborted(result, "place failed");
    return;
  }

  result.result_code = 1; // SUCCESS
  place_as->setSucceeded(result, "success");
  ROS_INFO("Place finished successfully");
}

/**
 * Start with a seed at (0,0). Add all neighbouring pixels that fulfil a condition. Repeat with new seeds until all pixels are 
 * @param[in] img the input image
 * @param[out] out the output image, the regions are marked with labels // TODO maybe a std::set with the pixels instead?
 * @param[out] map the mean of each region
 * @param[out] num_pixels the number of pixels for each region
 */
void successiveRegionGrowing(const cv::Mat& img, const cv::Mat& mask, cv::Mat_<std::uint16_t>& out, std::vector<cv::Vec3b>& map, std::vector<std::size_t>& num_pixels) {
    map.clear();
    num_pixels.clear();
    map.reserve(256);
    num_pixels.reserve(256);
    // TODO check parameters: sizes, types, possibly resize
    std::function<bool(cv::Vec3f,cv::Vec3b)> same = [](cv::Vec3f a, cv::Vec3b b)->bool {
        return std::sqrt(seg_weight[0]*std::pow(a.val[0]-b.val[0], 2)+seg_weight[1]*std::pow(a.val[1]-b.val[1], 2)+seg_weight[2]*std::pow(a.val[2]-b.val[2], 2))<100.0;
        //return (std::abs(a.val[0]-b.val[0])+std::abs(a.val[1]-b.val[1])+std::abs(a.val[2]-b.val[2]))<20.0; // TODO sqrt+pow instead of abs? TODO mean of region!
    };
    cv::Mat_<std::uint8_t> unassigned=cv::Mat::zeros(img.rows, img.cols, CV_8UC1);
    for(int r=0; r<out.rows; ++r) {
        for(int c=0; c<out.cols; ++c) {
            if(mask.at<cv::Vec3b>(r, c).val[0]==0) {
                unassigned(r, c)=1;
            }
        }
    }
    cv::Vec3f mean; // no need to init mean, because it will be ignored later if num==0
    size_t num=0; // number of pixels in the current region
    std::function<void(int,int,std::uint16_t)> grow_region = [&same, &grow_region, &img, &unassigned, &out, &mean, &num](int row, int col, std::uint16_t label)->void {
        //printf("row=%i, col=%i, label=%u\n", row, col, label);
        std::set<std::pair<int, int>> next; // TODO maybe sorted by similarity to mean?
        next.insert({row, col});
        while(!next.empty()) {
            const auto cur=next.begin();
            //printf("%i, %i, size=%lu\n", cur->first, cur->second, next.size());
            const cv::Vec3f pixel(img.at<cv::Vec3b>(cur->first, cur->second).val[0], img.at<cv::Vec3b>(cur->first, cur->second).val[1], img.at<cv::Vec3b>(cur->first, cur->second).val[2]);
            out(cur->first, cur->second)=label;
            unassigned(cur->first, cur->second)=1;
            mean=((num+0.0f)*mean+pixel)/(num+1.0f);
            ++num;
        
            if(cur->first>0 && unassigned(cur->first-1, cur->second)==0 && same(mean, img.at<cv::Vec3b>(cur->first-1, cur->second)))
                next.insert(std::make_pair(cur->first-1, cur->second  ));
            if(cur->first+1<img.rows && unassigned(cur->first+1, cur->second)==0 && same(mean, img.at<cv::Vec3b>(cur->first+1, cur->second)))
                next.insert(std::make_pair(cur->first+1, cur->second  ));
            if(cur->second>0 && unassigned(cur->first, cur->second-1)==0 && same(mean, img.at<cv::Vec3b>(cur->first, cur->second-1)))
                next.insert(std::make_pair(cur->first  , cur->second-1));
            if(cur->second+1<img.cols && unassigned(cur->first, cur->second+1)==0 && same(mean, img.at<cv::Vec3b>(cur->first, cur->second+1)))
                next.insert(std::make_pair(cur->first  , cur->second+1));
            next.erase(cur);
        }
        
    };
    std::uint16_t label=0; // the label of the current region
    for(int r=0; r<out.rows; ++r) {
        for(int c=0; c<out.cols; ++c) {
            if(unassigned(r, c)==0) {
                printf("Growing new region with label %u, starting at (%i, %i)\n", label, r, c);
                num=0;
                grow_region(r, c, label);
                printf("mean=(%g, %g, %g), num=%lu\n", mean.val[0], mean.val[1], mean.val[2], num);
                map.push_back(cv::Vec3b(mean.val[0], mean.val[1], mean.val[2])); // TODO round?
                num_pixels.push_back(num);
                ++label;
                //if(label==255) { // TODO
                //    printf("Too many regions\n");
                //    return;
                //}
            }
        }
    }
    printf("highest label is %u\n", label);
}

bool findCutleryAndDishesService(
    clf_object_recognition_msgs::Detect3D::Request& req,
    clf_object_recognition_msgs::Detect3D::Response& res) {
  //Goal: Cutlery: bbox on table. Dishes: 3D model (2 circles)
  std_msgs::Header orig_header;
  Eigen::Isometry3d transform;
  sensor_msgs::Image rgb_image;
  pcl::PointCloud<point_t>::Ptr raw_point_cloud=getSceneData(*nh, tfBuffer, cam_model, depth_image_topic, rgb_image_topic, orig_header, transform, rgb_image);
  auto img=cv_bridge::toCvCopy(rgb_image);
  //find table
  pcl::PointCloud<point_t>::Ptr point_cloud = getCloudROI(raw_point_cloud, 0.0, 3.0, -2.0, 2.0, 0.1, 2.0); // Remove ground
  try {
    pcl::io::savePCDFile ("/tmp/cloud_filtered_ROI.pcd", *point_cloud, true);
  } catch (pcl::IOException &e) {
    ROS_WARN("%s", e.what());
  }
  
  plane_t * table=searchAndAddTableToScene(point_cloud, orig_header.frame_id, cam_model, /*onlyHorizontalPlanes=*/ true, /*addToPlanningScene=*/ false, makeTableToBox, tableSafetyMargin); // Here point_cloud must be in BASE_FRAME
  try {
    pcl::io::savePCDFile ("/tmp/cloud_wo_table.pcd", *point_cloud, true);
  } catch (pcl::IOException &e) {
    ROS_WARN("%s", e.what());
  }
  if(table == nullptr) {
    ROS_ERROR("findCutleryAndDishesService: no table, cannot continue");
    return false;
  }
  cv::Point * hull=(cv::Point *) malloc(table->hull.size() * sizeof(cv::Point));
  for(size_t i=0; i<table->hull.size(); ++i) {
    ROS_INFO("Hull point: (%g,%g,%g)", table->hull[i].x, table->hull[i].y, table->hull[i].z);
    const Eigen::Vector3d p_tf=transform*Eigen::Vector3d(table->hull[i].x, table->hull[i].y, table->hull[i].z);
    ROS_INFO("Transformed hull point: (%g,%g,%g)", p_tf.x(), p_tf.y(), p_tf.z());
    cv::Point2d point=cam_model->project3dToPixel(cv::Point3d(p_tf.x(), p_tf.y(), p_tf.z()));
    hull[i] = cv::Point(std::round(point.x), std::round(point.y));//TODO check if in image bounds?
    ROS_INFO("Table point on image (%i,%i)", hull[i].x, hull[i].y);
  }
  cv::Mat table_mask=cv::Mat::zeros(img->image.rows, img->image.cols, CV_8UC3);
  for(int i=0; i<40; i+=10) {
    for(size_t j=0; j<table->hull.size(); ++j) {
        hull[j].y-=10;
    }
    cv::fillConvexPoly(table_mask, hull, table->hull.size(), cv::Scalar(255.0, 255.0, 255.0), cv::LINE_8, 0);
  }
  cv::imwrite("/tmp/table_mask.png", table_mask);
  //do segmentation (considering table_mask)
  cv::Mat_<std::uint16_t> regions=cv::Mat::zeros(img->image.rows, img->image.cols, CV_16UC1);
  std::vector<cv::Vec3b> map;
  std::vector<std::size_t> num_pixels;
  cv::Mat img_hsv(img->image.rows, img->image.cols, CV_8UC3);
  cv::cvtColor(img->image, img_hsv, cv::COLOR_BGR2HSV); // TODO or cv::COLOR_BGR2HSV
  successiveRegionGrowing(img_hsv, table_mask, regions, map, num_pixels);
  cv::Mat clustered=cv::Mat::zeros(img->image.rows, img->image.cols, CV_8UC3);
  std::vector<cv::Point> minimum(map.size(), cv::Point(img->image.cols, img->image.rows));
  std::vector<cv::Point> maximum(map.size());
  for(int r=0; r<clustered.rows; ++r) {
    for(int c=0; c<clustered.cols; ++c) {
      if(table_mask.at<cv::Vec3b>(r, c).val[0]==255) {
        clustered.at<cv::Vec3b>(r, c)=map.at(regions(r, c));
        minimum.at(regions(r, c)).x=std::min(minimum.at(regions(r, c)).x, c);
        minimum.at(regions(r, c)).y=std::min(minimum.at(regions(r, c)).y, r);
        maximum.at(regions(r, c)).x=std::max(maximum.at(regions(r, c)).x, c);
        maximum.at(regions(r, c)).y=std::max(maximum.at(regions(r, c)).y, r);
      }
    }
  }
  cv::cvtColor(clustered, clustered, cv::COLOR_HSV2RGB);
  cv::imwrite("/tmp/segmented_regions.png", regions);
  cv::imwrite("/tmp/segmented_regions_color.png", clustered);
  vision_msgs::Detection3DArray det_msg;
  det_msg.header = orig_header;
  shape_msgs::Plane table_tf=transformPlane(-table->plane.coef[0], -table->plane.coef[1], -table->plane.coef[2], -table->plane.coef[3], transform); // TODO why is negation necessary?
  for(std::size_t i=0; i<map.size(); ++i) { // All regions
    // dishes
    minimum[i].x = std::max(minimum[i].x-1, 0);
    minimum[i].y = std::max(minimum[i].y-1, 0);
    maximum[i].x = std::min(maximum[i].x+1, img->image.cols);
    maximum[i].y = std::min(maximum[i].y+1, img->image.rows);
    if((maximum[i].x-minimum[i].x)>20 && (maximum[i].y-minimum[i].y)>20) {
        ROS_INFO("Considering region %lu", i);
        cv::RotatedRect ellipse1;
        cv::RotatedRect ellipse2;
        const double quality=dishFitting(img->image(cv::Rect(minimum[i], maximum[i])), i, ellipse1, ellipse2);
        ellipse1.center.x+=minimum[i].x;
        ellipse1.center.y+=minimum[i].y;
        ellipse2.center.x+=minimum[i].x;
        ellipse2.center.y+=minimum[i].y;
        ROS_INFO("dishFitting returned quality %g", quality);
        if(quality<30) goto cutlery;
        moveit_msgs::CollisionObject dish;
        dish.header.frame_id = BASE_FRAME;
        dish.operation = dish.ADD;
        std::stringstream sstream;
        sstream << "dish" << i;
        dish.id = sstream.str();
        dish.mesh_poses.resize(1);
        dish.mesh_poses[0].orientation.x = 0.0;
        dish.mesh_poses[0].orientation.y = 0.0;
        dish.mesh_poses[0].orientation.z = 0.0;
        dish.mesh_poses[0].orientation.w = 1.0;
        // Compute the base point
        cv::Point3d ray2=cam_model->projectPixelTo3dRay(cv::Point2d(ellipse2.center.x, ellipse2.center.y)); // TODO or switched?
        double bcx, bcy, bcz; // base center
        if(intersectionPointOfLineAndPlane(table_tf.coef[0], table_tf.coef[1], table_tf.coef[2], table_tf.coef[3], ray2.x, ray2.y, ray2.z, bcx, bcy, bcz)) {
            //ROS_INFO("Intersection point of line and plane: (%g,%g,%g)", bcx, bcy, bcz);
            Eigen::Vector3d base_tf=transform.inverse()*Eigen::Vector3d(bcx, bcy, bcz);
            dish.mesh_poses[0].position.x = base_tf.x();
            dish.mesh_poses[0].position.y = base_tf.y();
            dish.mesh_poses[0].position.z = base_tf.z();
            ROS_INFO("Base point is (%g,%g,%g) (in base frame) or (%g,%g,%g) (in camera frame)", base_tf.x(), base_tf.y(), base_tf.z(), bcx, bcy, bcz);
        } else {
            ROS_ERROR("intersectionPointOfLineAndPlane failed (x=%g, y=%g)", ellipse2.center.x, ellipse2.center.y);
        }
        // Compute the height
        double height=0.0;
        cv::Point3d ray3=cam_model->projectPixelTo3dRay(cv::Point2d(ellipse1.center.x, ellipse1.center.y)); // TODO or switched?
        Eigen::Vector3d upper_base_middle;
        Eigen::Vector3d second_point;
        if(nearestIntersectionBetweenLines(Eigen::Vector3d(bcx, bcy, bcz), Eigen::Vector3d(table_tf.coef[0], table_tf.coef[1], table_tf.coef[2]), Eigen::Vector3d(0.0, 0.0, 0.0), Eigen::Vector3d(ray3.x, ray3.y, ray3.z), &upper_base_middle, &second_point)) {
            ROS_INFO("upper_base_middle=(%g,%g,%g)", upper_base_middle.x(), upper_base_middle.y(), upper_base_middle.z());
            const double dist_second_p_upper_base_middle=std::sqrt(std::pow(second_point.x()-upper_base_middle.x(), 2)+std::pow(second_point.y()-upper_base_middle.y(), 2)+std::pow(second_point.z()-upper_base_middle.z(), 2));
            if(dist_second_p_upper_base_middle>0.1) {
                ROS_WARN("dist_second_p_upper_base_middle=%g, seems too large", dist_second_p_upper_base_middle);
                goto cutlery;
            }
            height=std::sqrt(std::pow(bcx-upper_base_middle.x(), 2)+std::pow(bcy-upper_base_middle.y(), 2)+std::pow(bcz-upper_base_middle.z(), 2));
        } else {
            ROS_WARN("NearestIntersectionBetweenLines failed");
        }
        // Determine base and top radius
        double base_radius=0.03;
        double top_radius=0.05;
        cv::Point2d upper_ellipse_edge;
        if(ellipse1.size.width>ellipse1.size.height) {
            upper_ellipse_edge.x=ellipse1.center.x+( std::cos(ellipse1.angle/180.0*M_PI)*ellipse1.size.width/2.0);
            upper_ellipse_edge.y=ellipse1.center.y+( std::sin(ellipse1.angle/180.0*M_PI)*ellipse1.size.width/2.0); // Switched sign on sina because negative angle is desired
        } else {
            upper_ellipse_edge.x=ellipse1.center.x+(-std::sin(ellipse1.angle/180.0*M_PI)*ellipse1.size.height/2.0); // Switched sign on sina because negative angle is desired
            upper_ellipse_edge.y=ellipse1.center.y+( std::cos(ellipse1.angle/180.0*M_PI)*ellipse1.size.height/2.0);
        }
        cv::Point3d ray4=cam_model->projectPixelTo3dRay(upper_ellipse_edge); // TODO switch coordinates?
        double ix, iy, iz;
        if(intersectionPointOfLineAndPlane(table_tf.coef[0], table_tf.coef[1], table_tf.coef[2], table_tf.coef[3]+height, ray4.x, ray4.y, ray4.z, ix, iy, iz)) {
            ROS_INFO("Upper ellipse edge is at (%g,%g,%g), projected from (%g, %g)", ix, iy, iz, upper_ellipse_edge.x, upper_ellipse_edge.y);
            top_radius=std::sqrt(std::pow(ix-upper_base_middle.x(), 2)+std::pow(iy-upper_base_middle.y(), 2)+std::pow(iz-upper_base_middle.z(), 2));
        } else {
            ROS_ERROR("intersectionPointOfLineAndPlane failed (x=%g, y=%g)", ellipse1.center.x, ellipse1.center.y);
        }
        cv::Point2d lower_ellipse_edge;
        if(ellipse2.size.width>ellipse2.size.height) {
            lower_ellipse_edge.x=ellipse2.center.x+( std::cos(ellipse2.angle/180.0*M_PI)*ellipse2.size.width/2.0);
            lower_ellipse_edge.y=ellipse2.center.y+( std::sin(ellipse2.angle/180.0*M_PI)*ellipse2.size.width/2.0); // Switched sign on sina because negative angle is desired
        } else {
            lower_ellipse_edge.x=ellipse2.center.x+(-std::sin(ellipse2.angle/180.0*M_PI)*ellipse2.size.height/2.0); // Switched sign on sina because negative angle is desired
            lower_ellipse_edge.y=ellipse2.center.y+( std::cos(ellipse2.angle/180.0*M_PI)*ellipse2.size.height/2.0);
        }
        cv::Point3d ray5=cam_model->projectPixelTo3dRay(lower_ellipse_edge); // TODO switch coordinates?
        if(intersectionPointOfLineAndPlane(table_tf.coef[0], table_tf.coef[1], table_tf.coef[2], table_tf.coef[3], ray5.x, ray5.y, ray5.z, ix, iy, iz)) {
            ROS_INFO("Lower ellipse edge is at (%g,%g,%g), projected from (%g, %g)", ix, iy, iz, lower_ellipse_edge.x, lower_ellipse_edge.y);
            base_radius=std::sqrt(std::pow(ix-bcx, 2)+std::pow(iy-bcy, 2)+std::pow(iz-bcz, 2));
        } else {
            ROS_ERROR("intersectionPointOfLineAndPlane failed (x=%g, y=%g)", ellipse2.center.x, ellipse2.center.y);
        }
        if(base_radius>0.15 || top_radius>0.15 || height>0.15) {
            ROS_WARN("Dimensions seem too large: base_radius=%g, top_radius=%g, height=%g", base_radius, top_radius, height);
            goto cutlery;
        }
        dish.meshes.push_back(generateDish(base_radius, top_radius, height, 16)); // TODO real base_radius and upper_radius
        if(applyCollisionObject(dish)) {
            ROS_INFO("Added dish to planning scene");
        } else ROS_ERROR("Adding dish to planning scene failed!");
    }
    
    
    // cutlery
    cutlery:
    if(num_pixels.at(i)<20 || num_pixels.at(i)>5000) { // TODO tune values
        ROS_DEBUG("Region %lu has %lu pixels, probably not cutlery", i, num_pixels.at(i));
        continue;
    }
    ROS_INFO("Considering region %lu with %lu pixels and color (%u,%u,%u)", i, num_pixels.at(i), map.at(i).val[0], map.at(i).val[1], map.at(i).val[2]);
    pcl::PointCloud<point_t>::Ptr object_points(new pcl::PointCloud<point_t>);
    for(int r=0; r<regions.rows; ++r) {
      for(int c=0; c<regions.cols; ++c) {
        if(regions(r, c)==i) {
          cv::Point3d ray=cam_model->projectPixelTo3dRay(cv::Point2d(c, r)); // TODO or r, c?
          double ix, iy, iz;
          if(intersectionPointOfLineAndPlane(table_tf.coef[0], table_tf.coef[1], table_tf.coef[2], table_tf.coef[3], ray.x, ray.y, ray.z, ix, iy, iz)) {
              //ROS_INFO("Intersection point of line and plane: (%g,%g,%g)", ix, iy, iz);
              point_t point;
              point.x = ix;
              point.y = iy;
              point.z = iz;
              object_points->push_back(point);
          } else {
              ROS_ERROR("intersectionPointOfLineAndPlane failed (r=%i, c=%i)", r, c);
          }
        }
      }
    }
    ROS_INFO("Size of object_points=%lu", object_points->points.size());
    if(object_points->points.size()<10) continue;
    pcl::PointCloud<point_t>::Ptr hull_points(new pcl::PointCloud<point_t>);
    pcl::ConvexHull<point_t> chull;
    std::vector<pcl::Vertices> polygons;
    chull.setInputCloud(object_points);
    chull.setDimension(2); // Will be determined automatically if not set
    chull.reconstruct(*hull_points, polygons);
    ROS_INFO("Hull has %lu points", hull_points->points.size());
    for(size_t j=0; j<hull_points->points.size(); ++j) {
        ROS_DEBUG("Hull point: (%g,%g,%g)", hull_points->points[j].x, hull_points->points[j].y, hull_points->points[j].z);
    }
    vision_msgs::BoundingBox3D bbox=minimum_bbox_of_plane(*hull_points, polygons[0], table_tf); // TODO or find bbox in image and project that? is it still a box then?
    //test if dimensions of bbox are right for cutlery
    ROS_INFO_STREAM("Possibly found cutlery: " << bbox);
    if(bbox.size.x > 0.25 || bbox.size.x < 0.06 || bbox.size.y > 0.07 || bbox.size.y < 0.005) { // TODO maybe tune values
        ROS_INFO("BBox has size (%g,%g), probably not cutlery", bbox.size.x, bbox.size.y);
        continue;
    }
    //const float prob=std:exp(-std::pow(bbox.size.x-0.2, 2)+std::pow(bbox.size.y-0.03, 2));
    vision_msgs::Detection3D det;
    det.bbox = bbox;
    //TODO set header, results, source_cloud?
    det_msg.detections.push_back(det);
    
  }
  ROS_INFO("Publishing %lu detections", det_msg.detections.size());
  detection_pub.publish(det_msg);
  delete table; // free memory
  return true;
}

int main(int argc, char **argv) {
  ROS_INFO("Starting node ...");
  ros::init(argc, argv, "grasping_object_recognition");
  ros::NodeHandle new_handle("~");
  nh = &new_handle;
  
  //ROS_INFO("Getting params ...");
  //nh->param<std::string>("pointcloud_topic", pointcloud_topic, "/xtion/depth_registered/points");
  //nh->param<std::string>("rgb_image_topic", rgb_image_topic, "/xtion/rgb/image_raw");
  //ROS_INFO_STREAM("Params are: pointcloud_topic=" << pointcloud_topic << ", rgb_image_topic=" << rgb_image_topic);
  //std::string objects_yaml_path;
  //nh->getParam("objects_config_path", objects_yaml_path);
  //ROS_INFO_STREAM("Got objects_config_path:>" << objects_yaml_path << "<");
  //parseConfigYAML(objects_yaml_path);
  
  tf2_ros::TransformListener newListener(tfBuffer);
  tfListener = &newListener;

  ROS_INFO("Initializing sub-modules ...");
  init_high_level(nh, &tfBuffer);
  init_plane_publisher(*nh, "/planes");
  init_grasp_publisher(*nh);
  initActionClients(*nh, ros::WallDuration(1.0));
  detection_pub = nh->advertise<vision_msgs::Detection3DArray>("/object_detections", 100);

  dynamic_reconfigure::Server<grasping_object_recognition::dyn_reconfConfig> server;
  dynamic_reconfigure::Server<grasping_object_recognition::dyn_reconfConfig>::CallbackType f;
  f = boost::bind(&reconfigure_callback, _1, _2);
  server.setCallback(f);
  
  ROS_INFO("Getting camera info ... (5s timeout)");// only do this once, since it does not change
  boost::shared_ptr<sensor_msgs::CameraInfo const> sharedCameraInfo = ros::topic::waitForMessage<sensor_msgs::CameraInfo>(rgb_camera_info_topic, *nh, ros::Duration(5, 0));
  ROS_INFO("done");
  if (sharedCameraInfo != nullptr) {
      cam_model=new image_geometry::PinholeCameraModel();
      cam_model->fromCameraInfo(*sharedCameraInfo);
  } else {
      ROS_WARN("Failed to get camera info");
  }

  moveit::planning_interface::MoveGroupInterface new_group("arm_torso"); // can be arm or arm_torso
  group = &new_group;

  ROS_INFO("Advertising services ...");
  ros::ServiceServer tableService =
      nh->advertiseService("findTable", findTableService);
  ros::ServiceServer shelfService =
      nh->advertiseService("findShelf", findShelfService);
  ros::ServiceServer shelfAndObjectsService =
      nh->advertiseService("findShelfAndObjects", findShelfAndObjectsService);
  ros::ServiceServer objectsService =
      nh->advertiseService("findObjects", findObjectsInROIService);
  ros::ServiceServer learnService =
      nh->advertiseService("learnObjects", learnObjectsService);
  ros::ServiceServer targetService =
      nh->advertiseService("computeTarget", computeTargetRectangleService);
  ros::ServiceServer attachTestService =
      nh->advertiseService("attachTestObject", attachTestObjectService);
  ros::ServiceServer findDishesService =
      nh->advertiseService("findCutleryAndDishes", findCutleryAndDishesService);
  
  actionlib::SimpleActionServer<storing_groceries_msgs::PlaceOnRectangleAction>
      new_place_as(*nh, "placeObject", boost::bind(&placeObjectOnRectangleCB, _1),
                   false);
  place_as = &new_place_as;
  place_as->start();

  ROS_INFO("Services are running, node is reading, spinning ...");

  ros::spin(); // For the action servers
  return 0;
}
