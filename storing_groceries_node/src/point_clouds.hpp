/**
 * @file
 * @author  Markus Vieth
 * @section DESCRIPTION
 *
 * Point cloud functionality
 */
#pragma once
#include <pcl/point_cloud.h>
#include <pcl/point_types.h> // for PointXYZRGBA, PointXYZRGBNormal
namespace image_geometry { class PinholeCameraModel; }
namespace tf2_ros { class Buffer; }
namespace ros { class NodeHandle; }
// msgs
#include <vision_msgs/Detection3D.h>
#include <geometry_msgs/Point.h>
#include <shape_msgs/Plane.h>
#include <moveit_msgs/CollisionObject.h>
#include <sensor_msgs/Image.h>

#define DISTANCE_THRESHOLD 0.008
#define MIN_NUM_POINTS_SEG 5000

//typedef pcl::PointXYZ point_t;
typedef pcl::PointXYZRGBA point_t;
//typedef pcl::PointXYZRGBNormal point_t;

typedef struct {
  shape_msgs::Plane plane;
  vision_msgs::BoundingBox3D bbox;
  std::vector<geometry_msgs::Point> hull; // TODO maybe replace with geometry_msgs/Polygon?
  pcl::PointCloud<point_t>::Ptr cloud;
} plane_t;

/**
 * Init the planeviz publisher
 * @param nh A pointer to a node handle
 * @param topic The topic name to publish plane markers to, or leave empty to not publish anything
 */
void init_plane_publisher(ros::NodeHandle &nh, const std::string& topic="");

void deleteAllPlaneMarkers();

/**
 * Wait for a pointcloud message and return it as a pcl pointcloud.
 * @param pointcloud_topic is the topic on which should be listened
 * @param transform controls whether the cloud should be transformed to base_footprint
 * @param orig_frame the original frame of the cloud is stored here
 */
//template <typename type>
pcl::PointCloud<point_t>::Ptr
getPointCloud(ros::NodeHandle &nh,
              const tf2_ros::Buffer &tfBuffer,
              const std::string& pointcloud_topic,
              const bool transform = true,
              std::string *orig_frame = nullptr);

pcl::PointCloud<point_t>::Ptr getSceneData(ros::NodeHandle &nh, const tf2_ros::Buffer &tfBuffer, const image_geometry::PinholeCameraModel * cam_model, const std::string& depth_image_topic, const std::string& rgb_image_topic, std_msgs::Header& orig_header, Eigen::Isometry3d& transform, sensor_msgs::Image& rgb_image, uint8_t mode=0);

//https://stackoverflow.com/a/17820615
std::string type2str(int type);

/**
 * Combines a depth image and an rgb image to a point cloud.
 * @param depth_image_msg[in]
 * @param rgb_image_msg[in] may be NULL
 * @param cam_model[in]
 * @return the constructed point cloud, colored if rgb_image_msg is not NULL
 */
pcl::PointCloud<point_t>::Ptr depthImageToPointcloud(const sensor_msgs::Image& depth_image_msg, const sensor_msgs::Image * rgb_image_msg, const image_geometry::PinholeCameraModel& cam_model);

/**
 * Gets some images from a topic and averages them to reduce noise. Intended for depth images.
 * @param topic The image topic (depth images). Encoding should be 32FC1
 * @param num How many images to average
 * @param delay The delay between getting two images in seconds
 */
sensor_msgs::Image averageImages(ros::NodeHandle& nh, const std::string& topic, size_t num=10, float delay=0.1);
sensor_msgs::Image  medianImages(ros::NodeHandle& nh, const std::string& topic, size_t num=10, float delay=0.1);

/**
 * Find planes in a pointcloud. The points belonging to the planes are extracted from the cloud, making it smaller.
 * @param cloud search planes in this cloud
 * @param mode mode=0->search for horizontal planes (e.g. table surfaces). mode=1->search for vertical planes (e.g. walls, doors, shelf sides, ...). mode=2->no constraints
 * @param max_num_planes search for at most this many planes
 * @param min_num_points_seg each plane should have at least this many point belonging to it
 * @param dist_threshold the points must be at least this close to a plane to be considered part of it
 */
std::vector<plane_t>
findPlanes(pcl::PointCloud<point_t>::Ptr cloud,
           const int mode,
           const unsigned int max_num_planes = 100,
           const unsigned int min_num_points_seg = MIN_NUM_POINTS_SEG,
           const float dist_threshold = DISTANCE_THRESHOLD);
/**
 * Find a sphere in the given point cloud using samples consensus methods.
 * @return the average distance of all points in the cloud to the found model.
 */
double fit_sphere(pcl::PointCloud<point_t>::ConstPtr cloud, moveit_msgs::CollisionObject& collision_object); // TODO pcl::PointCloud instead?
/**
 * Find a box in the given point cloud using samples consensus methods. There must be at least two side of the box (two planes) in the cloud.
 * @return the average distance of all points in the cloud to the found model.
 */
double fit_box(pcl::PointCloud<point_t>::ConstPtr cloud, moveit_msgs::CollisionObject& collision_object); // TODO pcl::PointCloud instead?
/**
 * Find a cylinder in the given point cloud using samples consensus methods.
 * @return the average distance of all points in the cloud to the found model.
 */
double fit_cylinder(pcl::PointCloud<point_t>::ConstPtr cloud, moveit_msgs::CollisionObject& collision_object); // TODO pcl::PointCloud instead?

void fitDish(pcl::PointCloud<pcl::PointXYZRGBA>::ConstPtr cloud_in, double& height, double& base_r, double& top_r, geometry_msgs::Pose& pose);

/**
 * Segment the cloud using EuclideanClusterExtraction and return all clusters as Detection3D
 * @param src_frame is the frame that cloud is in
 * @param zScaling can be used to modify the merging of clusters in z-direction
 */
std::vector<vision_msgs::Detection3D>
segmentation(pcl::PointCloud<point_t>::ConstPtr cloud,
             const std::string& src_frame,
             const float zScaling = 1.0f);

/**
 * This is actually similar to segmentation, but it also applies a voxel grid filter, removes the ground, and other stuff.
 * Only the largest cluster is returned.
 */
pcl::PointCloud<point_t>::Ptr
cluster(pcl::PointCloud<point_t>::ConstPtr cloud, bool do_voxelgrid = true, bool do_passthrough = true); // TODO make voxelgrid and z-Passthrough optional

/**
 * Remove all points that are not in the specified 3D region of interest (axis-aligned)
 */
pcl::PointCloud<point_t>::Ptr
getCloudROI(pcl::PointCloud<point_t>::ConstPtr cloud,
            const float xmin, const float xmax,
            const float ymin, const float ymax,
            const float zmin, const float zmax);

/**
 * Remove all points that are not in the specified 3D region of interest (bbox)
 */
pcl::PointCloud<point_t>::Ptr
getCloudROI(pcl::PointCloud<point_t>::ConstPtr cloud,
            const vision_msgs::BoundingBox3D& bbox);
