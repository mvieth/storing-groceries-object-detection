/**
 * @file
 * @author  Markus Vieth
 * @section DESCRIPTION
 *
 * High level functions
 */
#pragma once
// general
#include <pcl/point_cloud.h>
namespace tf2_ros { class Buffer; }
namespace image_geometry { class PinholeCameraModel; }

// msgs
#include <vision_msgs/Detection3D.h>
#include <sensor_msgs/Image.h>
#include <moveit_msgs/CollisionObject.h>
#include <moveit_msgs/AttachedCollisionObject.h>

// own
#include "point_clouds.hpp"
#include "object_knowledge.hpp" // primitive_t

// Provide a method to set the tracking id for Detection3D, if defined (newer versions).
// Source: https://stackoverflow.com/a/13787502
struct general_ {};
struct special_ : general_ {};
template<typename> struct int_ { typedef int type; };

template <typename detection, typename int_<decltype(detection::is_tracking)>::type = 0>
void set_tracking_id_if_defined(detection& det, const std::string& id, special_) {
    ROS_INFO("Detected new Detection3D version, setting tracking id!");
    det.is_tracking = true;
    det.tracking_id = id;
}

template <typename detection>
void set_tracking_id_if_defined(detection& /*det*/, const std::string& /*id*/, general_) {
    ROS_INFO("Detected old Detection3D version, not setting tracking id!");
}

void init_high_level(ros::NodeHandle *nh, tf2_ros::Buffer *buf);

/** Accessor to the planning scene
 */
bool applyCollisionObject(const moveit_msgs::CollisionObject& collision_object);

/** Accessor to the planning scene
 */
bool applyAttachedCollisionObject(const moveit_msgs::AttachedCollisionObject &attached_collision_object);

/** Accessor to the planning scene
 */
std::map<std::string, moveit_msgs::CollisionObject> getObjects(const std::vector<std::string>& object_ids = std::vector<std::string>());

/** Accessor to the planning scene
 */
std::map<std::string, moveit_msgs::AttachedCollisionObject> getAttachedObjects();

bool addPointCloudAsOctomap(pcl::PointCloud<point_t>::ConstPtr pcl_cloud);

void set_high_level_options(bool useAlternativeFitting);

void publishCOasMarker(const moveit_msgs::CollisionObject& co, int id);

double dishFitting(const cv::Mat& img, size_t i, cv::RotatedRect& ellipse_out1, cv::RotatedRect& ellipse_out2);

/**
 * \brief Searches a horizontal surface (i.e. a table) in a point cloud, removes the points that belong to it, and optionally adds the surface in the moveit planning scene
 * @param cloud is a point cloud (must be in BASE_FRAME!)
 * @param frame is the original frame of the cloud
 * @param cam_model the camera model, can be NULL
 * @param onlyHorizontalPlanes 
 * @param addToPlanningScene controls whether to add the found surface/table to the moveit planning scene
 * @param makeTableToBox controls whether the planning scene object should be just the flat table surface or a big box that reaches all the way to the floor
 * @param safetyMargin add a padding to the table
 * @return plane, bbox, and hull describe the found surface. Has to be deleted (if not nullpointer)
 */
plane_t * searchAndAddTableToScene(pcl::PointCloud<point_t>::Ptr cloud,
                               const std::string& frame = "xtion_rgb_optical_frame",
                               const image_geometry::PinholeCameraModel * cam_model = nullptr,
                               const bool onlyHorizontalPlanes = true,
                               const bool addToPlanningScene = true,
                               const bool makeTableToBox = true,
                               const float safetyMargin = 0.06);

/**
 * Get the size of the shelf.
 */
void getShelfSize(double * shelf_height_p=nullptr, double * shelf_width_p=nullptr, double * shelf_depth_p=nullptr, std::vector<float> * shelf_floor_heights_p=nullptr);

/**
 * \brief The robot should look straight ahead, not down or up. It should see the backwall and at least one side of the shelf
 * @param point_cloud must be in BASE_FRAME // TODO remove all of the shelf from point_cloud, and nothing but the shelf.
 * @return true if the search was successful and the parameters (x, y, angle) are good enough
 */
bool searchForShelf(pcl::PointCloud<point_t>::Ptr point_cloud, float &x, float &y, float &angle);

/**
 * \brief Add the shelf (dimensions known) to the planning scene
 * @param x the x coordinate of the middle of the shelf on the ground
 * @param y the y coordinate of the middle of the shelf on the ground
 * @param angle the angle in radians, 0 means opening is towards origin
 * @param point_cloud all points that belong to the shelf are removed from this cloud
 * @return true if successful, else false
 */
bool addShelfToPlanningScene(const float x, const float y, const float angle, pcl::PointCloud<point_t>::Ptr point_cloud=nullptr);

/**
 * \brief Given an object (name) and the position of the shelf (dimensions known), compute where to put it (rectangle)
 *
 * Idea: search (1D, each shelf plate individually) for the highest similarity (like probability density estimation).
 * Solution for now: just place it next to another object of the same category, else place it on the second shelf floor
 * Gets the objects from moveit's planning scene TODO better solution?
 * @param[in] name can be something like "apple" or "apple123"
 * @param[out] object the collision object with name "name", if found in planning scene
 * @return the name of the support surface in the planning scene (a shelf floor)
 */
std::string computeTargetPosition(const std::string& name, double &x, double &y,
                                  double &z, double &sizex, double &sizey,
                                  double &angle, float shelf_x, float shelf_y,
                                  float shelf_angle, moveit_msgs::CollisionObject * object = nullptr); // TODO some kind of return value that communicates the quality of the result?

/**
 * \brief Given a geometric primitive (shape, size, position, orientation) and a plane, adapt position and orientation so that they are physically likely: E.g. a box or a cylinder would not balance on an edge, no body would float in the air; instead one face would completely touch the plane.
 * @param plane only the plane.plane component is used TODO use shape_msgs::Plane instead?
 */
void primitive_stability(plane_t plane, const Eigen::Vector3f& gravity, primitive_t shape, const Eigen::Vector3f& size, Eigen::Vector3f& position, Eigen::Quaternionf& orientation); // TODO why use plane (normal) AND gravity?
void primitive_stability_wrapper(plane_t& plane, const Eigen::Vector3f& gravity, moveit_msgs::CollisionObject& collision_object);

template <typename type>
void normalizeProbabilities(std::map<type, float>& x);

void orderVector(Eigen::Vector3f& v, bool asc);

/**
 * x1/x2/x3 can be the same, as well as P1/P2/P3
 */
template<int s>
void fuseContinuous(Eigen::Matrix<float, s, 1>& x3, Eigen::Matrix<float, s, s>& P3, // TODO what if a P is zero? warn? abort?
                    const Eigen::Matrix<float, s, 1>& x1, const Eigen::Matrix<float, s, s>& P1,
                    const Eigen::Matrix<float, s, 1>* xb = nullptr, const Eigen::Matrix<float, s, s>* Pb = nullptr);

// fuseContinuous for Quaternionf, since that is not derived of Eigen::Matrix
void fuseContinuous(Eigen::Quaternionf& x3, Eigen::Matrix4f& P3,
                    const Eigen::Quaternionf& x1, const Eigen::Matrix4f& P1,
                    const Eigen::Quaternionf* xb = nullptr, const Eigen::Matrix4f* Pb = nullptr);

// weights specify how certain the distribution is, from 0=totally unsafe, unvaluable information to 1=absolutely sure, very good information
template<typename key>
void fuseDiscrete(std::map<key, float>& x3, float& weight3,
                  const std::map<key, float>& x1, const float weight1,
                  const std::map<key, float>& x2, const float weight2);

// Given the probabilities for the object classes and the sizes of the object classes, calculate how large the object is.
// size is in descending order (x > y > z)
bool getSizeFromObjectProbabilities(const std::map<std::string, float>& obj_probs, Eigen::Vector3f& size, Eigen::Matrix3f& var);

std::map<primitive_t, float>
getShapeProbabilitiesFromObjectProbabilities(const std::map<std::string, float>& obj_probs);

std::map<std::string, float> getObjectProbabilitiesFromShapeProbabilities(const std::map<primitive_t, float>& shape_probs);

//size must be sorted
std::map<std::string, float> getObjectProbabilitiesFromSize(const Eigen::Vector3f& size, const Eigen::Matrix3f& var);

void subimageBoundaries(pcl::PointCloud<point_t>::ConstPtr cloud, const image_geometry::PinholeCameraModel& cam_model, float& xmin, float& ymin, float& xmax, float& ymax);
void subimageBoundaries(const tf2::Vector3& position, const tf2::Quaternion& orientation, const tf2::Vector3& size, const image_geometry::PinholeCameraModel& cam_model, float& xmin, float& ymin, float& xmax, float& ymax);

/**
 * Searches (graspable) objects in a point cloud, tries to classify them, tries
 * to fit a primitive to them (cylinder, box, ...), and optionally adds them to
 * the moveit planning scene
 * Internally calls the recognition and shape fitting services
 * Originally based on clf_grasping/.../test.py
 * @param table_height is the height of the table (or horizontal surface) the objects are placed on.
 *                     If this is zero, it is not used.
 * @param cloud is the point cloud TODO only remove the points that belong to objects
 * @param src_frame is
 * @param addToPlanningScene controls whether to add the found objects to the moveit planning scene
 * @param objectIds returns the names of the objects in the moveit planning scene (whether they are actually added or not)
 * @return the Detection3Ds (containing hypotheses, point clouds, bounding boxes)
 */
std::vector<vision_msgs::Detection3D> getObjectsAndAddToPlanningScene(
    const sensor_msgs::Image& cam_image,
    const image_geometry::PinholeCameraModel& cam_model,
    pcl::PointCloud<point_t>::ConstPtr cloud,
    const std::string& src_frame,
    const Eigen::Isometry3d * transform,
    const float table_height = 0.0,
    const bool addToPlanningScene = true,
    const bool make_all_standard_cylinder = false,
    std::vector<std::string> * objectIds = nullptr);
    
/*std::vector<vision_msgs::Detection3D> getObjectsAndAddToPlanningScene2(
    const sensor_msgs::Image& cam_image,
    const image_geometry::PinholeCameraModel& cam_model,
    pcl::PointCloud<point_t>::Ptr cloud,
    const std::string src_frame,
    const Eigen::Isometry3d * transform,
    const float table_height = 0.0,
    const bool addToPlanningScene = true,
    const bool make_all_standard_cylinder = false,
    std::vector<std::string> * objectIds = nullptr);*/
