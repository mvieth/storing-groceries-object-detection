#include "bounding_boxes.hpp"
#include "util.hpp" // for distPointToLine2D
#include <ros/console.h> // for ROS_WARN, ROS_INFO, ...
#include <pcl/Vertices.h> // for Vertices
#include <pcl/common/common.h> // for getMinMax3D
#include <pcl/common/transforms.h> // for transformPointCloud
#include <pcl/common/centroid.h> // for compute3DCentroid
#include <pcl/features/moment_of_inertia_estimation.h> // for MomentOfInertiaEstimation
#include <Eigen/Eigenvalues> // for SelfAdjointEigenSolver

namespace OOBB_Calculate {
vision_msgs::BoundingBox3D calculate(const pcl::PointCloud<point_t>& temp_cloud) {
  vision_msgs::BoundingBox3D ret;

  // http://codextechnicanum.blogspot.com/2015/04/find-minimum-oriented-bounding-box-of.html
  //pcl::PCLPointCloud2 pcl_pc2;
  /*pcl_conversions::toPCL(cloud, pcl_pc2);

  pcl::PointCloud<point_t>::Ptr temp_cloud(new
  pcl::PointCloud<point_t>);
  pcl::fromPCLPointCloud2(pcl_pc2, *temp_cloud);*/

  if (temp_cloud.size() < 3) {
    ROS_ERROR_STREAM("cloud too small, could not create BB");
    return ret;
  }

  Eigen::Vector4f pcaCentroid;
  pcl::compute3DCentroid(temp_cloud, pcaCentroid);

  Eigen::Matrix3f covariance;
  computeCovarianceMatrixNormalized(temp_cloud, pcaCentroid, covariance);
  Eigen::SelfAdjointEigenSolver<Eigen::Matrix3f> eigen_solver(
      covariance, Eigen::ComputeEigenvectors);
  Eigen::Matrix3f eigenVectorsPCA = eigen_solver.eigenvectors();
  eigenVectorsPCA.col(2) = eigenVectorsPCA.col(0).cross(eigenVectorsPCA.col(1));

  // Note that getting the eigenvectors can also be obtained via the PCL PCA
  // interface with something like:
  // pcl::PointCloud<point_t>::Ptr cloudPCAprojection (new
  // pcl::PointCloud<point_t>);
  // pcl::PCA<point_t> pca;
  // pca.setInputCloud(temp_cloud);
  // pca.project(*temp_cloud, *cloudPCAprojection);
  // std::cerr << std::endl << "PCLEigenVectors: " << pca.getEigenVectors() <<
  // std::endl;
  // std::cerr << std::endl << "EigenVectors: " << eigenVectorsPCA << std::endl;

  // Transform the original cloud to the origin where the principal components
  // correspond to the axes.
  Eigen::Matrix4f projectionTransform(Eigen::Matrix4f::Identity());
  projectionTransform.block<3, 3>(0, 0) = eigenVectorsPCA.transpose();
  projectionTransform.block<3, 1>(0, 3) =
      -1.f * (projectionTransform.block<3, 3>(0, 0) * pcaCentroid.head<3>());
  pcl::PointCloud<point_t>::Ptr cloudPointsProjected(
      new pcl::PointCloud<point_t>);
  pcl::transformPointCloud(temp_cloud, *cloudPointsProjected,
                           projectionTransform);

  // Get the minimum and maximum points of the transformed cloud.
  point_t minPoint, maxPoint;
  pcl::getMinMax3D(*cloudPointsProjected, minPoint, maxPoint);
  const Eigen::Vector3f meanDiagonal =
      0.5f * (maxPoint.getVector3fMap() + minPoint.getVector3fMap());

  // Final transform
  Eigen::Quaternionf bboxQuaternion(eigenVectorsPCA);
  const Eigen::Vector3f bboxTransform =
      eigenVectorsPCA * meanDiagonal + pcaCentroid.head<3>();

  Eigen::Vector3f size(maxPoint.x - minPoint.x, maxPoint.y - minPoint.y, maxPoint.z - minPoint.z);
  orderSizeAndAdaptOrientation(size, bboxQuaternion, false);
  tf2::toMsg(size, ret.size);
  ret.center.orientation = tf2::toMsg(bboxQuaternion);

  ret.center.position.x = bboxTransform[0];
  ret.center.position.y = bboxTransform[1];
  ret.center.position.z = bboxTransform[2];

  return ret;
}

vision_msgs::BoundingBox3D calculate2(const pcl::PointCloud<point_t>::ConstPtr temp_cloud) {
  vision_msgs::BoundingBox3D ret;
  pcl::MomentOfInertiaEstimation<point_t> feature_extractor;
  feature_extractor.setInputCloud(temp_cloud);
  feature_extractor.compute();

  point_t min_point_OBB;
  point_t max_point_OBB;
  point_t position_OBB;
  Eigen::Matrix3f rotational_matrix_OBB;
  feature_extractor.getOBB(min_point_OBB, max_point_OBB, position_OBB,
                           rotational_matrix_OBB);
  ret.size.x = max_point_OBB.x - min_point_OBB.x;
  ret.size.y = max_point_OBB.y - min_point_OBB.y;
  ret.size.z = max_point_OBB.z - min_point_OBB.z;
  ret.center.position.x =
      (max_point_OBB.x + min_point_OBB.x) / 2.0 + position_OBB.x;
  ret.center.position.y =
      (max_point_OBB.y + min_point_OBB.y) / 2.0 + position_OBB.y;
  ret.center.position.z =
      (max_point_OBB.z + min_point_OBB.z) / 2.0 + position_OBB.z;
  const Eigen::Quaternionf q(rotational_matrix_OBB);
  ret.center.orientation.w = q.w();
  ret.center.orientation.x = q.x();
  ret.center.orientation.y = q.y();
  ret.center.orientation.z = q.z();
  return ret;
}

vision_msgs::BoundingBox3D calculateAABB(const pcl::PointCloud<point_t>::ConstPtr temp_cloud) {
  vision_msgs::BoundingBox3D ret;
  pcl::MomentOfInertiaEstimation<point_t> feature_extractor;
  feature_extractor.setInputCloud(temp_cloud);
  feature_extractor.compute();

  point_t min_point_AABB;
  point_t max_point_AABB;
  feature_extractor.getAABB(min_point_AABB, max_point_AABB);

  Eigen::Vector3f size(max_point_AABB.x - min_point_AABB.x, max_point_AABB.y - min_point_AABB.y, max_point_AABB.z - min_point_AABB.z);
  Eigen::Quaternionf quat(1.0, 0.0, 0.0, 0.0);
  orderSizeAndAdaptOrientation(size, quat, false);
  tf2::toMsg(size, ret.size);
  ret.center.orientation = tf2::toMsg(quat);

  ret.center.position.x = (max_point_AABB.x + min_point_AABB.x) / 2.0;
  ret.center.position.y = (max_point_AABB.y + min_point_AABB.y) / 2.0;
  ret.center.position.z = (max_point_AABB.z + min_point_AABB.z) / 2.0;
  return ret;
}

/*vision_msgs::BoundingBox3D calculateAABB2(const pcl::PointCloud<point_t>::Ptr temp_cloud) { // TODO untested!
  vision_msgs::BoundingBox3D ret;
  point_t min_point_AABB;
  point_t max_point_AABB;
  pcl::getMinMax3D(temp_cloud, min_point_AABB, max_point_AABB);

  Eigen::Vector3f size(max_point_AABB.x - min_point_AABB.x, max_point_AABB.y - min_point_AABB.y, max_point_AABB.z - min_point_AABB.z);
  Eigen::Quaternionf quat(1.0, 0.0, 0.0, 0.0);
  orderSizeAndAdaptOrientation(size, quat, false);
  tf2::toMsg(size, ret.size);
  ret.center.orientation = tf2::toMsg(quat);

  ret.center.position.x = (max_point_AABB.x + min_point_AABB.x) / 2.0;
  ret.center.position.y = (max_point_AABB.y + min_point_AABB.y) / 2.0;
  ret.center.position.z = (max_point_AABB.z + min_point_AABB.z) / 2.0;
  return ret;
}*/

void orderSizeAndAdaptOrientation(Eigen::Vector3f& size, Eigen::Quaternionf& orientation, bool asc) {
    ROS_INFO("started orderSizeAndAdaptOrientation(size=[%g, %g, %g], orientation=[x=%g, y=%g, z=%g, w=%g], asc=%s)", size.x(), size.y(), size.z(), orientation.x(), orientation.y(), orientation.z(), orientation.w(), (asc?"true":"false"));
    const Eigen::Vector3f size_copy(size); // Necessary because operator<< overwrites the values, so without this, there will be garbage in size
    //TODO could all rotations be (0.5, +-0.5, +-0.5, +-0.5)? maybe that would be more elegant?
    if(size.x() < size.y()) {
        if(size.z() < size.y()) {
            if(size.z() < size.x()) {
                if(asc) {
                    size << size_copy.z(), size_copy.x(), size_copy.y();
                    orientation=orientation*Eigen::Quaternionf(-0.5, 0.5, 0.5, 0.5);
                } else {
                    size << size_copy.y(), size_copy.x(), size_copy.z();
                    orientation=orientation*Eigen::Quaternionf(M_SQRT1_2, 0.0, 0.0, M_SQRT1_2);
                }
            } else {
                if(asc) {
                    size << size_copy.x(), size_copy.z(), size_copy.y();
                    orientation=orientation*Eigen::Quaternionf(M_SQRT1_2, M_SQRT1_2, 0.0, 0.0);
                } else {
                    size << size_copy.y(), size_copy.z(), size_copy.x();
                    orientation=orientation*Eigen::Quaternionf(0.5, 0.5, 0.5, 0.5);
                }
            }
        } else {
            if(asc) {
                // order is already correct
                // size << size_copy.x(), size_copy.y(), size_copy.z();
                // orientation=orientation*Eigen::Quaternionf(1.0, 0.0, 0.0, 0.0);
            } else {
                size << size_copy.z(), size_copy.y(), size_copy.x();
                orientation=orientation*Eigen::Quaternionf(M_SQRT1_2, 0.0, M_SQRT1_2, 0.0);
            }
        }
    } else {
        if(size.z() < size.x()) {
            if(size.z() < size.y()) {
                if(asc) {
                    size << size_copy.z(), size_copy.y(), size_copy.x();
                    orientation=orientation*Eigen::Quaternionf(M_SQRT1_2, 0.0, M_SQRT1_2, 0.0);
                } else {
                    // order is already correct
                    // size << size_copy.x(), size_copy.y(), size_copy.z();
                    // orientation=orientation*Eigen::Quaternionf(1.0, 0.0, 0.0, 0.0);
                }
            } else {
                if(asc) {
                    size << size_copy.y(), size_copy.z(), size_copy.x();
                    orientation=orientation*Eigen::Quaternionf(0.5, 0.5, 0.5, 0.5);
                } else {
                    size << size_copy.x(), size_copy.z(), size_copy.y();
                    orientation=orientation*Eigen::Quaternionf(M_SQRT1_2, M_SQRT1_2, 0.0, 0.0);
                }
            }
        } else {
            if(asc) {
                size << size_copy.y(), size_copy.x(), size_copy.z();
                orientation=orientation*Eigen::Quaternionf(M_SQRT1_2, 0.0, 0.0, M_SQRT1_2);
            } else {
                size << size_copy.z(), size_copy.x(), size_copy.y();
                orientation=orientation*Eigen::Quaternionf(-0.5, 0.5, 0.5, 0.5);
            }
        }
    }
    ROS_INFO("finished orderSizeAndAdaptOrientation: size=[%g, %g, %g], orientation=[x=%g, y=%g, z=%g, w=%g]", size.x(), size.y(), size.z(), orientation.x(), orientation.y(), orientation.z(), orientation.w());
}

vision_msgs::BoundingBox3D chooseBest3(const pcl::PointCloud<point_t>::ConstPtr temp_cloud) { // TODO feature_extractor.compute() seems to take quite
                      // some time and gives more than we need. Maybe switch to
                      // calculate() and another method for the AABB instead?
  ROS_INFO("OOBB_Calculate::chooseBest3() called");

  pcl::MomentOfInertiaEstimation<point_t> feature_extractor;
  feature_extractor.setInputCloud(temp_cloud);
  feature_extractor.compute();
  ROS_INFO("Feature extractor finished computation");

  point_t min_point_AABB;
  point_t max_point_AABB;
  feature_extractor.getAABB(min_point_AABB, max_point_AABB);
  vision_msgs::BoundingBox3D aabb;
  aabb.center.position.x = (max_point_AABB.x + min_point_AABB.x) / 2.0;
  aabb.center.position.y = (max_point_AABB.y + min_point_AABB.y) / 2.0;
  aabb.center.position.z = (max_point_AABB.z + min_point_AABB.z) / 2.0;
  //float dim0 = max_point_AABB.x - min_point_AABB.x,
  //      dim1 = max_point_AABB.y - min_point_AABB.y,
  //      dim2 = max_point_AABB.z - min_point_AABB.z;
  // Make sure that aabb.size.z <= aabb.size.y <= aabb.size.x
  Eigen::Vector3f size(max_point_AABB.x - min_point_AABB.x, max_point_AABB.y - min_point_AABB.y, max_point_AABB.z - min_point_AABB.z);
  Eigen::Quaternionf quat(1.0, 0.0, 0.0, 0.0);
  orderSizeAndAdaptOrientation(size, quat, false);
  tf2::toMsg(size, aabb.size);
  aabb.center.orientation = tf2::toMsg(quat);
  /*if (dim0 <= dim1 && dim1 <= dim2) {
    a.size.x = dim2;
    a.size.y = dim1;
    a.size.z = dim0;
    a.center.orientation.w = M_SQRT1_2;
    a.center.orientation.x = 0.0;
    a.center.orientation.y = M_SQRT1_2;
    a.center.orientation.z = 0.0;
  } else if (dim0 <= dim2 && dim2 <= dim1) {
    a.size.x = dim1;
    a.size.y = dim2;
    a.size.z = dim0;
    a.center.orientation.w = 0.5;
    a.center.orientation.x = 0.5;
    a.center.orientation.y = 0.5;
    a.center.orientation.z = 0.5;
  } else if (dim1 <= dim0 && dim0 <= dim2) {
    a.size.x = dim2;
    a.size.y = dim0;
    a.size.z = dim1;
    a.center.orientation.w = -0.5;
    a.center.orientation.x = 0.5;
    a.center.orientation.y = 0.5;
    a.center.orientation.z = 0.5;
  } else if (dim1 <= dim2 && dim2 <= dim0) {
    a.size.x = dim0;
    a.size.y = dim2;
    a.size.z = dim1;
    a.center.orientation.w = M_SQRT1_2;
    a.center.orientation.x = M_SQRT1_2;
    a.center.orientation.y = 0.0;
    a.center.orientation.z = 0.0;
  } else if (dim2 <= dim0 && dim0 <= dim1) {
    a.size.x = dim1;
    a.size.y = dim0;
    a.size.z = dim2;
    a.center.orientation.w = M_SQRT1_2;
    a.center.orientation.x = 0.0;
    a.center.orientation.y = 0.0;
    a.center.orientation.z = M_SQRT1_2;
  } else if (dim2 <= dim1 && dim1 <= dim0) {
    a.size.x = dim0;
    a.size.y = dim1;
    a.size.z = dim2;
    a.center.orientation.w = 1.0;
    a.center.orientation.x = 0.0;
    a.center.orientation.y = 0.0;
    a.center.orientation.z = 0.0;
  }*/

  point_t min_point_OBB;
  point_t max_point_OBB;
  point_t position_OBB;
  Eigen::Matrix3f rotational_matrix_OBB;
  feature_extractor.getOBB(min_point_OBB, max_point_OBB, position_OBB,
                           rotational_matrix_OBB);
  vision_msgs::BoundingBox3D oobb;
  oobb.size.x = max_point_OBB.x - min_point_OBB.x;
  oobb.size.y = max_point_OBB.y - min_point_OBB.y;
  oobb.size.z = max_point_OBB.z - min_point_OBB.z;
  oobb.center.position.x =
      (max_point_OBB.x + min_point_OBB.x) / 2.0 + position_OBB.x;
  oobb.center.position.y =
      (max_point_OBB.y + min_point_OBB.y) / 2.0 + position_OBB.y;
  oobb.center.position.z =
      (max_point_OBB.z + min_point_OBB.z) / 2.0 + position_OBB.z;
  const Eigen::Quaternionf q(rotational_matrix_OBB);
  oobb.center.orientation.w = q.w();
  oobb.center.orientation.x = q.x();
  oobb.center.orientation.y = q.y();
  oobb.center.orientation.z = q.z();

  double aabb_size = std::max(aabb.size.x, 0.01) * std::max(aabb.size.y, 0.01) *
                  std::max(aabb.size.z, 0.01);
  double oobb_size = std::max(oobb.size.x, 0.01) * std::max(oobb.size.y, 0.01) *
                  std::max(oobb.size.z, 0.01);
  ROS_INFO("aabb dims: %g %g %g", aabb.size.x, aabb.size.y, aabb.size.z);
  ROS_INFO("oobb dims: %g %g %g", oobb.size.x, oobb.size.y, oobb.size.z);
  ROS_INFO("OOBB_Calculate::chooseBest3(): aabb_size=%g, oobb_size=%g", aabb_size, oobb_size);
  ROS_INFO("OOBB_Calculate::chooseBest3() finished");
  if (aabb_size <= oobb_size) {
    return aabb;
  } else {
    return oobb;
  }
}

vision_msgs::BoundingBox3D chooseBest2(const pcl::PointCloud<point_t>::ConstPtr temp_cloud) {
  ROS_INFO("OOBB_Calculate::chooseBest2 called");
  vision_msgs::BoundingBox3D oobb1 = calculate(*temp_cloud);
  ROS_INFO("calculate() gave dims: %g %g %g", oobb1.size.x, oobb1.size.y, oobb1.size.z);
  vision_msgs::BoundingBox3D oobb2 = calculate2(temp_cloud);
  ROS_INFO("calculate2() gave dims: %g %g %g", oobb2.size.x, oobb2.size.y, oobb2.size.z);
  vision_msgs::BoundingBox3D aabb = calculateAABB(temp_cloud);
  ROS_INFO("calculateAABB() gave dims: %g %g %g", aabb.size.x, aabb.size.y, aabb.size.z);
  double oobb1_size = std::max(oobb1.size.x, 0.01) * std::max(oobb1.size.y, 0.01) *
                  std::max(oobb1.size.z, 0.01);
  double oobb2_size = std::max(oobb2.size.x, 0.01) * std::max(oobb2.size.y, 0.01) *
                  std::max(oobb2.size.z, 0.01);
  double aabb_size = std::max(aabb.size.x, 0.01) * std::max(aabb.size.y, 0.01) *
                  std::max(aabb.size.z, 0.01);
  ROS_INFO("OOBB_Calculate::chooseBest2(): oobb1_size=%g, oobb2_size=%g, aabb_size=%g", oobb1_size, oobb2_size, aabb_size);
  if (oobb1_size <= oobb2_size) {
    if (oobb1_size <= aabb_size) {
      return oobb1;
    } else {
      return aabb;
    }
  } else {
    if (oobb2_size <= aabb_size) {
      return oobb2;
    } else {
      return aabb;
    }
  }
}

vision_msgs::BoundingBox3D chooseBest(const pcl::PointCloud<point_t>::ConstPtr temp_cloud) {
  ROS_INFO("OOBB_Calculate::chooseBest called");
  vision_msgs::BoundingBox3D oobb1 = calculate(*temp_cloud);
  ROS_INFO("calculate() gave dims: %g %g %g", oobb1.size.x, oobb1.size.y, oobb1.size.z);
  double oobb1_size = std::max(oobb1.size.x, 0.01) * std::max(oobb1.size.y, 0.01) *
                  std::max(oobb1.size.z, 0.01);
  ROS_INFO("OOBB_Calculate::chooseBest(): oobb1_size=%g", oobb1_size);
  return oobb1;
}
} // namespace OOBB_Calculate

std::vector<float> minimum_bounding_box2d(const pcl::PointCloud<point_t>& points, const pcl::Vertices& polygon) {
    ROS_INFO("minimum_bounding_box2d() called");
    if(points.size()<3 || polygon.vertices.size()<3) {
        ROS_ERROR("minimum_bounding_box2d has not enough points/vertices (%lu)", points.size());
        return std::vector<float>();
    }
    for(size_t i=0; i<points.size(); i++) {
        if(std::abs(points[i].z)>0.01) {
            ROS_WARN("point.z is not zero (%g) - in minimum_bounding_box2d", points[i].z);
        }
    }
    double best_area=INFINITY;
    std::vector<float> result;
    result.resize(5);
    size_t /*left_index=polygon.vertices.size()/2, right_index=polygon.vertices.size()/2,*/ top_index=polygon.vertices.size()/2;
    for(size_t i=0; i<polygon.vertices.size(); i++) {//Loop through all polygon lines
        //ROS_INFO("next base line segment");
        top_index = (top_index+1)%polygon.vertices.size();
        point_t p1=points[polygon.vertices[i]], p2=points[polygon.vertices[(i+1)%polygon.vertices.size()]];
        double height;
        double left_side=INFINITY;
        double right_side=-INFINITY;
        double top[3];
        top[0]=distPointToLine2D(points[polygon.vertices[(top_index-1+polygon.vertices.size())%polygon.vertices.size()]].x, points[polygon.vertices[(top_index-1+polygon.vertices.size())%polygon.vertices.size()]].y, p1.x, p1.y, p2.x, p2.y);
        top[1]=distPointToLine2D(points[polygon.vertices[top_index]].x, points[polygon.vertices[top_index]].y, p1.x, p1.y, p2.x, p2.y);
        top[2]=distPointToLine2D(points[polygon.vertices[(top_index+1)%polygon.vertices.size()]].x, points[polygon.vertices[(top_index+1)%polygon.vertices.size()]].y, p1.x, p1.y, p2.x, p2.y);
        for(;;) {
            if(top[0] <= (top[1]+1.0e-7)) {
                if(top[2] <= (top[1]+1.0e-7)) {
                    // top[1] is largest
                    height=top[1];
                    break;
                } else {
                    // 0 1 2
                    //puts("Shifting up");
                    top_index = (top_index+1)%polygon.vertices.size();
                    top[0] = top[1];
                    top[1] = top[2];
                    top[2] = distPointToLine2D(points[polygon.vertices[(top_index+1)%polygon.vertices.size()]].x, points[polygon.vertices[(top_index+1)%polygon.vertices.size()]].y, p1.x, p1.y, p2.x, p2.y);
                }
            } else {
                if(top[2] <= (top[1]+1.0e-7)) {
                    // 2 1 0
                    //puts("Shifting down");
                    top_index = (top_index-1+polygon.vertices.size())%polygon.vertices.size();
                    top[2] = top[1];
                    top[1] = top[0];
                    top[0] = distPointToLine2D(points[polygon.vertices[(top_index-1+polygon.vertices.size())%polygon.vertices.size()]].x, points[polygon.vertices[(top_index-1+polygon.vertices.size())%polygon.vertices.size()]].y, p1.x, p1.y, p2.x, p2.y);
                } else {
                    // top[1] is smallest
                    ROS_WARN("ERROR: top_index=%lu, top[0]=%.20g top[1]=%.20g top[2]=%.20g", top_index, top[0], top[1], top[2]);
                    height=INFINITY; //TODO set the height to something?
                    break;
                }
            }
        }
        for(size_t j=0; j<polygon.vertices.size(); j++) {//Loop through all points and calculate the distance to the base line, the left and right borders
            point_t p0=points[polygon.vertices[j]];
            double along_base = (p0.x*(p1.x-p2.x)+p0.y*(p1.y-p2.y))/sqrt((p1.x-p2.x)*(p1.x-p2.x)+(p1.y-p2.y)*(p1.y-p2.y));
            left_side=std::min(left_side, along_base);
            right_side=std::max(right_side, along_base);
        }

        if(height*fabs(left_side-right_side)<best_area) {
            //printf("area=%f left=%f right=%f\n", height*fabs(left_side-right_side), left_side, right_side);
            best_area=height*fabs(left_side-right_side);
            result[2]=atan2(p1.y-p2.y, p1.x-p2.x);
            double dist_p2=(p2.x*(p1.x-p2.x)+p2.y*(p1.y-p2.y))/sqrt((p1.x-p2.x)*(p1.x-p2.x)+(p1.y-p2.y)*(p1.y-p2.y));
            result[0]=p2.x+((left_side+right_side)/2.0-dist_p2)*(p1.x-p2.x)/sqrt((p1.x-p2.x)*(p1.x-p2.x)+(p1.y-p2.y)*(p1.y-p2.y))-sin(result[2])*height/2.0;
            result[1]=p2.y+((left_side+right_side)/2.0-dist_p2)*(p1.y-p2.y)/sqrt((p1.x-p2.x)*(p1.x-p2.x)+(p1.y-p2.y)*(p1.y-p2.y))+cos(result[2])*height/2.0;
            result[3]=fabs(left_side-right_side);
            result[4]=height;
        }
    }
    //printf("best_area=%f, coeff=(%f, %f, %f, %f, %f)\n", best_area, result[0], result[1], result[2], result[3], result[4]);
    ROS_INFO("minimum_bounding_box2d() finished, best_area=%g, coeff=(%g (center x), %g (center y), %g (rotation), %g (width), %g (height))", best_area, result[0], result[1], result[2], result[3], result[4]);
    return result;
}

std::vector<float> minimum_bounding_box2d_v2(const pcl::PointCloud<point_t>& points, const pcl::Vertices& polygon) {
    double best_area=INFINITY;
    std::vector<float> result;
    result.resize(5);
    for(size_t i=0; i<polygon.vertices.size(); i++) {//Loop through all polygon lines
        point_t p1=points[polygon.vertices[i]], p2=points[polygon.vertices[(i+1)%polygon.vertices.size()]];
        double height=0.0;
        float left_side=INFINITY;
        float right_side=-INFINITY;
        for(size_t j=0; j<polygon.vertices.size(); j++) {//Loop through all points and calculate the distance to the base line, the left and right borders
            point_t p0=points[polygon.vertices[j]];
            height=std::max(height, distPointToLine2D(p0.x, p0.y, p1.x, p1.y, p2.x, p2.y));
            float along_base = (p0.x*(p1.x-p2.x)+p0.y*(p1.y-p2.y))/sqrt((p1.x-p2.x)*(p1.x-p2.x)+(p1.y-p2.y)*(p1.y-p2.y));
            left_side=std::min(left_side, along_base);
            right_side=std::max(right_side, along_base);
        }

        if(height*fabs(left_side-right_side)<best_area) {
            //printf("area=%f left=%f right=%f\n", height*fabs(left_side-right_side), left_side, right_side);
            best_area=height*fabs(left_side-right_side);
            result[2]=atan2(p1.y-p2.y, p1.x-p2.x);
            float dist_p2=(p2.x*(p1.x-p2.x)+p2.y*(p1.y-p2.y))/sqrt((p1.x-p2.x)*(p1.x-p2.x)+(p1.y-p2.y)*(p1.y-p2.y));
            result[0]=p2.x+((left_side+right_side)/2.0-dist_p2)*(p1.x-p2.x)/sqrt((p1.x-p2.x)*(p1.x-p2.x)+(p1.y-p2.y)*(p1.y-p2.y))-sin(result[2])*height/2.0;
            result[1]=p2.y+((left_side+right_side)/2.0-dist_p2)*(p1.y-p2.y)/sqrt((p1.x-p2.x)*(p1.x-p2.x)+(p1.y-p2.y)*(p1.y-p2.y))+cos(result[2])*height/2.0;
            result[3]=fabs(left_side-right_side);
            result[4]=height;
        }
    }
    ROS_INFO("best_area=%g, coeff=(%g (center x), %g (center y), %g (rotation), %g (width), %g (height))", best_area, result[0], result[1], result[2], result[3], result[4]);
    return result;
}

vision_msgs::BoundingBox3D minimum_bbox_of_plane(const pcl::PointCloud<point_t>& points, const pcl::Vertices& polygon, const shape_msgs::Plane& plane) {
    // Rotate points so that z=0
    ROS_INFO("minimum_bbox_of_plane() called, plane=(a=%f, b=%f, c=%f, d=%f)", plane.coef[0], plane.coef[1], plane.coef[2], plane.coef[3]);
    Eigen::Affine3f transform=Eigen::Affine3f(Eigen::Quaternionf::FromTwoVectors(Eigen::Vector3f(plane.coef[0], plane.coef[1], plane.coef[2]), Eigen::Vector3f(0.0, 0.0, 1.0))*Eigen::Translation3f(plane.coef[3]*plane.coef[0], plane.coef[3]*plane.coef[1], plane.coef[3]*plane.coef[2]));
    pcl::PointCloud<point_t> points_transformed;
    pcl::transformPointCloud (points, points_transformed, transform);
    std::vector<float> box=minimum_bounding_box2d(points_transformed, polygon);
    if(box[3]*box[4]<1.0e-6) {//If the size of the found bounding box is very small, something might have gone wrong, so try the other method
        ROS_WARN("the bounding box seems too small, so trying another method (slower, but more reliable)");
        box=minimum_bounding_box2d_v2(points_transformed, polygon);
    }
    if(box[3]<box[4]) {
        ROS_INFO("switching height and width since %g<%g", box[3], box[4]);
        std::swap(box[3], box[4]);
        box[2]+=M_PI/2.0;
    }
    // Keep in (-M_PI/2; M_PI/2]
    while (box[2] > M_PI/2.0)
        box[2] -= M_PI;
    while (box[2] <= -M_PI/2.0)
        box[2] += M_PI;
    vision_msgs::BoundingBox3D result;
    result.center.position = tf2::toMsg(transform.inverse() * Eigen::Vector3f(box[0], box[1], 0.0));
    result.center.orientation = tf2::toMsg(Eigen::Quaternionf(transform.inverse().rotation()) * Eigen::Quaternionf(cos(box[2]/2.0), 0.0, 0.0, sin(box[2]/2.0)));
    result.size.x = box[3];
    result.size.y = box[4];
    result.size.z = 0.0;
    return result;
}
