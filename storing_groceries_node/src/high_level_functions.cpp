// general
#include <moveit/planning_scene_interface/planning_scene_interface.h>
#include <Eigen/Geometry>
#include <pcl_conversions/pcl_conversions.h>
#include <pcl/filters/random_sample.h> // for RandomSample
#include <pcl/filters/crop_box.h> // for CropBox
#include <tf2_eigen/tf2_eigen.h>
#include <cstddef>                          // for NULL, size_t
#include <utility>                          // for pair
#include <tf2/LinearMath/Transform.h> // for tf2::Transform
#include <opencv2/imgcodecs.hpp>
//#include <eigen_conversions/eigen_msg.h> // for tf::transformMsgToEigen
#include <tf2_geometry_msgs/tf2_geometry_msgs.h>
#include <octomap/octomap.h>
//#include <octomap_ros/conversions.h>
#include <octomap_msgs/conversions.h>
//#include <sensor_msgs/point_cloud2_iterator.h>
#include <image_geometry/pinhole_camera_model.h>

// msgs
#include <clf_grasping_msgs/CloudToCollision.h>
#include <clf_object_recognition_msgs/Classify2D.h>
#include <clf_object_recognition_msgs/BoundingBox3DArray.h>

// own
#include "high_level_functions.hpp"
#include "util.hpp" // for getIsometryTransform
#include "bounding_boxes.hpp" // for orderSizeAndAdaptOrientation

#define BASE_FRAME "base_footprint"

// TODO read from yaml? shelf_floor_heights must be ordered by size!
// clf shelf
const float shelf_height=2.0, shelf_width=0.78, shelf_depth=0.26, shelf_thickness=0.02/*, shelf_door_width = 0.39*/;
const std::vector<float> shelf_floor_heights={0.09, 0.405, 0.76, 1.05, 1.4, 1.685, 2.0};
// magdeburg shelf german open 2019
// const float shelf_height = 2.02, shelf_width = 0.8, shelf_depth = 0.26, shelf_thickness = 0.02, shelf_door_width = 0.39;
// const std::vector<float> shelf_floor_heights = {0.09, 0.41, 0.76, 1.05, 1.4, 1.685, 2.0};

ros::ServiceClient object_fitter_client;
ros::ServiceClient object_merger_client;
ros::ServiceClient classify_client;

ros::Publisher image_pub;
ros::Publisher bbox_pub;
ros::Publisher marker_pub;

ros::NodeHandle *nh1;

tf2_ros::Buffer *tfBuffer1;

moveit::planning_interface::PlanningSceneInterface *planning_scene_interface1;

unsigned int global_unique_id = 0;
bool useSACFitting=false;

// TODO make this a class?
void init_high_level(ros::NodeHandle *nh, tf2_ros::Buffer *buf/*,std::string image_topic*/) {
  nh1 = nh;
  tfBuffer1 = buf;

  // moveit::planning_interface::PlanningSceneInterface
  // new_planning_scene_interface;
  // planning_scene_interface1=&new_planning_scene_interface;
  try {
    planning_scene_interface1 = new moveit::planning_interface::PlanningSceneInterface("", false);
  } catch (const std::runtime_error& e) {
    ROS_ERROR_STREAM("Cannot init PlanningSceneInterface: " << e.what());
  }

  //object_merger_client = nh1->serviceClient<clf_object_recognition_msgs::Detect3D>("/object_merger/detect_objects");
  object_fitter_client = nh1->serviceClient<clf_grasping_msgs::CloudToCollision>("/cloud_to_co");
  classify_client = nh1->serviceClient<clf_object_recognition_msgs::Classify2D>("/classify"); // TODO make persistent for better performance? TODO safety: check if still connected/reconnect when calling
  ROS_INFO("Checking if services are available ...");
  if(!object_fitter_client.waitForExistence(ros::Duration(1, 0))) {
    ROS_WARN("Service /cloud_to_co seems to be unavailable (sq_fitting node not started?)");
  }
  if(!classify_client.waitForExistence(ros::Duration(1, 0))) {
    ROS_WARN("Service /classify seems to be unavailable (tf_recognition node not started?)");
  }

  image_pub = nh1->advertise<sensor_msgs::Image>("/objectimage", 1000); // TODO maybe ImageTransport would be better fitting?
  bbox_pub = nh1->advertise<clf_object_recognition_msgs::BoundingBox3DArray>("/bbs", 100);
  marker_pub = nh1->advertise<visualization_msgs::Marker>("/objects_viz_markers", 100);
}

bool applyCollisionObject(const moveit_msgs::CollisionObject& collision_object) {
  return planning_scene_interface1->applyCollisionObject(collision_object);
}

bool applyAttachedCollisionObject(const moveit_msgs::AttachedCollisionObject &attached_collision_object) {
  return planning_scene_interface1->applyAttachedCollisionObject(attached_collision_object);
}

std::map<std::string, moveit_msgs::CollisionObject> getObjects(const std::vector<std::string>& object_ids) {
  return planning_scene_interface1->getObjects(object_ids);
}

std::map<std::string, moveit_msgs::AttachedCollisionObject> getAttachedObjects() {
  return planning_scene_interface1->getAttachedObjects();
}

bool addPointCloudAsOctomap(pcl::PointCloud<point_t>::ConstPtr pcl_cloud) {
    ROS_INFO("addPointCloudAsOctomap called");
    /*sensor_msgs::PointCloud2 cloud;
    ROS_INFO("Calling toROSMsg() ..."); // TODO improve this: is detour with PointCloud2 necessary?
    pcl::toROSMsg(*pcl_cloud, cloud);*/
    octomap::Pointcloud octomapCloud;

    /*ROS_INFO("Calling pointClout2ToOctomap() ...");
    //octomap::pointCloud2ToOctomap (cloud, octomapCloud);
    octomapCloud.reserve(cloud.data.size() / cloud.point_step);
    sensor_msgs::PointCloud2ConstIterator<float> iter_x(cloud, "x");
    sensor_msgs::PointCloud2ConstIterator<float> iter_y(cloud, "y");
    sensor_msgs::PointCloud2ConstIterator<float> iter_z(cloud, "z");
    for (; iter_x != iter_x.end(); ++iter_x, ++iter_y, ++iter_z){
      // Check if the point is invalid
      if (!std::isnan (*iter_x) && !std::isnan (*iter_y) && !std::isnan (*iter_z))
        octomapCloud.push_back(*iter_x, *iter_y, *iter_z);
    }*/
    octomapCloud.reserve(pcl_cloud->size());
    for (const auto & point : *pcl_cloud) {
        if (!std::isnan (point.x) && !std::isnan (point.y) && !std::isnan (point.z))
            octomapCloud.push_back(point.x, point.y, point.z);
    }

    const double resolution=0.03;
    octomap::OcTree octree(resolution);
    ROS_INFO("Calling insertPointCloud() ...");
    octree.insertPointCloud(octomapCloud, octomap::point3d(0.0, 0.0, 0.0), -1.0, false, false); // TODO this is taking too long, make faster
    ROS_INFO("insertPointCloud() done");
    moveit_msgs::PlanningScene ps;
    ROS_INFO("Calling binaryMapToMsg() ...");
    //TODO call octree.toMaxLikelihood() and octree.prune() before? Might make data smaller
    if(!binaryMapToMsg(octree, ps.world.octomap.octomap)) { // or fullMapToMsg?
        ROS_WARN("Octomap to octomap msg function failed");
        return false;
    }
    ps.is_diff = true;
    ROS_INFO("Calling applyPlanningScene() ...");
    if(!planning_scene_interface1->applyPlanningScene(ps)) {
        ROS_WARN("applyPlanningScene() function failed");
        return false;
    }
    ROS_INFO("addPointCloudAsOctomap finished successfully");
    return true;
}

void set_high_level_options(bool useAlternativeFitting) {
    useSACFitting = useAlternativeFitting;
}

void publishCOasMarker(const moveit_msgs::CollisionObject& co, int id) {
    visualization_msgs::Marker marker;
    marker.header = co.header;
    marker.ns = "objects";
    marker.color.r = 0.0f;
    marker.color.g = 0.0f;
    marker.color.b = 1.0f;
    marker.color.a = 1.0f;
    marker.lifetime = ros::Duration();
    for(size_t i=0; i<co.primitives.size(); i++) {
        marker.id = id+i;
        switch(co.primitives[i].type) {
            case 1: // box
                marker.type = visualization_msgs::Marker::CUBE;
                marker.scale.x = co.primitives[i].dimensions[0];
                marker.scale.y = co.primitives[i].dimensions[1];
                marker.scale.z = co.primitives[i].dimensions[2];
                break;
            case 2: // sphere
                marker.type = visualization_msgs::Marker::SPHERE;
                marker.scale.x = 2.0*co.primitives[i].dimensions[0];
                marker.scale.y = 2.0*co.primitives[i].dimensions[0];
                marker.scale.z = 2.0*co.primitives[i].dimensions[0];
                break;
            case 3: // cylinder
                marker.type = visualization_msgs::Marker::CYLINDER;
                marker.scale.x = 2.0*co.primitives[i].dimensions[1];
                marker.scale.y = 2.0*co.primitives[i].dimensions[1];
                marker.scale.z = co.primitives[i].dimensions[0];
                break;
            case 4: // cone
                marker.type = visualization_msgs::Marker::ARROW;
                marker.scale.x = 2.0*co.primitives[i].dimensions[1];
                marker.scale.y = 2.0*co.primitives[i].dimensions[1];
                marker.scale.z = co.primitives[i].dimensions[0];
                break;
            default:
                ROS_WARN("Unknown shape type");
                marker.type = visualization_msgs::Marker::CUBE;
        }
        marker.action = visualization_msgs::Marker::ADD;
        marker.pose.position = co.primitive_poses[i].position;
        ROS_INFO("[publishCOasMarker] Marker (i=%lu, id=%i) has position (%g,%g,%g)", i, id, marker.pose.position.x, marker.pose.position.y, marker.pose.position.z);
        marker.pose.orientation = co.primitive_poses[i].orientation;
        marker_pub.publish(marker);
    }
}

// TODO maybe move the rgb-image opencv stuff somewhere else?

double ellipseCircumference(double a, double b) { // a and b are the semi-axes, like radius
    if(a<0.0 || b<0.0) {
        ROS_ERROR("ellipseCircumference(a=%g, b=%g): a or b are negative", a, b);
        return 0.0;
    }
    return M_PI*(3.0*(a+b)-std::sqrt((3.0*a+b)*(a+3.0*b)));
}

// Translated from here: https://wet-robots.ghost.io/simple-method-for-distance-to-ellipse/
// ellipse has the be centered at (0,0), unrotated
float dist2Ellipse(float a, float b, float pointx, float pointy, bool print_debug=false) {
    const float px = std::abs(pointx);
    const float py = std::abs(pointy);

    float tx = 0.707f;
    float ty = 0.707f;

    for(int i=0; i<3; i++) {
        const float x = a * tx;
        const float y = b * ty;

        const float ex = (a*a - b*b) * std::pow(tx, 3) / a;
        float ey = (b*b - a*a) * std::pow(ty, 3) / b;

        float rx = x - ex;
        float ry = y - ey;

        float qx = px - ex;
        float qy = py - ey;

        float r = std::hypot(ry, rx);
        float q = std::hypot(qy, qx);

        tx = std::min(1.0f, std::max(0.0f, (qx * r / q + ex) / a));
        ty = std::min(1.0f, std::max(0.0f, (qy * r / q + ey) / b));
        float t = std::hypot(ty, tx);
        tx /= t;
        ty /= t;
    }
    const float near_p_x = std::copysign(a * tx, pointx);
    const float near_p_y = std::copysign(b * ty, pointy);
    const float result = std::sqrt(std::pow(pointx-near_p_x, 2)+std::pow(pointy-near_p_y, 2));
    if(print_debug) {
        printf("dist2Ellipse(a=%g, b=%g, pointx=%g, pointy=%g): closest point is (%g,%g) returns %g\n", a, b, pointx, pointy, near_p_x, near_p_y, result);
    }
    return result;
}

double distPointToLine2D(double x0, double y0, double x1, double y1, double x2, double y2, bool only_within=false) {
    //printf("distPointToLine2D called\n");
    const double denominator=sqrt((y2-y1)*(y2-y1)+(x2-x1)*(x2-x1));
    if(denominator<1e-6) {
        puts("WARNING: denominator is too small in distPointToLine2D()");
        return INFINITY;
    }
    if(only_within) {
        const double t=((x0-x1)*(x2-x1)+(y0-y1)*(y2-y1))/(denominator*denominator); // Between 0 and 1 if between p1 and p2
        if(t<0.0 || t>1.0) return INFINITY;
    }
    return fabs((y2-y1)*x0 - (x2-x1)*y0 + x2*y1 - y2*x1)/denominator;
}

// Find an ellipse in a set of points using RANSAC
float fitEllipse(const std::vector<cv::Point2f>& points, const cv::Mat& img, cv::RotatedRect& result, const float inlier_dist=0.5, const cv::Point2f * fixed1=nullptr, const cv::Point2f * fixed2=nullptr) {
    printf("fitEllipse() started with %lu points, inlier_dist=%g\n", points.size(), inlier_dist);
    if(points.size()<5) {
        printf("Too few points, returning");
        return 0.0;
    }
    const size_t sac_iterations=1000;
    float best_inlier_sum=0.0f;
    float inlier_sum;
    cv::RotatedRect best_ellipse;
    for(size_t i=0; i<sac_iterations; ++i) {
        ROS_DEBUG("Iteration %lu. ", i);
        // Select points (randomly)
        std::vector<cv::Point2f> sample; // TODO points with a large value in edges should have a larger probability
        if(fixed1!=nullptr) sample.push_back(*fixed1);
        if(fixed2!=nullptr) sample.push_back(*fixed2);
        while(sample.size()<5) {
            sample.push_back(points.at(rand()/(RAND_MAX/points.size()))); // TODO this is biased and might produce the same point several times
        }
        ROS_DEBUG("Got %lu points: ", sample.size());
        for(const auto& p : sample) ROS_DEBUG("(%g,%g) ", p.x, p.y/*, edges(p.y, p.x)*/);
        ROS_DEBUG("\n");
        
        // Compute ellipse
        const auto coeff=cv::fitEllipse(sample); // coeff.angle is in degrees! (WTF!)
        ROS_DEBUG("Fitted ellipse (angle=%g, center=(%g,%g), height=%g, width=%g)\n", coeff.angle, coeff.center.x, coeff.center.y, coeff.size.height, coeff.size.width);
        if(coeff.center.x<0.0 || coeff.center.y<0.0 || coeff.center.x>img.cols || coeff.center.y>img.rows) { // TODO
            ROS_DEBUG("Center of ellipse is not in image, continuing with next iteration\n");
            continue;
        }
        const float sina=std::sin(coeff.angle/180.0*3.14159), cosa=std::cos(coeff.angle/180.0*3.14159);
        for(const auto& p : sample) { // Check if all sample points are inliers. If not, then that is a sign that something went wrong
            const float xrot =  cosa*(p.x-coeff.center.x)+sina*(p.y-coeff.center.y);
            const float yrot = -sina*(p.x-coeff.center.x)+cosa*(p.y-coeff.center.y);
            if(dist2Ellipse(coeff.size.width/2.0, coeff.size.height/2.0, xrot, yrot)>inlier_dist) {
                ROS_DEBUG("Not all samples are inliers, so this model is bad\n"); // TODO this happens very often, find a way to make this happen less often (duh)
                goto continue_outer;
            }
        }
        ///for(const auto& p : sample) {
        //    const float xrot =  cosa*(p.x-coeff.center.x)+sina*(p.y-coeff.center.y);
        //    const float yrot = -sina*(p.x-coeff.center.x)+cosa*(p.y-coeff.center.y);
        //    printf("%g ", dist2Ellipse(coeff.size.width/2.0, coeff.size.height/2.0, xrot, yrot, true)); // TODO check if the sample points are inliers
        //} printf("\n");
        // Count inliers
        inlier_sum=0.0f;
        for(const auto& p : points) {
            const float xrot =  cosa*(p.x-coeff.center.x)+sina*(p.y-coeff.center.y);
            const float yrot = -sina*(p.x-coeff.center.x)+cosa*(p.y-coeff.center.y);
            const float dist=dist2Ellipse(coeff.size.width/2.0, coeff.size.height/2.0, xrot, yrot);
            if(dist<inlier_dist) inlier_sum+=1.0f*std::exp(-dist); //edges(p.y, p.x); // TODO maybe add value of edges at point
        }
        inlier_sum/=ellipseCircumference(coeff.size.width/2.0, coeff.size.height/2.0);
        ROS_DEBUG("Has inlier_sum=%g\n", inlier_sum);
        if(inlier_sum>best_inlier_sum) {
            best_inlier_sum=inlier_sum;
            best_ellipse=coeff;
            ROS_DEBUG("New best ellipse (angle=%g, center=(%g,%g), height=%g, width=%g) with inlier_sum=%g\n", best_ellipse.angle, best_ellipse.center.x, best_ellipse.center.y, best_ellipse.size.height, best_ellipse.size.width, best_inlier_sum);
        }
        
        /*if(i%3==0) {
            cv::Mat ellipse=cv::Mat::zeros(img.rows, img.cols, img.type());
            cv::ellipse(ellipse, coeff, cv::Scalar(255, 0, 0));
            for(const auto& p : points) {
                const float xrot =  cosa*(p.x-coeff.center.x)+sina*(p.y-coeff.center.y);
                const float yrot = -sina*(p.x-coeff.center.x)+cosa*(p.y-coeff.center.y);
                if(dist2Ellipse(coeff.size.width/2.0, coeff.size.height/2.0, xrot, yrot)<inlier_dist) ellipse.at<cv::Vec3b>(p.y, p.x)=cv::Vec3b(0, 255, 0);
            }
            for(const auto& p : sample) {
                ellipse.at<cv::Vec3b>(p.y, p.x)=cv::Vec3b(255, 255, 255);
            }
            std::stringstream sstream;
            sstream << i << ".png";
            cv::imwrite(sstream.str(), ellipse);
        }*/
        continue_outer:;
    }
    printf("Best ellipse (angle=%g, center=(%g,%g), height=%g, width=%g) with inlier sum=%g (of %lu points total)\n", best_ellipse.angle, best_ellipse.center.x, best_ellipse.center.y, best_ellipse.size.height, best_ellipse.size.width, best_inlier_sum, points.size());

    // Find all inliers and improve ellipse coefficients
    std::vector<cv::Point2f> inliers;
    float sina=std::sin(best_ellipse.angle/180.0*3.14159), cosa=std::cos(best_ellipse.angle/180.0*3.14159);
    for(const auto& p : points) {
        const float xrot =  cosa*(p.x-best_ellipse.center.x)+sina*(p.y-best_ellipse.center.y);
        const float yrot = -sina*(p.x-best_ellipse.center.x)+cosa*(p.y-best_ellipse.center.y);
        if(dist2Ellipse(best_ellipse.size.width/2.0, best_ellipse.size.height/2.0, xrot, yrot)<inlier_dist) inliers.push_back(p);
    }
    if(inliers.size()>5) {
        const auto new_coeffs=cv::fitEllipse(inliers);
        sina=std::sin(new_coeffs.angle/180.0*3.14159), cosa=std::cos(new_coeffs.angle/180.0*3.14159);
        // Check if new_coeffs is actually better
        inlier_sum=0.0f;
        for(const auto& p : points) {
            const float xrot =  cosa*(p.x-new_coeffs.center.x)+sina*(p.y-new_coeffs.center.y);
            const float yrot = -sina*(p.x-new_coeffs.center.x)+cosa*(p.y-new_coeffs.center.y);
            const float dist=dist2Ellipse(new_coeffs.size.width/2.0, new_coeffs.size.height/2.0, xrot, yrot);
            if(dist<inlier_dist) inlier_sum+=1.0f*std::exp(-dist); //edges(p.y, p.x); // TODO maybe add value of edges at point
        }
        inlier_sum/=ellipseCircumference(new_coeffs.size.width/2.0, new_coeffs.size.height/2.0);
        if(inlier_sum>best_inlier_sum) {
            best_inlier_sum=inlier_sum;
            best_ellipse=new_coeffs;
            printf("Further improved ellipse (angle=%g, center=(%g,%g), height=%g, width=%g) with inlier_sum=%g\n", best_ellipse.angle, best_ellipse.center.x, best_ellipse.center.y, best_ellipse.size.height, best_ellipse.size.width, best_inlier_sum);
        }
    }
    
    result=best_ellipse;
    return best_inlier_sum;
}

// Find a line in a set of points using RANSAC
float fitLine(const std::vector<cv::Point2f>& points, std::vector<double>& result, const float inlier_dist=0.5, const cv::Point2f * fixed=nullptr) {
    const size_t sac_iterations=500;
    float inlier_sum;
    float best_inlier_sum_line=0.0f;
    std::vector<double> best_line;
    for(size_t i=0; i<sac_iterations; ++i) {
        ROS_DEBUG("Iteration %lu. ", i);
        // Select points
        std::vector<cv::Point2f> sample; // TODO points with a large value in edges should have a larger probability
        if(fixed!=nullptr) {
            sample.push_back(*fixed);
        }
        while(sample.size()<2) {
            sample.push_back(points.at(rand()/(RAND_MAX/points.size()))); // TODO this is biased and might produce the same point several times
        }
        ROS_DEBUG("Got %lu points: ", sample.size());
        for(const auto& p : sample) ROS_DEBUG("(%g,%g) ", p.x, p.y/*, edges(p.y, p.x)*/);
        //ROS_DEBUG("\n");
        
        // Compute line
        const std::vector<double> coeff={sample[0].x, sample[0].y, sample[1].x, sample[1].y};
        //ROS_DEBUG("Fitted line (angle=%g, center=(%g,%g), height=%g, width=%g)\n", coeff.angle, coeff.center.x, coeff.center.y, coeff.size.height, coeff.size.width);
        // Count inliers
        inlier_sum=0.0f;
        for(const auto& p : points) {
            const float dist=distPointToLine2D(p.x, p.y, coeff[0], coeff[1], coeff[2], coeff[3], true);
            if(dist<inlier_dist) inlier_sum+=1.0f*std::exp(-dist); //edges(p.y, p.x); // TODO maybe add value of edges at point
        }
        ROS_DEBUG("Has inlier_sum=%g\n", inlier_sum);
        if(inlier_sum>best_inlier_sum_line) {
            best_inlier_sum_line=inlier_sum;
            best_line=coeff;
            ROS_DEBUG("New best line (throught point (%g,%g) and point (%g,%g)) with inlier_sum=%g\n", best_line.at(0), best_line.at(1), best_line.at(2), best_line.at(3), best_inlier_sum_line);
        }
    }
    result=best_line;
    return best_inlier_sum_line;
}

double dishFitting(const cv::Mat& img, size_t i, cv::RotatedRect& ellipse_out1, cv::RotatedRect& ellipse_out2) { // TODO return 2 ellipses, 2-4 lines
    // TODO make approach more like pictorial structures?
    cv::Mat hsv;
    cv::cvtColor(img, hsv, cv::COLOR_RGB2HSV);
    cv::Mat channels[3];
    cv::split(hsv, channels);
    cv::imwrite("/tmp/hue.png", channels[0]);
    cv::imwrite("/tmp/saturation.png", channels[1]);
    cv::imwrite("/tmp/value.png", channels[2]);
    //cv::Mat binary(img.rows, img.cols, CV_8UC1);
    //std::pair<std::uint16_t, float> thresh1=computeThreshold(channels[1]);
    //applyThreshold(channels[1], thresh1.first, binary);
    //cv::MorphologyEx(binary, binary, cv::MORPH_CLOSE, cv::getStructuringElement(cv::MORPH_RECT, cv::Size(3, 3)));
    //cv::imwrite("saturation_binary.png", binary);
    cv::Mat_<std::uint8_t> edges;
    // TODO or Canny?
    cv::Canny(channels[1], edges, 70, 150);
    //const int ddepth=CV_16S, dx=1, dy=1, ksize=1;
    //cv::Sobel(channels[2], edges, ddepth, dx, dy, ksize); // TODO: edges on what channel? 0=hue, 1=saturation 2=value
    //for(int r=0; r<edges.rows; ++r) {
    //    for(int c=0; c<edges.cols; ++c) {
    //        edges.at<int16_t>(r, c)=std::abs(edges.at<int16_t>(r, c));
    //    }
    //}
    std::stringstream sstream2;
    sstream2 << "/tmp/edges" << i << ".png";
    cv::imwrite(sstream2.str(), edges);
    // TODO maybe use direction of edges? 
    std::vector<cv::Point2f> points;
    for(int r=0; r<edges.rows; ++r) {
        for(int c=0; c<edges.cols; ++c) {
            if(std::abs(edges(r, c))>25) { // TODO eliminate parameter
                points.push_back(cv::Point2f(c, r));
                //edges.at<uint8_t>(r, c)=255;
            }
        }
    }
    printf("points.size()=%lu\n", points.size());
    
    const float inlier_dist=1.0; // In pixels?
    //auto ellipse=fitEllipse(points);
    cv::RotatedRect ellipse;
    const float ellipse_fit=fitEllipse(points, img, ellipse, inlier_dist);
    const float sina=std::sin(ellipse.angle/180.0*3.14159), cosa=std::cos(ellipse.angle/180.0*3.14159);
    if(ellipse_fit/*/ellipseCircumference(ellipse.size.width/2.0, ellipse.size.height/2.0)*/>0.5) {
        printf("Found good ellipse! (%g)\n", ellipse_fit/*/ellipseCircumference(ellipse.size.width/2.0, ellipse.size.height/2.0)*/);
        cv::ellipse(img, ellipse, cv::Scalar(255, 255, 255)); // TODO this does not (should not) work with the const img.
        // TODO remove all inliers from points
        for(int i=points.size()-1; i>0; --i) { // Draw all inliers
            const float xrot =  cosa*(points[i].x-ellipse.center.x)+sina*(points[i].y-ellipse.center.y);
            const float yrot = -sina*(points[i].x-ellipse.center.x)+cosa*(points[i].y-ellipse.center.y);
            if(dist2Ellipse(ellipse.size.width/2.0, ellipse.size.height/2.0, xrot, yrot)<inlier_dist) {
                points.erase(points.begin()+i);
            }
        }
        printf("%lu points remain\n", points.size());
    } else {
        printf("No good ellipse found\n");
        return -INFINITY;
    }
    // Now try to fit a line
    cv::Point2f ellipse_edge1, ellipse_edge2;
    if(ellipse.size.width>ellipse.size.height) {
        ellipse_edge1.x=ellipse.center.x+( cosa*ellipse.size.width/2.0);
        ellipse_edge1.y=ellipse.center.y+( sina*ellipse.size.width/2.0); // Switched sign on sina because negative angle is desired
        ellipse_edge2.x=ellipse.center.x-( cosa*ellipse.size.width/2.0);
        ellipse_edge2.y=ellipse.center.y-( sina*ellipse.size.width/2.0); // Switched sign on sina because negative angle is desired
    } else {
        ellipse_edge1.x=ellipse.center.x+(-sina*ellipse.size.height/2.0); // Switched sign on sina because negative angle is desired
        ellipse_edge1.y=ellipse.center.y+( cosa*ellipse.size.height/2.0);
        ellipse_edge2.x=ellipse.center.x-(-sina*ellipse.size.height/2.0); // Switched sign on sina because negative angle is desired
        ellipse_edge2.y=ellipse.center.y-( cosa*ellipse.size.height/2.0);
    }
    std::vector<double> line(4, 0.0); // a line that goes through point (line[0], line[1]) and point (line[2], line[3])
    const float line_fit=fitLine(points, line, inlier_dist, &ellipse_edge1);
    cv::line(img, cv::Point(line[0], line[1]), cv::Point(line[2], line[3]), cv::Scalar(255, 255, 255));
    printf("line_fit=%g\n", line_fit/std::sqrt(std::pow(line[0]-line[2], 2)+std::pow(line[1]-line[3], 2)));
    for(int i=points.size()-1; i>0; --i) { // Draw all inliers
        if(distPointToLine2D(points[i].x, points[i].y, line[0], line[1], line[2], line[3], true)<inlier_dist) {
            points.erase(points.begin()+i);
        }
    }
    printf("%lu points remain\n", points.size());
    // Fit a second line
    std::vector<double> line2(4, 0.0); // a line that goes through point (line[0], line[1]) and point (line[2], line[3])
    const float line2_fit=fitLine(points, line2, inlier_dist, &ellipse_edge2);
    cv::line(img, cv::Point(line2[0], line2[1]), cv::Point(line2[2], line2[3]), cv::Scalar(255, 255, 255));
    printf("line2_fit=%g\n", line2_fit/std::sqrt(std::pow(line2[0]-line2[2], 2)+std::pow(line2[1]-line2[3], 2)));
    for(int i=points.size()-1; i>0; --i) { // Draw all inliers
        if(distPointToLine2D(points[i].x, points[i].y, line2[0], line2[1], line2[2], line2[3], true)<inlier_dist) {
            points.erase(points.begin()+i);
        }
    }
    printf("%lu points remain\n", points.size());
    
    // Now try to fit a second ellipse
    cv::RotatedRect ellipse2;
    const float ellipse2_fit=fitEllipse(points, img, ellipse2, inlier_dist, new cv::Point2f(line[2], line[3]), new cv::Point2f(line2[2], line2[3]));
    cv::ellipse(img, ellipse2, cv::Scalar(0, 255, 255)); // TODO this does not (should not) work with the const img.
    printf("fit of second ellipse=%g\n", ellipse2_fit);
    
    /*// Mark all inliers
    const float sina=std::sin(ellipse.angle/180.0*3.14159), cosa=std::cos(ellipse.angle/180.0*3.14159);
    for(const auto& p : points) { // Draw all inliers
        const float xrot =  cosa*(p.x-ellipse.center.x)+sina*(p.y-ellipse.center.y);
        const float yrot = -sina*(p.x-ellipse.center.x)+cosa*(p.y-ellipse.center.y);
        if(dist2Ellipse(ellipse.size.width/2.0, ellipse.size.height/2.0, xrot, yrot)<inlier_dist) {
            img.at<cv::Vec3b>(p.y, p.x)=cv::Vec3b(255, 0, 0);
        }
    }*/
    std::stringstream sstream;
    sstream << "/tmp/output" << i << ".png";
    cv::imwrite(sstream.str(), img);
    ellipse_out1=ellipse;
    ellipse_out2=ellipse2;
    return ellipse_fit+line_fit+line2_fit+ellipse2_fit;
}

// cloud must be in BASE_FRAME!
plane_t * searchAndAddTableToScene(pcl::PointCloud<point_t>::Ptr cloud,
                               const std::string& frame,
                               const image_geometry::PinholeCameraModel * cam_model,//TODO rgb or depth cam model?
                               const bool onlyHorizontalPlanes,
                               const bool addToPlanningScene,
                               const bool makeTableToBox,
                               const float safetyMargin) {
  ROS_INFO("searchAndAddTableToScene(frame=%s, onlyHorizontalPlanes=%s, addToPlanningScene=%s, makeTableToBox=%s, safetyMargin=%g) called", frame.c_str(), (onlyHorizontalPlanes?"true":"false"), (addToPlanningScene?"true":"false"), (makeTableToBox?"true":"false"), safetyMargin);
  std::vector<plane_t> planes;
  for (;;) {
    // TODO this only works if planes is overwritten and always only contains the newest plane
    planes = findPlanes(cloud, 0, 1); // First try to find horizontal planes ...
    if (planes.empty()) {
      if (!onlyHorizontalPlanes) {
        ROS_WARN("Did not find horizontal plane, trying to find any planes");
        planes = findPlanes(cloud, 2, 1); // ... if that does not give any results, try to find any planes
      }
      if (planes.empty()) {
        break;
      }
    }
    ROS_INFO_STREAM("searchAndAddTableToScene(): plane: " << planes[0].plane << " " << planes[0].bbox);
    ROS_INFO_STREAM("Size of point cloud: " << cloud->size());
    if (planes[0].bbox.center.position.z > 0.2) {
        if(cam_model != nullptr) {
          // TODO project edges and check if they are distant enough to image edges
          Eigen::Isometry3d transform = getIsometryTransform(*tfBuffer1, frame, BASE_FRAME, ros::Time(0)); // TODO get as parameter? // TODO correct time
          for (int k = -1; k <= 1; k += 2) { // vary all three dims (all eight corners), even though only two would be necessary (four pairs of corners close to each other), but all is easier
            for (int l = -1; l <= 1; l += 2) {
              for (int m = -1; m <= 1; m += 2) { //TODO subimageBoundaries?
                float imx, imy;
                tf2::Vector3 vec(k * planes[0].bbox.size.x / 2.0,
                                 l * planes[0].bbox.size.y / 2.0,
                                 m * planes[0].bbox.size.z / 2.0);
                tf2::Quaternion rot;
                tf2::fromMsg(planes[0].bbox.center.orientation, rot);
                tf2::Transform trans(rot);
                vec = trans * vec;
                vec += tf2::Vector3(planes[0].bbox.center.position.x, planes[0].bbox.center.position.y, planes[0].bbox.center.position.z);
                Eigen::Vector3d vec3(vec.x(), vec.y(), vec.z());
                vec3 = transform * vec3;
                // ROS_INFO("vec=(%g,%g,%g)", vec.x(), vec.y(), vec.z());
                cv::Point3d pt_cv(vec3.x(), vec3.y(), vec3.z());
                cv::Point2d uv = cam_model->project3dToPixel(pt_cv);
                imx = uv.x;
                imy = uv.y;
                // ROS_INFO("imx=%f, imy=%f\n", imx, imy);
                const float margin = 5.0;
                if (imx < margin || imy < margin ||
                    imx > (cam_model->fullResolution().width - margin) ||
                    imy > (cam_model->fullResolution().height - margin)) {
                  ROS_WARN("Table corner out of the image: imx=%f, imy=%f (point at %f, %f, %f)", imx, imy, vec3.x(), vec3.y(), vec3.z());
                }
              }
            }
          }
      }

      if (addToPlanningScene) {
        ROS_INFO("searchAndAddTableToScene(): adding to planning scene");
        moveit_msgs::CollisionObject collision_object;
        collision_object.id = "table";
        collision_object.header.frame_id = BASE_FRAME;
        collision_object.primitives.resize(1);
        collision_object.primitives[0].type = collision_object.primitives[0].BOX;
        collision_object.primitives[0].dimensions.resize(3);
        collision_object.primitives[0].dimensions[0] = planes[0].bbox.size.x + safetyMargin;
        collision_object.primitives[0].dimensions[1] = planes[0].bbox.size.y + safetyMargin;
        collision_object.primitives[0].dimensions[2] = planes[0].bbox.size.z + safetyMargin; // TODO minimum/maximum/fixed size?
        collision_object.primitive_poses.resize(1);
        collision_object.primitive_poses[0] = planes[0].bbox.center;
        if (makeTableToBox) { // Add a box, not a plane
          // Change dimension and position
          size_t index;
          if (collision_object.primitives[0].dimensions[0] <=
              collision_object.primitives[0].dimensions[1]) {
            if (collision_object.primitives[0].dimensions[0] <=
                collision_object.primitives[0].dimensions[2]) {
              index = 0;
            } else {
              index = 2;
            }
          } else {
            if (collision_object.primitives[0].dimensions[1] <=
                collision_object.primitives[0].dimensions[2]) {
              index = 1;
            } else {
              index = 2;
            }
          }
          collision_object.primitives[0].dimensions[index] =
              planes[0].bbox.center.position.z + std::max(safetyMargin-0.03, 0.0); // Less safety margin in z direction
          collision_object.primitive_poses[0].position.z /= 2.0;
        }
        collision_object.operation = collision_object.ADD;
        if ((planning_scene_interface1) != nullptr) {
          if(!(planning_scene_interface1)->applyCollisionObject(collision_object)) {
            ROS_WARN("applyCollisionObject() failed");
          } else {
            ROS_INFO("added table successfully to the planning scene");
          }
        } else {
          ROS_WARN_STREAM("searchAndAddTableToScene(): "
                          "planning_scene_interface is NULL so could not add "
                          "collision object: "
                          << collision_object);
        }
      }
      ROS_INFO_STREAM("searchAndAddTableToScene: found table with height "
                      << planes[0].bbox.center.position.z);
      //return &planes[0]; // TODO this is invalid! pointer will point to invalid memory!
      return (new plane_t(planes[0]));
    } else {
        ROS_WARN("Plane z position is under 0.2, cannot be a table");
    }
  }
  ROS_WARN("searchAndAddTableToScene: found no table");
  return nullptr;
}

void getShelfSize(double * shelf_height_p, double * shelf_width_p, double * shelf_depth_p, std::vector<float> * shelf_floor_heights_p) {
    if(shelf_height_p!=nullptr) *shelf_height_p=shelf_height;
    if(shelf_width_p!=nullptr) *shelf_width_p=shelf_width;
    if(shelf_depth_p!=nullptr) *shelf_depth_p=shelf_depth;
    if(shelf_floor_heights_p!=nullptr) *shelf_floor_heights_p=shelf_floor_heights;
}

bool searchForShelf(pcl::PointCloud<point_t>::Ptr point_cloud, float &x,
                    float &y, float &angle) {
  ROS_INFO("searchForShelf called, cloud has size %lu", point_cloud->size());
  x = 0.0f;
  y = 0.0f;
  angle = 0.0f;
  if (point_cloud->empty()) {
    ROS_WARN("searchForShelf(): cloud is empty, so returning");
    return false;
  }

  deleteAllPlaneMarkers();

  //TODO maybe filter by color?
  pcl::PointCloud<point_t>::Ptr clustered =
  //    cluster(point_cloud, false, true); // TODO cluster might "destroy" the point_cloud?
        point_cloud;
  ROS_INFO("Clustered point cloud, size: %lu", clustered->size());

  std::vector<plane_t> vplanes = findPlanes(clustered, 1, 3, 15000); // TODO is searching for 3 planes enough?
  ROS_INFO("cloud size after extracting %lu vertical planes: %lu", vplanes.size(), clustered->size());

  std::vector<plane_t> hplanes = findPlanes(clustered, 0, 15, 10000);
  ROS_INFO("cloud size after extracting %lu horizontal planes: %lu", hplanes.size(), clustered->size());

  //std::vector<plane_t> more_hplanes = findPlanes(clustered, 0, 15, 10000); // TODO why does this make sense?
  //hplanes.insert(hplanes.end(), more_hplanes.begin(), more_hplanes.end());

  if ((hplanes.size() + vplanes.size()) == 0) {
    ROS_WARN("searchForShelf: found no planes");
    std::vector<plane_t> any_planes = findPlanes(clustered, 2, 15, 2000);
    ROS_INFO("any_planes size: %lu", any_planes.size());
    if (!any_planes.empty()) {
      x = any_planes[0].bbox.center.position.x - shelf_depth / 2.0;
    }
    return false;
  }
  ROS_INFO("searchForShelf: got %lu horizontal planes and %lu vertical planes",
           hplanes.size(), vplanes.size());
  double x_hguess=0.0, y_hguess=0.0, angle_hguess=0.0;
  for (size_t i = 0; i < hplanes.size();) { // At first look at horizontal planes and try to match them to the shelf floors (with known heights)
    ROS_INFO_STREAM("hplanes[i].bbox=" << std::endl << hplanes[i].bbox);
    bool isFloor = false; // Shelf floor, not ground
    for (size_t j = 0; j < shelf_floor_heights.size(); j++) {
      if (fabs(hplanes[i].bbox.center.position.z - shelf_floor_heights[j]) <
          3.0 * DISTANCE_THRESHOLD) {
        ROS_INFO("Seems to be shelf floor %lu", j);
        isFloor = true;
        break;
      }
    }
    if (!isFloor || fabs(hplanes[i].bbox.center.position.z) < 0.1) {
      ROS_INFO("plane is not a shelf floor");
      hplanes.erase(hplanes.begin() + i);
      continue;
    }
    x_hguess += hplanes[i].bbox.center.position.x;
    y_hguess += hplanes[i].bbox.center.position.y;
    double yaw = angleModulo(yaw_from_quat(hplanes[i].bbox.center.orientation) - M_PI / 2.0);
    if (yaw < -M_PI / 2.0 || yaw > M_PI / 2.0) {
      yaw = angleModulo(yaw + M_PI);
    }
    ROS_INFO("yaw=%f", yaw);
    angle_hguess += yaw; // Substract PI/2 because of how the oobb is oriented
    i++;
  }
  if (!hplanes.empty()) {
    x_hguess /= hplanes.size();
    y_hguess /= hplanes.size();
    angle_hguess = angleModulo(angle_hguess / hplanes.size());
  }
  tf2::Vector3 *veca = nullptr, *vecb = nullptr;
  for (size_t i = 0; i < vplanes.size(); i++) {
    ROS_INFO_STREAM("vertical plane (i=" << i << "): " << vplanes[i].bbox);
    tf2::Vector3 vec1(vplanes[i].plane.coef[0], vplanes[i].plane.coef[1], vplanes[i].plane.coef[2]);
    for (size_t j = i + 1; j < vplanes.size(); j++) {
      ROS_INFO_STREAM("vertical plane (j=" << j << "): " << vplanes[j].bbox);
      tf2::Vector3 vec2(vplanes[j].plane.coef[0], vplanes[j].plane.coef[1], vplanes[j].plane.coef[2]);
      const double angle_between_planes = acos(std::abs(vec1.x() * vec2.x() + vec1.y() * vec2.y() + vec1.z() * vec2.z()));
      ROS_INFO("angle between planes: %g", angle_between_planes);
      ROS_INFO("vec1: (%g,%g,%g)", vec1.x(), vec1.y(), vec1.z());
      ROS_INFO("vec2: (%g,%g,%g)", vec2.x(), vec2.y(), vec2.z());
      if (fabs(angle_between_planes - M_PI / 2.0) < 0.07) { // 0.07 rad = 4 degree
        ROS_INFO("Found perpendicular planes: %lu %lu", i, j);
        // Make sure vec1 and vec2 point "inward", else flip them
        double sdist=signedDistancePointToPlane(vec1.x(), vec1.y(), vec1.z(),
                       vec1.x() * vplanes[i].bbox.center.position.x + vec1.y() * vplanes[i].bbox.center.position.y + vec1.z() * vplanes[i].bbox.center.position.z, // TODO isn't this vplanes[i].plane.coef[3]?
                       vplanes[j].bbox.center.position.x, vplanes[j].bbox.center.position.y, vplanes[j].bbox.center.position.z);
        if (sdist < -0.1 || (sdist > -0.1 && sdist < 0.1 && signedDistancePointToPlane(vec1.x(), vec1.y(), vec1.z(),
                vec1.x() * vplanes[i].bbox.center.position.x + vec1.y() * vplanes[i].bbox.center.position.y + vec1.z() * vplanes[i].bbox.center.position.z, // TODO isn't this vplanes[i].plane.coef[3]?
                x_hguess, y_hguess, 0.0)<0.0)) { // If just looking at the two vertical planes does not give a clear result, also look at the guess from the horizontal planes
          vec1 = -vec1;
          ROS_INFO("Flipping vec1, is now (%g,%g,%g)", vec1.x(), vec1.y(), vec1.z());
        }
        sdist=signedDistancePointToPlane(vec2.x(), vec2.y(), vec2.z(),
                vec2.x() * vplanes[j].bbox.center.position.x + vec2.y() * vplanes[j].bbox.center.position.y + vec2.z() * vplanes[j].bbox.center.position.z, // TODO isn't this vplanes[j].plane.coef[3]?
                vplanes[i].bbox.center.position.x, vplanes[i].bbox.center.position.y, vplanes[i].bbox.center.position.z);
        if (sdist < -0.1 || (sdist > -0.1 && sdist < 0.1 && signedDistancePointToPlane(vec2.x(), vec2.y(), vec2.z(),
                vec2.x() * vplanes[j].bbox.center.position.x + vec2.y() * vplanes[j].bbox.center.position.y + vec2.z() * vplanes[j].bbox.center.position.z, // TODO isn't this vplanes[j].plane.coef[3]?
                x_hguess, y_hguess, 0.0)<0.0)) { // If just looking at the two vertical planes does not give a clear result, also look at the guess from the horizontal planes
          vec2 = -vec2;
          ROS_INFO("Flipping vec2, is now (%g,%g,%g)", vec2.x(), vec2.y(), vec2.z());
        }
        double xs, ys;
        intersectionPointOfTwoLines2D(
            vec1.x(), vec1.y(),
            vec1.x() * vplanes[i].bbox.center.position.x +
                vec1.y() * vplanes[i].bbox.center.position.y,
            vec2.x(), vec2.y(),
            vec2.x() * vplanes[j].bbox.center.position.x +
                vec2.y() * vplanes[j].bbox.center.position.y,
            xs, ys);
        ROS_INFO("Intersection point: (%g,%g)", xs, ys);
        if (fabs(vplanes[i].bbox.size.y - shelf_width) < 0.05 || // TODO is it really certain that size.y is the horizontal length?
            fabs(vplanes[j].bbox.size.y - shelf_depth) < 0.05) { // Plane i is back and plane j is side
          x = xs + vec1.x() * shelf_depth / 2.0 + vec2.x() * shelf_width / 2.0;
          y = ys + vec1.y() * shelf_depth / 2.0 + vec2.y() * shelf_width / 2.0;
          angle = angleModulo(atan2(vec1.y(), vec1.x()) + M_PI);
          ROS_INFO("Computed x = %g + %g * %g / 2.0 + %g * %g / 2.0 = %g", xs, vec1.x(), shelf_depth, vec2.x(), shelf_width, x);
          ROS_INFO("Computed y = %g + %g * %g / 2.0 + %g * %g / 2.0 = %g", ys, vec1.y(), shelf_depth, vec2.y(), shelf_width, y);
          ROS_INFO("Computed angle= angleModulo(atan2(%g, %g) + M_PI) = angle (with atan2(%g, %g)=%g)", vec1.y(), vec1.x(), vec1.y(), vec1.x(), atan2(vec1.y(), vec1.x()));
          ROS_INFO("Estimate 2 from edge: x=%g y=%g angle=%g", x, y, angle);
          return true;
        }
        if (fabs(vplanes[i].bbox.size.y - shelf_depth) < 0.05 || // TODO is it really certain that size.y is the horizontal length?
            fabs(vplanes[j].bbox.size.y - shelf_width) < 0.05) { // Plane i is side and plane j is back
          x = xs + vec1.x() * shelf_width / 2.0 + vec2.x() * shelf_depth / 2.0;
          y = ys + vec1.y() * shelf_width / 2.0 + vec2.y() * shelf_depth / 2.0;
          angle = angleModulo(atan2(vec2.y(), vec2.x()) + M_PI);
          ROS_INFO("Estimate 1 from edge: x=%g, y=%g angle=%g", x, y, angle);
          return true;
        }
      } else if (angle_between_planes < 0.07) { // 0.07 rad = 4 degree
        const double plane_distance = fabs(vec1.x() * vplanes[i].bbox.center.position.x +
                               vec1.y() * vplanes[i].bbox.center.position.y +
                               vec1.z() * vplanes[i].bbox.center.position.z -
                               vec1.x() * vplanes[j].bbox.center.position.x -
                               vec1.y() * vplanes[j].bbox.center.position.y -
                               vec1.z() * vplanes[j].bbox.center.position.z);
        ROS_INFO("Parallel planes: %lu %lu dist=%g", i, j, plane_distance);
        double d1 = -(vec1.x() * vplanes[i].bbox.center.position.x +
                      vec1.y() * vplanes[i].bbox.center.position.y);
        double d2 = -(vec2.x() * vplanes[j].bbox.center.position.x +
                      vec2.y() * vplanes[j].bbox.center.position.y);
        if (fabs(plane_distance - shelf_depth) < 0.1) {
          ROS_INFO("Probably front and back");
          veca = new tf2::Vector3(vec1.y(), -vec1.x(), (d1 + d2) / 2.0);
        }
        if (fabs(plane_distance - shelf_width) < 0.1) {
          ROS_INFO("Probably the two sides");
          vecb = new tf2::Vector3(vec1.y(), -vec1.x(), (d1 + d2) / 2.0);
        }
      } else {
        ROS_INFO("Planes are neither perpendicular nor parallel.");
      }
    }
    ROS_INFO("yaw=%g", yaw_from_quat(vplanes[i].bbox.center.orientation));
    //*angle+=yaw_from_quat(vplanes[i].bbox.center.orientation);
  }
  if (veca != nullptr) {
    ROS_INFO("veca: (%g,%g,%g)", veca->x(), veca->y(), veca->z());
  }
  if (vecb != nullptr) {
    ROS_INFO("vecb: (%g,%g,%g)", vecb->x(), vecb->y(), vecb->z());
  }
  /**x=(veca->z()*vecb->y()-vecb->z()*veca->y())/(veca->x()*vecb->y()-vecb->x()*veca->y());
  *y=(veca->x()*vecb->z()-vecb->x()*veca->z())/(veca->x()*vecb->y()-vecb->x()*veca->y());
  *angle=atan2(veca->x(), veca->y());*/
  // Eigen::Quaterniond quat(.center.orientation.w, .center.orientation.x,
  // .center.orientation.y, .center.orientation.z);
  // toEulerAngle(quat, &roll, &pitch, &yaw);
  if (!hplanes.empty()) {
    x = x_hguess;
    y = y_hguess;
    angle = angle_hguess;
    return true;
  } else {
    ROS_WARN("searchForShelf: no hplanes");
    return false;
  }
}

bool addShelfToPlanningScene(const float x, const float y, const float angle, pcl::PointCloud<point_t>::Ptr point_cloud) {
  const float safety = 0.04;
  ROS_INFO("addShelfToPlanningScene called with parameters x=%g y=%g angle=%g", x, y, angle);
  if(x<0.0 || x>10.0 || y<-5.0 || y>5.0 || angle<-3.141 || angle>3.141)
    ROS_WARN("addShelfToPlanningScene(): parameters seem unlikely, expect 0<x<10, -5<y<5, -3.141<angle<3.141");
  const Eigen::Isometry3d transform(Eigen::Translation3d(Eigen::Vector3d(x, y, 0.0)) * Eigen::AngleAxisd(angle, Eigen::Vector3d::UnitZ()));
  std::vector<moveit_msgs::CollisionObject> shelf_parts;
  shelf_parts.reserve(1 + shelf_floor_heights.size());
  moveit_msgs::CollisionObject shelf;
  shelf.header.frame_id = BASE_FRAME;
  shelf.id = "shelf";
  shelf.primitives.resize(3);
  shelf.primitive_poses.resize(3);
  // left side
  shelf.primitives[0].type = shelf.primitives[0].BOX;
  shelf.primitives[0].dimensions.resize(3);
  shelf.primitives[0].dimensions[0] = shelf_depth + safety;
  shelf.primitives[0].dimensions[1] = shelf_thickness + safety;
  shelf.primitives[0].dimensions[2] = shelf_height + safety;
  shelf.primitive_poses[0] = tf2::toMsg(transform * Eigen::Isometry3d(Eigen::Translation3d(Eigen::Vector3d(0.0, shelf_width / 2.0, shelf_height / 2.0))));
  // right side
  shelf.primitives[1].type = shelf.primitives[1].BOX;
  shelf.primitives[1].dimensions.resize(3);
  shelf.primitives[1].dimensions[0] = shelf_depth + safety;
  shelf.primitives[1].dimensions[1] = shelf_thickness + safety;
  shelf.primitives[1].dimensions[2] = shelf_height + safety;
  shelf.primitive_poses[1] = tf2::toMsg(transform * Eigen::Isometry3d(Eigen::Translation3d(Eigen::Vector3d(0.0, -shelf_width / 2.0, shelf_height / 2.0))));
  // back
  shelf.primitives[2].type = shelf.primitives[2].BOX;
  shelf.primitives[2].dimensions.resize(3);
  shelf.primitives[2].dimensions[0] = shelf_thickness + safety;
  shelf.primitives[2].dimensions[1] = shelf_width + safety;
  shelf.primitives[2].dimensions[2] = shelf_height + safety;
  shelf.primitive_poses[2] = tf2::toMsg(transform * Eigen::Isometry3d(Eigen::Translation3d(Eigen::Vector3d(shelf_depth / 2.0, 0.0, shelf_height / 2.0))));
  // door
  /*const float door_angle=0.75;
  shelf.primitives[3].type = shelf.primitives[3].BOX;
  shelf.primitives[3].dimensions.resize(3);
  shelf.primitives[3].dimensions[0] = shelf_door_width + safety;
  shelf.primitives[3].dimensions[1] = shelf_thickness + safety;
  shelf.primitives[3].dimensions[2] = shelf_height + safety;
  shelf.primitive_poses[3] = tf2::toMsg(transform * Eigen::Isometry3d(Eigen::Translation3d(Eigen::Vector3d(-shelf_depth / 2.0, shelf_width / 2.0, shelf_height / 2.0))
                                                  * Eigen::AngleAxisd(door_angle, Eigen::Vector3d::UnitZ())
                                                  * Eigen::Translation3d(Eigen::Vector3d(-shelf_door_width / 2.0, 0.0, 0.0))));*/

  shelf.operation = shelf.ADD;
  shelf_parts.push_back(shelf);
  publishCOasMarker(shelf, 0);
  // shelf floors
  for (size_t i = 0; i < shelf_floor_heights.size(); i++) {
    moveit_msgs::CollisionObject shelf_floor;
    shelf_floor.header.frame_id = BASE_FRAME;
    std::ostringstream oss;
    oss << "shelf_floor" << i;
    shelf_floor.id = oss.str();
    shelf_floor.primitives.resize(1);
    shelf_floor.primitive_poses.resize(1);
    shelf_floor.primitives[0].type = shelf_floor.primitives[0].BOX;
    shelf_floor.primitives[0].dimensions.resize(3);
    shelf_floor.primitives[0].dimensions[0] = shelf_depth + safety;
    shelf_floor.primitives[0].dimensions[1] = shelf_width + safety;
    shelf_floor.primitives[0].dimensions[2] =
        shelf_thickness /*+safety*/; // Here no safety because space between
                                     // shelf floors is pretty tight anyway
    shelf_floor.primitive_poses[0] = tf2::toMsg(transform * Eigen::Isometry3d(Eigen::Translation3d(Eigen::Vector3d(0.0, 0.0, shelf_floor_heights[i]))));
    shelf_floor.operation = shelf_floor.ADD;
    shelf_parts.push_back(shelf_floor);
    publishCOasMarker(shelf_floor, 10*i+10);
  }
  // filter point cloud
  if(point_cloud!=nullptr) {
    ROS_INFO("addShelfToPlanningScene(): point cloud size before filtering: %lu", point_cloud->size());
    pcl::CropBox<point_t> cbox;
    cbox.setNegative(true);
    for (const moveit_msgs::CollisionObject& co : shelf_parts) {
      for (size_t i=0; i<co.primitives.size(); ++i) {
        if(co.primitives.at(i).type == co.primitives.at(i).BOX && co.primitives.at(i).dimensions.size()==3) {
          cbox.setInputCloud(point_cloud);
          cbox.setMin(Eigen::Vector4f(-co.primitives.at(i).dimensions[0] / 2.0, -co.primitives.at(i).dimensions[1] / 2.0, -co.primitives.at(i).dimensions[2] / 2.0, 1.0));
          cbox.setMax(Eigen::Vector4f( co.primitives.at(i).dimensions[0] / 2.0,  co.primitives.at(i).dimensions[1] / 2.0,  co.primitives.at(i).dimensions[2] / 2.0, 1.0));
          Eigen::Affine3f t =
            Eigen::Translation3f(co.primitive_poses.at(i).position.x, co.primitive_poses.at(i).position.y,
                                 co.primitive_poses.at(i).position.z) *
            Eigen::Quaternionf(co.primitive_poses.at(i).orientation.w, co.primitive_poses.at(i).orientation.x,
                               co.primitive_poses.at(i).orientation.y, co.primitive_poses.at(i).orientation.z);
          cbox.setTransform(t.inverse());
          cbox.filter(*point_cloud);
        }
      }
    }
    ROS_INFO("addShelfToPlanningScene(): point cloud size after filtering: %lu", point_cloud->size());
  }
  if (planning_scene_interface1 != nullptr) {
    if(!planning_scene_interface1->applyCollisionObjects(shelf_parts)) {
        ROS_WARN("applyCollisionObjects() failed");
        return false;
    }
    return true;
  } else {
    ROS_WARN("addShelfToPlanningScene(): planning_scene_interface is NULL");
    return false;
  }
}

std::string computeTargetPosition(const std::string& name, double &x, double &y,
                                  double &z, double &sizex, double &sizey,
                                  double &angle, float shelf_x, float shelf_y,
                                  float shelf_angle, moveit_msgs::CollisionObject * object) { // TODO maybe determine shelf position from planning scene if possible?
  ROS_INFO("computeTargetPosition with parameters name=%s, shelf_x=%g, shelf_y=%g, shelf_angle=%g",
           name.c_str(), shelf_x, shelf_y, shelf_angle);
  std::map<std::string, moveit_msgs::CollisionObject> objects =
        planning_scene_interface1->getObjects();
  if(object!=nullptr) {
    try {
      *object = objects.at(name);
    } catch (const std::out_of_range& oor) {
      ROS_INFO("computeTargetPosition(): object is not known to planning scene");
    }
  }
  // look up category of object
  const uint8_t category = getCategoryOfObject(getNameFromId(name));
  if (category != 255) {
    ROS_INFO("computeTargetPosition: category=%u (%s)", category, getCategories()[category].c_str());
    for (std::map<std::string, moveit_msgs::CollisionObject>::const_iterator
             iter = objects.begin();
         iter != objects.end(); ++iter) {
      if (/*!iter->second.type.key.empty() && category==getCategoryOfObject(iter->second.type.key)*/ category ==
              getCategoryOfObject(getNameFromId(iter->second.id)) &&
          !(name == iter->second.id)) {
        ROS_INFO_STREAM(
            "computeTargetPosition: found object from same category: found "
            << iter->second.id);
        std::ostringstream oss;
        for (size_t j = 0; j < (shelf_floor_heights.size() - 1); j++) {
          if (iter->second.primitive_poses[0].position.z >
                  (shelf_floor_heights[j] - 0.03) &&
              iter->second.primitive_poses[0].position.z <
                  (shelf_floor_heights[j + 1] - 0.03)) {
            ROS_INFO("computeTargetPosition: seems to be shelf floor %lu", j);
            z = (shelf_floor_heights[j] + shelf_floor_heights[j + 1]) / 2.0;
            oss << "shelf_floor" << j;
            break;
          }
        } // TODO what if not found?
        float dist_along_shelf =
            (iter->second.primitive_poses[0].position.x - shelf_x) *
                sin(shelf_angle) +
            (iter->second.primitive_poses[0].position.y - shelf_y) *
                cos(shelf_angle);
        if (dist_along_shelf > shelf_width / 2.0) {
          dist_along_shelf = shelf_width / 2.0;
        }
        if (dist_along_shelf < -shelf_width / 2.0) {
          dist_along_shelf = -shelf_width / 2.0;
        }
        x = shelf_x + dist_along_shelf * sin(shelf_angle);
        y = shelf_y + dist_along_shelf * cos(shelf_angle);
        sizex = shelf_depth;
        sizey = 0.4; // 40 centimetres
        angle = shelf_angle;
        // TODO publish marker to marker_pub
        return oss.str();
      }
    }
  }
  // TODO consider objects with high similarity or likeliness, whatever that exactly may mean (shapes/dimensions?)
  ROS_WARN("computeTargetPosition: could not find category of object");
  // No similar object found
  ROS_WARN("computeTargetPosition: no similar object found, return middle shelf");
  x = shelf_x;
  y = shelf_y;
  z = (shelf_floor_heights[2] + shelf_floor_heights[3]) / 2.0;
  sizex = shelf_depth;
  sizey = shelf_width;
  angle = shelf_angle;
  return "shelf_floor2";
}

void primitive_stability_wrapper(const plane_t& plane, const Eigen::Vector3f& gravity, moveit_msgs::CollisionObject& collision_object) {
  Eigen::Vector3f position(collision_object.primitive_poses[0].position.x, collision_object.primitive_poses[0].position.y, collision_object.primitive_poses[0].position.z);
  Eigen::Quaternionf orientation(collision_object.primitive_poses[0].orientation.w, collision_object.primitive_poses[0].orientation.x,
                                 collision_object.primitive_poses[0].orientation.y, collision_object.primitive_poses[0].orientation.z);
  
  if(collision_object.primitives[0].type == 1) {
    primitive_stability(plane, gravity, BOX, Eigen::Vector3f(collision_object.primitives[0].dimensions[0], collision_object.primitives[0].dimensions[1], collision_object.primitives[0].dimensions[2]), position, orientation);
  } else if(collision_object.primitives[0].type == 3) {
    primitive_stability(plane, gravity, CYLINDER, Eigen::Vector3f(collision_object.primitives[0].dimensions[0], 2.0*collision_object.primitives[0].dimensions[1], 2.0*collision_object.primitives[0].dimensions[1]), position, orientation);
  }
  ROS_INFO_STREAM("corrected position: " << position << " and corrected orientation: " << orientation);
  
  collision_object.primitive_poses[0].position = tf2::toMsg(position);
  //collision_object.primitive_poses[0].position.x = position.x();
  //collision_object.primitive_poses[0].position.y = position.y();
  //collision_object.primitive_poses[0].position.z = position.z();
  tf2::Quaternion q(orientation.x(), orientation.y(), orientation.z(), orientation.w()); //TODO use orientation directly?
  q.normalize();
  collision_object.primitive_poses[0].orientation = tf2::toMsg(q);
}

void primitive_stability(const plane_t plane, const Eigen::Vector3f& gravity, const primitive_t shape, const Eigen::Vector3f& size, Eigen::Vector3f& position, Eigen::Quaternionf& orientation) { // TODO consider center of gravity? uneven distribution of weight? adjust size? reject?
    ROS_INFO_STREAM("primitive_stability called, gravity=\n" << gravity << "\nplane=\n" << plane.plane);
    Eigen::Vector3f plane_normal(plane.plane.coef[0], plane.plane.coef[1], plane.plane.coef[2]);//TODO maybe flip 180 degrees?
    ROS_INFO("Distance of object to plane: %f", signedDistancePointToPlane(plane.plane.coef[0], plane.plane.coef[1], plane.plane.coef[2], -plane.plane.coef[3], position.x(), position.y(), position.z()));
    switch(shape) {
        case OTHER:
            ROS_INFO("primitive_stability: shape OTHER, nothing can be done");
            break;
        case BOX: {// can stand on each of its six faces
            Eigen::Vector3f inv_grav=(orientation.inverse()) * gravity;
            ROS_INFO_STREAM("inverse rotated gravity:"<< inv_grav);
            uint8_t index;
            Eigen::Vector3f up;
            if(fabs(inv_grav.x())>fabs(inv_grav.y())) {
                if(fabs(inv_grav.x())>fabs(inv_grav.z())) {
                    index=0;
                    up << (inv_grav.x()>0.0?-1.0:1.0), 0.0, 0.0;
                } else {
                    index=2;
                    up << 0.0, 0.0, (inv_grav.z()>0.0?-1.0:1.0);
                }
            } else {
                if(fabs(inv_grav.y())>fabs(inv_grav.z())) {
                    index=1;
                    up << 0.0, (inv_grav.y()>0.0?-1.0:1.0), 0.0;
                } else {
                    index=2;
                    up << 0.0, 0.0, (inv_grav.z()>0.0?-1.0:1.0);
                }
            }
            ROS_INFO_STREAM("index=" << (index+'\0') << " up=" << up.x() << "," << up.y() << "," << up.z());
            orientation=Eigen::Quaternionf::FromTwoVectors(orientation * up, plane_normal) * orientation;
            position+=(size[index]/2.0-fabs(signedDistancePointToPlane(plane.plane.coef[0], plane.plane.coef[1], plane.plane.coef[2], -plane.plane.coef[3], position.x(), position.y(), position.z())))*plane_normal;
            break;
        }
        case SPHERE: // just has to touch the plane somewhere, the orientation is totally arbitrary. Maybe check if plane is not horizontal and sphere might roll away?
            position+=(size.x()/2.0-fabs(signedDistancePointToPlane(plane.plane.coef[0], plane.plane.coef[1], plane.plane.coef[2], -plane.plane.coef[3], position.x(), position.y(), position.z())))*plane_normal;
            break;
        case CYLINDER: { // can stand on the two sides (bases) or lie on the side
            ROS_INFO("it's a cylinder!");
            Eigen::Vector3f up=orientation * Eigen::Vector3f(0.0, 0.0, 1.0);
            double angle=angleBetweenVectors(up.x(), up.y(), up.z(), gravity.x(), gravity.y(), gravity.z());
            ROS_INFO("Angle between up and gravity: %f, up=(%f, %f, %f), orientation=(%f, %f, %f, %f)", angle, up.x(), up.y(), up.z(),
                     orientation.x(), orientation.y(), orientation.z(), orientation.w());
            if(angle < M_PI / 4.0) { // top base
                ROS_INFO("Cylinder is standing on top base");
                orientation=Eigen::Quaternionf::FromTwoVectors(up, plane_normal)*orientation;
                position+=(size.x()/2.0-fabs(signedDistancePointToPlane(plane.plane.coef[0], plane.plane.coef[1], plane.plane.coef[2], -plane.plane.coef[3],
                                                                        position.x(), position.y(), position.z())))*plane_normal;
            } else if(angle > 3.0 * M_PI / 4.0) { // bottom base
                ROS_INFO("Cylinder is standing on bottom base");
                Eigen::Vector3f down=orientation * Eigen::Vector3f(0.0, 0.0, -1.0);
                orientation=Eigen::Quaternionf::FromTwoVectors(down, plane_normal)*orientation;
                position+=(size.x()/2.0-fabs(signedDistancePointToPlane(plane.plane.coef[0], plane.plane.coef[1], plane.plane.coef[2], -plane.plane.coef[3],
                                                                        position.x(), position.y(), position.z())))*plane_normal;
            } else { // side
                ROS_INFO("Cylinder is lying on side");
                Eigen::Vector3f axis=up.cross(plane_normal);//We can be sure that the vectors are not (near) parallel to each other
                axis*=1.0/axis.norm();
                double angle=acos(plane_normal.dot(up))-M_PI/2.0;
                ROS_INFO("Rotating by angle=%f around axis=(%f, %f, %f)", angle, axis.x(), axis.y(), axis.z());
                orientation=Eigen::Quaternionf(cos(angle/2.0), axis.x()*sin(angle/2.0), axis.y()*sin(angle/2.0), axis.z()*sin(angle/2.0))*orientation;
                position+=(size.y()/2.0-fabs(signedDistancePointToPlane(plane.plane.coef[0], plane.plane.coef[1], plane.plane.coef[2], -plane.plane.coef[3],
                                                                        position.x(), position.y(), position.z())))*plane_normal;
            }
            break;
        }
        case CONE: // can stand on the one side (base) or lie on the side
            ROS_ERROR("primitive_stability: shape CONE is not yet supported");
            break;
        default:
            ROS_ERROR("primitive_stability: unknown or unsupported shape: %d", shape);
    }
}


typedef struct {
    std::map<std::string, float> object_probabilities;
    float object_uncertainty;
    std::map<primitive_t, float> shape_probabilities;
    float shape_uncertainty;
    Eigen::Vector3f position; // TODO what frame?
    Eigen::Matrix3f position_var;
    Eigen::Quaternionf orientation; // TODO what frame?
    Eigen::Matrix4f orientation_var;
    Eigen::Vector3f size;
    Eigen::Matrix3f size_var;
    // TODO point cloud?
} object_info_t;

template <typename type>
void normalizeProbabilities(std::map<type, float>& x) {
    ROS_DEBUG("normalizeProbabilities() called");
    float sum=0.0;
    for (auto it : x) {
        ROS_DEBUG_STREAM("Item: " << it.first << " " << it.second);
        sum+=it.second;
    }
    ROS_DEBUG("Sum of all probabilities is %g, size of input map is %lu, normalizing ...", sum, x.size());
    if(sum>1.0e-5) {
        for (auto it = x.begin(); it != x.end(); ++it) { // This can't be a C++11 range-based for loop
            it->second/=sum;
        }
    } else {
        ROS_ERROR("sum is too small, so not normalizing");
    }
}

void orderVector(Eigen::Vector3f& v, bool asc) {
    ROS_INFO("orderVector() called: v=(%g,%g,%g)", v.x(), v.y(), v.z());
    const Eigen::Vector3f v_copy(v); // Necessary because operator<< overwrites the values, so without this, there will be garbage in v
    if(v.x() < v.y()) {
        if(v.z() < v.y()) {
            if(v.z() < v.x()) {
                if(asc) v << v_copy.z(), v_copy.x(), v_copy.y();
                else v << v_copy.y(), v_copy.x(), v_copy.z();
            } else {
                if(asc) v << v_copy.x(), v_copy.z(), v_copy.y();
                else v << v_copy.y(), v_copy.z(), v_copy.x();
            }
        } else {
            if(asc) v << v_copy.x(), v_copy.y(), v_copy.z();
            else v << v_copy.z(), v_copy.y(), v_copy.x();
        }
    } else {
        if(v.z() < v.x()) {
            if(v.z() < v.y()) {
                if(asc) v << v_copy.z(), v_copy.y(), v_copy.x();
                else v << v_copy.x(), v_copy.y(), v_copy.z();
            } else {
                if(asc) v << v_copy.y(), v_copy.z(), v_copy.x();
                else v << v_copy.x(), v_copy.z(), v_copy.y();
            }
        } else {
            if(asc) v << v_copy.y(), v_copy.x(), v_copy.z();
            else v << v_copy.z(), v_copy.x(), v_copy.y();
        }
    }
    ROS_INFO("orderVector() finished: v=(%g,%g,%g)", v.x(), v.y(), v.z());
}

template<int s>
void fuseContinuous(Eigen::Matrix<float, s, 1>& x3, Eigen::Matrix<float, s, s>& P3, // TODO what if a P is zero? warn? abort?
                    const Eigen::Matrix<float, s, 1>& x1, const Eigen::Matrix<float, s, s>& P1,
                    const Eigen::Matrix<float, s, 1>* xb, const Eigen::Matrix<float, s, s>* Pb) {
    const Eigen::Matrix<float, s, 1>& x2=(xb == nullptr || Pb == nullptr?x3:*xb);
    const Eigen::Matrix<float, s, s>& P2=(xb == nullptr || Pb == nullptr?P3:*Pb);
    ROS_DEBUG_STREAM("fuseContinuous called:\nx1:\n" << x1 << "\nP1:\n" << P1 << "\nx2:\n" << x2 << "\nP2:\n" << P2);
    if(P1.norm()<1e-6 && P2.norm()<1e-6)
        ROS_ERROR("fuseContinuous: norms of P1 and P2 are too small, so inverse might be unstable!");
    Eigen::Matrix<float, s, s> tmp0=(P1+P2).inverse();
    Eigen::Matrix<float, s, s> tmp1=P2*tmp0;
    Eigen::Matrix<float, s, s> tmp2=P1*tmp0;
    x3=tmp1*x1+tmp2*x2;
    P3=P1*tmp1*tmp1+P2*tmp2*tmp2; // TODO what's the correct order here? P*tmp*tmp or tmp*tmp*P?
    ROS_DEBUG_STREAM("fuseContinuous result:\n" << x3 << "\nP3:\n" << P3);
}

void fuseContinuous(Eigen::Quaternionf& x3, Eigen::Matrix4f& P3,
                    const Eigen::Quaternionf& x1, const Eigen::Matrix4f& P1,
                    const Eigen::Quaternionf* xb, const Eigen::Matrix4f* Pb) { // TODO detect identical quaternions? also with respect to shapes?
    if(xb==nullptr || Pb==nullptr) {
        Eigen::Vector4f x3_vec(x3.x(), x3.y(), x3.z(), x3.w());
        fuseContinuous<4>(x3_vec, P3, Eigen::Vector4f(x1.x(), x1.y(), x1.z(), x1.w()), P1);
        x3 = Eigen::Quaternionf(x3_vec.w(), x3_vec.x(), x3_vec.y(), x3_vec.z()).normalized();
    } else {
        Eigen::Vector4f x3_vec; // No initialization necessary, because the values are not read, only written to
        Eigen::Vector4f xb_vec(xb->x(), xb->y(), xb->z(), xb->w());
        fuseContinuous<4>(x3_vec, P3, Eigen::Vector4f(x1.x(), x1.y(), x1.z(), x1.w()), P1, &xb_vec, Pb);
        x3 = Eigen::Quaternionf(x3_vec.w(), x3_vec.x(), x3_vec.y(), x3_vec.z()).normalized();
    }
}

template<typename key>
void fuseDiscrete(std::map<key, float>& x3, float& weight3,
                  const std::map<key, float>& x1, const float weight1,
                  const std::map<key, float>& x2, const float weight2) {//TODO can x1, x2, and/or x3 be the same? TODO weight3? TODO remove weights (pow on x1/x2? somewhere else?)
    ROS_DEBUG("fuseDiscrete() started, weight1=%f, weight2=%f", weight1, weight2);
    ROS_DEBUG("x1:");
    for (auto it = x1.cbegin(); it != x1.cend(); ++it) {
        ROS_DEBUG_STREAM("entry: " << it->first << " " << it->second);
    }
    if(x1.empty()) {
        ROS_DEBUG("<empty>");
    }
    ROS_DEBUG("x2:");
    for (auto it = x2.cbegin(); it != x2.cend(); ++it) {
        ROS_DEBUG_STREAM("entry: " << it->first << " " << it->second);
    }
    if(x2.empty()) {
        ROS_DEBUG("<empty>");
    }
    
    /*
    // Weighted average:
    for (auto it = x1.cbegin(); it != x1.cend(); ++it) {
        x3[it->first] = weight1 * it->second;
    }
    for (auto it = x2.cbegin(); it != x2.cend(); ++it) {
        try {
            x3.at(it->first)=(weight1*x3.at(it->first)+weight2*(it->second))/(weight1+weight2);
        }
        catch (const std::out_of_range& oor) { // the key was not previously in the map
            x3[it->first] = weight2 * it->second;
        }
    }*/
    // Multiplication: TODO what if the intersection of x1 and x2 is empty?
    for (auto it = x1.cbegin(); it != x1.cend(); ++it) {
        try {
            x3[it->first] = pow(x2.at(it->first), weight2) * pow(it->second, weight1);
            ROS_DEBUG_STREAM("Fusing: " << it->first << " " << x2.at(it->first) << " " << it->second << ", result=" << x3[it->first]);
        }
        catch (const std::out_of_range& oor) { // the key was not in x2
            x3[it->first] = pow(0.001, weight2) * pow(it->second, weight1); // TODO This feels like a stopgap solution. Any better idea?
            //x3.erase(it->first);
        }
    }
    for (auto it = x2.cbegin(); it != x2.cend(); ++it) {
        if(x1.count(it->first) == 0) { // the key was not in x1
            x3[it->first] = pow(0.001, weight1) * pow(it->second, weight2); // TODO This feels like a stopgap solution. Any better idea?
            //x3.erase(it->first);
        }
    }
    
    normalizeProbabilities(x3);
    weight3 = weight1 + weight2 - weight1 * weight2;
    
    ROS_DEBUG("x3:");
    for (auto it = x3.cbegin(); it != x3.cend(); ++it) {
        ROS_DEBUG_STREAM("entry: " << it->first << " " << it->second);
    }
    if(x3.empty()) {
        ROS_DEBUG("<empty>");
        ROS_WARN("The output of fuseDiscrete is empty. Did something go wrong?");
    }
}

bool getSizeFromObjectProbabilities(const std::map<std::string, float>& obj_probs, Eigen::Vector3f& size, Eigen::Matrix3f& var) {//TODO what happens if the size of the object is not known? Especially "other"?
    ROS_DEBUG("getSizeFromObjectProbabilities() started");
    size << 0.0, 0.0, 0.0;
    var << 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0;
    if(getPossibleObjects().empty()) {
        ROS_WARN("getSizeFromObjectProbabilities(): no objects known, so returning false");
        return false;
    }
    float probs_sum = 0.0;
    for(size_t i=0; i<getPossibleObjects().size(); i++) {
        float dims2[3]={getPossibleObjects()[i].dim0, getPossibleObjects()[i].dim1,
getPossibleObjects()[i].dim2};
        std::sort(dims2, dims2+3, std::greater<float>());
        try {
            ROS_DEBUG("name = %s, dims2=(%f, %f, %f), prob=%f", getPossibleObjects()[i].name.c_str(), dims2[0], dims2[1], dims2[2], obj_probs.at(getPossibleObjects()[i].name));
        }
        catch (const std::out_of_range& oor) {
            ROS_DEBUG("name = %s, dims2=(%f, %f, %f), prob=0.0", getPossibleObjects()[i].name.c_str(), dims2[0], dims2[1], dims2[2]);
        }
        Eigen::Vector3f obj_dims(dims2[0], dims2[1], dims2[2]);
        try {
            size+=obj_probs.at(getPossibleObjects()[i].name)*obj_dims;
            probs_sum += obj_probs.at(getPossibleObjects()[i].name);
        }
        catch (const std::out_of_range& oor) { /* the object is not in map obj_probs, meaning the probability is zero, so ignore */ }
    }
    //size/=getPossibleObjects().size();
    for(size_t i=0; i<getPossibleObjects().size(); i++) {
        float dims2[3]={getPossibleObjects()[i].dim0, getPossibleObjects()[i].dim1,
getPossibleObjects()[i].dim2};
        std::sort(dims2, dims2+3, std::greater<float>());
        Eigen::Vector3f obj_dims(dims2[0], dims2[1], dims2[2]);
        Eigen::Vector3f tmp=obj_dims-size;
        try {
            var+=obj_probs.at(getPossibleObjects()[i].name)*(tmp*tmp.transpose());
        }
        catch (const std::out_of_range& oor) { /* the object is not in map obj_probs, meaning the probability is zero, so ignore */ }
    }
    if(fabs(probs_sum-1.0)>0.01) {
        ROS_WARN("getSizeFromObjectProbabilities(): probabilities do not sum to 1 (%f)", probs_sum);
    }
    ROS_DEBUG_STREAM("getSizeFromObjectProbabilities() finished.\nsize:\n" << size << "\nvar:\n" << var);
    return true;
}

std::map<primitive_t, float>
getShapeProbabilitiesFromObjectProbabilities(const std::map<std::string, float>& obj_probs) {
    ROS_DEBUG("getShapeProbabilitiesFromObjectProbabilities() started");
    std::map<primitive_t, float> shape_probs;
    for(size_t i=0; i<getPossibleObjects().size(); i++) {
        try {
            shape_probs[getPossibleObjects()[i].primitive]+=obj_probs.at(getPossibleObjects()[i].name);
        }
        catch (const std::out_of_range& oor) { /* the object is not in map obj_probs, meaning the probability is zero, so ignore */ }
    }
    normalizeProbabilities(shape_probs);
    ROS_DEBUG("result: shape_probs{OTHER:%f, BOX:%f, SPHERE:%f, CYLINDER:%f, CONE:%f}", shape_probs[OTHER], shape_probs[BOX], shape_probs[SPHERE], shape_probs[CYLINDER], shape_probs[CONE]);
    return shape_probs;
}

std::map<std::string, float> getObjectProbabilitiesFromShapeProbabilities(const std::map<primitive_t, float>& shape_probs) {
    ROS_DEBUG("getObjectProbabilitiesFromShapeProbabilities() started");
    std::map<std::string, float> obj_probs;
    for(size_t i=0; i<getPossibleObjects().size(); i++) {
        try {
            obj_probs[getPossibleObjects()[i].name]+=shape_probs.at(getPossibleObjects()[i].primitive);
        }
        catch (const std::out_of_range& oor) { /* the object is not in map shape_probs, meaning the probability is zero, so ignore */ }
    }
    normalizeProbabilities(obj_probs);
    ROS_DEBUG("getObjectProbabilitiesFromShapeProbabilities() finished");
    return obj_probs;
}

std::map<std::string, float> getObjectProbabilitiesFromSize(const Eigen::Vector3f& size, const Eigen::Matrix3f& var) {//size must be sorted
    ROS_DEBUG("getObjectProbabilitiesFromSize() started");
    std::map<std::string, float> result;
    float scoresum=0.0;
    std::vector<float> scores(getPossibleObjects().size());
    for(size_t i=0; i<getPossibleObjects().size(); i++) {
        float dims2[3]={getPossibleObjects()[i].dim0, getPossibleObjects()[i].dim1,
getPossibleObjects()[i].dim2};
        std::sort(dims2, dims2+3, std::greater<float>());
        Eigen::Vector3f obj_dims(dims2[0], dims2[1], dims2[2]);
        //float score=1.0/(fabsf(dims[0]-dims2[0])+fabsf(dims[1]-dims2[1])+fabsf(dims[2]-dims2[2]));
            //Alternative: difference of volume, but that is unsuited, because a 2x2x2 object and a 1x2x4 object have the same volume
        Eigen::Vector3f tmp=size-obj_dims;
        float score=exp(-0.5*tmp.transpose()*var.inverse()*tmp)/sqrt(pow(2.0*M_PI, 3)*var.determinant());//TODO log?
        scores[i]=score;
        scoresum+=score;
    }
    for(size_t i=0; i<getPossibleObjects().size(); i++) { // TODO use normalizeProbabilities instead? Does that do the same?
        result.insert(std::pair<std::string, float>(getPossibleObjects()[i].name,
scores[i]/scoresum));
    }
    ROS_DEBUG("getObjectProbabilitiesFromSize() finished");
    return result;
}

std::map<std::string, float> getObjectProbabilitiesFromSizeAndShape(const Eigen::Vector3f& size, const Eigen::Matrix3f& var, const std::map<primitive_t, float>& shape_probs) {
    ROS_DEBUG_STREAM("getObjectProbabilitiesFromSizeAndShape() started, size=(" << size.x() << ", " << size.y() << ", " << size.z() << ")");
    //std::map<std::string, float> result;
    //float weight;
    //fuseDiscrete<std::string>(result, weight, getObjectProbabilitiesFromSize(size, var), 1.0, getObjectProbabilitiesFromShapeProbabilities(shape_probs), 1.0); // TODO better idea?
    std::map<std::string, float> result;
    float scoresum=0.0;
    std::vector<float> scores(getPossibleObjects().size());
    for(size_t i=0; i<getPossibleObjects().size(); i++) {
        float dims2[3]={getPossibleObjects()[i].dim0, getPossibleObjects()[i].dim1, getPossibleObjects()[i].dim2};
        std::sort(dims2, dims2+3, std::greater<float>());
        Eigen::Vector3f obj_dims(dims2[0], dims2[1], dims2[2]);
        //float score=1.0/(fabsf(dims[0]-dims2[0])+fabsf(dims[1]-dims2[1])+fabsf(dims[2]-dims2[2]));
            //Alternative: difference of volume, but that is unsuited, because a 2x2x2 object and a 1x2x4 object have the same volume
        Eigen::Vector3f tmp=size-obj_dims;
        const float score=exp(-0.5*tmp.transpose()*var.inverse()*tmp)/sqrt(pow(2.0*M_PI, 3)*var.determinant());//TODO log?
        float prob_OTHER=0.0, prob_primitive=0.0; // if one of the .at()-functions throws an exception, that means that the probability is zero
        try { prob_OTHER=shape_probs.at(OTHER); } catch (const std::out_of_range& oor) { }
        try { prob_primitive=shape_probs.at(getPossibleObjects()[i].primitive); } catch (const std::out_of_range& oor) { }
        scores[i]=score*(getPossibleObjects()[i].primitive==OTHER?1.0f:(prob_OTHER+prob_primitive)); // TODO does this make sense?
        scoresum+=scores[i];
        ROS_DEBUG_STREAM("score=" << score << ", prob_OTHER=" << prob_OTHER << ", prob_primitive=" << prob_primitive);
    }
    for(size_t i=0; i<getPossibleObjects().size(); i++) { // TODO use normalizeProbabilities instead? Does that do the same?
        result.insert(std::pair<std::string, float>(getPossibleObjects()[i].name, scores[i]/scoresum));
        ROS_DEBUG_STREAM("Result: " << getPossibleObjects()[i].name << " has probability " << scores[i]/scoresum);
    }
    ROS_DEBUG("getObjectProbabilitiesFromSizeAndShape() finished");
    return result;
}

template <typename type>
std::pair<type, float> getHighestScore(const std::map<type, float> map) {
    if(map.empty()) {
        ROS_ERROR("getHighestScore(): input map is empty");
        return std::pair<type, float>();
    }
    std::pair<type, float> best=*map.cbegin();
    for (auto it = map.cbegin(); it != map.cend(); ++it) {
        if(it->second > best.second) {
            best=*it;
        }
    }
    return best;
}


void subimageBoundaries(const tf2::Vector3& position, const tf2::Quaternion& orientation, const tf2::Vector3& size, const image_geometry::PinholeCameraModel& cam_model, float& xmin, float& ymin, float& xmax, float& ymax) {
    xmin=cam_model.fullResolution().width;
    ymin=cam_model.fullResolution().height;
    xmax=0.0;
    ymax=0.0;
    //Iterate through all 8 edges of the bounding box, project them on the camera image, and find a 2D box that includes them all.
    const tf2::Transform trans(orientation, position);
    for(int k=-1; k<=1; k+=2) {
        for(int l=-1; l<=1; l+=2) {
            for(int m=-1; m<=1; m+=2) {
                const tf2::Vector3 vec = trans * tf2::Vector3(k*size.x()/2.0, l*size.y()/2.0, m*size.z()/2.0);
                const cv::Point2d uv = cam_model.project3dToPixel(cv::Point3d(vec.x(), vec.y(), vec.z()));
                if(uv.x<xmin) xmin=uv.x;
                if(uv.x>xmax) xmax=uv.x;
                if(uv.y<ymin) ymin=uv.y;
                if(uv.y>ymax) ymax=uv.y;
           }
        }
    }
    if(xmin < 0.0) xmin = 0.0;
    if(ymin < 0.0) ymin = 0.0;
    if(xmax > cam_model.fullResolution().width) xmax = cam_model.fullResolution().width;
    if(ymax > cam_model.fullResolution().height) ymax = cam_model.fullResolution().height;
}

void subimageBoundaries(pcl::PointCloud<point_t>::ConstPtr cloud, const image_geometry::PinholeCameraModel& cam_model, float& xmin, float& ymin, float& xmax, float& ymax) {
    xmin=cam_model.fullResolution().width;
    ymin=cam_model.fullResolution().height;
    xmax=0.0;
    ymax=0.0;
    //iterate through all points, apply cam_model_in.project3dToPixel, and store min/max of all points
    //pcl::PointCloud<point_t>::Ptr cloud(new pcl::PointCloud<point_t>);
    //pcl::fromROSMsg(cloud_in, *cloud);
    ROS_INFO("subimageBoundaries: cloud has %lu points", cloud->points.size()); 
    for (size_t j = 0; j < cloud->points.size(); j++) {
      const cv::Point2d uv = cam_model.project3dToPixel(cv::Point3d(cloud->points[j].x, cloud->points[j].y, cloud->points[j].z));
      //if(j%300==0) { ROS_INFO_STREAM("point=" << cloud->points[j] << " uv=" << uv); }
      if(uv.x<xmin) xmin=uv.x;
      if(uv.x>xmax) xmax=uv.x;
      if(uv.y<ymin) ymin=uv.y;
      if(uv.y>ymax) ymax=uv.y;
    }
    if(xmin < 0.0) xmin = 0.0;
    if(ymin < 0.0) ymin = 0.0;
    if(xmax > cam_model.fullResolution().width) xmax = cam_model.fullResolution().width;
    if(ymax > cam_model.fullResolution().height) ymax = cam_model.fullResolution().height;
}

bool callClassifyService(const Eigen::Vector3f& /*size_in*/,
                    const Eigen::Quaternionf& /*orientation_in*/,
                    const Eigen::Vector3f& /*position_in*/,
                    pcl::PointCloud<point_t>::ConstPtr cloud_in, // TODO this has to be in the same frame as cam_image, right?
                    std::map<std::string, float>& object_probs_out,
                    const sensor_msgs::Image& cam_image,
                    const image_geometry::PinholeCameraModel& cam_model_in) {
    ROS_INFO("callClassifyService() started");
    const uint8_t min_image_size=5;
    sensor_msgs::Image image;
    float xmin, ymin, xmax, ymax;
    //subimageBoundaries(tf2::Vector3(position_in.x(), position_in.y(), position_in.z()), tf2::Quaternion(orientation_in.x(), orientation_in.y(), orientation_in.z(), orientation_in.w()), tf2::Vector3(size_in.x(), size_in.y(), size_in.z()), cam_model_in, xmin, ymin, xmax, ymax);
    subimageBoundaries(cloud_in, cam_model_in, xmin, ymin, xmax, ymax); // Should be more accurate than the other function, maybe slightly slower, but not significantly
    //ROS_INFO("cam_image.width=%u, cam_image.height=%u\n", cam_image.width, cam_image.height);
    //image=getSubImage(cam_image, cam_image.width*(estimatedBbox2D.center.x-estimatedBbox2D.size_x/2), cam_image.height*(estimatedBbox2D.center.y-estimatedBbox2D.size_y/2), cam_image.width*(estimatedBbox2D.center.x+estimatedBbox2D.size_x/2), cam_image.height*(estimatedBbox2D.center.y+estimatedBbox2D.size_y/2));
    clf_object_recognition_msgs::Classify2D srv;
    if(fabs(xmax-xmin)>min_image_size && fabs(ymax-ymin)>min_image_size) {//If image is not at least min_image_size by min_image_size pixel, this does not make sense
        image=getSubImage(cam_image, xmin, ymin, xmax, ymax);
        srv.request.images.push_back(image);
    } else {
        ROS_WARN("Sub image of object is too small");
        return false;
    }
    ROS_INFO("Calling image classify service");
    if(classify_client.call(srv)) { // This takes about 0.047s (17.11.2019)
        ROS_INFO_STREAM("classify result: " << srv.response);
        for(size_t i=0; i<srv.response.classifications[0].results.size(); i++) {
            object_probs_out[getObjectLabelById(*nh1, srv.response.classifications[0].results[i].id)]=srv.response.classifications[0].results[i].score;
        }
        normalizeProbabilities(object_probs_out);
        ROS_INFO("callClassifyService() finished successfully");
        return true;
    }
    return false;
}

bool callFitterService(pcl::PointCloud<point_t>::ConstPtr cloud_in,
                       const std::map<primitive_t, float>& shape_probs_in,
                       Eigen::Vector3f& position_out,
                       Eigen::Matrix3f& position_var_out,
                       Eigen::Quaternionf& orientation_out,
                       Eigen::Matrix4f& orientation_var_out,
                       Eigen::Vector3f& size_out,
                       Eigen::Matrix3f& size_var_out,
                       std::map<primitive_t, float>& shape_probs_out,
                       const float table_height_in,
                       const Eigen::Isometry3d * transform) {
    const float shape_prob_threshold=0.2;
    clf_grasping_msgs::CloudToCollision srv;
    // Sub-sample points, it is probably faster to call the fitter with an already small cloud
    pcl::PointCloud<point_t> cloud_filtered;
    //pcl::PointCloud<point_t>::Ptr cloud_in(new pcl::PointCloud<point_t>);
    //pcl::fromROSMsg(cloud_msg_in, *cloud_in);
    pcl::RandomSample<point_t> sor;//TODO or maybe use a VoxelGrid filter?
    sor.setInputCloud (cloud_in);
    sor.setSample(300);
    sor.filter (cloud_filtered);
    pcl::toROSMsg(cloud_filtered, srv.request.source_cloud);
    //srv.request.source_cloud=cloud_msg_in;
    
    //Add shapes
    float prob_OTHER=0.0, prob_BOX=0.0, prob_SPHERE=0.0, prob_CYLINDER=0.0, prob_CONE=0.0;
    try { prob_OTHER=shape_probs_in.at(OTHER); } catch (const std::out_of_range& oor) { }
    try { prob_BOX=shape_probs_in.at(BOX); } catch (const std::out_of_range& oor) { }
    try { prob_SPHERE=shape_probs_in.at(SPHERE); } catch (const std::out_of_range& oor) { }
    try { prob_CYLINDER=shape_probs_in.at(CYLINDER); } catch (const std::out_of_range& oor) { }
    try { prob_CONE=shape_probs_in.at(CONE); } catch (const std::out_of_range& oor) { }
    ROS_INFO("Probable shapes: other: %f, box: %f, sphere: %f, cylinder: %f, cone: %f", prob_OTHER, prob_BOX, prob_SPHERE, prob_CYLINDER, prob_CONE);
    if(prob_BOX>shape_prob_threshold) {
        srv.request.shapes.push_back("box");
    }
    if(prob_CYLINDER>shape_prob_threshold) {
        srv.request.shapes.push_back("cylinder");
    }
    if(prob_SPHERE>shape_prob_threshold) {
        srv.request.shapes.push_back("sphere");
    }
    if((prob_OTHER+prob_CONE)>shape_prob_threshold || srv.request.shapes.empty()) {
        srv.request.shapes.push_back("any");
    }
    std::cout << "Telling fitter to try shapes: ";
    for(size_t j=0; j<srv.request.shapes.size(); j++) {
        std::cout << srv.request.shapes[j] << " ";
    }
    std::cout << std::endl;
    
    //Add plane
    if(table_height_in!=0.0 && transform!=nullptr) {
        srv.request.table_plane=transformPlane(0.0, 0.0, 1.0, -table_height_in, *transform);
        srv.request.use_plane=true;
    } else {
        srv.request.use_plane=false;
    }
    
    // Concept: if srv.request.shapes only contains one entry (box, cylinder, or sphere) then call the corresponding sac fitting function. Else, call the sq fitting service. If that fails, call all three sac fitting functions (cylinder, sphere, box) and choose the result with the best quality.
    if(useSACFitting && srv.request.shapes.size() == 1 && srv.request.shapes[0] == "box") {
        /*const double dist_box=*/fit_box(cloud_in, srv.response.collision_object); // TODO check if the fit is good enough?
        shape_probs_out[BOX] = 1.0;
    } else if(useSACFitting && srv.request.shapes.size() == 1 && srv.request.shapes[0] == "cylinder") {
        /*const double dist_cylinder=*/fit_cylinder(cloud_in, srv.response.collision_object); // TODO check if the fit is good enough?
        shape_probs_out[CYLINDER] = 1.0;
    } else if(useSACFitting && srv.request.shapes.size() == 1 && srv.request.shapes[0] == "sphere") {
        /*const double dist_sphere=*/fit_sphere(cloud_in, srv.response.collision_object); // TODO check if the fit is good enough?
        shape_probs_out[SPHERE] = 1.0;
    } else {
        ROS_INFO("Calling object fitter service");
        if(object_fitter_client.call(srv)) {
            ROS_INFO("Call service success (fitter)");
            switch(srv.response.collision_object.primitives[0].type) {
                case 1: // Box
                    shape_probs_out[BOX] = 1.0;
                    break;
                case 2: // SPHERE
                    shape_probs_out[SPHERE] = 1.0;
                    break;
                case 3: // CYLINDER
                    shape_probs_out[CYLINDER] = 1.0;
                    break;
                case 4: // CONE
                    shape_probs_out[CONE] = 1.0;
                    break;
                default:
                    ROS_ERROR("callFitterService: unexpected shape");
            }
        } else {
            ROS_WARN("Possibly error with service call (fitter). Using sac-functions as backup");
            moveit_msgs::CollisionObject co_cylinder;
            moveit_msgs::CollisionObject co_sphere;
            moveit_msgs::CollisionObject co_box;
            const double dist_cylinder = fit_cylinder(cloud_in, co_cylinder);
            const double dist_sphere = fit_sphere(cloud_in, co_sphere);
            const double dist_box = fit_box(cloud_in, co_box);
            ROS_INFO("dist_cylinder=%g, dist_sphere=%g, dist_box=%g", dist_cylinder, dist_sphere, dist_box); // TODO what if all fail?
            if(dist_cylinder < dist_sphere) {
                if(dist_cylinder < dist_box) {
                    srv.response.collision_object=co_cylinder;
                } else {
                    srv.response.collision_object=co_box;
                }
            } else {
                if(dist_sphere < dist_box) {
                    srv.response.collision_object=co_sphere;
                } else {
                    srv.response.collision_object=co_box;
                }
            }
            const double probs_sum=(1.0/dist_box)+(1.0/dist_sphere)+(1.0/dist_cylinder);
            shape_probs_out[BOX] = (1.0/dist_box)/probs_sum;
            shape_probs_out[SPHERE] = (1.0/dist_sphere)/probs_sum;
            shape_probs_out[CYLINDER] = (1.0/dist_cylinder)/probs_sum;
        }
    }
    {
        ROS_INFO_STREAM("Fitted collision object:\n" << srv.response.collision_object);
        if(srv.response.collision_object.primitive_poses.empty()) {
          ROS_ERROR("srv.response.collision_object.primitive_poses has size 0");
          return false;
        }
        tf2::fromMsg(srv.response.collision_object.primitive_poses[0].position, position_out);
        position_var_out << 0.01, 0.0 , 0.0 ,
                            0.0 , 0.01, 0.0 ,
                            0.0 , 0.0 , 0.01;//TODO tune
        tf2::fromMsg(srv.response.collision_object.primitive_poses[0].orientation, orientation_out);
        ROS_INFO_STREAM("orientation_out: " << orientation_out);
        //orientation_out << srv.response.collision_object.primitive_poses[0].orientation.x, srv.response.collision_object.primitive_poses[0].orientation.y, srv.response.collision_object.primitive_poses[0].orientation.z, srv.response.collision_object.primitive_poses[0].orientation.w;
        orientation_var_out << 0.001, 0.0  , 0.0  , 0.0  ,
                               0.0  , 0.001, 0.0  , 0.0  ,
                               0.0  , 0.0  , 0.001, 0.0  ,
                               0.0  , 0.0  , 0.0  , 0.001;//TODO tune
        shape_msgs::SolidPrimitive fitted_prim = srv.response.collision_object.primitives[0];
        switch(fitted_prim.type) {
            case 1: // Box
                size_out << fitted_prim.dimensions[0], fitted_prim.dimensions[1], fitted_prim.dimensions[2];
                OOBB_Calculate::orderSizeAndAdaptOrientation(size_out, orientation_out, false);
                break;
            case 2: // SPHERE
                size_out << 2.0*fitted_prim.dimensions[0], 2.0*fitted_prim.dimensions[0], 2.0*fitted_prim.dimensions[0];
                break;
            case 3: // CYLINDER
                size_out << 2.0*fitted_prim.dimensions[1], 2.0*fitted_prim.dimensions[1], fitted_prim.dimensions[0];
                orderVector(size_out, false);
                break;
            case 4: // CONE
                size_out << 2.0*fitted_prim.dimensions[1], 2.0*fitted_prim.dimensions[1], fitted_prim.dimensions[0];
                orderVector(size_out, false);
                break;
            default:
                ROS_ERROR("callFitterService: unexpected shape");
        }
        size_var_out << 0.001, 0.0  , 0.0  ,
                        0.0  , 0.001, 0.0  ,
                        0.0  , 0.0  , 0.001;//TODO tune
        ROS_INFO("size_out=(%g,%g,%g)", size_out.x(), size_out.y(), size_out.z());
        return true;
    }
    // TODO should there be a failure case, i.e. where false is returned?
}


void refine(object_info_t& info_out,
            const sensor_msgs::Image& cam_image,
            const image_geometry::PinholeCameraModel& cam_model_in,
            pcl::PointCloud<point_t>::ConstPtr cloud_in,
            const float table_height,
            const Eigen::Isometry3d * transform) {
    ROS_INFO_STREAM("refine() started");
    ROS_INFO_STREAM("info_out:\nposition\n" << info_out.position << "\norientation=\n" << info_out.orientation << "\nsize:\n" << info_out.size);
    std::map<std::string, float> classify_object_probs;
    if(callClassifyService(info_out.size, info_out.orientation, info_out.position, cloud_in, classify_object_probs, cam_image, cam_model_in)) {
        std::map<std::string, float> temp1(info_out.object_probabilities);
        fuseDiscrete<std::string>(info_out.object_probabilities, info_out.object_uncertainty, temp1, info_out.object_uncertainty, classify_object_probs, 0.8);
        if(info_out.size.x() < info_out.size.y() || info_out.size.y() < info_out.size.z()) { // This should normally be false, but better check it
            ROS_WARN("refine: sizes are not in descending order!");
        }
        Eigen::Vector3f size;
        Eigen::Matrix3f size_var;
        if(getSizeFromObjectProbabilities(classify_object_probs, size, size_var))
            fuseContinuous<3>(info_out.size, info_out.size_var, size, size_var);
        std::map<primitive_t, float> temp2(info_out.shape_probabilities);
        fuseDiscrete<primitive_t>(info_out.shape_probabilities, info_out.shape_uncertainty, temp2, info_out.shape_uncertainty,
                                  getShapeProbabilitiesFromObjectProbabilities(classify_object_probs), 0.8);
    } else {
        ROS_WARN("classify service call failed");
    }

    Eigen::Vector3f position_out;
    Eigen::Matrix3f position_var_out;
    Eigen::Quaternionf orientation_out;
    Eigen::Matrix4f orientation_var_out;
    Eigen::Vector3f size_out;
    Eigen::Matrix3f size_var_out;
    std::map<primitive_t, float> shape_probs_out;
    if(callFitterService(cloud_in, info_out.shape_probabilities, position_out, position_var_out, orientation_out, orientation_var_out, 
                         size_out, size_var_out, shape_probs_out, table_height, transform)) {
        ROS_INFO("Fusing position ...");
        fuseContinuous<3>(info_out.position, info_out.position_var, position_out, position_var_out);
        ROS_INFO("Fusing orientation ...");
        fuseContinuous(info_out.orientation, info_out.orientation_var, orientation_out, orientation_var_out);
        ROS_INFO("Fusing size ...");
        fuseContinuous<3>(info_out.size, info_out.size_var, size_out, size_var_out); // TODO different orders?
        std::map<primitive_t, float> temp1(info_out.shape_probabilities);
        ROS_INFO("Fusing shape probabilities ...");
        fuseDiscrete<primitive_t>(info_out.shape_probabilities, info_out.shape_uncertainty, temp1, info_out.shape_uncertainty, shape_probs_out, 0.8);
        std::map<std::string, float> temp2(info_out.object_probabilities);
        ROS_INFO("Fusing object probabilities ...");
        fuseDiscrete<std::string>(info_out.object_probabilities, info_out.object_uncertainty, temp2, info_out.object_uncertainty,
                                  getObjectProbabilitiesFromSizeAndShape(size_out, size_var_out, shape_probs_out), 0.5);
    } else {
        ROS_WARN("fitter service call failed");
        if(getHighestScore(info_out.shape_probabilities).first == 3 && fabs(info_out.size.x() - info_out.size.y()) > fabs(info_out.size.y() - info_out.size.z())) {
            info_out.orientation = Eigen::Quaternionf(M_SQRT1_2, 0.0, M_SQRT1_2, 0.0) * info_out.orientation;//TODO better solution? move this somewhere else?
        }
    }
    ROS_INFO("refine() finished");
}

std::vector<vision_msgs::Detection3D>
getObjectsAndAddToPlanningScene(const sensor_msgs::Image& cam_image,
                                const image_geometry::PinholeCameraModel& cam_model,
                                pcl::PointCloud<point_t>::ConstPtr cloud,
                                const std::string& src_frame,
                                const Eigen::Isometry3d * transform,
                                const float table_height,
                                const bool addToPlanningScene,
                                const bool make_all_standard_cylinder,
                                std::vector<std::string> * objectIds) {
  ROS_INFO("getObjectsAndAddToPlanningScene() called");
  //ROS_INFO("Making call to Detection3D service");
  //clf_object_recognition_msgs::Detect3D srv1;
  //if (object_merger_client.call(srv1))
  //{
  //  ROS_INFO("Call service success (object merger)");
  //}
  //else
  //{
  //  ROS_ERROR("Error with service call (object merger)");
  //  return nullptr;
  //}
  if (cloud->empty()) {
    ROS_WARN("getObjectsAndAddToPlanningScene(): input cloud is empty");
    return *(new std::vector<vision_msgs::Detection3D>());
  }
  std::vector<vision_msgs::Detection3D> detections = segmentation(cloud, src_frame);//, 2.0

  ROS_INFO("Got %lu detections", detections.size());
  
  //TODO clear markers
  visualization_msgs::Marker marker1;
  marker1.action = 3; // DELETE ALL
  marker_pub.publish(marker1);
      
  clf_object_recognition_msgs::BoundingBox3DArray bboxes;
  if (!detections.empty()) {
    bboxes.header.frame_id = detections[0].source_cloud.header.frame_id;
  }
  std::vector<moveit_msgs::CollisionObject> collision_objects;
  std::vector<vision_msgs::Detection3D> detections_out;
  for (size_t i = 0; i < detections.size(); i++) { // Loop over all detections
    if(detections[i].bbox.size.x > 0.3) {
        ROS_WARN("Detection is too big (> 0.3), probably not a graspable object");
        // TODO put points back into cloud?
        continue;
    }
    object_info_t info_out;
    info_out.position << detections[i].bbox.center.position.x, detections[i].bbox.center.position.y, detections[i].bbox.center.position.z;
    info_out.position_var << 0.02, 0.0 , 0.0 ,
                             0.0 , 0.02, 0.0 ,
                             0.0 , 0.0 , 0.02;// TODO are these good values?
    //info_out.orientation = Eigen::Quaternionf(detections[i].bbox.center.orientation.w, detections[i].bbox.center.orientation.x, detections[i].bbox.center.orientation.y, detections[i].bbox.center.orientation.z); // TODO remove this line if the line below works
    tf2::fromMsg(detections[i].bbox.center.orientation, info_out.orientation);
    info_out.orientation_var << 0.1, 0.0, 0.0, 0.0,
                                0.0, 0.1, 0.0, 0.0,
                                0.0, 0.0, 0.1, 0.0,
                                0.0, 0.0, 0.0, 0.1; //TODO tune this as well
    ROS_INFO("bbox size=(%f, %f, %f)", detections[i].bbox.size.x, detections[i].bbox.size.y, detections[i].bbox.size.z);
    info_out.size << detections[i].bbox.size.x, detections[i].bbox.size.y, detections[i].bbox.size.z; // TODO different order?
    info_out.size_var << 0.02, 0.0 , 0.0 ,
                         0.0 , 0.02, 0.0 ,
                         0.0 , 0.0 , 0.02;//TODO is the last dimensions always the z-dimensions?
    //info_out.object_probabilities["other"]=1.0;
    info_out.object_probabilities = getObjectProbabilitiesFromSize(info_out.size, info_out.size_var);
    info_out.object_uncertainty = 0.1;
    //info_out.shape_probabilities[OTHER]=1.0;
    info_out.shape_probabilities = getShapeProbabilitiesFromObjectProbabilities(info_out.object_probabilities);
    info_out.shape_uncertainty = 0.1;
    pcl::PointCloud<point_t>::Ptr cloud(new pcl::PointCloud<point_t>);
    pcl::fromROSMsg(detections[i].source_cloud, *cloud);
    refine(info_out, cam_image, cam_model, cloud, table_height, transform);
    // TODO set size with size of most probable object (if prob>0.6)
    if(transform!=nullptr && table_height!=0.0) { // TODO even if table_height==0, primitive_stability could still work with the plane normal?
        plane_t plane;
        plane.plane = transformPlane(0.0, 0.0, 1.0, -table_height, *transform);
        primitive_stability(plane, transform->linear().cast<float>() * Eigen::Vector3f(0.0, 0.0, -1.0), 
                            getHighestScore(info_out.shape_probabilities).first, info_out.size, info_out.position, info_out.orientation);
    }
    
    // Create a CollisionObject
    moveit_msgs::CollisionObject collision_object;
    
    collision_object.header = detections[i].source_cloud.header;
    std::ostringstream oss;
    auto best = getHighestScore(info_out.object_probabilities);
    oss << (best.second > 0.6 ? best.first : "object") << global_unique_id++;
    collision_object.id = oss.str();
    if(best.second > 0.6) collision_object.type.key = best.first;
    collision_object.primitives.resize(1);
    collision_object.primitives[0].type = getHighestScore(info_out.shape_probabilities).first;
    if(collision_object.primitives[0].type == 0) collision_object.primitives[0].type = 1; // if shape is "other", make it "box" instead
    ROS_INFO("Name: %s, primitive type: %u", collision_object.id.c_str(), collision_object.primitives[0].type);
    //ROS_INFO("Setting size ...");
    switch(collision_object.primitives[0].type) {
        case 1: // box
            collision_object.primitives[0].dimensions.resize(3);
            collision_object.primitives[0].dimensions[0] = info_out.size.x();
            collision_object.primitives[0].dimensions[1] = info_out.size.y();
            collision_object.primitives[0].dimensions[2] = info_out.size.z();
            break;
        case 2: // sphere
            collision_object.primitives[0].dimensions.resize(1);
            collision_object.primitives[0].dimensions[0] = (info_out.size.x() + info_out.size.y() + info_out.size.z()) / 6.0; // Divide by 6 because average (3) and radius instead of diameter (2)
            break;
        case 3: // cylinder
            collision_object.primitives[0].dimensions.resize(2); // TODO maybe some height-padding?
            if(fabs(info_out.size.x() - info_out.size.y()) < fabs(info_out.size.y() - info_out.size.z())) {
                collision_object.primitives[0].dimensions[0] = info_out.size.z(); // Height
                collision_object.primitives[0].dimensions[1] = (info_out.size.x() + info_out.size.y()) / 4.0; // Radius, divide by 4 because average (2) and radius instead of diameter (2)
                // TODO adapt orientation? Probably not necessary
            } else {
                collision_object.primitives[0].dimensions[0] = info_out.size.x(); // Height
                collision_object.primitives[0].dimensions[1] = (info_out.size.y() + info_out.size.z()) / 4.0; // Radius
                // TODO adapt orientation?
            }
            break;
        case 4: // cone
            collision_object.primitives[0].dimensions.resize(2);
            if(fabs(info_out.size.x() - info_out.size.y()) < fabs(info_out.size.y() - info_out.size.z())) {
                collision_object.primitives[0].dimensions[0] = info_out.size.z(); // Height
                collision_object.primitives[0].dimensions[1] = (info_out.size.x() + info_out.size.y()) / 4.0; // Radius
                // TODO adapt orientation? Probably not necessary
            } else {
                collision_object.primitives[0].dimensions[0] = info_out.size.x(); // Height
                collision_object.primitives[0].dimensions[1] = (info_out.size.y() + info_out.size.z()) / 4.0; // Radius
                // TODO adapt orientation?
            }
            break;
        default:
            ROS_WARN("Unknown shape type");
    }
    //ROS_INFO("Setting position ...");
    collision_object.primitive_poses.resize(1);
    collision_object.primitive_poses[0].position = tf2::toMsg(info_out.position);
    collision_object.primitive_poses[0].orientation = tf2::toMsg(info_out.orientation);
    collision_object.operation = collision_object.ADD;
    
    if(make_all_standard_cylinder) {
        ROS_WARN("Making this object to a cylinder (height=0.08, radius=0.02), no matter what it is!");
        collision_object.primitives[0].type = 3;
        collision_object.primitives[0].dimensions.resize(2);
        collision_object.primitives[0].dimensions[0] = 0.08;
        collision_object.primitives[0].dimensions[1] = 0.02;
        if(transform!=nullptr) {
            Eigen::Quaterniond new_orientation(transform->rotation());
            collision_object.primitive_poses[0].orientation = tf2::toMsg(new_orientation);
        } else {
            ROS_ERROR("transformation is not given, so the orientation of the standard cylinder will be set to identity.");
            collision_object.primitive_poses[0].orientation.w = 1.0;
        }
        //if(srv2.response.collision_object.primitive_poses[0].position.z < (table_height + 0.04 + 0.01)) { // This does not work because the coordinates are in the camera frame
        //    srv2.response.collision_object.primitive_poses[0].position.z = (table_height + 0.04 + 0.01);
        //}
    }
    
    ROS_INFO_STREAM("Adding collision object:" << collision_object);
    collision_objects.push_back(collision_object);
    if (objectIds != nullptr) {
      objectIds->push_back(collision_object.id);
    }
    
    // Create marker that can be displayed in RViz // TODO use publishCOasMarker?
    publishCOasMarker(collision_object, global_unique_id-1);
    /*visualization_msgs::Marker marker;
    marker.header = detections[i].source_cloud.header;
    marker.ns = "objects";
    marker.id = global_unique_id-1;
    switch(collision_object.primitives[0].type) {
        case 1: // box
            marker.type = visualization_msgs::Marker::CUBE;
            marker.scale.x = collision_object.primitives[0].dimensions[0];
            marker.scale.y = collision_object.primitives[0].dimensions[1];
            marker.scale.z = collision_object.primitives[0].dimensions[2];
            break;
        case 2: // sphere
            marker.type = visualization_msgs::Marker::SPHERE;
            marker.scale.x = 2.0*collision_object.primitives[0].dimensions[0];
            marker.scale.y = 2.0*collision_object.primitives[0].dimensions[0];
            marker.scale.z = 2.0*collision_object.primitives[0].dimensions[0];
            break;
        case 3: // cylinder
            marker.type = visualization_msgs::Marker::CYLINDER;
            marker.scale.x = 2.0*collision_object.primitives[0].dimensions[1];
            marker.scale.y = 2.0*collision_object.primitives[0].dimensions[1];
            marker.scale.z = collision_object.primitives[0].dimensions[0];
            break;
        case 4: // cone
            marker.type = visualization_msgs::Marker::ARROW;
            marker.scale.x = 2.0*collision_object.primitives[0].dimensions[1];
            marker.scale.y = 2.0*collision_object.primitives[0].dimensions[1];
            marker.scale.z = collision_object.primitives[0].dimensions[0];
            break;
        default:
            ROS_WARN("Unknown shape type");
            marker.type = visualization_msgs::Marker::CUBE;
    }
    marker.action = visualization_msgs::Marker::ADD;
    marker.pose.position = tf2::toMsg(info_out.position);
    marker.pose.orientation = tf2::toMsg(info_out.orientation);
    marker.color.r = 0.0f;
    marker.color.g = 0.0f;
    marker.color.b = 1.0f;
    marker.color.a = 1.0f;
    marker.lifetime = ros::Duration();
    marker_pub.publish(marker);*/
    
    // Bounding box
    /*vision_msgs::BoundingBox3D bbox;
    bbox.center.orientation = tf2::toMsg(info_out.orientation);
    //bbox.center.orientation.x = info_out.orientation.x();
    //bbox.center.orientation.y = info_out.orientation.y();
    //bbox.center.orientation.z = info_out.orientation.z();
    //bbox.center.orientation.w = info_out.orientation.w();
    bbox.center.position = tf2::toMsg(info_out.position);
    //bbox.center.position.x = info_out.position.x();
    //bbox.center.position.y = info_out.position.y();
    //bbox.center.position.z = info_out.position.z();
    bbox.size = tf2::toMsg(info_out.size);
    //bbox.size.x = info_out.size.x();
    //bbox.size.y = info_out.size.y();
    //bbox.size.z = info_out.size.z();
    bboxes.boxes.push_back(bbox);*/
    bboxes.boxes.push_back(detections[i].bbox);//TODO put refined infos in here?
    
    // Detection3D
    for (auto it=info_out.object_probabilities.begin(); it!=info_out.object_probabilities.end(); ++it) {
        if(it->second > 0.01) {
            vision_msgs::ObjectHypothesisWithPose hypo;
            hypo.id = getIdByObjectLabel(*nh1, it->first);
            hypo.score = it->second;
            detections[i].results.push_back(hypo);
        }
    }
    std::sort(detections[i].results.begin(), detections[i].results.end(),
              [](const vision_msgs::ObjectHypothesisWithPose& a, const vision_msgs::ObjectHypothesisWithPose& b) -> bool { return a.score > b.score; }); // Sort descending
    set_tracking_id_if_defined(detections[i], oss.str(), special_()); // add new label to detection3d
    detections_out.push_back(detections[i]);//TODO put refined infos in here?
  }
  bbox_pub.publish(bboxes);
  ROS_INFO("Found %lu objects", collision_objects.size());

  if (addToPlanningScene) {
    ROS_INFO("Adding %lu collision objects to planning scene",
           collision_objects.size());
    if (planning_scene_interface1 != nullptr) {
      if(!planning_scene_interface1->applyCollisionObjects(collision_objects)) {
        ROS_WARN("applyCollisionObjects() failed");
      }
    } else {
      ROS_WARN("planning_scene_interface is NULL");
    }
  }
  return detections_out;
}
