#include <cstddef>  // for NULL, size_t
#include <string>   // for string
#include "point_clouds.hpp"
#include "util.hpp" // for getIsometryTransform, angleBetweenVectors, transformPlane
#include "topic2.hpp"
#include "bounding_boxes.hpp"
#include "sac_model_normal_perpendicular_plane.h"

//#include <eigen_conversions/eigen_msg.h>
#include <tf2_eigen/tf2_eigen.h>
#include <cv_bridge/cv_bridge.h>
#include <opencv2/imgcodecs.hpp>
//pcl
#include <pcl/ModelCoefficients.h>
//#include <pcl/point_types.h>
//#include <pcl/conversions.h>
#include <pcl/common/common.h> // for getMinMax3D
#include <pcl/filters/crop_box.h>
#include <pcl/filters/passthrough.h>
#include <pcl/filters/project_inliers.h>
#include <pcl/filters/voxel_grid.h>
#include <pcl/filters/extract_indices.h>
#include <pcl/features/normal_3d_omp.h>
#include <pcl/io/pcd_io.h>
#include <pcl/sample_consensus/method_types.h>
#include <pcl/sample_consensus/model_types.h>
//#include <pcl/sample_consensus/ransac.h>
#include <pcl/sample_consensus/rransac.h>
#include <pcl/sample_consensus/sac_model_normal_parallel_plane.h>
#include <pcl/segmentation/extract_clusters.h>
#include <pcl/segmentation/sac_segmentation.h>
//#include <pcl/segmentation/seeded_hue_segmentation.h>
#include <pcl/surface/convex_hull.h>
//#include <pcl/search/search.h>
#include <pcl_conversions/pcl_conversions.h>
#include <pcl_ros/transforms.h>
#include <image_geometry/pinhole_camera_model.h>
// msgs
#include <visualization_msgs/Marker.h>
#include <sensor_msgs/PointCloud2.h>

#define BASE_FRAME "base_footprint" // For the markers
#define SAC_WITH_NORMALS true
#define SAC_EPS_ANGLE 0.2

ros::Publisher publisher;// TODO better solution for this? Maybe shared_ptr?
ros::Publisher * planeviz_pub;

void init_plane_publisher(ros::NodeHandle &nh, const std::string& topic) {
  if(!topic.empty()) {
    publisher = nh.advertise<visualization_msgs::Marker>(topic, 1000);
    planeviz_pub = &publisher;
  }
}

void deleteAllPlaneMarkers() {
  if(planeviz_pub != nullptr) {
      visualization_msgs::Marker marker1;
      marker1.header.frame_id = BASE_FRAME;
      marker1.header.stamp = ros::Time::now();
      marker1.ns = "basic_shapes";
      marker1.id = 0;
      marker1.action = 3; // DELETE ALL
      planeviz_pub->publish(marker1);
  }
}

//template <typename type>
pcl::PointCloud<point_t>::Ptr
getPointCloud(ros::NodeHandle &nh,
              const tf2_ros::Buffer &tfBuffer,
              const std::string& pointcloud_topic,
              const bool transform,
              std::string *orig_frame) {
  ROS_INFO("getPointCloud called");
  pcl::PointCloud<point_t>::Ptr cloud(new pcl::PointCloud<point_t>);

  // Get PointCloud
  // Adapted from
  // https://answers.ros.org/question/293890/how-to-use-waitformessage-properly/
  ROS_INFO("getPointCloud(): waiting for point cloud message ...");
  boost::shared_ptr<sensor_msgs::PointCloud2 const> sharedCloud =
      ros::topic::waitForMessage<sensor_msgs::PointCloud2>(
          pointcloud_topic, nh, ros::Duration(15, 0));
  ROS_INFO("waitForMessage done");
  if (sharedCloud != nullptr) {
    // ROS_INFO("getPointCloud(): success");
    //sensor_msgs::PointCloud2 msg_in = *sharedCloud;
    ROS_INFO("getPointCloud(): Size of cloud is %u",
             sharedCloud->height * sharedCloud->width);

    if (transform) {
      ROS_INFO("getPointCloud(): looking up transform");
      // Transform the right frame TODO use getAffineTransform() form util.hpp?
      geometry_msgs::TransformStamped transformStamped;
      try {
        transformStamped = tfBuffer.lookupTransform(
            BASE_FRAME, sharedCloud->header.frame_id, sharedCloud->header.stamp, //ros::Time(0),
            ros::Duration(10, 0)); // Latest transform
      } catch (tf2::TransformException &ex) {
        ROS_WARN("%s", ex.what());
        return cloud;
      }
      ROS_INFO("getPointCloud(): successfully got transform");
      const Eigen::Isometry3d e=tf2::transformToEigen(transformStamped);
      sensor_msgs::PointCloud2 transCloud;
      const Eigen::Matrix4f transform_matrix=e.matrix().cast<float>(); // Seems to work only like this, else this error appears http://eigen.tuxfamily.org/dox-devel/group__TopicUnalignedArrayAssert.html
      ROS_INFO("Calling transformPointCloud ...");
      pcl_ros::transformPointCloud(transform_matrix, *sharedCloud, transCloud); // TODO this line takes about 0.75s. Make that faster if possible // In pcl 1.9.0, it is much faster through SIMD // TODO maybe transform to pcl cloud first, then transform?
      ROS_INFO("done");
      ROS_INFO("Calling fromROSMsg ...");
      pcl::moveFromROSMsg(transCloud, *cloud); // can be moved since transCloud is not used after this line
      ROS_INFO("done");
    } else {
      pcl::fromROSMsg(*sharedCloud, *cloud); // TODO maybe moveFromROSMsg?
    }
    ROS_INFO("fromROSMsg finished, cloud has size %lu", cloud->size());
    if (orig_frame != nullptr) {
      *orig_frame = sharedCloud->header.frame_id;
    }
    // Make unorganized // TODO maybe leave organized?
    cloud->width = (cloud->width) * (cloud->height);
    cloud->height = 1;
    ROS_INFO("getPointCloud() finished function, cloud has size %u (%u by %u)",
             cloud->width * cloud->height, cloud->height, cloud->width);
    return cloud;
  } else {
    ROS_WARN("getPointCloud(): failed to get point cloud");
    return cloud;
  }
}

pcl::PointCloud<point_t>::Ptr getSceneData(ros::NodeHandle &nh, const tf2_ros::Buffer &tfBuffer, const image_geometry::PinholeCameraModel * cam_model, const std::string& depth_image_topic, const std::string& rgb_image_topic, std_msgs::Header& orig_header, Eigen::Isometry3d& transform, sensor_msgs::Image& rgb_image, uint8_t mode) {
  ROS_INFO("Waiting for rgb image and depth image ...");
  boost::shared_ptr<sensor_msgs::Image const> sharedRGBImage;
  sensor_msgs::Image depthImage;
  if(mode==0) {
    boost::shared_ptr<sensor_msgs::Image const> sharedDepthImage;
    ros::topic::MessagesHelper<sensor_msgs::Image, sensor_msgs::Image> helper;
    helper.waitForMessages(depth_image_topic, rgb_image_topic, nh, -1.0f, ros::Duration(10, 0), sharedDepthImage, sharedRGBImage);//rgb+depth image instead of pointcloud because much smaller (rgb image: ~0.9Mb, depth image: ~1.2Mb, point cloud: ~9.8Mb)
    //boost::shared_ptr<sensor_msgs::PointCloud2 const> sharedCloud;
    //ros::topic::waitForMessages<sensor_msgs::PointCloud2, sensor_msgs::Image>(pointcloud_topic, rgb_image_topic, nh, ros::Duration(10, 0), sharedCloud, sharedRGBImage);
    ROS_INFO("done");

    if (sharedDepthImage == nullptr) {
      ROS_ERROR("Failed to get depth image");
      return nullptr;
    }
    depthImage=*sharedDepthImage;
  } else if(mode==1) {
      depthImage=medianImages(nh, depth_image_topic, 20, 0.0);
      sharedRGBImage = ros::topic::waitForMessage<sensor_msgs::Image>(rgb_image_topic, nh, ros::Duration(10, 0));;
  } else {
    ROS_ERROR("getSceneData(): wrong mode");
    return nullptr;
  }
  if (sharedRGBImage == nullptr) {
    ROS_ERROR("Failed to get camera image. Point cloud will be uncolored");
    return nullptr;
  }
  
  pcl::PointCloud<point_t>::Ptr cloud=depthImageToPointcloud(depthImage, &(*sharedRGBImage), *cam_model); // Regarding the second parameter: is a boost shared pointer, should be a normal pointer
  ROS_INFO_STREAM("Got depth image with stamp " << depthImage.header.stamp);
  transform=getIsometryTransform(tfBuffer, depthImage.header.frame_id, BASE_FRAME, depthImage.header.stamp);
  ROS_INFO("Transforming point cloud to %s", BASE_FRAME);
  pcl::transformPointCloud(*cloud, *cloud, isometry2affine(transform.inverse()));//TODO this seems to take some time, maybe there is a solution where this is not necessary? Or maybe it's faster when source and target are not the same?
  ROS_INFO("Transformation done");
  // Make unorganized
  cloud->width = (cloud->width) * (cloud->height);
  cloud->height = 1;
  
  //ROS_INFO("Getting camera image");
  //boost::shared_ptr<sensor_msgs::Image const> sharedRGBImage = ros::topic::waitForMessage<sensor_msgs::Image>(rgb_image_topic, nh, ros::Duration(10, 0));
  ROS_INFO_STREAM("Got camera image with stamp " << sharedRGBImage->header.stamp);
  rgb_image = *sharedRGBImage;
  // Save images (e.g. for logging/introspection purposes)
  const uint32_t time = ros::WallTime::now().sec;
  std::ostringstream cloudstream;
  cloudstream << "/tmp/" << time << ".pcd";
  try {
    ROS_INFO_STREAM("Saving cloud under " << cloudstream.str());
    pcl::io::savePCDFile (cloudstream.str(), *cloud, true);
  } catch (pcl::IOException &e) {
    ROS_WARN("%s", e.what());
  }
  std::ostringstream imgstream;
  imgstream << "/tmp/" << time << ".png";
  cv::imwrite(imgstream.str(), cv_bridge::toCvShare(sharedRGBImage, sensor_msgs::image_encodings::BGR8)->image);
  //std::ostringstream imgstream_depth;
  //imgstream_depth << "/tmp/" << time << ".tiff";
  //cv::imwrite(imgstream_depth.str(), cv_bridge::toCvCopy(depthImage)->image); // Might work with very new opencv versions

  orig_header = depthImage.header;
  return cloud;
}

//https://stackoverflow.com/a/17820615
std::string type2str(int type) {
  std::string r;

  uchar depth = type & CV_MAT_DEPTH_MASK;
  uchar chans = 1 + (type >> CV_CN_SHIFT);

  switch ( depth ) {
    case CV_8U:  r = "8U"; break;
    case CV_8S:  r = "8S"; break;
    case CV_16U: r = "16U"; break;
    case CV_16S: r = "16S"; break;
    case CV_32S: r = "32S"; break;
    case CV_32F: r = "32F"; break;
    case CV_64F: r = "64F"; break;
    default:     r = "User"; break;
  }

  r += "C";
  r += (chans+'0');

  return r;
}

//TODO: function to convert rgb-pointcloud to rgb image and depth image
/*void cloud2RGBDepthImages(pcl::PointCloud<point_t>::ConstPtr cloud, sensor_msgs::Image& depth_image_msg, sensor_msgs::Image& rgb_image_msg) {
    for(size_t y=0; y<cloud->height; y++) {
        for(size_t x=0; x<cloud->width; x++) {
            depth_iamge.at<float>(y,x) = cloud->points.z;
            rgb_image.at<cv::Vec3b>(y, x)[0] = cloud->points.r;
            rgb_image.at<cv::Vec3b>(y, x)[1] = cloud->points.g;
            rgb_image.at<cv::Vec3b>(y, x)[2] = cloud->points.b;
        }
    }
}*/

pcl::PointCloud<point_t>::Ptr depthImageToPointcloud(const sensor_msgs::Image& depth_image_msg, const sensor_msgs::Image * rgb_image_msg, const image_geometry::PinholeCameraModel& cam_model) {
    ROS_INFO("Starting depthImageToPointcloud()");
    ROS_INFO_STREAM("Depth image encoding: " << depth_image_msg.encoding);
    ROS_INFO_STREAM("RGB image encoding: " << (rgb_image_msg==nullptr?"none":rgb_image_msg->encoding));
    if(depth_image_msg.encoding.compare("32FC1")!=0) { // TODO add support for 16UC1
        ROS_ERROR("The depth image has a bad encoding");
        return nullptr;
    }
    if(rgb_image_msg!=nullptr && (rgb_image_msg->encoding.compare("bgr8")!=0 && rgb_image_msg->encoding.compare("rgb8")!=0)) {
        ROS_ERROR("The rgb image has a bad encoding");
        return nullptr;
    }
    //TODO: check if images are same size, have the correct encoding, maybe two cam models?
    cv::Mat depth_image=cv_bridge::toCvCopy(depth_image_msg)->image;//TODO share instead of copy?
    cv::Mat rgb_image;
    if(rgb_image_msg!=nullptr) {
        rgb_image=cv_bridge::toCvCopy(*rgb_image_msg, sensor_msgs::image_encodings::BGR8)->image;//TODO share instead of copy
        ROS_INFO_STREAM("RGB image cv encoding " << type2str(rgb_image.type()));
        //cv::imwrite("rgb_image.jpg", rgb_image);
    }
    ROS_INFO_STREAM("Depth image cv encoding " << type2str(depth_image.type()));
    //depth_image.dims=
    ROS_INFO("Depth image: cols=%i, rows=%i, dims=%i, flags=%i, channels=%i, depth=%i, type=%i, elemSize=%lu", depth_image.cols, depth_image.rows, depth_image.dims, depth_image.flags, depth_image.channels(), depth_image.depth(), depth_image.type(), depth_image.elemSize());
    //std::vector<int> compression_params;
    //compression_params.push_back(IMWRITE_TIFF_COMPRESSION);
    //compression_params.push_back();
    //cv::imwrite("depth_image.tiff", depth_image, compression_params); // Might work with very new opencv versions
    pcl::PointCloud<point_t>::Ptr cloud(new pcl::PointCloud<point_t>);
    cloud->width  = depth_image_msg.width;
    cloud->height = depth_image_msg.height;
    cloud->points.resize (cloud->width * cloud->height);
    cloud->header.frame_id=depth_image_msg.header.frame_id;
    //TODO is_dense, sensor_origin, header field, ...
    size_t i=0;
    for(size_t y=0; y<depth_image_msg.height; ++y) {
        for(size_t x=0; x<depth_image_msg.width; ++x) {
            const cv::Point3d p = cam_model.projectPixelTo3dRay(cv::Point2d(x, y));
            float depth=depth_image.at<float>(y, x);
            if(depth>10.0) depth=10.0;//TODO does this make sense?
            else if(depth<0.1) depth=0.1;
            cloud->points[i].x = depth*p.x;
            cloud->points[i].y = depth*p.y;
            cloud->points[i].z = depth;//p.z is 1
            if(rgb_image_msg!=nullptr) {
                cloud->points[i].r = rgb_image.at<cv::Vec3b>(y, x)[2];
                cloud->points[i].g = rgb_image.at<cv::Vec3b>(y, x)[1];
                cloud->points[i].b = rgb_image.at<cv::Vec3b>(y, x)[0];
            } else {
                cloud->points[i].r = 0;
                cloud->points[i].g = 0;
                cloud->points[i].b = 0;
            }
            cloud->points[i].a = 255;
            //if(i%1000==0) {
            //    printf("x=%lu, y=%lu, Projected point=(%f, %f, %f), depth=%g\n", x, y, p.x, p.y, p.z, depth_image.at<float>(y, x));
            //}
            ++i; // TODO ordering in organized cloud correct?
        }
    }
    ROS_INFO("Finished depthImageToPointcloud(), cloud->size=%lu, %lu points", cloud->size(), cloud->points.size());
    return cloud;
}

sensor_msgs::Image averageImages(ros::NodeHandle& nh, const std::string& topic, size_t num, float delay) { // TODO what data types are possible?
    ROS_INFO("averageImages called, num=%lu, delay=%g", num, delay);
    cv_bridge::CvImagePtr result=cv_bridge::toCvCopy(ros::topic::waitForMessage<sensor_msgs::Image>(topic, nh, ros::Duration(10, 0))); // TODO subscribe instead of waitForMessage?
    cv::Mat num_valid=cv::Mat::zeros(result->image.rows, result->image.cols, result->image.type());
    for(int r=0; r<result->image.rows; ++r) {
        for(int c=0; c<result->image.cols; ++c) {
            if(!std::isnan(result->image.at<float>(r, c))) {
                num_valid.at<float>(r, c)+=1.0f;
            } else {
                result->image.at<float>(r, c)=0.0f;
            }
        }
    }
    for(size_t i=1; i<num; i++) {
        ros::Duration(delay).sleep();
        boost::shared_ptr<sensor_msgs::Image const> sharedImage = ros::topic::waitForMessage<sensor_msgs::Image>(topic, nh, ros::Duration(10, 0));
        if (sharedImage != nullptr) {
            cv::Mat depth_image=cv_bridge::toCvShare(sharedImage)->image;
            //result->image+=depth_image;
            for(int r=0; r<result->image.rows; ++r) {
                for(int c=0; c<result->image.cols; ++c) {
                    if(!std::isnan(depth_image.at<float>(r, c))) {
                        result->image.at<float>(r, c)+=depth_image.at<float>(r, c);
                        num_valid.at<float>(r, c)+=1.0f;
                    }
                }
            }
        } else {
            ROS_WARN("shared image is NULL - in averageImages");
        }
    }
    //result->image/=num_valid;
    for(int r=0; r<result->image.rows; ++r) {
        for(int c=0; c<result->image.cols; ++c) {
            result->image.at<float>(r, c)/=num_valid.at<float>(r, c);
        }
    }

    return *result->toImageMsg();
}

sensor_msgs::Image medianImages(ros::NodeHandle& nh, const std::string& topic, size_t num, float delay) {
    ROS_INFO("medianImages called, num=%lu, delay=%g", num, delay);
    cv_bridge::CvImagePtr result=cv_bridge::toCvCopy(ros::topic::waitForMessage<sensor_msgs::Image>(topic, nh, ros::Duration(10, 0)));
    cv_bridge::CvImagePtr images[num];
    for(size_t i=0; i<num; i++) {
        boost::shared_ptr<sensor_msgs::Image const> sharedImage = ros::topic::waitForMessage<sensor_msgs::Image>(topic, nh, ros::Duration(10, 0));
        if (sharedImage != nullptr) {
            images[i]=cv_bridge::toCvCopy(sharedImage);
        } else {
            ROS_WARN("shared image is NULL - in medianImages");
        }
        ros::Duration(delay).sleep();
    }
    std::vector<float> values;
    for(int y=0; y<result->image.rows; y++) {
        for(int x=0; x<result->image.cols; x++) {
            values.clear();
            values.reserve(num);
            for(size_t i=0; i<num; i++) {
                if(!std::isnan(images[i]->image.at<float>(y, x))) {
                  values.push_back(images[i]->image.at<float>(y, x)); // TODO what about NaN? Different data types?
                }
            }
            if(!values.empty()) {
                std::nth_element (values.begin(), values.begin()+values.size()/2, values.end()); // TODO not correct if num is even
                result->image.at<float>(y, x)=*(values.begin()+values.size()/2);
            } else {
                result->image.at<float>(y, x)=images[0]->image.at<float>(y, x);
            }
        }
    }

    return *result->toImageMsg();
}

std::vector<plane_t> findPlanes(pcl::PointCloud<point_t>::Ptr cloud,
                                const int mode,
                                const unsigned int max_num_planes,
                                const unsigned int min_num_points_seg,
                                const float dist_threshold) {
  std::map<std::string, ros::console::levels::Level> loggers;
  if(ros::console::get_loggers(loggers) && loggers[ROSCONSOLE_DEFAULT_NAME]==ros::console::levels::Debug) {
    pcl::console::setVerbosityLevel(pcl::console::L_VERBOSE);
  }
  ROS_INFO("findPlanes() called, cloud has size %lu with %lu points, width %u, height %u, mode=%i (%s), max_num_planes=%u, min_num_points_seg=%u, dist_threshold=%g",
           cloud->size(), cloud->points.size(), cloud->width, cloud->height, mode, (mode==0?"horizontal":(mode==1?"vertical":"unconstrained")), max_num_planes, min_num_points_seg, dist_threshold);
  std::vector<plane_t> planes;
  if (cloud->empty()) {
    ROS_WARN("findPlanes(): input cloud is empty");
    return planes;
  }
  pcl::PointCloud<point_t>::Ptr cloud_projected(new pcl::PointCloud<point_t>);
  // Do the segmentation
  pcl::ModelCoefficients::Ptr coefficients(new pcl::ModelCoefficients);
  pcl::PointIndices::Ptr inliers(new pcl::PointIndices);
  // Create the segmentation object
#if SAC_WITH_NORMALS
  typedef pcl::Normal point_normal_t;
  pcl::ExtractIndices<pcl::Normal> extract_normals;
  //pcl::search::KdTree<point_t>::Ptr tree (new pcl::search::KdTree<point_t> ());
  pcl::PointCloud<pcl::Normal>::Ptr cloud_normals (new pcl::PointCloud<pcl::Normal>);
  pcl::PointCloud<pcl::Normal>::Ptr cloud_normals2 (new pcl::PointCloud<pcl::Normal>);
  pcl::NormalEstimationOMP<point_t, pcl::Normal> ne;
  //ne.setSearchMethod (tree); // Chosen automatically in pcl::Feature::initCompute()
  ne.setInputCloud (cloud);
  ne.setKSearch (50); // TODO good value? Tune?
  ROS_INFO("Computing point cloud normals ...");
  ne.compute (*cloud_normals); // TODO This function takes some time. Make faster? Maybe faster if cloud is organized?
  ROS_INFO("done");
  //pcl::SACSegmentationFromNormals<point_t, pcl::Normal> seg;
  //seg.setNormalDistanceWeight (0.07);
#else
  //pcl::SACSegmentation<point_t> seg;
#endif
  //seg.setOptimizeCoefficients(true);

#if SAC_WITH_NORMALS
  //pcl::SampleConsensusModelFromNormals<point_t, pcl::Normal>::Ptr sac_model_normals;
  pcl::SampleConsensusModelFromNormals<point_t, point_normal_t> * sac_model_normals;
#endif
  pcl::SampleConsensusModel<point_t>::Ptr sac_model;
  if (mode == 0) { //TODO change to normal-based?
#if SAC_WITH_NORMALS
    //seg.setModelType(pcl::SACMODEL_NORMAL_PARALLEL_PLANE); // Misnomer: SACMODEL_NORMAL_PERPENDICULAR_PLANE would be a more fitting name (the plane normal is parallel to the specified axis)
    auto plane = new pcl::SampleConsensusModelNormalParallelPlane<point_t, point_normal_t>(cloud, true);
    plane->setAxis(Eigen::Vector3f(0.0, 0.0, 1.0));
    plane->setEpsAngle(SAC_EPS_ANGLE);
    plane->setNormalDistanceWeight (0.07);
    sac_model.reset(plane);
    sac_model_normals = plane;
#else
    //seg.setModelType(pcl::SACMODEL_PERPENDICULAR_PLANE);
    auto plane = new pcl::SampleConsensusModelPerpendicularPlane<point_t>(cloud, true);
    plane->setAxis(Eigen::Vector3f(0.0, 0.0, 1.0));
    plane->setEpsAngle(SAC_EPS_ANGLE);
    sac_model.reset(plane);
#endif
    //seg.setAxis(Eigen::Vector3f(0.0, 0.0, 1.0));
    //seg.setEpsAngle(SAC_EPS_ANGLE);
  } else if (mode == 1) {
#if SAC_WITH_NORMALS
    //ROS_WARN("findPlanes: there is no parallel-to-axis-with-normals option, so using SACMODEL_NORMAL_PLANE"); // TODO maybe better PARALLEL_PLANE?
    //seg.setModelType(pcl::SACMODEL_NORMAL_PLANE);
    auto plane = new pcl::SampleConsensusModelNormalPerpendicularPlane<point_t, point_normal_t>(cloud, true);
    plane->setAxis(Eigen::Vector3f(0.0, 0.0, 1.0));
    plane->setEpsAngle(SAC_EPS_ANGLE);
    plane->setNormalDistanceWeight (0.07);
    sac_model.reset(plane);
    sac_model_normals = plane;
#else
    //seg.setModelType(pcl::SACMODEL_PARALLEL_PLANE);
    auto plane = new pcl::SampleConsensusModelParallelPlane<point_t>(cloud, true);
    plane->setAxis(Eigen::Vector3f(0.0, 0.0, 1.0));
    plane->setEpsAngle(SAC_EPS_ANGLE);
    sac_model.reset(plane);
    //seg.setAxis(Eigen::Vector3f(0.0, 0.0, 1.0));
    //seg.setEpsAngle(SAC_EPS_ANGLE);
#endif
  } else {
#if SAC_WITH_NORMALS
    //seg.setModelType(pcl::SACMODEL_NORMAL_PLANE);
    auto plane = new pcl::SampleConsensusModelNormalPlane<point_t, point_normal_t>(cloud, true);
    plane->setNormalDistanceWeight (0.07);
    sac_model.reset(plane);
    sac_model_normals = plane;
#else
    //seg.setModelType(pcl::SACMODEL_PLANE);
    sac_model.reset(new pcl::SampleConsensusModelPlane<point_t>(cloud, true));
#endif
  }

  //seg.setMethodType(pcl::SAC_RANSAC); // TODO maybe switch to RRANSAC for better performance?
  double threshold_;
#if SAC_WITH_NORMALS
  threshold_ = 0.025+dist_threshold;
#else
  threshold_=dist_threshold;
#endif
  //seg.setDistanceThreshold(threshold_);
  pcl::RandomizedRandomSampleConsensus<point_t> sac(sac_model, threshold_);
  //pcl::RandomSampleConsensus<point_t> sac(sac_model, threshold_);

  sac.setMaxIterations(300);
  //seg.setMaxIterations(1000); // TODO What is a good value here? Tune?

  pcl::PointCloud<point_t>::Ptr cloud_f(new pcl::PointCloud<point_t>);
  int num = 0;
  while (cloud->size() > min_num_points_seg && planes.size() < max_num_planes /* && num++<20*/) {
    plane_t plane;
    ROS_INFO_STREAM("findPlanes(): cloud height=" << cloud->height << ", width=" << cloud->width << ", points=" << cloud->points.size() /* << ", cloud size: " << cloud.width << " by " << cloud.height*/);
    ROS_INFO("findPlanes(): cloud_normals height=%u, width=%u, points=%lu", cloud_normals->height, cloud_normals->width, cloud_normals->points.size());
    num++;
//    seg.setInputCloud(cloud);
    sac_model->setInputCloud(cloud);
#if SAC_WITH_NORMALS
//    seg.setInputNormals (cloud_normals);
    //sac_model_normals->setInputNormals(cloud);
    sac_model_normals->setInputNormals(cloud_normals);
#endif
    ROS_INFO("sac_model->getIndices()->size()=%lu", sac_model->getIndices()->size());
    sac_model->getIndices()->resize(cloud->size());
    sac_model->setIndices(sac_model->getIndices()); // For shuffled_indices_
    ROS_INFO("sac_model->getIndices()->size()=%lu", sac_model->getIndices()->size());
    ROS_INFO("now calling SAC segmentation");
    
    ROS_INFO("fractionNrPretest=%g", 100.0/cloud->size()); // Desired: one point for pretest
    sac.setFractionNrPretest(100.0/cloud->size());
    ROS_ERROR("Errors from pcl that the given model is invalid can be ignored!");
    if(!sac.computeModel(10)) {
      ROS_ERROR("computeModel failed");
      break;
    }
    ROS_INFO("computeModel finished, now refining model");
    /*if(!sac.refineModel(3.0, 1000)) {
      ROS_ERROR("refineModel failed");
      break;
    }*/
    Eigen::VectorXf coeffvector(4);
    sac.getModelCoefficients(coeffvector);
    sac.getInliers(inliers->indices);
    
    // this instead of refineModel because that uses a strange threshold
    ROS_INFO("before refining: %lu inliers, model=[%g,%g,%g,%g]", inliers->indices.size(), coeffvector(0), coeffvector(1), coeffvector(2), coeffvector(3));
    Eigen::VectorXf coeff_refined(4);
    sac_model->optimizeModelCoefficients (inliers->indices, coeffvector, coeff_refined);
    coeffvector=coeff_refined;
    sac_model->selectWithinDistance (coeff_refined, threshold_, inliers->indices); // Refine inliers
    
    coefficients->values.resize(4);
    coefficients->values[0]=coeffvector(0);
    coefficients->values[1]=coeffvector(1);
    coefficients->values[2]=coeffvector(2);
    coefficients->values[3]=coeffvector(3);
    
//    seg.segment(*inliers, *coefficients); // TODO idea: pre-set coefficients if shelf is known? // TODO This function probably takes some time, maybe make it faster somehow? Parallelize?
    ROS_INFO("segment() finished, number of inliers: %lu", inliers->indices.size());

    /*if (true) {
      ROS_INFO("Doing SeededHueSegmentation ...");
      pcl::PointIndices inliers_correct_color;
      pcl::SeededHueSegmentation shs;
      shs.setInputCloud(cloud); // TODO SeedHueSegmentation is not templated (WTF?) and only works with PointXYZRGB, but we have PointXYZRGBA
      shs.setClusterTolerance(0.5); // in metres
      shs.setDeltaHue(120.0); // hues are probably in degrees (0 to 360)
      shs.segment(*inliers, inliers_correct_color);
      inliers->indices.swap(inliers_correct_color.indices);
      ROS_INFO("SeededHueSegmentation done, number of inliers is now %lu", inliers->indices.size());
    }*/

    if (inliers->indices.size() < min_num_points_seg /*|| inliers->indices.size() < 0.1*cloud.size()*/) {
      // PCL_ERROR ("Could not estimate a planar model for the given dataset.");
      ROS_WARN("Not enough inliers (should be more than %u), counting this as unsuccessful", min_num_points_seg);
      break;
    }

    ROS_INFO("Found plane with coefficients=[%g, %g, %g, %g]",
             coefficients->values[0], coefficients->values[1],
             coefficients->values[2], coefficients->values[3]);
    for(size_t i=0; i<4; i++) {
      plane.plane.coef[i] = coefficients->values[i];
    }

    ROS_INFO("Saving cloud under /tmp/plane.pcd");
    try {
      pcl::io::savePCDFile ("/tmp/plane.pcd", *cloud, inliers->indices, true);
    } catch (pcl::IOException &e) {
      ROS_WARN("%s", e.what());
    }
    ROS_INFO("Saving done");

    // Project the model inliers TODO maybe don't do this?
    pcl::ProjectInliers<point_t> proj;
    proj.setModelType(pcl::SACMODEL_PLANE);
    proj.setIndices(inliers);
    proj.setInputCloud(cloud);
    proj.setModelCoefficients(coefficients);
    ROS_INFO("Projecting inliers");
    proj.filter(*cloud_projected);
    ROS_INFO("Projected point cloud has %lu data points.", cloud_projected->points.size());

    // Create a Convex Hull representation of the projected inliers
    pcl::PointCloud<point_t>::Ptr cloud_hull(
        new pcl::PointCloud<point_t>);
    pcl::ConvexHull<point_t> chull;
    std::vector<pcl::Vertices> polygons;
    chull.setInputCloud(cloud_projected);
    // chull.setAlpha (0.1);
    chull.setDimension(2); // Will be determined automatically if not set
    ROS_INFO("Calling ConvexHull.reconstruct");
    chull.reconstruct(*cloud_hull, polygons);
    ROS_INFO("Cloud hull has size %lu", cloud_hull->size());
    for (size_t j = 0; j < cloud_hull->points.size(); j++) {
      //ROS_INFO("Hull point: (%g,%g,%g)", cloud_hull->points[j].x, cloud_hull->points[j].y, cloud_hull->points[j].z);
      geometry_msgs::Point point;
      point.x = cloud_hull->points[j].x;
      point.y = cloud_hull->points[j].y;
      point.z = cloud_hull->points[j].z;
      plane.hull.push_back(point);
    }

    // get bbox
    //plane.bbox = OOBB_Calculate::chooseBest(cloud_hull);
    plane.bbox = minimum_bbox_of_plane(*cloud_hull, polygons[0], plane.plane);
    planes.push_back(plane);

    // Remove points from cloud
    pcl::ExtractIndices<point_t> eifilter /*(true)*/;
    eifilter.setInputCloud(cloud);
    eifilter.setIndices(inliers);
    eifilter.setNegative(true);
    eifilter.filter(*cloud_f);
    /*for (size_t j = (inliers->indices.size() - 1); j > 0; j--) {//This directly extracts the points, but is pretty slow
        cloud->points.erase( cloud->points.begin() + (inliers->indices)[j] );
    }*/
#if SAC_WITH_NORMALS
    extract_normals.setNegative (true);
    extract_normals.setInputCloud (cloud_normals);
    extract_normals.setIndices (inliers);
    extract_normals.filter (*cloud_normals2);
    cloud_normals->swap(*cloud_normals2);
#endif
    ROS_INFO("findPlanes(): cloud_f size: %lu, cloud size: %lu", cloud_f->size(), cloud->size());
    cloud->swap(*cloud_f);
    ROS_INFO("cloud->width=%u, cloud->points.size()=%lu", cloud->width, cloud->points.size());
    cloud->width = cloud->points.size(); // TODO is this correct?
    ROS_INFO("findPlanes(): cloud size after extracting: %lu, width: %u, height: %u, points: %lu", cloud->size(), cloud->width, cloud->height, cloud->points.size());

    if(planeviz_pub != nullptr) {
      // Source: https://wiki.ros.org/rviz/Tutorials/Markers%3A%20Basic%20Shapes
      visualization_msgs::Marker marker;
      marker.header.frame_id = BASE_FRAME;
      marker.header.stamp = ros::Time::now();
      marker.ns = "planes";
      marker.id = 50 * mode + num;
      marker.type = visualization_msgs::Marker::CUBE;
      marker.action = visualization_msgs::Marker::ADD;
      marker.pose = plane.bbox.center;
      marker.scale.x = std::max(0.01, plane.bbox.size.x);
      marker.scale.y = std::max(0.01, plane.bbox.size.y);
      marker.scale.z = std::max(0.01, plane.bbox.size.z);
      marker.color.r = 1.0f;
      marker.color.g = 0.0f;
      marker.color.b = 0.0f;
      marker.color.a = 1.0f;
      marker.lifetime = ros::Duration();
      planeviz_pub->publish(marker);
      ROS_INFO("Published plane as marker");
    }
  }
  return planes;
}

// TODO fit_sphere
double fit_sphere(pcl::PointCloud<point_t>::ConstPtr cloud, moveit_msgs::CollisionObject& collision_object) {
  //pcl::console::setVerbosityLevel(pcl::console::L_VERBOSE);
  ROS_INFO("fit_sphere: cloud has %u points", cloud->height*cloud->width);
  // All the objects needed
  pcl::NormalEstimationOMP<point_t, pcl::Normal> ne;
  pcl::SACSegmentationFromNormals<point_t, pcl::Normal> seg;
  pcl::ExtractIndices<point_t> extract;
  pcl::ExtractIndices<pcl::Normal> extract_normals;

  // Datasets
  //pcl::PointCloud<point_t>::Ptr cloud(new pcl::PointCloud<point_t>);
  pcl::PointCloud<pcl::Normal>::Ptr cloud_normals (new pcl::PointCloud<pcl::Normal>);
  pcl::ModelCoefficients::Ptr coefficients_sphere (new pcl::ModelCoefficients);
  pcl::PointIndices::Ptr inliers_sphere (new pcl::PointIndices);

  // Convert parameter to point cloud
  //pcl::fromROSMsg(msg_in, *cloud);
  //try {
  //  pcl::io::savePCDFile ("test_cloud_sphere.pcd", *cloud, true);
  //} catch (pcl::IOException &e) {
  //  ROS_WARN("%s", e.what());
  //}

  //std::cerr << "PointCloud has: " << cloud->points.size () << " data points." << std::endl;

  // Estimate point normals
  //pcl::search::KdTree<point_t>::Ptr tree (new pcl::search::KdTree<point_t> ());
  //ne.setSearchMethod (tree); // Chosen automatically in pcl::Feature::initCompute()
  ne.setInputCloud (cloud);
  ne.setKSearch (50);
  ROS_INFO("Computing point cloud normals ...");
  ne.compute (*cloud_normals);
  ROS_INFO("done");
  //std::cerr << "Normals cloud has: " << cloud_normals->points.size () << " data points." << std::endl;

  // Create the segmentation object for sphere segmentation and set all the parameters
  seg.setOptimizeCoefficients (true);
  seg.setModelType (pcl::SACMODEL_NORMAL_SPHERE);
  seg.setMethodType (pcl::SAC_RANSAC);
  seg.setNormalDistanceWeight (0.1); // TODO smaller?
  seg.setMaxIterations (200); // TODO fewer?
  seg.setDistanceThreshold (0.05); // TODO smaller?
  seg.setRadiusLimits (0.0, 0.2);
  seg.setInputCloud (cloud);
  seg.setInputNormals (cloud_normals);

  // Obtain the sphere inliers and coefficients
  ROS_INFO("calling segment() for sphere");
  seg.segment (*inliers_sphere, *coefficients_sphere);
  ROS_INFO("done");
  if (inliers_sphere->indices.empty ()) {
      ROS_WARN("no inliers, returning");
      return 1000.0;
  }
  ROS_INFO_STREAM("sphere coefficients: " << *coefficients_sphere);

  extract.setInputCloud (cloud);
  extract.setIndices (inliers_sphere);
  extract.setNegative (false);
  pcl::PointCloud<point_t>::Ptr cloud_sphere (new pcl::PointCloud<point_t> ());
  extract.filter (*cloud_sphere);
  //cloud->swap(*cloud_sphere);
  if (cloud_sphere->points.empty ())
    ROS_WARN_STREAM("Can't find the spherical component.");
  else {
    ROS_INFO_STREAM("PointCloud representing the spherical component: " << cloud_sphere->points.size () << " data points.");
    try {
      pcl::io::savePCDFile ("/tmp/sphere_output.pcd", *cloud_sphere, true);
    } catch (pcl::IOException &e) {
      ROS_WARN("%s", e.what());
    }
  }

  ROS_INFO("Putting data into collision object");
  collision_object.header = pcl_conversions::fromPCL(cloud->header); //msg_in.header;
  collision_object.primitives.resize(1);
  collision_object.primitives[0].type = collision_object.primitives[0].SPHERE;
  collision_object.primitives[0].dimensions.resize(1);
  collision_object.primitives[0].dimensions[0] = coefficients_sphere->values[3]; // radius
  collision_object.primitive_poses.resize(1);
  collision_object.primitive_poses[0].position = tf2::toMsg(Eigen::Vector3d(coefficients_sphere->values[0], coefficients_sphere->values[1], coefficients_sphere->values[2])); // TODO
  collision_object.primitive_poses[0].orientation.w = 1.0;

  ROS_INFO("Computing average distance ...");
  double avgDist=0.0;
  for(const auto& point : *cloud) {
    avgDist+=fabs(sqrt((point.x-coefficients_sphere->values[0])*(point.x-coefficients_sphere->values[0])+(point.y-coefficients_sphere->values[1])*(point.y-coefficients_sphere->values[1])+(point.z-coefficients_sphere->values[2])*(point.z-coefficients_sphere->values[2]))-coefficients_sphere->values[3]);
  }
  avgDist/=cloud->size();
  ROS_INFO("fit_sphere is done, average distance of the %lu points to computed model is %g.", cloud->size(), avgDist);
  return avgDist;
}

// TODO fit_box
double fit_box(pcl::PointCloud<point_t>::ConstPtr cloud, moveit_msgs::CollisionObject& collision_object) {
  // Idea: find two planes perpendicular to each other, the rotate the given points so that the planes are parallel to two of the coordinate planes
  //pcl::console::setVerbosityLevel(pcl::console::L_VERBOSE);
  ROS_INFO("fit_box: cloud has %u points", cloud->height*cloud->width);
  // Convert parameter to point cloud
  //pcl::PointCloud<point_t>::Ptr cloud(new pcl::PointCloud<point_t>);
  //pcl::fromROSMsg(msg_in, *cloud);

  //std::vector<plane_t> planes=findPlanes(cloud, 2, 3, 10, 0.015);

  // All the objects needed
  pcl::NormalEstimationOMP<point_t, pcl::Normal> ne;
  pcl::SACSegmentationFromNormals<point_t, pcl::Normal> seg_plane1;
  pcl::SACSegmentation<point_t> seg_plane2;
  pcl::ExtractIndices<point_t> extract;
  pcl::ExtractIndices<pcl::Normal> extract_normals;

  // Datasets
  pcl::PointCloud<pcl::Normal>::Ptr cloud_normals (new pcl::PointCloud<pcl::Normal>);
  pcl::PointCloud<point_t>::Ptr aligned_cloud(new pcl::PointCloud<point_t>);
  pcl::ModelCoefficients::Ptr coeff_plane1 (new pcl::ModelCoefficients);
  pcl::ModelCoefficients::Ptr coeff_plane2 (new pcl::ModelCoefficients);
  pcl::PointIndices::Ptr inliers_plane1 (new pcl::PointIndices);
  pcl::PointIndices::Ptr inliers_plane2 (new pcl::PointIndices);
  pcl::PointCloud<point_t>::Ptr cloud_wo_plane1 (new pcl::PointCloud<point_t> ()); // The cloud without plane1

  try {
    pcl::io::savePCDFile ("/tmp/fit_box_input_cloud.pcd", *cloud, true);
  } catch (pcl::IOException &e) {
    ROS_WARN("%s", e.what());
  }

  //std::cerr << "PointCloud has: " << cloud->points.size () << " data points." << std::endl;

  // Estimate point normals
  //pcl::search::KdTree<point_t>::Ptr tree (new pcl::search::KdTree<point_t> ()); // For the normal estimation
  //ne.setSearchMethod (tree); // Chosen automatically in pcl::Feature::initCompute()
  ne.setInputCloud (cloud);
  ne.setKSearch (50);
  ROS_INFO("Computing point cloud normals ...");
  ne.compute (*cloud_normals);
  ROS_INFO("done");
  //std::cerr << "Normals cloud has: " << cloud_normals->points.size () << " data points." << std::endl;

  // Create the segmentation object for plane 1 segmentation and set all the parameters
  seg_plane1.setOptimizeCoefficients (true);
  seg_plane1.setModelType (pcl::SACMODEL_NORMAL_PLANE);
  seg_plane1.setMethodType (pcl::SAC_RANSAC);
  seg_plane1.setNormalDistanceWeight (0.05);
  seg_plane1.setMaxIterations (1000); // TODO fewer?
  seg_plane1.setDistanceThreshold (0.03); // TODO smaller?
  seg_plane1.setInputCloud (cloud);
  seg_plane1.setInputNormals (cloud_normals);

  // Obtain the box inliers and coefficients
  seg_plane1.segment (*inliers_plane1, *coeff_plane1); // TODO check if plane was found
  ROS_INFO_STREAM("Coefficients of first plane: " << *coeff_plane1 << ", number of inliers: " << inliers_plane1->indices.size());

  extract.setInputCloud (cloud);
  extract.setIndices (inliers_plane1);
  extract.setNegative (true);
  extract.filter (*cloud_wo_plane1);
  //cloud->swap(*cloud_box);
  if (cloud_wo_plane1->points.empty ()) {
    ROS_WARN("cloud_wo_plane1 is empty, meaning only one side of the box is visible");
    // Create a Convex Hull representation of the projected inliers
    /*pcl::PointCloud<point_t>::Ptr cloud_hull(new pcl::PointCloud<point_t>); TODO
    pcl::ConvexHull<point_t> chull;
    std::vector<pcl::Vertices> polygons;
    chull.setInputCloud(cloud);
    chull.setIndices(inliers_plane1);
    chull.setDimension(2); // Will be determined automatically if not set
    chull.reconstruct(*cloud_hull, polygons);
    shape_msgs::Plane plane;
    for(size_t i=0; i<4; i++) {
      plane.coef[i] = coeff_plane1->values[i];
    }
    auto bbox = minimum_bbox_of_plane(cloud_hull, polygons[0], plane);
    ROS_INFO("Putting data into collision object");
    collision_object.header = msg_in.header;
    collision_object.primitives.resize(1);
    collision_object.primitives[0].type = collision_object.primitives[0].BOX;
    collision_object.primitives[0].dimensions.resize(3);
    collision_object.primitives[0].dimensions[0] = bbox.size.x;
    collision_object.primitives[0].dimensions[1] = bbox.size.y;
    collision_object.primitives[0].dimensions[2] = bbox.size.z;
    collision_object.primitive_poses.resize(1);
    collision_object.primitive_poses[0] = bbox.center;*/
    return 1000.0; // TODO maybe try to fit the box even with only the one plane
  } else {
    try {
      pcl::io::savePCDFile ("/tmp/cloud_wo_plane1.pcd", *cloud_wo_plane1, true);
    } catch (pcl::IOException &e) {
      ROS_WARN("%s", e.what());
    }

    seg_plane2.setOptimizeCoefficients(true);
    seg_plane2.setModelType(pcl::SACMODEL_PARALLEL_PLANE);
    seg_plane2.setAxis(Eigen::Vector3f(coeff_plane1->values[0], coeff_plane1->values[1], coeff_plane1->values[2]));
    seg_plane2.setEpsAngle(0.07); // 0.07 rad is about 4 deg
    seg_plane2.setMethodType(pcl::SAC_RANSAC);
    seg_plane2.setMaxIterations(1000); // Default is 50
    seg_plane2.setDistanceThreshold(0.008); // TODO smaller?
    seg_plane2.setInputCloud(cloud_wo_plane1);
    seg_plane2.segment(*inliers_plane2, *coeff_plane2); // TODO check if plane was found
    ROS_INFO_STREAM("Coefficients of second plane: " << *coeff_plane2 << ", number of inliers: " << inliers_plane2->indices.size());
    if(inliers_plane2->indices.empty() || coeff_plane2->values.empty()) {
      ROS_WARN("Could not find second plane");
      return 1000.0;
    }
    ROS_INFO("Angle between planes: %g (should be close to 1.570796)", angleBetweenVectors(coeff_plane1->values[0], coeff_plane1->values[1], coeff_plane1->values[2], coeff_plane2->values[0], coeff_plane2->values[1], coeff_plane2->values[2]));

    const Eigen::Vector3f x_axis(coeff_plane1->values[0], coeff_plane1->values[1], coeff_plane1->values[2]);
    const Eigen::Vector3f y_axis(coeff_plane2->values[0], coeff_plane2->values[1], coeff_plane2->values[2]);
    const Eigen::Vector3f z_axis=(x_axis.cross(y_axis)).normalized();
    double borders2[6]={INFINITY, -INFINITY, INFINITY, -INFINITY, INFINITY, -INFINITY};
    for(const auto& point : *cloud) {
      double proj_x=point.x*x_axis.x()+point.y*x_axis.y()+point.z*x_axis.z();
      double proj_y=point.x*y_axis.x()+point.y*y_axis.y()+point.z*y_axis.z();
      double proj_z=point.x*z_axis.x()+point.y*z_axis.y()+point.z*z_axis.z();
      if(proj_x<borders2[0]) {
        borders2[0]=proj_x;
      }
      if(proj_x>borders2[1]) {
        borders2[1]=proj_x;
      }
      if(proj_y<borders2[2]) {
        borders2[2]=proj_y;
      }
      if(proj_y>borders2[3]) {
        borders2[3]=proj_y;
      }
      if(proj_z<borders2[4]) {
        borders2[4]=proj_z;
      }
      if(proj_z>borders2[5]) {
        borders2[5]=proj_z;
      }
    }
    ROS_INFO("borders2={%g, %g, %g, %g, %g, %g}", borders2[0], borders2[1], borders2[2], borders2[3], borders2[4], borders2[5]);
    if(fabs(borders2[0]-(-coeff_plane1->values[3]))<fabs(borders2[1]-(-coeff_plane1->values[3]))) {
      borders2[0]=-coeff_plane1->values[3];
    } else {
      borders2[1]=-coeff_plane1->values[3];
    }
    if(fabs(borders2[2]-(-coeff_plane2->values[3]))<fabs(borders2[3]-(-coeff_plane2->values[3]))) {
      borders2[2]=-coeff_plane2->values[3];
    } else {
      borders2[3]=-coeff_plane2->values[3];
    }
    ROS_INFO("borders2 after correcting={%g, %g, %g, %g, %g, %g}", borders2[0], borders2[1], borders2[2], borders2[3], borders2[4], borders2[5]);
    ROS_INFO("size2=(%g, %g, %g)", borders2[1]-borders2[0], borders2[3]-borders2[2], borders2[5]-borders2[4]);

    // idea from https://stackoverflow.com/a/20038551
    Eigen::Vector3d u0(coeff_plane1->values[0], coeff_plane1->values[1], coeff_plane1->values[2]),
                    v0(coeff_plane2->values[0], coeff_plane2->values[1], coeff_plane2->values[2]),
                    u2(1.0, 0.0, 0.0), v2(0.0, 1.0, 0.0);
    Eigen::Quaterniond q2 = Eigen::Quaterniond::FromTwoVectors(u0, u2);
    Eigen::Vector3d v1 = (q2.conjugate()) * v2;
    Eigen::Vector3d v0_proj = v0 - v0.dot(u0)*u0;//v0.projectPlane(u0);
    Eigen::Vector3d v1_proj = v1 - v1.dot(u0)*u0;//v1.projectPlane(u0);
    Eigen::Quaterniond q1 = Eigen::Quaterniond::FromTwoVectors(v0_proj, v1_proj);
    Eigen::Quaterniond result = (q2 * q1).normalized();
    Eigen::Affine3d axis_transform(result);
    pcl::transformPointCloud(*cloud, *aligned_cloud, axis_transform);
    
    shape_msgs::Plane plane1_tf=transformPlane(coeff_plane1->values[0], coeff_plane1->values[1], coeff_plane1->values[2], coeff_plane1->values[3], axis_transform);
    ROS_INFO("transformed plane 1=(%g,%g,%g,%g)", plane1_tf.coef[0], plane1_tf.coef[1], plane1_tf.coef[2], plane1_tf.coef[3]);
    
    shape_msgs::Plane plane2_tf=transformPlane(coeff_plane2->values[0], coeff_plane2->values[1], coeff_plane2->values[2], coeff_plane2->values[3], axis_transform);
    ROS_INFO("transformed plane 2=(%g,%g,%g,%g)", plane2_tf.coef[0], plane2_tf.coef[1], plane2_tf.coef[2], plane2_tf.coef[3]);

    //for (size_t j = (cloud->points.size() - 1); j > 0; j--) {//This directly extracts the points, but is pretty slow
    //    if((pow(cloud->points[j].x, 2.0) + pow(cloud->points[j].y, 2.0) > pow(coefficients_box->values[6], 2.0)))
    //        cloud->points.erase(cloud->points.begin() + j);
    //}
    //cloud->width = cloud->points.size();
    //cloud->height = 1;
    //try {
    //  pcl::io::savePCDFile ("remaining_output.pcd", *cloud, false);
    //} catch (pcl::IOException &e) {
    //  ROS_WARN("%s", e.what());
    //}

    point_t min_point, max_point;
    pcl::getMinMax3D(*aligned_cloud, min_point, max_point);
    ROS_INFO_STREAM("min point: " << min_point << " max_point: " << max_point);
    double borders[6]={min_point.x, max_point.x, min_point.y, max_point.y, min_point.z, max_point.z};
    if(fabs(borders[0]-(-coeff_plane1->values[3]))<fabs(borders[1]-(-coeff_plane1->values[3]))) {
      borders[0]=-coeff_plane1->values[3];
    } else {
      borders[1]=-coeff_plane1->values[3];
    }
    /*TODO this does not seem to work so well, but the other one does. why?
    if(fabs(borders[2]-(-coeff_plane2->values[3]))<fabs(borders[3]-(-coeff_plane2->values[3]))) {
      borders[2]=-coeff_plane2->values[3];
    } else {
      borders[3]=-coeff_plane2->values[3];
    }*/
    ROS_INFO("box size=(%g,%g,%g)", borders[1]-borders[0], borders[3]-borders[2], borders[5]-borders[4]);

    ROS_INFO("Putting data into collision object");
    collision_object.header = pcl_conversions::fromPCL(cloud->header); //msg_in.header;
    collision_object.primitives.resize(1);
    collision_object.primitives[0].type = collision_object.primitives[0].BOX;
    collision_object.primitives[0].dimensions.resize(3);
    collision_object.primitives[0].dimensions[0] = borders[1]-borders[0];
    collision_object.primitives[0].dimensions[1] = borders[3]-borders[2];
    collision_object.primitives[0].dimensions[2] = borders[5]-borders[4];
    collision_object.primitive_poses.resize(1);
    collision_object.primitive_poses[0].position = tf2::toMsg(axis_transform.inverse() * Eigen::Vector3d((borders[0]+borders[1])/2.0, (borders[2]+borders[3])/2.0, (borders[4]+borders[5])/2.0));
    //collision_object.primitive_poses[0].position = tf2::toMsg(Eigen::Vector3f((borders2[0]+borders2[1])/2.0*x_axis+(borders2[2]+borders2[3])/2.0*y_axis+(borders2[4]+borders2[5])/2.0*z_axis));
    Eigen::Quaterniond final_orientation(axis_transform.inverse().rotation());
    collision_object.primitive_poses[0].orientation = tf2::toMsg(final_orientation);
  }

  // Last step: compute the distance of all points to the fitted box model
  ROS_INFO("Computing average distance ...");
  Eigen::Quaternionf quat;
  tf2::fromMsg(collision_object.primitive_poses[0].orientation, quat);
  Eigen::Isometry3f transform = Eigen::Isometry3f(quat.inverse() * Eigen::Translation3f(Eigen::Vector3f(-collision_object.primitive_poses[0].position.x, -collision_object.primitive_poses[0].position.y, -collision_object.primitive_poses[0].position.z)));
  double avgDist=0.0;
  //size_t count=0;
  for(const auto& point : *cloud) {
    Eigen::Vector3f pt_tf = transform * Eigen::Vector3f(point.x, point.y, point.z);
    //if((count++)%50==0)
    //  ROS_INFO_STREAM("pt=(" << point.x << "," << point.y << "," << point.z << "), pt_tf=(" << pt_tf.x() << "," << pt_tf.y() << "," << pt_tf.z() << "), (" << fabsf (pt_tf.x())-collision_object.primitives[0].dimensions[0]/2.0 << "," << fabsf (pt_tf.y())-collision_object.primitives[0].dimensions[1]/2.0 << "," << fabsf (pt_tf.z())-collision_object.primitives[0].dimensions[2]/2.0 << ")");
    pt_tf.x() = fabsf (pt_tf.x())-collision_object.primitives[0].dimensions[0]/2.0;
    pt_tf.y() = fabsf (pt_tf.y())-collision_object.primitives[0].dimensions[1]/2.0;
    pt_tf.z() = fabsf (pt_tf.z())-collision_object.primitives[0].dimensions[2]/2.0;
    avgDist += fabs (pt_tf.maxCoeff());
  }
  avgDist/=cloud->size();
  //dist=0.0;//For testing
  ROS_INFO("fit_box is done, average distance of the %lu points to computed model is %g.", cloud->size(), avgDist);
  return avgDist;
}

double fit_cylinder(pcl::PointCloud<point_t>::ConstPtr cloud, moveit_msgs::CollisionObject& collision_object) { // TODO option to pre-set coefficients TODO report number of inliers/other quality measure?
  //pcl::console::setVerbosityLevel(pcl::console::L_VERBOSE);
  ROS_INFO("fit_cylinder: cloud has %u points", cloud->height*cloud->width);
  // All the objects needed
  pcl::NormalEstimationOMP<point_t, pcl::Normal> ne;
  pcl::SACSegmentationFromNormals<point_t, pcl::Normal> seg;
  pcl::ExtractIndices<point_t> extract;
  pcl::ExtractIndices<pcl::Normal> extract_normals;

  // Datasets
  //pcl::PointCloud<point_t>::Ptr cloud(new pcl::PointCloud<point_t>);
  pcl::PointCloud<point_t>::Ptr aligned_cloud(new pcl::PointCloud<point_t>);
  pcl::PointCloud<pcl::Normal>::Ptr cloud_normals (new pcl::PointCloud<pcl::Normal>);
  pcl::ModelCoefficients::Ptr coefficients_cylinder (new pcl::ModelCoefficients);
  pcl::PointIndices::Ptr inliers_cylinder (new pcl::PointIndices);

  // Convert parameter to point cloud
  //pcl::fromROSMsg(msg_in, *cloud);
  //try {
  //  pcl::io::savePCDFile ("test_cloud_cylinder.pcd", *cloud, true);
  //} catch (pcl::IOException &e) {
  //  ROS_WARN("%s", e.what());
  //}

  //std::cerr << "PointCloud has: " << cloud->points.size () << " data points." << std::endl;

  // Estimate point normals
  //pcl::search::KdTree<point_t>::Ptr tree (new pcl::search::KdTree<point_t> ());
  //ne.setSearchMethod (tree); // Chosen automatically in pcl::Feature::initCompute()
  ne.setInputCloud (cloud);
  ne.setKSearch (50);
  ROS_INFO("Computing point cloud normals ...");
  ne.compute (*cloud_normals);
  ROS_INFO("done");
  //std::cerr << "Normals cloud has: " << cloud_normals->points.size () << " data points." << std::endl;

  // Create the segmentation object for cylinder segmentation and set all the parameters
  seg.setOptimizeCoefficients (true);
  seg.setModelType (pcl::SACMODEL_CYLINDER);
  seg.setMethodType (pcl::SAC_RANSAC);
  seg.setNormalDistanceWeight (0.1); // TODO good value?
  seg.setMaxIterations (100); // TODO fewer?
  seg.setDistanceThreshold (0.05); // TODO smaller?
  seg.setRadiusLimits (0.0, 0.2);
  seg.setInputCloud (cloud);
  seg.setInputNormals (cloud_normals);

  // Obtain the cylinder inliers and coefficients
  ROS_INFO("calling segment() for cylinder");
  seg.segment (*inliers_cylinder, *coefficients_cylinder);
  ROS_INFO("done");
  if (inliers_cylinder->indices.empty ()) {
      ROS_WARN("no inliers");
      return 1000.0;
  }
  Eigen::Affine3d axis_transform(Eigen::Quaterniond::FromTwoVectors(Eigen::Vector3d(coefficients_cylinder->values[3], coefficients_cylinder->values[4], coefficients_cylinder->values[5]), Eigen::Vector3d(0.0, 0.0, 1.0))*Eigen::Translation3d(-coefficients_cylinder->values[0], -coefficients_cylinder->values[1], -coefficients_cylinder->values[2]));
  pcl::transformPointCloud(*cloud, *aligned_cloud, axis_transform);
  ROS_INFO_STREAM("Cylinder coefficients: " << *coefficients_cylinder);

  extract.setInputCloud (aligned_cloud);
  extract.setIndices (inliers_cylinder);
  extract.setNegative (false);
  pcl::PointCloud<point_t>::Ptr cloud_cylinder (new pcl::PointCloud<point_t> ());
  extract.filter (*cloud_cylinder);
  //cloud->swap(*cloud_cylinder);
  if (cloud_cylinder->points.empty ())
    ROS_INFO_STREAM("Can't find the cylindrical component.");
  else {
    ROS_INFO_STREAM("PointCloud representing the cylindrical component: " << cloud_cylinder->points.size () << " data points.");
    try {
      pcl::io::savePCDFile ("/tmp/cylinder_output.pcd", *cloud_cylinder, true);
    } catch (pcl::IOException &e) {
      ROS_WARN("%s", e.what());
    }
  }
  point_t min_point, max_point;
  pcl::getMinMax3D(*cloud_cylinder, min_point, max_point);
  ROS_INFO_STREAM("min point: " << min_point << " max_point: " << max_point);
  ROS_INFO_STREAM("cylinder height: " << max_point.z - min_point.z);

  /*for (size_t j = (cloud->points.size() - 1); j > 0; j--) {//This directly extracts the points, but is pretty slow
      if((pow(cloud->points[j].x, 2.0) + pow(cloud->points[j].y, 2.0) > pow(coefficients_cylinder->values[6], 2.0)))
          cloud->points.erase(cloud->points.begin() + j);
  }
  cloud->width = cloud->points.size();
  cloud->height = 1;
  try {
    pcl::io::savePCDFile ("remaining_output.pcd", *cloud, false);
  } catch (pcl::IOException &e) {
    ROS_WARN("%s", e.what());
  }
  pcl::SACSegmentation<point_t> seg_plane;
  pcl::ModelCoefficients::Ptr coefficients(new pcl::ModelCoefficients);
  pcl::PointIndices::Ptr inliers(new pcl::PointIndices);
  seg_plane.setOptimizeCoefficients(true);
  seg_plane.setModelType(pcl::SACMODEL_PERPENDICULAR_PLANE);
  seg_plane.setAxis(Eigen::Vector3f(0.0, 0.0, 1.0));
  seg_plane.setEpsAngle(0.1);
  seg_plane.setMethodType(pcl::SAC_RANSAC);
  seg_plane.setMaxIterations(100); // Default is 50?
  seg_plane.setDistanceThreshold(0.01);
  seg_plane.setInputCloud(cloud);
  seg_plane.segment(*inliers, *coefficients);
  double a=coefficients->values[3];*/

  ROS_INFO("Putting data into collision object");
  collision_object.header = pcl_conversions::fromPCL(cloud->header); //msg_in.header;
  collision_object.primitives.resize(1);
  collision_object.primitives[0].type = collision_object.primitives[0].CYLINDER;
  collision_object.primitives[0].dimensions.resize(2);
  collision_object.primitives[0].dimensions[0] = max_point.z - min_point.z; // height TODO
  collision_object.primitives[0].dimensions[1] = coefficients_cylinder->values[6]; // radius
  collision_object.primitive_poses.resize(1);
  collision_object.primitive_poses[0].position = tf2::toMsg(axis_transform.inverse() * Eigen::Vector3d(0.0, 0.0, (min_point.z + max_point.z)/2.0)); // TODO
  collision_object.primitive_poses[0].orientation = tf2::toMsg(Eigen::Quaternionf::FromTwoVectors(Eigen::Vector3f(0.0, 0.0, 1.0), Eigen::Vector3f(coefficients_cylinder->values[3], coefficients_cylinder->values[4], coefficients_cylinder->values[5])));

  ROS_INFO("Computing average distance ...");
  Eigen::Isometry3f transform = Eigen::Isometry3f(Eigen::Quaternionf::FromTwoVectors(Eigen::Vector3f(coefficients_cylinder->values[3], coefficients_cylinder->values[4], coefficients_cylinder->values[5]), Eigen::Vector3f(0.0, 0.0, 1.0)) * Eigen::Translation3f(Eigen::Vector3f(-collision_object.primitive_poses[0].position.x, -collision_object.primitive_poses[0].position.y, -collision_object.primitive_poses[0].position.z)));
  double avgDist=0.0;
  //size_t count=0;
  for(const auto& point : *cloud) {
    // Transform the point so that the cylinder axis lies on the z-axis and the center is (0,0,0). Then align the cylinder so that the rim is at (0,0,0). The max function determines which distance is dominant (to base or to the side)
    Eigen::Vector3f pt_tf = transform * Eigen::Vector3f(point.x, point.y, point.z);
    const double a=sqrt(pt_tf.x()*pt_tf.x()+pt_tf.y()*pt_tf.y())-collision_object.primitives[0].dimensions[1];
    const double b=fabsf (pt_tf.z())-collision_object.primitives[0].dimensions[0]/2.0;
    avgDist += fabs (std::max(a, b));
    //if((count++)%100==0)
    //  ROS_INFO_STREAM("pt=(" << point.x << "," << point.y << "," << point.z << "), pt_tf=(" << pt_tf.x() << "," << pt_tf.y() << "," << pt_tf.z() << "), a=" << a << ", b=" << b << ", dist=" << fabs (std::max(a, b)));
  }
  avgDist/=cloud->size();
  ROS_INFO("fit_cylinder is done, average distance of the %lu points to computed model is %g.", cloud->size(), avgDist);
  return avgDist;
}

void fitDish(pcl::PointCloud<pcl::PointXYZRGBA>::ConstPtr cloud_in, double& height, double& base_r, double& top_r, geometry_msgs::Pose& pose) {
    if(cloud_in==nullptr || cloud_in->empty()) {
        ROS_ERROR("fitDish: cloud_in is NULL or has zero size");
        return;
    }
    //TODO with SAC?
    pcl::console::setVerbosityLevel(pcl::console::L_VERBOSE);
    // All the objects needed
    pcl::NormalEstimationOMP<pcl::PointXYZRGBA, pcl::Normal> ne;
    pcl::SACSegmentationFromNormals<pcl::PointXYZRGBA, pcl::Normal> seg;
    pcl::ExtractIndices<pcl::PointXYZRGBA> extract(false); // extract_removed_indices = false

    // Datasets
    pcl::PointCloud<pcl::PointXYZRGBA>::Ptr cloud(new pcl::PointCloud<pcl::PointXYZRGBA>(*cloud_in));
    pcl::PointCloud<pcl::Normal>::Ptr cloud_normals (new pcl::PointCloud<pcl::Normal>);
    pcl::ModelCoefficients::Ptr coefficients_cone (new pcl::ModelCoefficients);
    pcl::PointIndices::Ptr inliers_cone (new pcl::PointIndices);

    pcl::PCDWriter writer;
    writer.write ("scene.pcd", *cloud, true);

    std::cerr << "PointCloud has: " << cloud->points.size () << " data points." << std::endl;

    // Estimate point normals
    //pcl::search::KdTree<pcl::PointXYZRGBA>::Ptr tree (new pcl::search::KdTree<pcl::PointXYZRGBA> ());
    //ne.setSearchMethod (tree); // Chosen automatically in pcl::Feature::initCompute()
    ne.setInputCloud (cloud);
    ne.setKSearch (50);
    ROS_INFO("Computing point cloud normals ...");
    ne.compute (*cloud_normals);
    ROS_INFO("done");
    //std::cerr << "Normals cloud has: " << cloud_normals->points.size () << " data points." << std::endl;

    // Create the segmentation object for cone segmentation and set all the parameters
    seg.setOptimizeCoefficients (true);
    seg.setModelType (pcl::SACMODEL_CONE);
    seg.setMethodType (pcl::SAC_RANSAC);
    seg.setNormalDistanceWeight (0.05);
    seg.setMaxIterations (200);
    seg.setDistanceThreshold (0.02); // TODO smaller?
    //seg.setRadiusLimits (0.0, 0.2);
    seg.setMinMaxOpeningAngle(0.0, M_PI_2);
    seg.setInputCloud (cloud);
    seg.setInputNormals (cloud_normals);

    // Obtain the cone inliers and coefficients
    ROS_INFO("calling segment() for cone");
    seg.segment (*inliers_cone, *coefficients_cone);
    ROS_INFO("done");
    ROS_INFO_STREAM("cone coefficients: " << *coefficients_cone);

    Eigen::Affine3d axis_transform(Eigen::Quaterniond::FromTwoVectors(Eigen::Vector3d(coefficients_cone->values[3], coefficients_cone->values[4], coefficients_cone->values[5]), Eigen::Vector3d(0.0, 0.0, 1.0))*Eigen::Translation3d(-coefficients_cone->values[0], -coefficients_cone->values[1], -coefficients_cone->values[2]));
    pcl::transformPointCloud(*cloud, *cloud, axis_transform);

    pcl::PointCloud<pcl::PointXYZRGBA>::Ptr cloud_cone (new pcl::PointCloud<pcl::PointXYZRGBA> ());
    pcl::PointCloud<pcl::PointXYZRGBA>::Ptr cloud_rest (new pcl::PointCloud<pcl::PointXYZRGBA> ());
    extract.setInputCloud (cloud);
    extract.setIndices (inliers_cone);
    extract.setNegative (false);
    extract.filter (*cloud_cone);
    extract.setNegative (true);
    extract.filter (*cloud_rest);
    if (cloud_cone->points.empty ()) {
        ROS_INFO_STREAM("Can't find the cone component.");
    } else {
	    ROS_INFO_STREAM("PointCloud representing the cone component: " << cloud_cone->points.size () << " data points. Rest: " << cloud_rest->points.size() << " data points.");
	    writer.write ("cone.pcd", *cloud_cone, true);
	    writer.write ("rest.pcd", *cloud_rest, true);
    }
    // TODO maybe segment plane for bottom?
    pcl::PointXYZRGBA min_point, max_point;
    pcl::getMinMax3D(*cloud_cone, min_point, max_point);
    ROS_INFO_STREAM("min point: " << min_point << " max_point: " << max_point);
    ROS_INFO_STREAM("cone height: " << max_point.z - min_point.z);
    ROS_INFO("height=%f, base_radius=%f, top_radius=%f", max_point.z - min_point.z, tan(coefficients_cone->values[6])*min_point.z, tan(coefficients_cone->values[6])*max_point.z);
    height=max_point.z - min_point.z;
    base_r=tan(coefficients_cone->values[6])*min_point.z;
    top_r=tan(coefficients_cone->values[6])*max_point.z;
    pose.position = tf2::toMsg(axis_transform.inverse() * Eigen::Vector3d(0.0, 0.0, min_point.z)); // TODO
    pose.orientation = tf2::toMsg(Eigen::Quaterniond::FromTwoVectors(Eigen::Vector3d(0.0, 0.0, 1.0), Eigen::Vector3d(coefficients_cone->values[3], coefficients_cone->values[4], coefficients_cone->values[5])));
}

std::vector<vision_msgs::Detection3D>
segmentation(pcl::PointCloud<point_t>::ConstPtr cloud,
             const std::string& src_frame,
             const float zScaling) {
  std::vector<vision_msgs::Detection3D> result;
  ROS_INFO_STREAM("segmentation(): point cloud has " << cloud->points.size()
                                                     << " data points.");

  // Creating the KdTree object for the search method of the extraction - should be chosen automatically
  //pcl::search::KdTree<point_t>::Ptr tree(new pcl::search::KdTree<point_t>);
  //tree->setInputCloud(cloud);

  // Idea: transform with affine (scale z values), then cluster, then transform back
  if (zScaling != 1.0f) {
    ROS_WARN("The zScaling parameter currently has no effect.");
    /*pcl::transformPointCloud(
        *cloud, *cloud, TODO would have to be a new point cloud
        Eigen::Scaling(Eigen::Vector4f(1.0, 1.0, 1.0 / zScaling, 1.0)));*/
  }

  std::vector<pcl::PointIndices> cluster_indices;
  pcl::EuclideanClusterExtraction<point_t> ec;
  ec.setClusterTolerance(0.01); // 1cm
  ec.setMinClusterSize(500);
  ec.setMaxClusterSize(50000);
  //ec.setSearchMethod(tree);
  ec.setInputCloud(cloud);
  ROS_INFO("Calling EuclideanClusterExtraction.extract() ...");
  ec.extract(cluster_indices); // TODO This probably takes some time, make faster somehow?
  ROS_INFO("segmentation(): cluster extraction done, got %lu clusters", cluster_indices.size());

  /*if (zScaling != 1.0f) {
    pcl::transformPointCloud(
        *cloud, *cloud,
        Eigen::Scaling(Eigen::Vector4f(1.0, 1.0, zScaling, 1.0)));
  }*/

  unsigned int j = 0;
  for (const auto& it : cluster_indices) {
    pcl::PointCloud<point_t>::Ptr cloud_cluster(
        new pcl::PointCloud<point_t>);
    for (const auto& index : it.indices)
      cloud_cluster->points.push_back((*cloud)[index]);
    cloud_cluster->width = cloud_cluster->points.size();
    cloud_cluster->height = 1;
    cloud_cluster->is_dense = true;
    // save cloud_cluster as pcd?
    std::stringstream ss;
    ss << "/tmp/cloud_cluster_" << j++ << ".pcd";
    try {
      pcl::io::savePCDFile (ss.str(), *cloud_cluster, true);
    } catch (pcl::IOException &e) {
      ROS_WARN("%s", e.what());
    }

    ROS_INFO_STREAM("PointCloud representing the Cluster: " << cloud_cluster->points.size() << " data points.");
    vision_msgs::Detection3D cluster;
    cluster.header.frame_id = src_frame; // TODO set stamp?
    cluster.bbox = OOBB_Calculate::chooseBest(cloud_cluster);
    //vision_msgs::BoundingBox3D tmp = OOBB_Calculate::chooseBest2(cloud_cluster);//just for tests
    pcl::toROSMsg(*cloud_cluster, cluster.source_cloud);
    cluster.source_cloud.header.frame_id = src_frame; // TODO set stamp? // TODO
    result.push_back(cluster);
  }
  return result;
}

pcl::PointCloud<point_t>::Ptr
cluster(pcl::PointCloud<point_t>::ConstPtr cloud, bool do_voxelgrid, bool do_passthrough) {
  if (cloud->empty()) {
    return nullptr;
  }
  ROS_INFO_STREAM("[cluster] PointCloud before filtering has: " << cloud->points.size()
            << " data points.");
  pcl::PointCloud<point_t>::ConstPtr cloud_filtered = cloud;
  if(do_voxelgrid) {
    // Downsample the dataset using a leaf size of 1cm
    pcl::VoxelGrid<point_t> vg;
    pcl::PointCloud<point_t>::Ptr cloud_voxel(new pcl::PointCloud<point_t>);
    vg.setInputCloud(cloud_filtered);
    vg.setLeafSize(0.01f, 0.01f, 0.01f);
    vg.filter(*cloud_voxel);
    cloud_filtered = cloud_voxel;
  }

  if(do_passthrough) {
   // Remove ground
    pcl::PointCloud<point_t>::Ptr cloud_pass(new pcl::PointCloud<point_t>);
    pcl::PassThrough<point_t> pass;
    pass.setInputCloud(cloud_filtered);
    pass.setFilterFieldName("z");
    pass.setFilterLimits(0.1, 4.0);
    // pass.setFilterLimitsNegative (true);
    pass.filter(*cloud_pass);
    cloud_filtered = cloud_pass;
  }
  
  ROS_INFO_STREAM("[cluster] PointCloud after filtering has: "
            << cloud_filtered->points.size() << " data points.");

  // Creating the KdTree object for the search method of the extraction
  pcl::search::KdTree<point_t>::Ptr tree(new pcl::search::KdTree<point_t>);
  tree->setInputCloud(cloud_filtered);

  std::vector<pcl::PointIndices> cluster_indices;
  pcl::EuclideanClusterExtraction<point_t> ec;
  ec.setClusterTolerance(0.02); // 2cm
  ec.setMinClusterSize(100);
  ec.setMaxClusterSize(25000);
  ec.setSearchMethod(tree);
  ec.setInputCloud(cloud_filtered);
  ROS_INFO("[cluster] calling EuclideanClusterExtraction.extract() ...");
  ec.extract(cluster_indices);
  ROS_INFO("[cluster] got %lu clusters", cluster_indices.size());
  for(size_t i=0; i<cluster_indices.size() && i<4; i++) {
    ROS_INFO("[cluster] cluster %lu has %lu points", i, cluster_indices[i].indices.size());
  }

  int j = 0;
  for (std::vector<pcl::PointIndices>::const_iterator it =
           cluster_indices.begin();
       it != cluster_indices.end(); ++it) { // TODO only first cluster is returned, so for loop is unnecessary
    pcl::PointCloud<point_t>::Ptr cloud_cluster(
        new pcl::PointCloud<point_t>);
    for (std::vector<int>::const_iterator pit = it->indices.begin();
         pit != it->indices.end(); ++pit)
      cloud_cluster->points.push_back(cloud_filtered->points[*pit]);
    cloud_cluster->width = cloud_cluster->points.size();
    cloud_cluster->height = 1;
    cloud_cluster->is_dense = true;

    ROS_INFO_STREAM("[cluster] PointCloud representing the Cluster: "
              << cloud_cluster->points.size() << " data points.");
    std::stringstream ss;
    ss << "/tmp/cloud_cluster_" << j << ".pcd";
    try {
      pcl::io::savePCDFile (ss.str(), *cloud_cluster, true);
    } catch (pcl::IOException &e) {
      ROS_WARN("%s", e.what());
    }
    return cloud_cluster;
    // j++;
  }
  return nullptr;
}

pcl::PointCloud<point_t>::Ptr
getCloudROI(pcl::PointCloud<point_t>::ConstPtr cloud,
            const float xmin, const float xmax,
            const float ymin, const float ymax,
            const float zmin, const float zmax) {
  ROS_INFO("getCloudROI called, input cloud has size %lu (%u by %u)", cloud->size(), cloud->height, cloud->width);
  pcl::PointCloud<point_t>::Ptr x_filtered(new pcl::PointCloud<point_t>),
      y_filtered(new pcl::PointCloud<point_t>),
      z_filtered(new pcl::PointCloud<point_t>);
  pcl::PassThrough<point_t> pass;
  if(cloud->isOrganized()) {
    pass.setKeepOrganized(true);
  }
  pass.setInputCloud(cloud);
  pass.setFilterFieldName("x");
  pass.setFilterLimits(xmin, xmax);
  pass.filter(*x_filtered);

  pass.setInputCloud(x_filtered);
  pass.setFilterFieldName("y");
  pass.setFilterLimits(ymin, ymax);
  pass.filter(*y_filtered);

  pass.setInputCloud(y_filtered);
  pass.setFilterFieldName("z");
  pass.setFilterLimits(zmin, zmax);
  pass.filter(*z_filtered);

  ROS_INFO("getCloudROI() done, output cloud has size %lu (%u by %u)", z_filtered->size(), z_filtered->height, z_filtered->width);
  return z_filtered;
}

pcl::PointCloud<point_t>::Ptr getCloudROI(pcl::PointCloud<point_t>::ConstPtr cloud, const vision_msgs::BoundingBox3D& bbox) { // TODO test if this works. the axes might be switched?
  //pcl::console::setVerbosityLevel(pcl::console::L_VERBOSE);
  ROS_INFO("getCloudROI called, input cloud has size %lu, bbox has position (%g,%g,%g) and orientation (%g,%g,%g,%g) and size (%g,%g,%g)", cloud->size(), bbox.center.position.x, bbox.center.position.y, bbox.center.position.z, bbox.center.orientation.x, bbox.center.orientation.y, bbox.center.orientation.z, bbox.center.orientation.w, bbox.size.x, bbox.size.y, bbox.size.z);
  point_t minPoint, maxPoint;
  pcl::getMinMax3D(*cloud, minPoint, maxPoint);
  ROS_INFO_STREAM("minPoint: " << minPoint << " maxPoint: " << maxPoint);
  pcl::CropBox<point_t> filter;
  filter.setInputCloud(cloud);
  filter.setMin(Eigen::Vector4f(-bbox.size.x / 2.0, -bbox.size.y / 2.0,
                                -bbox.size.z / 2.0, 1.0));
  filter.setMax(Eigen::Vector4f(bbox.size.x / 2.0, bbox.size.y / 2.0,
                                bbox.size.z / 2.0, 1.0));
  ROS_INFO_STREAM("filter.getMin()= x:" << filter.getMin()[0] << " y:" << filter.getMin()[1]
            << " z:" << filter.getMin()[2] << " w:" << filter.getMin()[3]);
  ROS_INFO_STREAM("filter.getMax()= x:" << filter.getMax()[0] << " y:" << filter.getMax()[1]
            << " z:" << filter.getMax()[2] << " w:" << filter.getMax()[3]);
  Eigen::Affine3f t =
      Eigen::Translation3f(bbox.center.position.x, bbox.center.position.y,
                           bbox.center.position.z) *
      Eigen::Quaternionf(bbox.center.orientation.w, bbox.center.orientation.x,
                         bbox.center.orientation.y, bbox.center.orientation.z);
  filter.setTransform(t.inverse());
  ROS_INFO_STREAM("t.matrix()=" << std::endl << t.matrix());
  ROS_INFO_STREAM("t.inverse().matrix()=" << std::endl << t.inverse().matrix());
  pcl::PointCloud<point_t>::Ptr filtered(
      new pcl::PointCloud<point_t>);
  filter.filter(*filtered);
  ROS_INFO("getCloudROI() done, output cloud has size %lu, input cloud has size %lu",
           filtered->size(), cloud->size());
  pcl::getMinMax3D(*filtered, minPoint, maxPoint);
  ROS_INFO_STREAM("minPoint: " << minPoint << " maxPoint: " << maxPoint);
  return filtered;
}
