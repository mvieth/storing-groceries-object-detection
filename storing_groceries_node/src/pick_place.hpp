/**
 * @file
 * @author  Markus Vieth
 * @section DESCRIPTION
 *
 * Pick and place functionalities
 */
#pragma once

#include <moveit/move_group_interface/move_group_interface.h>
#include <moveit_msgs/CollisionObject.h>
#include <moveit_msgs/Grasp.h>
#include <moveit_msgs/PlaceLocation.h>

// TODO detect automatically? Gazebo topics?
#define SIMULATION 1

#define BASE_FRAME "base_footprint"
#ifdef SIMULATION
#define MAX_GRIPPER_OPEN 0.09
#else
#define MAX_GRIPPER_OPEN 0.12
#endif

/**
 * Init the graspviz publisher
 */
void init_grasp_publisher(ros::NodeHandle &nh);

/**
 * Add a random collision object to the planning scene.
 * The size of the object is random (within bounds, it is graspable), but the position is fixed.
 * @type 1 for box, 3 for cylinder
 */
moveit_msgs::CollisionObject addRandomCollisionObject(uint8_t type = 1);

/**
  * Generate a generic dish mesh. The top is open. Can e.g. represent cups, bowls, plates.
  * @param base_radius[in] the radius of the base (what the dish stands on)
  * @param upper_radius[in] the radius of the upper rim
  * @param height[in] the height of the dish
  * @param points[in] how many points should be used to approximate the circles? Default: 32
  */
shape_msgs::Mesh generateDish(const double base_radius, const double upper_radius, const double height, const size_t points=32);

void openGripper(trajectory_msgs::JointTrajectory& posture, bool simulation);

void closedGripper(trajectory_msgs::JointTrajectory& posture, double dist, bool simulation);

/**
 * Generates possible Grasps for the first primitive of the CollisionObject.
 * Works with cylinders, boxes, and maybe spheres (but further testing needed)
 */
std::vector<moveit_msgs::Grasp> generateGrasps(const moveit_msgs::CollisionObject& object);

// generates possible PlaceLocations on a horizontal rectangle (sizex by sizey,
// yaw angle is angle) which has its center at (x,y,z)
std::vector<moveit_msgs::PlaceLocation>
generatePlaces(const moveit_msgs::CollisionObject& object, float x, float y, float z,
               float sizex, float sizey, float angle, bool simulation=false);

// void pick(const std::string name, const float x, const float y, const float
// z);

// void place(std::string name);

// The following three functions are mostly copied from the
// moveit-implementation, but some aspects are changed

void initActionClients(ros::NodeHandle &nh, const ros::WallDuration &wait_for_servers = ros::WallDuration());

template <typename T>
void waitForAction(ros::NodeHandle &nh, const T &action,
                   const std::string &name, const ros::WallTime &timeout,
                   double allotted_time);

/**
 * Here the bool allow_gripper_support_collision is set to false, whereas in the standard moveit it its set to true
 */
moveit::planning_interface::MoveItErrorCode
my_pick(moveit::planning_interface::MoveGroupInterface *group,
        const std::string &object,
        const std::vector<moveit_msgs::Grasp> &grasps,
        const std::string& support_surface_, bool plan_only);

/**
 * Here the bool place_eef is set to true, whereas in the standard moveit it is set to false
 */
moveit::planning_interface::MoveItErrorCode
my_place(moveit::planning_interface::MoveGroupInterface *group,
         const std::string &object,
         const std::vector<moveit_msgs::PlaceLocation> &locations,
         const std::string& support_surface_, const bool allow_gripper_support_collision = false, bool place_eef = true, bool plan_only = false);
