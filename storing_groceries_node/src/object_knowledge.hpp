/**
 * @file
 * @author  Markus Vieth
 * @section DESCRIPTION
 *
 * Knowledge about objects
 */
#pragma once

#include <map>       // for multimap
#include <string>    // for string, basic_string
#include <vector>    // for vector
namespace ros { class NodeHandle; }

typedef enum { OTHER, BOX, SPHERE, CYLINDER, CONE } primitive_t;

class Object {
public:
  std::string name;
  uint8_t category; // index in "categories"-vector
  primitive_t primitive;
  float dim0;
  float dim1;
  float dim2;
};

/**
 * Needs the library yaml-cpp
 */
bool parseConfigYAML(const std::string& yaml_filename);

std::vector<Object>& getPossibleObjects();

/** Get all possible categories
 */
std::vector<std::string>& getCategories();

/**
 * "classify" bounding box into object ("0") or surface ("1")
 */
int classifyObject(float x, float y, float z);

/**
 * Compare the given bounding box (x, y, z) with the bounding boxes of the
 * possible Objects, return the scores of each object (in a multimap), smaller
 * scores are better (TODO actually, are now greater scores better?)
 * TODO: maybe switch to vision_msgs/ObjectHypothesisWithPose Message?
 */
std::multimap<float, std::string> classifyBySize(float x, float y, float z);

/**
 * Look up the object label corresponding to id in the map objectLabels, if it
 * is not there, get it from the param server and memorize it for later
 */
std::string getObjectLabelById(const ros::NodeHandle &nh, const int id);
std::string getObjectLabelById(const ros::NodeHandle &nh, const std::string& id);

/**
 * Look up the id corresponding to object label in the map objectLabels, if it
 * is not there, get it from the param server and memorize it for later.
 * If not found, return -1.
 */
int getIdByObjectLabel(const ros::NodeHandle &nh, const std::string& label);

/**
 * @param name something like "apple", "soda", "pringles", ...
 */
uint8_t getCategoryOfObject(const std::string& name);

primitive_t getPrimitiveOfObject(const std::string& name);

/**
 * @return can be NULL if object not found
 */
Object *getObject(const std::string& name);

/**
 * Removes all characters that are not alphabetic characters or underscores.
 * E.g. make "pringles10" to "pringles" or "red_bowl5" to "red_bowl"
 */
std::string getNameFromId(const std::string& in);
