// Adapted from pcl's sac_model_normal_parallel_plane
#pragma once

#include <pcl/sample_consensus/sac_model_normal_plane.h>
#include <pcl/sample_consensus/model_types.h>
#include <pcl/memory.h>
#include <pcl/pcl_macros.h>

namespace pcl
{
  template <typename PointT, typename PointNT>
  class SampleConsensusModelNormalPerpendicularPlane : public SampleConsensusModelNormalPlane<PointT, PointNT>
  {
    public:
      using SampleConsensusModel<PointT>::model_name_;
      using SampleConsensusModel<PointT>::input_;
      using SampleConsensusModel<PointT>::indices_;
      using SampleConsensusModelFromNormals<PointT, PointNT>::normals_;
      using SampleConsensusModelFromNormals<PointT, PointNT>::normal_distance_weight_;
      using SampleConsensusModel<PointT>::error_sqr_dists_;

      using PointCloud = typename SampleConsensusModel<PointT>::PointCloud;
      using PointCloudPtr = typename SampleConsensusModel<PointT>::PointCloudPtr;
      using PointCloudConstPtr = typename SampleConsensusModel<PointT>::PointCloudConstPtr;

      using PointCloudNPtr = typename SampleConsensusModelFromNormals<PointT, PointNT>::PointCloudNPtr;
      using PointCloudNConstPtr = typename SampleConsensusModelFromNormals<PointT, PointNT>::PointCloudNConstPtr;

      using Ptr = shared_ptr<SampleConsensusModelNormalPerpendicularPlane<PointT, PointNT> >;
      using ConstPtr = shared_ptr<const SampleConsensusModelNormalPerpendicularPlane<PointT, PointNT>>;

      /** \brief Constructor for base SampleConsensusModelNormalPerpendicularPlane.
        * \param[in] cloud the input point cloud dataset
        * \param[in] random if true set the random seed to the current time, else set to 12345 (default: false)
        */
      SampleConsensusModelNormalPerpendicularPlane (const PointCloudConstPtr &cloud,
                                               bool random = false) 
        : SampleConsensusModelNormalPlane<PointT, PointNT> (cloud, random)
        , axis_ (Eigen::Vector4f::Zero ())
        , distance_from_origin_ (0)
        , eps_angle_ (-1.0)
        , sin_angle_ (-1.0)
        , eps_dist_ (0.0)
      {
        model_name_ = "SampleConsensusModelNormalPerpendicularPlane";
        sample_size_ = 3;
        model_size_ = 4;
      }

      /** \brief Constructor for base SampleConsensusModelNormalPerpendicularPlane.
        * \param[in] cloud the input point cloud dataset
        * \param[in] indices a vector of point indices to be used from \a cloud
        * \param[in] random if true set the random seed to the current time, else set to 12345 (default: false)
        */
      SampleConsensusModelNormalPerpendicularPlane (const PointCloudConstPtr &cloud, 
                                               const Indices &indices,
                                               bool random = false) 
        : SampleConsensusModelNormalPlane<PointT, PointNT> (cloud, indices, random)
        , axis_ (Eigen::Vector4f::Zero ())
        , distance_from_origin_ (0)
        , eps_angle_ (-1.0)
        , sin_angle_ (-1.0)
        , eps_dist_ (0.0)
      {
        model_name_ = "SampleConsensusModelNormalPerpendicularPlane";
        sample_size_ = 3;
        model_size_ = 4;
      }
      
      /** \brief Empty destructor */
      ~SampleConsensusModelNormalPerpendicularPlane () {}

      /** \brief Set the axis along which we need to search for a plane perpendicular to.
        * \param[in] ax the axis along which we need to search for a plane perpendicular to
        */
      inline void
      setAxis (const Eigen::Vector3f &ax) { axis_.head<3> () = ax; axis_.normalize ();}

      /** \brief Get the axis along which we need to search for a plane perpendicular to. */
      inline Eigen::Vector3f
      getAxis () const { return (axis_.head<3> ()); }

      /** \brief Set the angle epsilon (delta) threshold.
        * \param[in] ea the maximum allowed deviation from 90 degrees between the plane normal and the given axis.
        * \note You need to specify an angle > 0 in order to activate the axis-angle constraint!
        */
      inline void
      setEpsAngle (const double ea) { eps_angle_ = ea; sin_angle_ = std::abs (std::sin (ea));}

      /** \brief Get the angle epsilon (delta) threshold. */
      inline double
      getEpsAngle () const { return (eps_angle_); }

      /** \brief Set the distance we expect the plane to be from the origin
        * \param[in] d distance from the template plane to the origin
        */
      inline void
      setDistanceFromOrigin (const double d) { distance_from_origin_ = d; }

      /** \brief Get the distance of the plane from the origin. */
      inline double
      getDistanceFromOrigin () const { return (distance_from_origin_); }

      /** \brief Set the distance epsilon (delta) threshold.
        * \param[in] delta the maximum allowed deviation from the template distance from the origin
        */
      inline void
      setEpsDist (const double delta) { eps_dist_ = delta; }

      /** \brief Get the distance epsilon (delta) threshold. */
      inline double
      getEpsDist () const { return (eps_dist_); }

      /** \brief Return a unique id for this model (SACMODEL_NORMAL_PLANE). */
      inline pcl::SacModel
      getModelType () const override { return (SACMODEL_NORMAL_PLANE); }

    	PCL_MAKE_ALIGNED_OPERATOR_NEW

    protected:
      using SampleConsensusModel<PointT>::sample_size_;
      using SampleConsensusModel<PointT>::model_size_;

      /** \brief Check whether a model is valid given the user constraints.
        * \param[in] model_coefficients the set of model coefficients
        */
      bool
      isModelValid (const Eigen::VectorXf &model_coefficients) const override
      {
        if (!SampleConsensusModel<PointT>::isModelValid (model_coefficients))
          return (false);

        // Check against template, if given
        if (eps_angle_ > 0.0)
        {
          // Obtain the plane normal
          Eigen::Vector4f coeff = model_coefficients;
          coeff[3] = 0.0f;
          coeff.normalize ();

          Eigen::Vector4f axis (axis_[0], axis_[1], axis_[2], 0.0f);
          if (std::abs (axis.dot (coeff)) > sin_angle_)
          {
            return  (false);
          }
        }

        if (eps_dist_ > 0.0)
        {
          if (std::abs (-model_coefficients[3] - distance_from_origin_) > eps_dist_)
            return (false);
        }

        return (true);
      }

   private:
      /** \brief The axis along which we need to search for a plane perpendicular to. */
      Eigen::Vector4f axis_;

      /** \brief The distance from the template plane to the origin. */
      double distance_from_origin_;

      /** \brief The maximum allowed difference between the plane normal and the given axis.  */
      double eps_angle_;

      /** \brief The sine of the angle*/
      double sin_angle_;
      /** \brief The maximum allowed deviation from the template distance from the origin. */
      double eps_dist_;
  };
}
