# Storing Groceries Object Detection

This project was created for the RoboCup task "Storing Groceries". It contains functionalities for object detection/recognition, also in 2D (images) but more in 3D (point clouds), as well as functionalities for robotic manipulation of objects. It uses and extends the Point Cloud Library (PCL), MoveIt, and other libraries. The main component is intended to run as a ROS node in the Robot Operating System, but some parts of this project may also be useful in other contexts.
Some examples of functionalities are (mostly as ROS services or actions): detecting horizontal surfaces like tables, detecting a shelf, detecting objects, building a scene model for MoveIt, generating grasps and place locations for primitives (sphere, cylinder, box), placing an object on a specified surface.
Below is a more detailed description, but also check out the doxygen-documented header files.

## util
Contains utility functions, e.g. geometric functions to compute angles and distances.

## object_knowledge
Information about objects, e.g. what category do they belong to, what shape do they have.

## high_level_functions
### getObjectsAndAddToPlanningScene
Input: point cloud and rgb image. Segments the cloud and treats every cluster as a detection/object. Then tries to find out more about the objects (size, position, orientation, shape, category) using the known information about the possible objects as well as e.g. classification on rgb images and shape fitting (sq fitting or sample consensus based fitting). All the information is brought together inter alia by Kalman-like fusing.

## point_clouds
Contains everything related to point clouds, e.g. finding planes, cylinders, spheres, or boxes in clouds via sample consensus; box filters, or computing the minimum bounding box in 2 dimensions.

## bounding_boxes

## pick_place
Everything related to manipulation/pick/place, e.g. computing grasps and place locations, and adapted functions from moveit
